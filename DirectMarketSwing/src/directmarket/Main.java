/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket;

import directmarket.excepciones.DirectMarketException;
import directmarket.logica.manejadores.ManejadorUsuarios;
import directmarket.logica.utilidades.DatosPrueba;
import directmarket.presentacion.Menu;
import directmarket.servidor.PublAccesos;
import directmarket.servidor.PublOrden;
import directmarket.servidor.PublComent;
import directmarket.servidor.PublGeneral;
import directmarket.servidor.PublUsu;
import directmarket.servidor.PublProd;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JOptionPane;

/**
 *
 * @author guillermo
 */
public class Main {

    private static FileInputStream cargarArchivoConfig() {
        try {
            String dir = System.getProperty("user.home") + "/.Direct Market/" + "estacion.properties";
            File f = new File(dir);
            if (!f.exists()) {

                f.createNewFile(); // crea nuevo archivo
                FileOutputStream os = new FileOutputStream(f);
                Properties prop = new Properties();
                // opciones de web service
                prop.setProperty("ws_protocolo", "http");
                prop.setProperty("ws_host", "localhost");
                prop.setProperty("ws_port", "8080");
                // opciones por defecto de correo
                prop.setProperty("mail_activarEnvio", "true");
                prop.setProperty("mail_urlWeb", "localhost:10018");
                prop.setProperty("mail_puertoSMTP", "20018");
                prop.setProperty("mail_hostSMTP", "localhost");
                prop.setProperty("mail_fromEmail", "no-reply@directmarket.com");
                prop.store(os, "Opciones de la estacion de trabajo");

               
            }
             return new FileInputStream(dir);
        } catch (IOException ex) {
            ex.printStackTrace(); // no deberia ocurrir
            
        }
        return null;
    }

    public static void main(String args[]) {
        // creo los web services
  
        Properties defaultProps = new Properties();
        FileInputStream in = cargarArchivoConfig();
        try {
            defaultProps.load(in);
            in.close();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        String protocolo = defaultProps.getProperty("ws_protocolo");
        String host = defaultProps.getProperty("ws_host");
        String port = defaultProps.getProperty("ws_port");
        String base =  host + ":" + port + "/"; // cambiar protocolo no esta soportado
        try {     
             PublUsu p = new PublUsu();
            p.publicar(base);

            PublComent pc = new PublComent();
            pc.publicar(base);

            PublOrden po = new PublOrden();
            po.publicar(base);

            PublProd pp = new PublProd();
            pp.publicar(base);

            PublGeneral pg = new PublGeneral();
            pg.publicar(base);
            PublAccesos pa = new PublAccesos();
            pa.publicar(base);
            try {

                DatosPrueba.cargarDatosPrueba();
                ManejadorUsuarios.getInstance().cargarPreferenciasEnvio();
            } catch (DirectMarketException ex) {
                JOptionPane.showMessageDialog(null, ex.toString());
            }

            // corro la GUI
            Menu.main(args);
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause() instanceof java.net.BindException) {
                JOptionPane.showMessageDialog(null, "El servidor central ya está iniciado (o " + base + " no permite publicar), detalle:\n" + ex.toString(), "Puerto en Uso", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error al inicial el servidor central, detalle:\n" + ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
