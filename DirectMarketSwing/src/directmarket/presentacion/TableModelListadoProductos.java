/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion;

import directmarket.accesoDatos.dataTypes.DataProductoBas;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author tprog087
 */
public class TableModelListadoProductos extends AbstractTableModel {

    private List<DataProductoBas> listado = new ArrayList<>();

    @Override
    public int getRowCount() {
        return listado.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        DataProductoBas prod = listado.get(rowIndex);
        return columnIndex == 0 ? prod.getNroReferencia() : prod.getTitulo();
        //DataUsuarioBas d = usuarios.get(rowIndex);
        //return  columnIndex == 0 ? d.getNick() : d.getEmail();
    }

    @Override
    public String getColumnName(int column) {
        return new String[]{"Nro.", "Título"}[column]; //To change body of generated methods, choose Tools | Templates.
    }

    public List<DataProductoBas> getProductos() {
        return listado;
    }

    public void setProductos(List<DataProductoBas> produc) {
        this.listado = produc;
    }
}
