/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion.utilidades;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gonzalo Miraballes
 */
public class FormatoTabla extends DefaultTableCellRenderer {

    Color c1 = new Color(245, 245, 255);
    Color c2 = new Color(255, 255, 255);

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean selected, boolean focused, int row, int column) {
        if (row % 2 == 0) {
            setBackground(c1);
        } else {
            setBackground(c2);
        }
        super.getTableCellRendererComponent(table, value, selected, focused,
                row, column);
        return this;

    }
}
