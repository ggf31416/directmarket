/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion.utilidades;

import java.text.DecimalFormat;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Guillermo
 */
public class FltRenderer extends DefaultTableCellRenderer {

    DecimalFormat fmt = new DecimalFormat("#,##0.00");

    @Override
    protected void setValue(Object value) {
        setText((value == null) ? "" : fmt.format(value));
    }
}