/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion.utilidades;

import directmarket.excepciones.DirectMarketException;
import directmarket.excepciones.MultiExcepcion;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;

/**
 *
 * @author tprog088
 */
public class AyudanteException {

    private Map<String, JLabel> campos = new HashMap<>();

    public void agregarCampo(String campo, JLabel comp) {
        campos.put(campo.toLowerCase(), comp);
    }

    private void mostrar(String c, String msg) {
        if (campos.containsKey(c.toLowerCase())) {
            JLabel lbl = campos.get(c.toLowerCase());

            lbl.setText(msg);
            lbl.setForeground(Color.red);
        }
    }

    public void mostrarError(DirectMarketException ex) {
        if (ex instanceof MultiExcepcion) {
            MultiExcepcion mu = (MultiExcepcion) ex;
            for (String campo : campos.keySet()) {
                if (mu.getMensajes().containsKey(campo)) {
                    String msg = mu.getMensajes().get(campo);
                    mostrar(campo, msg);
                } else {
                    campos.get(campo).setText("");
                }
            }
        } else {
            mostrar(ex.getCampo(), ex.getMessage());
        }
    }

    public void limpiar() {
        for (JLabel lbl : campos.values()) {
            lbl.setText("");
        }
    }
}
