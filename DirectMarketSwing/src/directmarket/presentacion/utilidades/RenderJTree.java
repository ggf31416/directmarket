/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion.utilidades;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.interfaces.IProductos;
import java.util.Collections;
import java.util.List;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author Guillermo
 */
public class RenderJTree {

    public static void renderTreeCategoria(JTree arbolCategorias, boolean muestroTodo) {
        DefaultTreeModel arbol = (DefaultTreeModel) arbolCategorias.getModel();
        IProductos ctrlNegocios = FabricaControladores.getProductosInstance();
        List<DataCategoria> cats = ctrlNegocios.listarCategorias();
        Collections.reverse(cats);
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Categorías");
        arbol.setRoot(root);
        recursiveTree(root, ctrlNegocios.getCategoriasPrimerNivel(), muestroTodo);
        // Nuevo
        arbolCategorias.setRootVisible(false);
        DefaultTreeModel m = (DefaultTreeModel) arbolCategorias.getModel();
        m.reload();
        // Fin nuevo
        for (int i = 0; i < arbolCategorias.getRowCount(); i++) {
            arbolCategorias.expandRow(i);
        }
    }

    // Se llama con root
    private static void recursiveTree(DefaultMutableTreeNode parent, List<DataCategoria> hijos, boolean muestroTodo) {
        if (!hijos.isEmpty()) {
            for (DataCategoria dc : hijos) {
                if (!muestroTodo && !dc.getTieneProductos()) {
                    parent.insert(new DefaultMutableTreeNode(dc.getNombre()), parent.getChildCount());
                    List<DataCategoria> new_hijos = dc.getSubcategorias();
                    recursiveTree(parent.getLastLeaf(), new_hijos, muestroTodo);
                } else if (muestroTodo) {
                    parent.insert(new DefaultMutableTreeNode(dc.getNombre()), parent.getChildCount());
                    List<DataCategoria> new_hijos = dc.getSubcategorias();
                    recursiveTree(parent.getLastLeaf(), new_hijos, muestroTodo);
                }
            }
        }
    }
}
