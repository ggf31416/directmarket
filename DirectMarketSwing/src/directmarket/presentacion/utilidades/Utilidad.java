/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion.utilidades;

import com.toedter.calendar.JDateChooser;
import java.awt.*;

import java.awt.event.*;

import java.text.*;

import java.util.*;

import javax.swing.*;

import javax.swing.JSpinner.DefaultEditor;

import javax.swing.text.JTextComponent;

/**
 *
 * @author Guillermo
 */
public class Utilidad {

    /**
     * Recibe una varargs de componentes y valores y asigna los valores a los
     * componentes Componentes soportados: Jtextfield,
     * jtogglebutton,Jdatechooser,jlist,jcombobox (para estos 2 últimos un valor
     * de colección establece el modelo) se pueden realizar varias asignaciones
     * en la misma llamada pero debe haber el mismo número de componentes que de
     * valores. Al primer componente le corresponde el primer valor, al segundo
     * el segundo, etc.
     *
     * @param obj varargs de componentes y valores
     */
    public static void setValues(Object... obj) {

        ArrayList componentes = new ArrayList<Component>();
        ArrayList valores = new ArrayList<Object>();
        for (Object o : obj) {
            if (o instanceof Component) {
                componentes.add(o);
            } else {
                valores.add(o);
            }
        }
        DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
        for (int i = 0; i < componentes.size() && i < valores.size(); i++) {
            try {
                Object v = valores.get(i);
                Component c = (Component) componentes.get(i);
                if (c instanceof JTextComponent) {
                    if (v instanceof Calendar) {
                        ((JTextComponent) c).setText(df.format(((Calendar) v).getTime()));
                    } else {
                        ((JTextComponent) c).setText(v.toString());
                    }
                } else if (c instanceof JToggleButton) {
                    ((JToggleButton) c).setSelected((Boolean) v);
                } else if (c instanceof JDateChooser) {
                    setValor((JDateChooser) c, v);
                } else if (v instanceof Collection) {
                    setValor(c, (Collection) v);
                } else if (v != null && v.getClass().isArray()) {
                    setValor(c, Arrays.asList((Object[]) v));
                } else if (c instanceof JList) {
                    ((JList) c).setSelectedValue(v, true);
                } else if (c instanceof JComboBox) {
                    ((JComboBox) c).setSelectedItem(v);
                } else if (c instanceof JSpinner) {
                    ((JSpinner) c).setValue(v);
                }
            } catch (Exception ex) {
                //ex.printStackTrace();
            }
        }
    }

    private static void setValor(Component c, Collection v) {
        Vector col = new Vector((Collection) v);
        if (v != null) {
            if (c instanceof JList) {
                ((JList) c).setListData(col);
            } else if (c instanceof JComboBox) {
                JComboBox cb = (JComboBox) c;
                DefaultComboBoxModel dcm = new DefaultComboBoxModel(col);
                cb.setModel(dcm);
                cb.setSelectedIndex(-1);
            } /*else if (c instanceof JTable) {
             BeanTableModel modeloDef = new BeanTableModel(col);
             ((JTable) c).setModel(modeloDef);
             }*/
        }
    }

    private static void setValor(JDateChooser c, Object v) {
        if (v instanceof Calendar) {
            ((JDateChooser) c).setCalendar((Calendar) v);
        } else if (v instanceof Date) {
            ((JDateChooser) c).setDate((Date) v);
        }
    }

    public static void soloNumeros(JTextField txt, KeyEvent evt) {
        txt.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((Character.isDigit(c)))) {
                    e.consume();
                }
            }
        });
        int k = (int) (evt.getKeyChar());
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana de Error", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana de Error", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txt.transferFocus();
        }

    }

    public static int getFilaSeleccionada(JTable tabla) {
        if (tabla.getSelectedRow() < 0) {
            return tabla.getSelectedRow();
        }
        return tabla.convertRowIndexToModel(tabla.getSelectedRow());
    }

    public static boolean verificarVacio(JTextField txt, String queDebeIngresar) {
        if (txt.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar " + queDebeIngresar);
            return false;
        }
        return true;
    }

    public static boolean verificarVacio(JComboBox cb, String queDebeIngresar) {
        if (cb.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "Debe ingresar " + queDebeIngresar);
            return false;
        }
        return true;
    }

    public static boolean verificarNull(Object obj, String queDebeIngresar) {
        if (obj == null) {
            JOptionPane.showMessageDialog(null, "Debe ingresar " + queDebeIngresar);
            return false;
        }
        return true;
    }

    public static void activarCampos(boolean activar, Component... componentes) {
        for (Component com : componentes) {
            if (com != null) {
                habilitarComponente(com, activar, false);
            }
        }
    }

    public static void activarCamposEditar(boolean activar, Component... componentes) {
        for (Component com : componentes) {
            if (com != null) {
                habilitarComponente(com, activar, true);
            }
        }
    }

    private static void habilitarComponente(Component com, boolean habilitar, boolean usarEdicion) {
        if (com instanceof JTextComponent) {

            if (usarEdicion) {
                ((JTextComponent) com).setEditable(habilitar);
                //((JTextComponent) com).setBackground(com.getBackground());
            } else {
                ((JTextComponent) com).setEnabled(habilitar);
            }
        } else if (com instanceof JLabel) {
            ((JLabel) com).setEnabled(habilitar || usarEdicion);
        } else if (com instanceof JSpinner) {
            if (usarEdicion) {
                JSpinner spinner = (JSpinner) com;
                spinner.setEnabled(habilitar);
                ((DefaultEditor) spinner.getEditor()).getTextField().setBackground(Color.WHITE);//setEditable(habilitar);
                spinner.setBackground(Color.WHITE);
            } else {
                JSpinner spinner = (JSpinner) com;
                spinner.setEnabled(habilitar);
            }
        } else if (com instanceof JPanel || com instanceof JScrollPane || com instanceof JViewport) {
            Container panel = (Container) com;
//            panel.setEnabled(habilitar);
            for (Component comp : panel.getComponents()) {
                habilitarComponente(comp, habilitar, usarEdicion);
            }
        } else {
            com.setEnabled(habilitar);
        }
    }

    public static void borrarDatos(Component... componentes) {
        for (Component c : componentes) {
            if (c instanceof JTextComponent) {
                ((JTextComponent) c).setText("");
            } else if (c instanceof JComboBox) {
                ((JComboBox) c).setSelectedIndex(-1);
            } else if (c instanceof JDateChooser) {
                ((JDateChooser) c).setCalendar(null);
            } else if (c instanceof JToggleButton) {
                ((JToggleButton) c).setSelected(false);
            } else {
                if (c instanceof JPanel || c instanceof JScrollPane) {
                    Container j = (Container) c;
                    borrarDatos(j.getComponents());
                }
            }
        }
    }

    /**
     *
     * @param componentes
     */
    public static void recheckarToggle(Component... componentes) {
        for (Component c : componentes) {
            if (c instanceof JToggleButton) {
                JToggleButton b = (JToggleButton) c;
                b.setSelected(b.isSelected());
            } else {
                if (c instanceof JComponent) {
                    {
                        recheckarToggle(((JComponent) c).getComponents());
                    }
                }
            }
        }
    }
}