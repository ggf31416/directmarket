/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion.utilidades;

import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.logica.fabrica.FabricaDataImagen;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;


/**
 *
 * @author Guillermo
 */
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ImagenesUtilGUI {

    public static JFileChooser obtenerChooser() {
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagenes", "jpg","jpeg", "gif", "png","bmp");
        fc.setFileFilter(filter);
        fc.setAcceptAllFileFilterUsed(false);
        return fc;
    }

    /**
     * Obtiene una minuatura que mantiene el aspecto
     *
     * @param img
     * @param pixels
     * @return
     */
    public static Image obtenerMiniatura(BufferedImage img, int pixels) {
        if (pixels == 0) {
            pixels = 64;
        }
        int w = -1;
        int h = -1;
        if (img.getHeight() > img.getWidth()) {
            h = pixels;
        }
        if (img.getHeight() < img.getWidth()) {
            w = pixels;
        }
        return img.getScaledInstance(w, h, Image.SCALE_SMOOTH);
    }

    public static DataImagen cargar(File f) {
        try {
            DataImagen di = FabricaDataImagen.desdeFile(f);
            return di;
            //return directmarket.guardarImagen(di);

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    public static void mostrarImagen(File f, JLabel lbl) throws IOException {
        String soportados[] = ImageIO.getReaderFormatNames();
        BufferedImage myPicture = ImageIO.read(f);
        Image miniatura = ImagenesUtilGUI.obtenerMiniatura(myPicture, lbl.getHeight());
        lbl.setIcon(new ImageIcon(miniatura));
    }

    public static void mostrarImagen(DataImagen di, JLabel lbl) {
        int pix = lbl.getHeight();
        if (pix == 0) {
            pix = 64; // hack
        }
        mostrarImagen(di, lbl, pix, false);
    }

    public static void mostrarImagen(DataImagen di, JLabel lbl, int pixels, boolean aumentarSiMenor) {
        //String soportados[] = ImageIO.getReaderFormatNames();
        if (di != null) {
            BufferedImage myPicture = di.getImagen();
            // si el alto o ancho son mayores a pixeles o si se especifico que se aumente la imagen si es de menor tamano
            if ((myPicture.getHeight() > pixels && myPicture.getWidth() > pixels) || aumentarSiMenor) {
                Image miniatura = obtenerMiniatura(myPicture, pixels);
                lbl.setIcon(new ImageIcon(miniatura));
            } else {
                lbl.setIcon(new ImageIcon(myPicture));
            }
        } else {
            lbl.setIcon(null);
        }
    }
}
