/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion;

import directmarket.accesoDatos.dataTypes.DataOrden;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Gonzalo Miraballes
 */
public class OrdenesTableModel extends AbstractTableModel {

    private List<DataOrden> ordenes = new ArrayList<>();

    @Override
    public int getRowCount() {
        return ordenes.size();
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DataOrden d = ordenes.get(rowIndex);
        return d.getNroOrden();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false; //To change body of generated methods, choose Tools | Templates.
    }

    public List<DataOrden> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<DataOrden> ordenes) {
        this.ordenes = ordenes;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "Nro. Orden";
        }
        return "";
    }
}
