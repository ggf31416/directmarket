/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.interfaces.IProductos;
import java.awt.Dimension;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author tprog086
 */
public class IFListadoProductos extends javax.swing.JInternalFrame {

    private JDesktopPane pane;
    private IFModificarProducto IF;
    private IProductos ctrl;

    /**
     * Creates new form IFListadoProductos
     */
    public IFListadoProductos(JDesktopPane pane, IFModificarProducto MP) throws Exception {
        this.pane = pane;
        this.IF = MP;
        initComponents();
        ctrl = FabricaControladores.getProductosInstance();
        //ConfigTable();
        renderTreeCategoria();

        TCategorias.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                //resetMessage();
                DefaultMutableTreeNode nodo = (DefaultMutableTreeNode) (e.getPath().getLastPathComponent());
                Object nodoInfo = nodo.getUserObject();

                if (nodo.isLeaf()) {
                    String categ = (String) nodoInfo;
                    try {
                        ConfigTable(categ);
                    } catch (Exception ex) {
                        Logger.getLogger(IFListadoProductos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    //Pasarle la categoria seleccionada al modelo de la tabla.
    private void ConfigTable(String categoria) throws Exception {
        TableModelListadoProductos tmlp = new TableModelListadoProductos();
        tmlp.setProductos(ctrl.productosPorCategoria(categoria));
        TProductos.setModel(tmlp);
        TProductos.setAutoCreateRowSorter(true);
        TProductos.getColumnModel().getColumn(0).setMinWidth(15);
        // la unica manera que encontre que swing me haga caso
        TProductos.getColumnModel().getColumn(1).setPreferredWidth(1000);
        final IFListadoProductos IFLP = this;
        TProductos.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int v = TProductos.getSelectedRow();
                if (v > -1 && !e.getValueIsAdjusting()) {

                    int rowModel = TProductos.convertRowIndexToModel(v);
                    int NroRef = (int) TProductos.getModel().getValueAt(rowModel, 0);
                    try {
                        IFModificarProducto IF = new IFModificarProducto(pane, IFLP);
                        IF.CargarDatos(NroRef);
                        IF.setResizable(true);
                        IF.setMaximizable(true);
                        IF.setPreferredSize(new Dimension(700, 700));
                        Menu.getInstance().abrirInternal(IF);
                        TProductos.getSelectionModel().clearSelection();
                    } catch (Exception eX) {
                        lblMensaje.setText(eX.toString());
                    }
                }

            }
        });



    }

    void recursiveTree(DefaultMutableTreeNode parent, List<DataCategoria> hijos, boolean muestroTodo) {
        if (!hijos.isEmpty()) {
            for (DataCategoria dc : hijos) {
                if (!muestroTodo && !dc.getTieneProductos()) {
                    parent.insert(new DefaultMutableTreeNode(dc.getNombre()), parent.getChildCount());
                    List<DataCategoria> new_hijos = dc.getSubcategorias();
                    recursiveTree(parent.getLastLeaf(), new_hijos, muestroTodo);
                } else if (muestroTodo) {
                    parent.insert(new DefaultMutableTreeNode(dc.getNombre()), parent.getChildCount());
                    List<DataCategoria> new_hijos = dc.getSubcategorias();
                    recursiveTree(parent.getLastLeaf(), new_hijos, muestroTodo);
                }
            }
        }
    }

    void renderTreeCategoria() {
        DefaultTreeModel arbol = (DefaultTreeModel) TCategorias.getModel();
        IProductos ctrlNegocios = FabricaControladores.getProductosInstance();
        List<DataCategoria> cats = ctrlNegocios.listarCategorias();
        Collections.reverse(cats);
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Categorías");
        arbol.setRoot(root);
        recursiveTree(root, ctrlNegocios.getCategoriasPrimerNivel(), true);
        // Nuevo
        TCategorias.setRootVisible(false);
        DefaultTreeModel m = (DefaultTreeModel) TCategorias.getModel();
        m.reload();
        // Fin nuevo
        for (int i = 0; i < TCategorias.getRowCount(); i++) {
            TCategorias.expandRow(i);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblMensaje = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TProductos = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        TCategorias = new javax.swing.JTree();

        setClosable(true);
        setIconifiable(true);
        setMinimumSize(new java.awt.Dimension(200, 200));

        lblMensaje.setText("Seleccione el producto que desea modificar :");

        TProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro.", "Título"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TProductos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        TProductos.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TProductos);
        TProductos.getColumnModel().getColumn(0).setMinWidth(15);
        TProductos.getColumnModel().getColumn(1).setPreferredWidth(1000);

        jScrollPane2.setViewportView(TCategorias);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(lblMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(21, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1)
                        .addGap(10, 10, 10))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblMensaje)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree TCategorias;
    private javax.swing.JTable TProductos;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblMensaje;
    // End of variables declaration//GEN-END:variables
}
