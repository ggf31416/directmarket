/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion;

import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Guillermo
 */
public class UsuariosBTableModel extends AbstractTableModel {

    private List<DataUsuarioBas> usuarios = new ArrayList<>();

    @Override
    public int getRowCount() {
        return usuarios.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DataUsuarioBas d = usuarios.get(rowIndex);
        return columnIndex == 0 ? d.getNick() : d.getEmail();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false; //To change body of generated methods, choose Tools | Templates.
    }

    public List<DataUsuarioBas> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<DataUsuarioBas> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "Nick";
        }
        if (column == 1) {
            return "Correo";
        }
        return "";
    }
}
