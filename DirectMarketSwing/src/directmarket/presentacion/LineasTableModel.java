/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion;

import directmarket.accesoDatos.dataTypes.DataLinea;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Guillermo
 */
public class LineasTableModel extends AbstractTableModel {

    private List<DataLinea> lineas = new ArrayList<>();

    public List<DataLinea> getLineas() {
        return lineas;
    }

    public void setLineas(List<DataLinea> lineas) {
        this.lineas = lineas;
    }

    @Override
    public int getRowCount() {
        return lineas.size();
    }
    // ref,titulo,precio,cantidad, subtotal

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex > -1 && rowIndex < getRowCount()) {
            DataLinea dl = lineas.get(rowIndex);
            if (columnIndex == 0) {
                return dl.getRef();
            }
            if (columnIndex == 1) {
                return dl.getTitulo();
            }
            if (columnIndex == 2) {
                return dl.getPrecio();
            }
            if (columnIndex == 3) {
                return dl.getCantidad();
            }
            if (columnIndex == 4) {
                return dl.getSubtotal();
            }
        }
        return "";

    }

    @Override
    public String getColumnName(int column) {
        String titulos[] = new String[]{"Nro.", "Título", "Precio", "Cantidad", "Subtotal"};
        if (column < 4) {
            return titulos[column];
        }
        return super.getColumnName(column); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0 || columnIndex == 3) {
            return Integer.class;
        }
        if (columnIndex == 2 || columnIndex == 4) {
            return Double.class;
        }
        if (columnIndex == 1) {
            return String.class;
        }
        return super.getColumnClass(columnIndex);
    }
}
