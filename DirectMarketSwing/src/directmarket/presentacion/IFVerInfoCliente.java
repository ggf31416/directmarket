/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion;

import directmarket.accesoDatos.dataTypes.DataCliente;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.interfaces.IUsuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import directmarket.presentacion.utilidades.ImagenesUtilGUI;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 *
 * @author fabian
 */
public class IFVerInfoCliente extends javax.swing.JInternalFrame {

    private IUsuario ctrl;
    private JDesktopPane desk;

    /**
     * Creates new form IFVerInfoProveedor
     */
    public IFVerInfoCliente(JDesktopPane desk) {
        initComponents();
        this.desk = desk;
        ctrl = FabricaControladores.getUsuarioInstance();
        configTabla();
    }

    private void configTabla() {
        UsuariosBTableModel utm = new UsuariosBTableModel();
        utm.setUsuarios(ctrl.listarClientes());
        tbClientes.setModel(utm);
        tbClientes.setAutoCreateRowSorter(true);
        tbClientes.getSelectionModel().setSelectionInterval(-1, -1);
        tbClientes.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int v = tbClientes.getSelectedRow();
                if (v > -1) {
                    //int rowModelo = tbClientes.convertRowIndexToModel(v);
                    String nick = (String) tbClientes.getValueAt(v, 0);
                    try {
                        mostrar(nick);
                    } catch (IOException ex) {
                        Logger.getLogger(IFVerInfoCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

    }

    void mostrar(String nick) throws IOException {
        try {
            DataCliente dp = ctrl.VerInformacionCliente(nick);
            lblNickInfo.setText(dp.getNick());
            lblEmailInfo.setText(dp.getEmail());
            lblNombreInfo.setText(dp.getNombre());
            lblApellidoInfo.setText(dp.getApellido());
            lstOrdenes.setListData(dp.getnumOrdenes().toArray());
            lblFechaInfo.setText(new SimpleDateFormat("dd/MM/yyyy").format(dp.getFecha()));


            ImagenesUtilGUI.mostrarImagen(dp.getImagen(), lblAvatar);
        } catch (NoSeEncontroException ex) {
            Logger.getLogger(IFVerInfoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNick = new javax.swing.JLabel();
        lblNickInfo = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        lblEmailInfo = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblNombreInfo = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();
        lblApellidoInfo = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        lblFechaInfo = new javax.swing.JLabel();
        lblAvatar = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbClientes = new javax.swing.JTable();
        btnCerrar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstOrdenes = new javax.swing.JList();
        btDetalle = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Información del Cliente");

        lblNick.setText("Nick:");

        lblNickInfo.setPreferredSize(new java.awt.Dimension(80, 14));
        lblNickInfo.setVerifyInputWhenFocusTarget(false);

        lblEmail.setText("Email: ");

        lblEmailInfo.setPreferredSize(new java.awt.Dimension(80, 14));

        lblNombre.setText("Nombre:");

        lblNombreInfo.setPreferredSize(new java.awt.Dimension(80, 14));

        lblApellido.setText("Apellido:");

        lblApellidoInfo.setPreferredSize(new java.awt.Dimension(80, 14));

        lblFecha.setText("Fecha de Nacimiento: ");

        lblFechaInfo.setPreferredSize(new java.awt.Dimension(80, 14));

        lblAvatar.setBackground(new java.awt.Color(255, 255, 255));
        lblAvatar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAvatar.setAlignmentX(6.0F);
        lblAvatar.setAlignmentY(6.0F);
        lblAvatar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tbClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nick", "Email"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbClientes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbClientes.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tbClientes);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        jLabel1.setText("Ordenes de Compra:");

        jScrollPane2.setViewportView(lstOrdenes);

        btDetalle.setText("Detalle de orden");
        btDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDetalleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblFecha)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblFechaInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(12, 12, 12))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblNick)
                                            .addComponent(lblEmail)
                                            .addComponent(lblNombre)
                                            .addComponent(lblApellido))
                                        .addGap(10, 10, 10)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblApellidoInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblNombreInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblEmailInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblNickInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel1))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btDetalle)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 112, Short.MAX_VALUE)))
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCerrar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(btDetalle))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNick)
                            .addComponent(lblNickInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblEmailInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblEmail))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNombre)
                            .addComponent(lblNombreInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblApellidoInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblApellido))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblFechaInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblFecha))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCerrar)
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:

        this.setVisible(false);

    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDetalleActionPerformed
        // TODO add your handling code here:
        if (lstOrdenes.getSelectedIndex() > -1) {
            Integer nroOrden = (Integer) lstOrdenes.getSelectedValue();
            IFVerInfoOrden ifver = new IFVerInfoOrden(desk, nroOrden);
            Menu.getInstance().abrirInternal(ifver);


        }

    }//GEN-LAST:event_btDetalleActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btDetalle;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblApellidoInfo;
    private javax.swing.JLabel lblAvatar;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEmailInfo;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblFechaInfo;
    private javax.swing.JLabel lblNick;
    private javax.swing.JLabel lblNickInfo;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblNombreInfo;
    private javax.swing.JList lstOrdenes;
    private javax.swing.JTable tbClientes;
    // End of variables declaration//GEN-END:variables
}
