/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.presentacion;

import directmarket.accesoDatos.dataTypes.DataImagen;
import java.util.List;

/**
 *
 * @author guillermo
 */
public interface I_IFLI {

    List<DataImagen> getLImgs();

    void setLImgs(List<DataImagen> LImgs);

    void agregarImagen(DataImagen img);
}
