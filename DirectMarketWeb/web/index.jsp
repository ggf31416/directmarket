<%-- 
    Document   : index
    Created on : 10-sep-2013, 10:29:05
    Author     : guillermo
--%>

<%@page import="directmarket.servidor.DataUsuarioBas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!--<base href="${request.contextPath}" />-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DirectMarket</title>
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    </head>
    <body>
       <jsp:forward page="/listadoproductos"></jsp:forward>
        <% String error = "";
            if (request.getAttribute("error") != null){
                error = request.getAttribute("error").toString();
            }
            %>
        <%=  error %> <br>
        
        <% String mostrar = "Hola visitante!";
            if (request.getAttribute("error") == null) {
                if (session.getAttribute("datosUsuario") != null) {
                    DataUsuarioBas d = (DataUsuarioBas) session.getAttribute("datosUsuario");
                    mostrar = "Hola " + d.getNick() + "! (" + d.getTipo().toString() + ")";
                }
            } else {
                mostrar = "Error: " + request.getAttribute("error").toString();
            }%>

        <h1><%= mostrar%></h1>
        <a href="/RegistrarProducto.jsp">LINK A REGISTRAR PRODUCTO</a> <br>
        <a href="/ListadoProductos.jsp">LINK A LISTAR PRODUCTOS</a><br>
        <a href="cargardatos">Cargar Datos de Prueba</a>
    </body>
</html>
