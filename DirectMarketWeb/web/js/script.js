function verificarTipoImagen(fileid) {
    var file = document.getElementById(fileid);

    file.onchange = function(e) {
        var ext = this.value.match(/\.([^\.]+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'jpeg':
            case 'bmp':
            case 'png':
            case 'tif':
                break;
            default:
                alert('Imagen no valida!');
                this.value = '';
        }
    };
}
;

function verificacionTipoImagen(file) {
    var ext = file.value.match(/\.([^\.]+)$/)[1];
    switch (ext)
    {
        case 'jpg':
        case 'jpeg':
        case 'bmp':
        case 'png':
        case 'tif':
            break;
        default:
            alert('Imagen no valida!');
            file.value = '';
    }
}
;

if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    };
}
;

function bien(id, msg) {
    $(id).empty();
    $(id).append("<i class='icon-warning-sign'></i> ");
    $(id).append(msg);
    $(id).css('color', 'green');
}

function mal(id, msg) {
    $(id).empty();
    $(id).append("<i class='icon-warning-sign'></i> ");
    $(id).append(msg);
    $(id).css('color', 'red');
}
