<%-- 
    Document   : Login
    Created on : 11-sep-2013, 1:56:19
    Author     : guillermo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login de usuarios</title>
        <jsp:include page="head.jsp"></jsp:include>
    </head>
        
            <script type="text/javascript">
                    function logueo() {
                        var rem = $("#RememberMe").is(':checked') ? "1" : "0";
                        $.post("/loginAjax", {nick: $("#nick_login").val(),
                            pass: $("#pass_login").val(), remember: rem }, function(data) {
                            if (data !== "OK") {
                                $("#errorLogin").html(data);
                            }
                            else {
                                window.location.reload();
                                
                            }
                        });
                    }
            </script>
        
        <body>
            
            <%-- <jsp:include page="navbar.jsp"></jsp:include>--%>        
            
            <div class="container">
            <style type="text/css">
                .container {
                margin-right: auto;
                margin-left: auto;
                max-width: 250px; /* or 950px */
                }
            </style>
                <div class="row">
                    <div class="alert alert-error">
                        <h4 class="alert-heading">¡Advertencia!</h4>
                            ${error}
                    </div>

                    <div class="well">
                        <form class="form login-form" action="" method="post" id="loginForm" > 
                            <h2>Logueo</h2>
                            <div>
                                <label>Usuario</label>
                                <input id="nick_login" name="nick" class="span2" type="text" />

                                <label>Contraseña</label>
                                <input id="pass_login" name="pass" class="span2" type="password" />

                                <label class="checkbox inline">
                                    <input type="checkbox" id="RememberMe" name="remember" value="1">&nbsp;Recordarme
                                </label>

                                <br /><br />
                                <p id ="errorLogin" class="error"></p>
                                <input type="button" class="btn btn-success" id="submit" onclick="logueo();" value="Login">&nbsp;&nbsp;

                            </div>
                            <br />
                            <a href="/registrarusuario">Registrarse</a>
                        </form> 
                    </div>
                </div>
            </div>
    </body>
</html>