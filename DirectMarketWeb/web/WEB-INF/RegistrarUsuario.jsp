<%-- 
    Document   : RegistrarUsuario
    Created on : 10-sep-2013, 16:49:14
    Author     : guillermo
--%>
<%@ page errorPage="/WEB-INF/errorPages/500.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de Usuario</title>
        <jsp:include page="head.jsp"></jsp:include>

            <script type="text/javascript" >



                function ch_nick() {
                    $("#nickerror").empty();
                    if (!$("#nick").val()) {
                        $("#divnick").attr("class", "control-group error");
                        mal("#nickerror","Debe ingresar el nick");
                    }
                    else {
                        $.post("verificarusuarioajax", {nick: $("#nick").val()}, function(data) {
                            
                            if (data === "OK") {
                                $("#divnick").attr("class", "control-group success");
                                bien("#nickerror","El nick está disponible");
                                return true;
                            }
                            else {
                                $("#divnick").attr("class", "control-group error");
                                mal("#nickerror","El nick " + $("#nick").val() + " está en uso");
                                return false;
                            }
                        });
                    }
                }

                function ch_email() {
                    $("#emailerror").empty();
                    if (!$("#email").val()) {
                        
                        $("#divemail").attr("class", "control-group error");
                        mal("#emailerror","Debe ingresar el email");
                        return false;
                    }
                    $.post("verificarusuarioajax", {email: $("#email").val()}, function(data) {
                        if (data === "OK") {
                            $("#divemail").attr("class", "control-group success");
                            bien("#emailerror","El email está disponible");
                            return true;
                        }
                        else {
                            $("#divemail").attr("class", "control-group error");
                            if (data === "YaExiste") {
                                 mal("#emailerror","El email está en uso");
                            }
                            else {
                                 mal("#emailerror","El email no es válido");
                            }
                            return false;
                        }
                    });
                }

                function ch_pass() {
                    if ($("#passconf").val() !== $("#pass").val()) {
                        $("#passError").text(" Las contraseñas no coinciden");
                        return false;
                    }
                    else if ($("#passconf").val().length === 0) {
                        $("#passError").text(" Debe confirmar la contraseña");
                        return false;
                    }
                    $("#passError").text("");
                    return true;
                }

                function ch_nacimiento() {
                    var fecha = $("#nacimiento").val();

                    var parts = fecha.split("/");
                    var d1 = new Date(Number(parts[2]), Number(parts[1]) - 1, Number(parts[0]));
                    var dias = (new Date() - d1) / (24 * 3600 * 1000);
                    var valido = dias >= 366;
                    if (!valido) {
                        $("#ErrorNacimiento").text("La fecha de nacimiento no es válida (al menos 1 año)");
                    }
                    else {
                        $("#ErrorNacimiento").text("");
                    }
                    return valido;
                }

                function ch_web() {
                    if ($('#web').attr('required')) {
                        var regex = /^((https?:\/\/)| ([^:]+)\.)(.+)\.(.+)/;
                        valida = $('#web').val().match(regex);
                        $('#webError').text(valida ? '' : "Web no válida");
                        return valida;
                    }
                    return true;
                }

                function validarForm() {
                    var Res = true;

                    if (!$("#nick").val().trim()) {
                        $("#nickerror").text(" Este campo no puede ser vacio");
                        Res = false;
                    }

                    if (!$("#pass").val().trim())
                    {
                        $("#ErrorPass1").text(" Debe ingresar la contraseña");
                        Res = false;
                    }
                    if (!$("#nombre").val().trim())
                    {
                        $("#ErrorNombre").text(" Ingresar nombre por favor");
                        Res = false;
                    }
                    if (!$("#apellido").val().trim()) {
                        $("#ErrorApellido").text(" Ingresar apellido por favor");
                        Res = false;
                    }

                    if (!$("#nacimiento").val().trim()) {
                        $("#ErrorNacimiento").text(" Este campo no puede ser vacio");
                        Res = false;
                    }

                    if ($("#compania").attr('required') && !($("#compania").val().trim())) {
                        $("#ErrorCompania").text("Este campo no puede ser vacio");
                        Res = false;
                    }
                    Res = Res && ch_nacimiento() && ch_pass() && ch_web();
                    // no puedo comprobar con los ch_ porque son asincronicos
                    nickOK = ($("#nickerror").text().indexOf("isponible") > -1);
                    emailOK = ($("#emailerror").text().indexOf("isponible") > -1);
                    Res = Res && nickOK && emailOK;
                    return Res;
                }

                $(function() {
                    $('#divNacimiento').datetimepicker({
                        pickTime: false
                    });
                });

                function esCliente() {
                    if (document.getElementById('accordion3').style.display === 'block') {
                        document.getElementById('accordion3').style.display = 'none';
                        document.getElementById('paso4').textContent = 'Paso 3: Imágen de Usuario';
                        $('#web').removeAttr('required');
                        $('#compania').removeAttr('required');
                    }
                }

                function esProveedor() {
                    if (document.getElementById('accordion3').style.display === 'none') {
                        document.getElementById('accordion3').style.display = 'block';
                        document.getElementById('paso4').textContent = 'Paso 4: Imágen de Usuario';
                        $('#web').addAttr('required');
                        $('#compania').addAttr('required');
                    }
                }

                $(document).ready(function() {
                    // lo que contenga error en el id lo pongo en rojo
                    $("[id*=error]").css('color', 'red');
                    $("[id*=Error]").css('color', 'red');
                });

                $(document).ready(function() {
                    // agrega handler para verificar extension de la imagen
                    verificarTipoImagen('imagen');
                });
            </script>

        </head>
        <body>  
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <h1>Registro de Usuario</h1>
                        <br />
                        <span id="errores" value="{errores}"> </span>
                        <form  method="post" action="registrarusuario" enctype="multipart/form-data" onSubmit="return validarForm();">
                            <div class="accordion" id="accordion1">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">
                                            Paso 1: Validación de Nick
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="accordion-body collapse">
                                        <div class="accordion-inner">

                                            <div>
                                                <label for="nick">Nick:</label>
                                                <div id= "divnick">
                                                    <input type="text" name="nick" id ="nick" class="large-field" onblur="ch_nick();" value="${param.nick}" required/>
                                                <span id="nickerror" name="nickerror" ></span>
                                            </div>
                                        </div>

                                        <div id="divemail">
                                            <label for="email">Email:</label>
                                            <div >
                                                <input type="text" name="email" id="email" class="large-field" onblur="ch_email();"  value="${param.email}" required/>
                                                <span id="emailerror" name="emailerror"></span>
                                            </div>
                                        </div>            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion" id="accordion2">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                        Paso 2: Datos de Usuario
                                    </a>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div>
                                            <label for="nombre">Nombre:</label>
                                            <div>
                                                <input type="text" name="nombre" id="nombre" class="large-field"  value="${param.nombre}" required/>
                                                <span id="ErrorNombre"></span>
                                            </div>
                                        </div>
                                        <div>
                                            <label for="apellido">Apellido:</label>
                                            <div>
                                                <input type="text" name="apellido" id="apellido" class="large-field"  value="${param.apellido}" required/>
                                                <span id="ErrorApellido"></span>
                                            </div>
                                        </div>
                                        <div>
                                            <label for="nacimiento">Fecha de Nacimiento:</label>
                                            <div >
                                                <div id="divNacimiento" class="input-append">
                                                    <input class="span3" type="text" id="nacimiento" data-format="dd/MM/yyyy" name="nacimiento" required onblur="ch_nacimiento();">
                                                    <span class="add-on">
                                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                                    </span>
                                                </div>
                                                <span id="ErrorNacimiento"></span>
                                            </div>
                                        </div>
                                        <div >
                                            <label for="password">Contrase&ntilde;a:</label>
                                            <div >
                                                <input type="password" name="pass" id ="pass" class="large-field" required />
                                                <span id="ErrorPass1"></span>
                                            </div>
                                        </div>
                                        <div>
                                            <label for="passconf">Confirmar Contrase&ntilde;a:</label>
                                            <div >
                                                <input type="password" name="passconf" id ="passconf" class="large-field" onblur="ch_pass();" required />
                                                <span id="passError" name="passError"></span>
                                            </div>
                                        </div>
                                        <div>
                                            <label for="tipo">Perfil:</label>
                                            <label class="radio">
                                                <input type="radio" name="tipo" id="rbCliente" value="c" class="form-control" onclick=" esCliente();" checked >
                                                Cliente
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="tipo" id="rbProveedor" value="p" class="form-control" onclick=" esProveedor();">
                                                Proveedor
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion" id="accordion3" style="display: none;">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a id="paso3" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree">
                                        Paso 3: Datos de Compañia
                                    </a>
                                </div>
                                <div id="collapseThree" class="accordion-body collapse">
                                    <div class="accordion-inner">

                                        <div >
                                            <label for="compania">Nombre:</label>
                                            <div >
                                                <input type="text" id="compania" name="compania" class="large-field"/>
                                                <span id="ErrorCompania"></span>
                                            </div>
                                        </div>
                                        <div >
                                            <label for="web">Web:</label>
                                            <div >
                                                <input type="text" id="web" name="web" class="large-field" onblur="ch_web()"/>
                                                <span id="ErrorWeb"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion" id="accordion4">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a id="paso4" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour">
                                        Paso 3: Imagen de Usuario
                                    </a>
                                </div>
                                <div id="collapseFour" class="accordion-body collapse">
                                    <div class="accordion-inner">

                                        <div >
                                            <label for="imagen">Cargar Imagen:</label>
                                            <div >
                                                <input type="file" id="imagen" name="Cargar" class="large-field" />
                                            </div>  
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <input type="submit" value="Enviar" class="btn btn-primary"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
