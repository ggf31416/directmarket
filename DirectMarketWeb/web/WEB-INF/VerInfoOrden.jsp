<%-- 
    Document   : VerInfoOrden
    Created on : Sep 28, 2013, 11:02:07 AM
    Author     : tprog088
--%>


<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="directmarket.servidor.DataEstado"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="directmarket.web.logicaweb.Logos"%>
<%@page import="java.util.List"%>
<%@page import="directmarket.servidor.DataOrdenLin"%>
<%@page import="directmarket.servidor.DataLinea"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="com.itextpdf.text.pdf.*"%>
<%@page import="com.itextpdf.text.*"%>
<%@page import="com.itextpdf.text.pdf.BarcodeQRCode"%>
<!DOCTYPE html>
<html>


    <head>
        <%  DataOrdenLin dol = (DataOrdenLin) request.getAttribute("dataorden");
            List<DataLinea> lineas = dol.getLineas();
             String nick = (String)session.getAttribute("nick");
             if(!(nick.equals(dol.getNick())))
             {
                String url = request.getServerName()+ ":"+request.getLocalPort()+"/";
                 response.sendRedirect(url);
             }
           
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Información de Orden</title>
        <jsp:include page="head.jsp"></jsp:include>
            <script>
                function confirmar() {
                    $.post("/OrdenConfirma", {nroOrden: <%= dol.getNroOrden()%>}, function(data) {
                        $("#confirmar").css("display", "none");
                        $("#recepcionConfirmada").css("display", "block");
                        <% SimpleDateFormat sdfe = new SimpleDateFormat("dd/MM/yyyy");%>
                              var  html = '<tr> <td> Confirmada</td> <td><%sdfe.format(new Date());%></td> </tr>'
                        $("#estados").add(html(html));
                    }

                    );
                }
        </script>
    </head>
    <body>
        <jsp:include page="navbar.jsp"></jsp:include> 
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div id="recepcionConfirmada" class="alert alert-success" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Se ha confirmado la recepcion de la orden de Compra
                        </div>
                        <h2>Orden de Compra</h2>
                        <form action="/publico/infoproducto">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Logo</th>
                                        <th>Nro. Ref</th>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <%  int i = 0;
                                    for (DataLinea dl : lineas) {%>
                                <tr>
                                    <td>
                                        <label class="radio">
                                            <% if (i == 0) {%>
                                            <input type="radio" name="ref"  value="<%= dl.getRef()%>" checked>
                                            <% } else {%>
                                            <input type="radio" name="ref"  value="<%= dl.getRef()%>">
                                            <% }%>
                                        </label>
                                    </td>
                                    <td><img src="<%= Logos.mostrarLogo(dl.getLogo())%>" class="img-rounded" style="width: 30px; height: 30px;"></td>
                                    <td><%= dl.getRef()%></td>
                                    <td><%= dl.getTitulo()%></td>
                                    <td><%= dl.getCantidad()%></td>
                                    <td><%= dl.getPrecio()%></td>
                                    <td><%= dl.getSubtotal()%></td>
                                </tr>
                                <% i++;
                                    }%>
                            </tbody>
                        </table>
                        <dl class="dl-horizontal pull-right">
                            <dt>Total:</dt>
                            <dt>$ <%= dol.getTotal()%></dt>
                        </dl>
                        <div class="clearfix"></div>
                        <input type="submit" value="Ver Info del Producto" class="btn btn-success pull-right">

                    </form>
                    <div class="container">
                        <div class="row">
                            <div class=" span3">
                                <form>   
                                    <h3>Seguimiento de orden</h3>
                                    <table class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Estado</th>
                                                <th>Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody id="estados">
                                            <%  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                    for (DataEstado de : dol.getEstados()) {%>
                                          <tr >
                                                <td><%= de.getEstado().toString()%></td>
                                                <td><%= sdf.format(de.getFecha().toGregorianCalendar().getTime())%> </td>
                                            </tr>
                                            <% }%>
                                        </tbody>
                                    </table>
                                </form>   
                            </div>   
                        </div> 
                    </div>


                    <% if ((Boolean) request.getAttribute("estaPreparada")) {%>
                    <input id="confirmar" type="button" value="Confirmar Recepcion" class="btn btn-success pull-right" onclick="confirmar();">

                    <% }%>
                        


                           
                              
                           <form action="/DescargarPDF" method="get" >
                               <input name="Nroorden" class="hide" value="<%=dol.getNroOrden()%>"/>
                                <input type="submit" value="Descargar PDF" class="btn btn-primary"/>
                           </form>
                           
                       
                       
                      
                       
                       <%--<a id ="pdf" href="/InfoOrden?optionsRadios=<%= dol.getNroOrden()%>&descargar=1" >descargarPDF</a> --%>
                    
                </div>        
            </div>
        </div>
    </body>
</html>

