<%-- 
    Document   : PuntuarProducto
    Created on : Oct 28, 2013, 11:06:19 AM
    Author     : Gonzalo Miraballes
--%>

<%@page import="directmarket.servidor.DataPuntaje"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <!-- Rateit -->	
        <script type="text/javascript" src="../js/rateit/src/jquery.rateit.js"></script>
        <link rel="stylesheet" href="../js/rateit/src/rateit.css" type="text/css" />
        
        <!-- Google Charts -->	
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        
        <% List<DataPuntaje> lst_pjs = (List<DataPuntaje>) request.getAttribute("Puntajes");
        int[] pjs = {0,0,0,0,0};
        double count = 0; 
        double avg = 0;
        for (DataPuntaje dp : lst_pjs) {
            pjs[dp.getValor()-1]++;
            count+=dp.getValor();
        }
        avg = (lst_pjs.size()!=0) ? (count /lst_pjs.size()) : 0;
        avg = Math.round(100 * avg) / 100.0; 
        %>
        <script type="text/javascript">
          google.load("visualization", "1", {packages:["corechart"]});
          google.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Stars', '1 star', '2 stars', '3 stars', '4 stars', '5 stars'],
              ['', <%= pjs[0]%>, <%= pjs[1]%>, <%= pjs[2]%>, <%= pjs[3]%>, <%= pjs[4]%>]
            ]);

            var options = {
              title: 'VALORACION DE CLIENTES'
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);
          }
        </script>
        <script>
            function cambiarAReadOnly() { $('.rateit').rateit('readonly',true); };
            function obtenerSeleccion() { $('#contValor').attr('value',$('#puntUsuario.rateit').rateit('value'));}
        </script>
        <script type="text/javascript">
            var tooltipvalues = ['bad', 'poor', 'ok', 'good', 'awesome!'];
            $(".rateit").bind('over', function (event, value) { $(this).attr('title', tooltipvalues[value-1]); });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="chart_div" class="span6" style="width: 500px; height: 500px;"></div>
                <div class="span6">
                    <c:if test="${puedeComentar}">
                            <form method="post" action="/PuntuarProducto">
                                <input type="hidden" name="Ref" value="${Ref}">
                                <input id="contValor" type="hidden" name="valor" value="0">
                                <ul class="inline">
                                    <% boolean puntuo = (Boolean) request.getAttribute("puntuo");
                                       if (puntuo != true) {%>
                                         <li><label>Ingrese su <b>Puntuación</b> para este Producto:</label></li>
                                         <li><div id="puntUsuario" class="rateit" data-rateit-value="1" data-rateit-step="1" data-rateit-resetable="false" data-rateit-readonly="false" onclick="obtenerSeleccion();"></div></li>
                                         <li>&nbsp;<input type="submit" value="Puntuar" id="btn_puntuar" class="btn btn-small btn-primary" onclick="cambiarAReadOnly();"></li>
                                    <% } else { %>
                                         <li><label>Su <b>Puntuación</b> ingresada para este Producto es:</label></li>
                                         <li><div class="rateit" data-rateit-step="1" data-rateit-resetable="false" data-rateit-readonly="true" data-rateit-value="<%=request.getAttribute("puntuacion")%>"></div></li>
                                    <% } %>
                                </ul>
                            </form>
                    </c:if>
                    <div class="well span3 text-center">
                        <h4><b>&nbsp;Puntuación promedio:&nbsp;&nbsp;</b></h4>
                        <h4><b>&nbsp;<%=avg%>&nbsp;/&nbsp;5.0&nbsp;</b></h4>
                        <div id="puntajeProm" class="rateit" data-rateit-step="0.5" data-rateit-resetable="false" data-rateit-readonly="true" data-rateit-value="<%=avg%>"></div>
                    </div>
                </div>
            </div>
        </div>
        <script>jQuery(document).ready(function(){jQuery('div.rateit').rateit();});</script>
    </body>
</html>




