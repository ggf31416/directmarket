<%-- 
    Document   : info_carrito
    Created on : Sep 26, 2013, 8:00:45 PM
    Author     : tprog088
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="directmarket.servidor.DataOrdenLin"%>
<%@page import="directmarket.servidor.TipoUsuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
          <% HttpSession sesion = request.getSession();
       if (sesion.getAttribute("tipo") != null && sesion.getAttribute("tipo").equals(TipoUsuario.CLIENTE) && request.getAttribute("carrito") != null) {
           DataOrdenLin carrito = (DataOrdenLin)request.getAttribute("carrito");
           DecimalFormat fmt = new DecimalFormat("USD #,##0.00");
           
        %>
                
        <div class="well" style="margin: 5px;padding-bottom: 5px;padding-top: 5px">
        <%  %>
        <div class="dropdown">
            <a class="dropdown-toggle" href="/InfoCarrito" id="infocarrito">
                <i class="icon-shopping-cart" ></i> 
                (${carrito.getLineas().size()}) - (<%= fmt.format(carrito.getTotal()) %>)
                   
                </a>
            <!--
            <div class="dropdown-menu well" role="menu" aria-labelledby="dLabel">
                <p>Item x 1 <span class="pull-right">$333.33</span></p>
                <p>Item x 1 <span class="pull-right">$333.33</span></p>
                <p>Item x 1 <span class="pull-right">$333.33</span></p>
                <a href="#" class="btn btn-primary">Confirmar Orden</a>
            </div>
            -->
        </div>
          

    </div>
    <% } %>
