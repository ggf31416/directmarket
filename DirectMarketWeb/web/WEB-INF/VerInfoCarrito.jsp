<%-- 
    Document   : VerInfoCarrito
    Created on : Sep 24, 2013, 8:49:26 PM
    Author     : tprog089
--%>


<%@page import="directmarket.servidor.CarritoException"%>
<%@page import="directmarket.web.logicaweb.Logos"%>
<%@page import="java.util.List"%>
<%@page import="directmarket.servidor.DataLinea"%>
<%@page import="directmarket.servidor.DataOrdenLin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Información de Carrito</title>
        <jsp:include page="head.jsp"></jsp:include>
        </head>
    <%  DataOrdenLin dol = (DataOrdenLin) request.getAttribute("carrito");
        List<DataLinea> lineas = dol.getLineas();
    %>
    <body>
        <jsp:include page="navbar.jsp"></jsp:include>
        <div class="container">
            <div class="row">
                <div class="span12">
                    <h2>Carrito de Compra</h2>
                    <form>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nro. Ref</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio Unitario</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% for (DataLinea dl : lineas) { %>
                                    <tr>
                                        <td><img src="<%= Logos.mostrarLogo(dl.getLogo())%>" class="img-rounded" style="width: 30px; height: 30px;"></td>
                                        <td><%= dl.getRef() %></td>
                                        <td><%= dl.getTitulo() %></td>
                                        <td><input id="quantity" class="span1" type="number" style="width: 45px; padding: 1px" value="<%= dl.getCantidad()%>">&nbsp;&nbsp;<a href="#"><i class="icon-refresh"></i></a>&nbsp;&nbsp;<a href="#"><i class="icon-trash"></i></a></td>
                                        <td><%= dl.getPrecio() %></td>
                                        <td><%= dl.getSubtotal() %></td>
                                    </tr>
                                <% } %>
                            </tbody>
                        </table>
                    </form>
                    <dl class="dl-horizontal pull-right">
                        <dt>Total:</dt>
                        <dt>$ <%= dol.getTotal()%></dt>
                    </dl>
                    <div class="clearfix"></div>
                    <% if(dol.getLineas().size() != 0) {%>
                    <a href="/ConfirmarOrden" class="btn btn-success pull-right">Confirmar Orden</a>
                    <a href="/CancelarOrden" class="btn btn-danger pull-right" style="margin-right: 10px;">Cancelar Orden</a>
                    <% } else { %>
                    <button type="button"href="/ConfirmarOrden" class="btn btn-success pull-right" disabled="disabled" disable>Enviar Orden</button> 
                    <button type="button"href="/CancelarOrden" class="btn btn-danger pull-right" style="margin-right: 10px;" disabled="disabled" disable>Borrar Orden</button>
                    <% } %>
                </div>

            </div>
        </div>
    </body>
</html>

