<%-- 
    Document   : RegistrarProducto
    Created on : Sep 21, 2013, 11:03:40 AM
    Author     : tprog086
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/WEB-INF/errorPages/500.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de Producto</title>
        <jsp:include page="head.jsp"></jsp:include>


            <script>

                $(document).ready(function() {
                    // Muestra las categorias
                    $.get("/CategoriasSinProductos", function(data) {
                        // todo tiene que ser llamado por el callback. Estuve 2 horas determinando porque 
                        // no estaban los elementos en la pagina y resulta que como no estaban en el callback
                        // los estaba llamando inmediatamente despues de hacer la llamada ajax y por lo tanto 
                        // antes que llegaran los datos
                        $("#topeArbol").html(data);
                        $('ul.principal').addClass("nav nav-list");
                        $('ul.nodo').addClass("nav nav-list tree");
                        $('label.nodo').addClass("tree-toggler nav-header");

                        $('label.tree-toggler').on("click", function() {
                            $(this).parent().children('ul.tree').toggle(300);
                        });
                        var catActual = '${catActual}';
                        if (catActual) {
                            $("#cat" + catActual).parents('ul.tree').toggle(0);
                        }
                        $(".hoja > i").attr('rel', "tooltip");
                        $(".hoja > i").attr('title', "Agregar");
                        //$(".hoja > i").html("<small> Agregar</small>");
                    });
                });



               

                function verificarImg(elem, i) {
                    verificacionTipoImagen(elem);
                    if ($(elem).val()) {
                        activarLogo(i);
                    }
                }

                $(document).ready(function() {

                    for (var i = 0; i < 3; i++) {
                        (function(i) {
                            $("#imagen" + i).change(function() {
                                verificarImg(this, i);
                            });
                        })(i);
                    }
                });

                function activarLogo(num) {
                    $("#logo" + num).removeClass("hide"); // hace visible el radio de logo
                    // hace visible el radio de logo y el boton de eliminar
                    $("#ctrlImg" + num).removeClass("hide");
                    // hace visible el input siguiente
                    $("#div_img" + (num + 1)).removeClass("hide");
                }
                function eliminarArchivo(num) {
                    $("#imagen" + num).val("");
                    if (num > 0) {
                        $("#div_img" + num).addClass("hide");
                        if ($("#logo" + num + ":checked")) {
                            $("#logo0").prop("checked", true);
                        }
                    }
                    $("#ctrlImg" + num).addClass("hide");
                }

                function verificarDispRef(){
                    if(verifFormatoRef()){
                        $.get("/existeRef",{ref:$("#NroRef").val()},function (data){
                            if (data === "OK"){
                                bien("#ErrorNroRef",'Disponible');
                            }
                            else{
                                mal("#ErrorNroRef",'En uso');                         
                            }
                        });
                    }
                }
                
                function verificarDispTitulo(){
                        if(!$("#Titulo").val().trim()){
                            mal("#ErrorTitulo","Este campo no puede ser vacio");
                            return;
                        }
                        $.get("/dispTitulo",{titulo:$("#Titulo").val()},function (data){
                            if (data === "OK"){
                                bien("#ErrorTitulo",'Disponible');
                            }
                            else{
                                mal("#ErrorTitulo",'En uso');                         
                            }
                        });
                }
                
                function verifFormatoRef(){
                    valido = $("#NroRef").val().trim() && $("#NroRef").val().match('^(0|[1-9][0-9]*)$');
                    if(!valido) {
                        mal("#ErrorNroRef","Ingresar dato numerico por favor");
                    }
                    return valido;
                }

                function validarProducto() {

                    var Res = true;

                    // $("#Titulo").val() obtiene el valor del texto de Titulo y si es vacio es 
                    // equivalente a falso
                    // por otro lado :empty lo que hace es detectar si tiene hijos
                    if (!$("#Titulo").val().trim()) {
                        $("#ErrorTitulo").text("Este campo no puede ser vacio");
                        Res = false;
                    }


                    if (!$("#Precio").val().trim() || !($("#Precio").val().match('^(0|[1-9][0-9]*)$')))
                    {
                        $("#ErrorPrecio").text("Ingresar dato numerico por favor");
                        Res = false;
                    }
                    if (!verifFormatoRef())
                    {
                       
                        Res = false;
                    }
                    if (!$("#Descripcion").val().trim()) {
                        $("#ErrorDescripcion").text("Este campo no puede ser vacio");
                        Res = false;
                    }

                    if (!$("#Especificacion").val()) { // voy a aceptar que pongan espec con solo un espacio
                        $("#ErrorEspecificacion").text("Este campo no puede ser vacio");
                        Res = false;
                    }
                    var rowCount = $('#cats_agr tr').length;
                    if (rowCount <= 1) // 1 por la cabecera
                    {
                        alert("seleccione una categoria por favor");
                        Res = false;
                    }
                    refOK = $("#ErrorNroRef").text().indexOf("isponible") > -1;
                    titOK = $("#ErrorTitulo").text().indexOf("isponible") > -1;
                    return Res && refOK && titOK;

                }

                $(document).ready(function() {
                    $("#div_img1").addClass('hide');
                    $("#div_img2").addClass('hide');
                }
                );


                $(document).ready(function() {
                    $('label.tree-toggler').click(function() {
                        $(this).parent().children('ul.tree').toggle(300);
                    });
                });


                $(document).ready(function() {
                    // lo que contenga error en el id lo pongo en rojo
                    $("[id*=error]").css('color', 'red');
                    $("[id*=Error]").css('color', 'red');
                });

/*function formatearPrecio(){
    $("#Precio").text(Math.round($("#Precio").val(),2).toFixed(2));
}*/
        </script>
    </head>
    <body>



        <jsp:include page="navbar.jsp" ></jsp:include>

            <div class="container">
                <div class="row">
                    <form method="post" action="/proveedores/RegistrarProducto" enctype="multipart/form-data" onSubmit="return validarProducto();">

                        <div class="span6">
                            <fieldset>
                                <h2>Registro de Producto</h2>

                                <label> Título:</label>

                                <input id="Titulo" type="text" class="large-field" value="" name="titulo"  onblur="verificarDispTitulo();" required>
                                <span id="ErrorTitulo"></span>

                                <label> Nro. de Referencia:</label>
                                <input id="NroRef" type="text" class="large-field" value="" name="nro_ref" onblur="verificarDispRef();" required >
                                <span id="ErrorNroRef"></span>

                                <label> Precio:</label>
                                <div class="input-prepend input-append">
                                    <span class="add-on">USD</span>
                                    <input id="Precio" name ="precio"class="span2" id="Precio" type="text" required >
                                    <span class="add-on" > </span>
                                    <span id="ErrorPrecio"></span>
                                </div>

                                <label> Descripción:</label>
                                <input id="Descripcion" type="text" class="large-field span5" value="" name="desc" required >
                                <span id="ErrorDescripcion"></span>

                                <label> Especificación:</label>
                                <textarea id="Especificacion" class="large-field span5" rows="10" name="esp" required ></textarea>
                                <span id="ErrorEspecificacion"></span>



                            </fieldset>
                            <button id="Continuar"class="btn btn-primary">Continuar</button>
                            <button id="Cancelar"class="btn">Cancelar</button>
                        </div>



                        <div class="span6">
                            <fieldset>  <!-- inicio castegorias -->

                                <style type="text/css">
                                    .tree {
                                        display: none;
                                    }
                                </style>


                                <label>Categorias:</label>
                                <div class="row">
                                    <div class="span3">
                                        <div class="well" style="padding: 8px 0;">

                                            <div style="overflow-x: auto; height: 250px;" id="topeArbol" class="table table-striped">
                                                // categorias aqui
                                            </div>
                                        </div>
                                    </div>


                                    <script type="text/javascript">
                            function agregarCat(id, nombre) {
                                if (!$('#cata' + id).length) { // si ya no esta
                                    hidden = '<input type="hidden" name="cata' + id + '" id="cata' + id + '" value="' + nombre + '">';
                                    label = '<label>' + nombre + '</label>';
                                    button = '<input type="button" class = "btn" onclick="eliminarCat(' + id + ')"; value="Eliminar">';
                                    // agrega un input
                                    $('#cats_agr').append('<tr id="catAgr' + id + '">' + '<td>' + hidden + label + '</td><td>' + button + '</td></tr>');


                                }
                            }

                            function eliminarCat(id) {
                                $('#cats_agr').find('#catAgr' + id).remove();
                            }

                                    </script>

                                    <div class="span3">
                                       <!-- <div class="well" style=" padding: 9px 0;"> -->
                                        
                                            <div  class="well" style="overflow-x: auto; height: 250px;padding: 9px">
                                                <table id="cats_agr" class="table table-striped"> 
                                                    <tr><th>Categor&iacute;as</th><th>Agregadas</th></tr>
                                                </table>
                                            </div>
                                         
                                    </div>

                                    <div class="container" id="ContenedorImages">

                                        <!--<div id="Images" class="" overflow="scroll" ></div>-->
                                    </div>

                                    <label> Adjuntar imágenes:</label>

                                    <!-- crea tres input y muestra solo  el primero -->
                                <c:forEach var="i" begin="0" end="2" >
                                    <ul class="inline" id="div_img${i}">
                                        <li><input type="file" id="imagen${i}" name="imagen${i}" text="Cargar"></li>                          
                                        <!--<li><input type="button" id="SaveFile${i}" class="btn"  onclick="activarLogo(${i});" value="Adjuntar Imagen"></li> -->
                                        <li id="ctrlImg${i}" class="hide">
                                            <input type="button" id="eliminarImagen${i}" class="btn" onclick="eliminarArchivo(${i});" value="Eliminar">
                                        </li>
                                    </ul> 

                                </c:forEach>
                            </div>

                        </fieldset> <!-- fin castegorias -->
                    </div>
                    <br />
                    <br />
                </form>

            </div>  
        </div> <!--fin container -->
        <br>
        <script>
                        $(document).ready(function() {
                            $("#Cancelar").click(function() {
                                $("#Titulo").text("");
                                $("#Precio").text("");
                                $("#Descripcion").text("");
                                $("#Especificacion").text("");
                                $("#NroRef").text("");

                            });
                        });
        </script>

        <script>
            //Expresión para validar un correo electrónico
            var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
            //Expresión para validar edad de 18 a 60 años
            var expr2 = /^1[8-9]|[2-5]\d|60$/;

        </script>


        <script>

            $(document).ready(function() {
                //función click
                $("#SaveFile").click(function() {
                    var imagen = "<p>" + $("#File").val() + "</p>";
                    $("#AdjuntarText").text("Imagenes del producto");
                    $("#Images").append(imagen);


                });
            });

            $(document).ready(function() {
                //función click
                $("#Images p").click(function() {
                    $("p").hide();

                });
            });
        </script>		




    </body>


</html>
