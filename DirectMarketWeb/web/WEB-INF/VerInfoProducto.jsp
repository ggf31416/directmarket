<%-- 
    Document   : verInfoProducto
    Created on : Sep 19, 2013, 9:12:05 PM
    Author     : tprog088
--%>

<%@page import="directmarket.servidor.DataReclamo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="directmarket.servidor.TipoUsuario"%>
<%@page import="directmarket.web.logicaweb.Logos"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Información de Producto</title>
        <jsp:include page="head.jsp"></jsp:include>

            <script type="text/javascript">
                function cantValida() {
                    q = $("#quantity").val();
                    pasa = q.match(/^[\d]{1,2}$/);
                    return pasa;
                }
            </script>
            <script>
                function confirmar() {
                    $.post("/proveedores/IngresarReclamo", {Mensaje: $("#Mensaje").val(), Ref: ${Ref}}, function(data) {
                        var html = '<div id="recepcionConfirmada" class="alert alert-success">\n\
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
<strong>Su reclamo ha sido recibido.</strong>\n\
</div>';
                        $('#contAlerta').html(html);
                    }

                    );
                }
        </script>
    </head>
    <body>
        <jsp:include page="navbar.jsp"></jsp:include>

            <div class="container">
                <div class ="row" style="padding-top: 20px;">
                    <div class="span12">
                        <div id="contAlerta"></div>

                        <div class="row">
                            <div class="span6">

                                <div id="myCarousel" class="carousel slide">
                                    <div class="carousel-inner">
                                        <!-- Carousel items -->
                                    <%  List<String> imagenes = (List<String>) request.getAttribute("Imagenes");
                                        if (imagenes.isEmpty()) {
                                    %>
                                    <div class="active item">
                                        <center>
                                            <img class="media-object thumbnails380" src="/static/imagenes/sinfoto.png" alt=""/>
                                        </center>
                                    </div>
                                    <%                                   } else {
                                        for (int i = 0; i < imagenes.size(); i++) {
                                            String URL = Logos.mostrarLogo(imagenes.get(i));
                                            String e = (i == 0) ? "active item" : "item";
                                    %>
                                    <div class="<%=e%>">
                                        <center>
                                            <img class="media-object thumbnails380" src="<%=URL%>" alt=""/>
                                        </center>
                                    </div>
                                    <%
                                            }
                                        }
                                    %>    
                                </div>
                                <!-- Carousel nav -->
                                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                            </div>
                        </div>
                        <!--<script src="jquery.js"></script>-->
                        <script><!-- Este script se añade para eliminar el auto sliding del carousel -->
                            $('.carousel').carousel({
                                interval: false
                            })
                        </script>                                

                        <div class="span6">
                            <h4><%= request.getAttribute("Titulo")%></h4>
                            <h5>Nro. Referencia: <%= request.getAttribute("Ref")%></h5>
                            <p><%= request.getAttribute("Descripcion")%></p>
                            <h4>$<%= request.getAttribute("Precio")%></h4>
                            <form method="post" action="/AgregarCarrito" onsubmit="return cantValida();" >
                                <br />
                                <input type="hidden" name="Ref" value="${Ref}">
                                <label>Proveedor: <%= request.getAttribute("Proveedor")%></label>
                                <label>Categorías:
                                    <c:forEach items="${Categorias}" var="c"  varStatus="i">
                                        <a href="/listadoproductos?catnombre=${c}">${c}</a>
                                        <c:if test="${!i.last}">
                                            &nbsp;&#124;&nbsp;
                                        </c:if>
                                    </c:forEach>
                                </label>
                                <br />
                                <% if (request.getAttribute("AgregoCarrito") != null) {
                                        if (request.getAttribute("AgregoCarrito").equals(true)) {
                                %><label>Agregado al carrito</label><%                                      } else {%>
                                <label>
                                    Cantidad:&nbsp;
                                    <input id="quantity" name="quantity" class="span1" type="number" style="width: 45px; padding: 1px" value="1" min="1" max="100">
                                </label>
                                <input type="submit" value="Agregar" class="btn btn-success">
                                <% }
                                    }%>
                            </form>
                        </div>
                    </div>
                </div>
            </div>         

            <div class="row">
                <div class="span12">
                    <ul class="nav nav-tabs" id="tabs"> 
                        <li class="active"><a href="#especificacion" data-toggle="tab">Especificación</a></li>
                        <li><a href="#comentarios" data-toggle="tab"><span class="badge badge-inverse">${Comentarios}</span> Comentarios</a></li>
                            <c:if test="${puedeComentar}">
                            <li><a href="#reclamos" data-toggle="tab">Ingresar Reclamo</a></li>
                            </c:if>
                        <li><a href="#puntuacion" data-toggle="tab">Puntuación</a></li>

                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="especificacion">
                            <p>
                                <%=(String) request.getAttribute("Especificacion")%>
                            </p>
                        </div>
                        <div class="tab-pane" id="comentarios">
                            <jsp:include page="listarcomentarios.jsp" ></jsp:include>
                        </div>
                        <script>
                            function validarTextoReclamo() {
                                var Res = true;
                                if (!$("#Mensaje").val().trim()) {
                                    $("#ErrorMensaje").text("Este campo no puede ser vacio");
                                    Res = false;
                                }
                                if (Res)
                                    confirmar();
                                return Res;
                            }
                        </script>
                        <div class="tab-pane" id="reclamos">
                            <div class="container-fluid">
                                <div class="row-fluid">
                                    <div class="span4">
                                        <fieldset>
                                            <label><b>Ingrese su Reclamo:</b></label>
                                            <textarea id="Mensaje" name="Mensaje" class="large-field span10" rows="6" name="esp" required></textarea>
                                            <p id="ErrorMensaje"></p>
                                        </fieldset>
                                        <input type="submit" value="Ingresar" class="btn btn-success" onclick="validarTextoReclamo();">
                                    </div>
                                    <div class="span8">
                                    <%  if (TipoUsuario.CLIENTE.equals(request.getSession().getAttribute("tipo"))) {
                                                boolean existeRec = (Boolean) request.getAttribute("existeReclamo");
                                                if (existeRec) {
                                                    List<DataReclamo> reclamos = (List<DataReclamo>) request.getAttribute("reclamos");      
                                        %>  
                                        <h4>Reclamos Enviados</h4>
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Reclamos</th>
                                                    <th>Fecha</th>
                                                    <th>Respuesta</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%  int i = 0;
                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                                    for (DataReclamo dr : reclamos) {
                                                        String fecha = sdf.format(dr.getFecha().toGregorianCalendar().getTime());%>
                                                <tr>
                                                    <td><%= dr.getTexto()%></td>
                                                    <td><%= fecha%></td>
                                                    <% if (dr.getRespuesta()!=null) { %>
                                                        <td><%= dr.getRespuesta()%></td>
                                                    <% } else { %>
                                                    <td><b><label style="color:red">Sin respuesta!</label></b></td>
                                                    <% } %> 
                                                </tr>
                                                <%      i++;
                                                    }%>
                                            </tbody>
                                        </table>
                                        <%}
                                            }%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="puntuacion">
                            <jsp:include page="PuntuarProducto.jsp" ></jsp:include>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function()
            {
                $(".Response").click(function()
                {
                    $(".Div-Res").append("<textarea class=\"Text-Res\"></textarea>");
                    $(".Div-Res").append("<input class=\"Save-Com\" value=\"Guardar\">");
                });
            });
        </script>
    </body>
</html>
