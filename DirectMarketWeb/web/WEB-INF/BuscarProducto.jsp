<%-- 
    Document   : BuscarProducto
    Created on : Sep 28, 2013, 1:41:23 PM
    Author     : tprog090
--%>

<%@page import="directmarket.web.logicaweb.Logos"%>
<%@page import="java.util.List"%>
<%@page import="directmarket.servidor.DataProductoBas"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscar Producto</title>
        <jsp:include page="head.jsp"></jsp:include>
        </head>

    <%  HttpSession sesion = request.getSession();
        List<DataProductoBas> lprod = (List<DataProductoBas>) request.getAttribute("listaproductos");
        String search = (String) request.getAttribute("search");
    %>
    <body>
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="container">
                <div class="row">
                    <div class="span7">
                        <legend id="titulo_busqueda"><i class="icon-search"></i>&nbsp;<span>${totalproductos}&nbsp;Resultados para&nbsp;"<%= search%>"</span></legend>
                </div>
                <div class="span5">
                    <form method="post" action="/OrdenarBusqueda?search=<%=search%>">
                        <ul class="inline">
                            <li>
                                <label>Ordenar:</label>
                            </li>
                            <li>
                                <select class="input-medium" name="c" id="orden_id">
                                    <option value=""> --- </option>
                                    <option value="pre">Precio</option>
                                    <option value="alfa">Alfabéticamente</option>
                                    <option value="com">Cant. Compras</option>
                                </select>
                            </li>
                            <li>
                                <select class="input-mini" name="o" id="orden_tipo">
                                    <option value=""> --- </option>
                                    <option value="asc">Asc</option>
                                    <option value="desc">Desc</option>
                                </select>
                            </li>
                            <li>
                                <input type="submit" value="Go!" class="btn btn-small"></input>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>    
            <div class="row">
                <div class="span12">
                    <form action="/publico/infoproducto">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Logo</th>
                                    <th>Nro. Ref</th>
                                    <th>Producto</th>
                                    <th>Descripción</th>
                                    <th>Precio Unitario</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%  int i = 0;
                                    for (DataProductoBas dp : lprod) {
                                %>
                                <tr>
                                    <td>
                                        <label class="radio">
                                            <%      if (i == 0) {%>
                                            <input type="radio" name="ref" value="<%= dp.getNroReferencia()%>" checked>
                                            <%      } else {%>
                                            <input type="radio" name="ref" value="<%= dp.getNroReferencia()%>">
                                            <%      }%>
                                        </label>
                                    </td>
                                    <td><center><img src="<%= Logos.mostrarLogo(dp.getLogo())%>" class="img-rounded" style="width: 90px; height: 90px;"></center></td>
                            <td><%= dp.getNroReferencia()%></td>
                            <td><%= dp.getTitulo()%></td>
                            <td><%= dp.getDescripcion()%></td>
                            <td>$<%= dp.getPrecio()%></td>
                            </tr>
                            <%      i++;
                                }
                            %>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <c:if test="${cantpaginas > 0}">
                        <input type="submit" value="Ver Info del Producto" class="btn btn-success pull-right">
                        </c:if>
                    </form>
                </div> 
                <div class="pagination">

                    <ul>   
                        <c:choose>
                            <c:when test="${paginaactual != 1}"> 
                                <li><a href="/OrdenarBusqueda?c=${c}&o=${o}&pagina=${paginaactual-1}&search=${search} "> Anterior </a></li>
                            </c:when>
                            <c:otherwise>
                                <li class="disabled"><span>Anterior</span></li>
                            </c:otherwise>
                        </c:choose>

                        <c:forEach var="i" begin="1" end="${cantpaginas}">
                            <c:choose>
                                <c:when test="${paginaactual != i}"> 
                                    <li><a href="/OrdenarBusqueda?c=${c}&o=${o}&pagina=${i}&search=${search}"> ${i}</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="disabled"><span> ${i} </span></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:choose>
                            <c:when test="${paginaactual != cantpaginas && cantpaginas != 0}"> 
                                <li><a href="/OrdenarBusqueda?search=${search}&c=${c}&o=${o}&pagina=${paginaactual + 1}&search=${search} "> Siguiente </a></li>
                            </c:when>
                            <c:otherwise>
                                <li class="disabled"><span>Siguiente</span></li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
