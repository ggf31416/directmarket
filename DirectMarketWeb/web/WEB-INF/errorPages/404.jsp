<%@ page errorPage="/WEB-INF/errorPages/500.jsp" %>
<%@ page isErrorPage="true" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<HTML>

    <HEAD>
        <jsp:include page="../head.jsp"></jsp:include>
        <TITLE>Elemento No Encontrado</TITLE>
    </HEAD>

    <BODY>
    <jsp:include page="../navbar.jsp" ></jsp:include>
    <div ><h1>404: Elemento no encontrado</h1></div>
    <% String msg = (String)request.getAttribute("javax.servlet.error.message"); 
    msg = (msg == null) ? "" : "Error:" + msg;
    %>

	<p>
	 <%= msg %>
        </p>
        <p>
            <a href="/" >Volver al inicio</a>
            <a href="javascript:void(0)" onclick="history.go(-1);">Volver atrás.</a> 
	</p>
    </BODY>
</HTML>
