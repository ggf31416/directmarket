<%-- 
    Document   : sidebar
    Created on : 24-sep-2013, 11:22:40
    Author     : guillermo
--%>


<%@page import="directmarket.servidor.TipoUsuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style type="text/css">
    .tree {
        display: none;
    }
</style>
<script>

    $(document).ready(function() {
        // Muestra las categorias
        $.get("/publico/categorias", function(data) {
            // todo tiene que ser llamado por el callback. Estuve 2 horas determinando porque 
            // no estaban los elementos en la pagina y resulta que como no estaban en el callback
            // los estaba llamando inmediatamente despues de hacer la llamada ajax y por lo tanto 
            // antes que llegaran los datos
            $("#topeArbol").html(data);
            $('ul.principal').addClass("nav nav-list");
            $('ul.nodo').addClass("nav nav-list tree");
            $('label.nodo').addClass("tree-toggler nav-header");
            $('label.tree-toggler').on("click", function() {
                $(this).parent().children('ul.tree').toggle(300);
            });
            var catActual = '${catActual}';
            if (catActual) {
                $("#cat" + catActual).parents('ul.tree').toggle(0);
            }
        });
    });

   


</script>

<script>

</script>
<div class="span3">
    <jsp:include page="/WEB-INF/info_carrito.jsp"></jsp:include>
    <% HttpSession sesion = request.getSession();%>
    <br />

        
        <div class="row">
            <div class="span3">
                 <h6>CATEGORIAS:</h6>
                <div class="well" style="padding: 8px 0;">

                    <div style="overflow-x: auto; height: 400px;" id="topeArbol">
                        
                        <!-- categorias aqui -->
                    </div>
                </div>
                <div class="divider"></div>

                <!--<script type="text/javascript">

                    function logueo() {
                        var rem = $("#RememberMe").is(':checked') ? "1" : "0";
                        $.post("/loginAjax", {nick: $("#nick_login").val(),
                            pass: $("#pass_login").val(), remember: rem }, function(data) {
                            if (data !== "OK") {
                                $("#errorLogin").html(data);
                            }
                            else {
                                window.location.reload();
                                
                            }
                        });
                    }
                </script>-->

            <% if (false && sesion.getAttribute("nick") == null) {%> 
            <!--<div class="well">
                <form class="form login-form" action="" method="post" id="loginForm" > 
                    <h2>Logueo</h2>
                    <div>
                        <label>Usuario</label>
                        <input id="nick_login" name="nick" class="span2" type="text" />

                        <label>Contraseña</label>
                        <input id="pass_login" name="pass" class="span2" type="password" />

                        <label class="checkbox inline">
                            <input type="checkbox" id="RememberMe" name="remember" value="1">&nbsp;Recordarme
                        </label>

                        <br /><br />
                        <p id ="errorLogin" class="error"></p>
                        <input type="button" class="btn btn-success" id="submit" onclick="logueo();" value="Login">&nbsp;&nbsp;
                        
                    </div>
                    <br />
                    <a href="/registrarusuario">Registrarse</a>
                </form> 
            </div> -->
            <% }%>
        </div>


    </div>
</div>
