<%-- 
/listadoproductosxcategoria
    Document   : ListadoProductos
    Created on : Sep 21, 2013, 12:10:44 PM
    Author     : tprog088
--%>


<%@page import="directmarket.web.logicaweb.Logos"%>
<%@ page errorPage="/WEB-INF/errorPages/500.jsp" %>
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="directmarket.servidor.DataProductoBas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!--<script type="text/javascript" src="scripts/jquery.min.js"></script> -->
        <!-- <script type="text/javascript" src="scripts/jquery.quick.pagination.min.js"></script> -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DirectMarket</title>
        <jsp:include page="head.jsp"></jsp:include>
        </head>
        <body>

        <% List<DataProductoBas> lista = (List<DataProductoBas>) request.getAttribute("listaproductos");
            int cantidad = lista.size();%>

        <jsp:include page="navbar.jsp"></jsp:include>

            <div class="container">
                <div class="row">

                <jsp:include page="sidebar.jsp"></jsp:include>

                    <div class="span9">

                    <% if (lista.size() > 0){ %>
                        <ul class="thumbnails pagination">


                        <% for (DataProductoBas dt : lista) {%>
                        <li class="w160">
                            <div class="thumbnail">
                                <img class ="thumbnails140" src="<%= Logos.mostrarLogo(dt.getLogo())%>" alt="<%= dt.getTitulo()%>">
                                <div class="caption">
                                    <h4><%= dt.getTitulo()%></h4>
                                    <p> $ <%= dt.getPrecio()%></p>
                                    <a class="btn btn-primary" href="/publico/infoproducto?ref=<%=dt.getNroReferencia()%>" >Info</a>
                                </div>
                                    
                            </div>
                        </li>
                        <% }%>
                    </ul>

                    <div class="pagination">

                        <ul>   
                            <c:choose>
                                <c:when test="${paginaactual != 1}"> 
                                    <li><a href="${origen}?pagina=${paginaactual-1} "> Anterior </a></li>
                                    </c:when>
                                    <c:otherwise>
                                    <li class="disabled"><span>Anterior</span></li>
                                    </c:otherwise>
                                </c:choose>

                            <c:forEach var="i" begin="1" end="${cantpaginas}">
                                <c:choose>
                                    <c:when test="${paginaactual != i}"> 
                                        <li><a href="${origen}?pagina=${i}"> ${i}</a></li>
                                        </c:when>
                                        <c:otherwise>
                                        <li class="disabled"><span> ${i} </span></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <c:choose>
                                    <c:when test="${paginaactual != cantpaginas}"> 
                                    <li><a href="${origen}?pagina=${paginaactual + 1} "> Siguiente </a></li>
                                    </c:when>
                                    <c:otherwise>
                                    <li class="disabled"><span>Siguiente</span></li>
                                    </c:otherwise>
                                </c:choose>
                        </ul>
                    </div>
                    <% } else{ %>
                    
                    <label><h3>No existen productos en esta categoría.</h3></label>
                    <% } %>
                </div>
                    
            </div>
        </div>

        <footer id="footer" class="vspace20">
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <p>&copy; Copyright 2013.&nbsp;</p>
                    </div>
                </div>
            </div>
        </footer>	
    </body>
</html>
