<%-- 
    Document   : ListarReclamos
    Created on : Oct 24, 2013, 5:02:26 PM
    Author     : tprog088
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="directmarket.servidor.DataReclamo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="head.jsp"></jsp:include>
    <jsp:include page="navbar.jsp"></jsp:include>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Listar Reclamos</title>
            <script>
                function obtenerNroRef() { $('#nroRef').attr('value', $("input:checked").parent().parent().next().next().next().text());}
                function anularBoton() { if ( $("input:checked").parent().parent().siblings().last().children().attr('class') === 'icon-ok' ) {
                            $('#btnAtender').attr('disabled', 'disabled');
                            $('#btnAtender').attr('href', '#foo');
                        } else {
                            $('#btnAtender').removeAttr("disabled");
                            $('#btnAtender').attr('href', '#myModal');
                        } 
                }
                $(document).ready(anularBoton);
            </script>
        </head>
        <body>
        <% List<DataReclamo> reclamos = (List<DataReclamo>) request.getAttribute("reclamos");%>
        <div class="container">
            <div class="row">
                <div class="span12">    
                    <form method="post" action="/proveedores/AtencionReclamo"> 
                        <h3>Reclamos recibidos</h3>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Cliente</th>
                                    <th>Producto</th>
                                    <th>Nro.&nbsp;Ref</th>
                                    <th>Reclamo</th>
                                    <th>Fecha</th>
                                    <th>Atendido</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%  int i = 0;
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                    for (DataReclamo dr : reclamos) {
                                        String fecha = sdf.format(dr.getFecha().toGregorianCalendar().getTime());%>
                                <tr>
                                    <td>
                                        <label class="radio">
                                            <%      if (i == 0) {%>
                                            <input type="radio" name="id" value="<%=dr.getId()%>" onclick="anularBoton();" checked>
                                            <%      } else {%>
                                            <input type="radio" name="id" value="<%=dr.getId()%>" onclick="anularBoton();">
                                            <%      }%>
                                        </label>
                                    </td>
                                    <td><%= dr.getCliente()%></td>
                                    <td><%= dr.getNombreProd()%></td>
                                    <td><%= dr.getNroRefProd()%></td>
                                    <td><%= dr.getTexto()%></td>
                                    <td><%= fecha%></td>
                                    <td class="text-center">
                                        <%  if (dr.isAtendido()) {%>
                                        <i class="icon-ok" style="color: green"></i>
                                        <%  } else { %>
                                            <i class="icon-remove" style="color: red"></i>
                                        <%  } %>
                                    </td>
                                </tr>
                                <% i++;
                                    }%>
                            </tbody>
                        </table>
                            
                        <!-- Button to trigger modal -->
                        <a id="btnAtender" href="#myModal" role="button" class="btn btn-primary pull-right" data-toggle="modal" onclick="obtenerNroRef()">Atender Reclamo</a>

                        <!-- Modal -->
                        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 id="myModalLabel">Ingrese su respuesta:</h4>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <input id="nroRef" type="hidden" name="refProdRec" value="-1">
                                    <textarea class="span5" name="respuesta" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button id="btnResponder" type="submit" class="btn btn-primary">Responder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
