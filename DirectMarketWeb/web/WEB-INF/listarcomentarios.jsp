<%-- 
    Document   : listarcomentarios
    Created on : 29-sep-2013, 18:48:33
    Author     : Guillermo
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!-- css especificos a los comentarios -->
<link rel="stylesheet" href="../css/comentarios.css" />


<script type="text/javascript">
    var idPadre = 0; // variable "global"
    var puedeComentar = false;
    //<c:if test="${puedeComentar}">
        puedeComentar = true;
    //</c:if>
        function listarComentarios() {
            $.get("/ListarComentarios", {NroRef: "${Ref}"}, function(data) {
                $("#listaComent").html(data);
                //$("#listaComent img").attr('data-src',"holder.js/140x140");
                $("#listaComent img").addClass('thumbnails80');
                if (puedeComentar) {
                    $("#listaComent input[type='button'].responder").addClass('btn btn-primary');
                    $("#listaComent input[type='button'].responder").attr('value', 'Responder');
                }
                else {
                    $("#listaComent input[type='button'].responder").addClass('hide');
                }
            });
        }

        function responder(id) {
            idPadre = id;
            $("#respuestaId").text("Respuesta al #" + id + ":");
            $("#btnNuevoCom").prop('disabled', false);
            $("#btnNuevoCom").addClass('btn-primary');
            $("#new_message").focus();
        }

        function nuevoCom() {
            idPadre = 0;
            $("#respuestaId").text("Nuevo Comentario:");
            $("#btnNuevoCom").prop('disabled', true);
            $("#btnNuevoCom").removeClass('btn-primary');
            $("#new_message").focus();
        }

        function mostrarMensajeComentario() {
            var html = '<div id="comentarioIngresado" class="alert alert-success">\n\
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
<strong>Comentario Ingresado.</strong>\n\
</div>';
            $('#comentAlerta').html(html);
        }

        function subirRespuesta() {
            if (!($("#new_message").val().trim())) {
                alert("Debe ingresar el texto del comentario");
                return;
            }
            entrada = {Tipo: idPadre > 0 ? "R" : "C", Texto: $("#new_message").val(), NroRef: "${Ref}", Padre: idPadre};
            $.post("/CrearComentario", entrada, function(data) {
                listarComentarios();
                nuevoCom();
                $("new_message").text("");
                mostrarMensajeComentario();
            });
        }
</script> 


<div style="width: 100%" id="listaComent">
    <script type="text/javascript"> listarComentarios();</script>
    <br />
</div>
<div class="span8">
    <c:if test="${puedeComentar}">
        <div id="comentAlerta"></div>
        <form accept-charset="UTF-8" action="" method="POST">
            <div class="inline">
                <h5 id='respuestaId' style="display: inline">Nuevo Comentario:</h5> 
                <input type="button" id="btnNuevoCom" class="btn " value="Es Un Nuevo Comentario" 
                       style="float: right;display: inline" disabled onclick="nuevoCom();"/>
            </div>
            <textarea class="span8" id="new_message" placeholder="Ingrese un comentario" rows="2"></textarea>
            <button class="btn btn-primary" type="submit" onclick="subirRespuesta();
            return false;">Comentar</button>
        </form>
    </c:if>
</div>