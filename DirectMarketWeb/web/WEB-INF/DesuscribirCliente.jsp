<%-- 
    Document   : DesuscribirCliente
    Created on : Nov 2, 2013, 11:19:04 AM
    Author     : tprog089
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Desuscribir Cliente</title> <!-- Se podría poner el nick del cliente que corresponda -->
        <jsp:include page="head.jsp"></jsp:include>

    <body>    
        
        <jsp:include page="navbar.jsp"></jsp:include>
        
        <form method="post" action="/clientes/editarsuscripciones">        
            <div class="container">            
                <h2>Editar suscripciones</h2><br>

                <h4>Deseo recibir mensajes de aviso de:</h4>
                <input type="checkbox" name="suscripciones" value="registroProducto" ${notif_pr}>
                        Registro de nuevo producto<br>

                    <input type="checkbox" name="suscripciones" value="nuevoComentario" ${notif_com}>
                        Nuevo comentario<br>

                    <input type="checkbox" name="suscripciones" value="cambioEstadoOrden" ${notif_ord}>
                        Cambio de estado de orden de compra<br>
                        
                    <input type="checkbox" name="suscripciones" value="cambioEstadoOrden" ${notif_rec}>
                        Atención de reclamos<br>   
                <br>
        
                <div>
                    <input type="submit" value="Aceptar" class="btn btn-primary"/>
                </div>            
            </div>
        </form>
    </body>
</html>
