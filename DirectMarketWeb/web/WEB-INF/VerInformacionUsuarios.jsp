<%-- 
    Document   : VerInfoProveedor
    Created on : Sep 12, 2013, 8:17:52 PM
    Author     : Fabian Gambetta
    Modified on : Sep 24, 2013, 7:17:21 PM
    Modified by : Gonzalo Miraballes
--%>

<%@page import="directmarket.servidor.DataEstadistica"%>
<%@page import="directmarket.servidor.DataProveedor"%>
<%@page import="directmarket.servidor.DataProductoBas"%>
<%@page import="directmarket.servidor.DataUsuario"%>
<%@page import="directmarket.servidor.DataInfoProveedor"%>
<%@page import="directmarket.servidor.TipoUsuario"%>
<%@page import="directmarket.web.logicaweb.Logos"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.util.List"%>
<%@page import="directmarket.servidor.DataInfoCliente"%>
<%@page import="directmarket.servidor.DataOrdenLin"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page errorPage="/WEB-INF/errorPages/500.jsp" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Información de Usuario</title>
        <jsp:include page="head.jsp"></jsp:include>


        <%
            DataUsuario dp = (DataUsuario) request.getAttribute("datos");
            List<DataOrdenLin> lineas = (List<DataOrdenLin>) request.getAttribute("datosOrdenes");
            List<DataProductoBas> productos = (List<DataProductoBas>) request.getAttribute("datosProductos");
            TipoUsuario tipo = (TipoUsuario) session.getAttribute("tipo");


        %>
        <% if (tipo == TipoUsuario.PROVEEDOR) {%>
        <!-- carga la api de google -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">

            // Load the Visualization API and the piechart package.
            google.load('visualization', '1.0', {'packages': ['corechart']});

            // Set a callback to run when the Google Visualization API is loaded.
            google.setOnLoadCallback(function(){hacerGraficas();});
            //google.setOnLoadCallback(graficaLinea);

        </script>
        <% }%>

    </head>
    <body>      
        <jsp:include page="navbar.jsp"></jsp:include>


            <div class = "container" > 
                <h3> Información de Usuario </h3>
                <br/>
                <div class = "row" >
                    <div class = "span2" >
                        <img id = "imagen" alt = "Imagen de Usuario" class = "img-rounded" src = "<%= Logos.mostrarAvatar(dp.getNombreImagen())%>" >
                </div>
                <div class = "span3" >
                    <blockquote >
                        <p id = "nick" ><%= dp.getNick()%> </p>
                        <small id = "nombre_completo" ><%= dp.getNombre() + " " + dp.getApellido()%> </small>
                    </blockquote>
                    <p >
                        <label id = "email" > <i class = "icon-envelope" > </i> <%= dp.getEmail()%> </label >
                        <label id = "fecha" > <i class = "icon-gift" > </i> <%= new SimpleDateFormat("dd/MM/yyyy").format(dp.getFecha().toGregorianCalendar().getTime())%></label >
                        <% HttpSession sesion = request.getSession();

                            if (sesion.getAttribute(
                                    "nick") != null) {
                                if (sesion.getAttribute("tipo").equals(TipoUsuario.CLIENTE)) {%>
                        <label id = "tipo" > <i class = "icon-user" > </i> Cliente</label >
                        <script type = "text/javascript" >
                            $(document).ready(function() {
                                document.getElementById("tablaOrdenes").style.display = "inline";
                            });
                        </script>
                        <%        } else {%>
                        <label id="tipo"><i class="icon-user"></i> Proveedor</label>
                        <script type="text/javascript">
                            $(document).ready(
                                    function()
                                    {
                                        document.getElementById("datos_empresa").style.display = "inline";
                                        document.getElementById("tablaProductos").style.display = "inline";
                                    }
                            );
                        </script>
                        <%  }%>

                    </p>
                    <p id="datos_empresa" style="display:none;">
                        <label>Datos de Empresa:</label>
                        <% if (request.getSession().getAttribute("tipo").equals(TipoUsuario.PROVEEDOR)) {
                                DataProveedor d = (DataProveedor) request.getAttribute("datos");%>
                        <label id="nombre"><i class="icon-home"></i><%=d.getCampania()%></label>
                        <label id="web"><i class="icon-globe"></i> <%=d.getWeb()%></label>  

                    </p>
                </div>  
                <div id="tablaProductos" style="display:none;" class="span6">
                    <h4>Mis Productos</h4>
                    <form>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Nro. Ref</th>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%   for (DataProductoBas dt : productos) {%>

                                <tr>
                                    <td><%= dt.getNroReferencia()%></td>
                                    <td><%= dt.getTitulo()%></td>
                                    <td><%=dt.getPrecio()%></td>
                                    <td><%=dt.getDescripcion()%></td>
                                </tr>
                                <% }
                                    }%>
                            </tbody>
                        </table>
                    </form>
                </div>
                <% }%>
                                                <% if (request.getSession()
                                            .getAttribute("tipo").equals(TipoUsuario.CLIENTE)) { %>
                <div id="tablaOrdenes" style="display:none;" class="span6">
                    <h4>Ordenes de Compra</h4>
                    <form method="get" action="/InfoOrden">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nro. Orden</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                        int i = 1;
                                        for (DataOrdenLin lin : lineas) {
                                %>        
                                <tr>
                                    <td>
                                        <label class="radio">
                                            <% if (i == 1) {%>        
                                            <input type="radio" name="optionsRadios" id="optionsRadios<%= i%>" value="<%= lin.getNroOrden()%>" checked>
                                            <% } else {%>
                                            <input type="radio" name="optionsRadios" id="optionsRadios<%= i%>" value="<%= lin.getNroOrden()%>">
                                            <% }%>
                                        </label>
                                    </td>
                                    <td><%= lin.getNroOrden()%></td>
                                    <td>$ <%= lin.getTotal()%></td>
                                </tr>
                                <% i++;%>
                                <% }%>  
                            </tbody>
                        </table> 
                        <% if (lineas.size() != 0) {%>
                        <input type="submit" class="btn btn-primary pull-right" value ="Ver Orden">
                        <% } else {%>
                        <input type="submit" class="btn btn-primary pull-right" value ="Ver Orden" disabled="disabled" disabled>
                        <% }
                            }%>
                    </form>

                </div>

            </div>
            
        </div>
                    <% if (tipo == TipoUsuario.PROVEEDOR) {%>

            <script type="text/javascript">
                /*function transpose(a) { // reemplaza columnas por filas
                   for (var i = 0; i < a.length; i++) {
                       for(var j = 0; j < i && j < a[i].length; j++){
                           var tmp = a[i][j];
                           a[i][j] = a[j][i];
                           a[j][i] = tmp;
                       }
                   }
                }*/
    
    function transpose(a)
{
return Object.keys(a[0]).map(function (c) { return a.map(function (r) { return r[c]; }); });
}

                function graficoTorta() {
                    // obtengo el array en JSON desde el controlador
                    var datos = google.visualization.arrayToDataTable(${statTotal});
                    var options = {title: 'Ventas Por Producto',pieHole: 0.4,chartArea:{ height:"auto",width:"auto",bottom:'auto',left:'auto' } };
                    var chart = new google.visualization.PieChart(document.getElementById('grTorta_div'));
                    chart.draw(datos, options);
                }


                function graficoLineas() {
                    // obtengo el array en JSON desde el controlador
                    var datos = ${statMes};
                    datos = transpose(datos);
                    // google charts me pide justamente transpuesto respecto a lo que me es conveniente
                    datosV = google.visualization.arrayToDataTable(datos);
                    var options = {title: 'Ventas Por Mes',hAxis : { slantedText: true, slantedTextAngle: 90 },chartArea:{left:"10%",width:"auto",height:"auto"}};
                    var chart = new google.visualization.LineChart(document.getElementById('grLinea_div'));
                    chart.draw(datosV, options);
                }

                function hacerGraficas() {
                    graficoTorta();
                    graficoLineas();
                }

            </script>
            <div class = "container" >
                <br /><br />
                <h4>Mis Estadisticas</h4>
                <div class="row">
                    <div id="graficas" class="span5">
                        <div  id="grTorta_div"  class="span5" style="padding: 0;margin: 0; height: 320px;" >
                            <!-- Grafica Torta Aqui -->
                        </div>
                    </div>
                    <div id="graficas" class="span7">
                        <div id="grLinea_div" class="span7" style="height: 320px; padding: 0;margin: 0;">
                            <!-- Grafica Lineas Aqui -->
                        </div>
                    </div>
                </div>
            </div>
            <% }%>
    </body>
</html>