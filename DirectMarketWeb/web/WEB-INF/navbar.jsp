
<%@page import="directmarket.servidor.TipoUsuario"%>

<div class="navbar navbar-inverse navbar-fixed-top" >
    
    <div class="navbar-inner">
        <div class="container" style="padding-top: 3px;padding-bottom: 3px">
            <a class="brand" href="/">DirectMarket</a>
            <div class="nav-collapse collapse">
                <div class="pull-left">
                
                </div>
                <div class="btn-group pull-right" style="margin-top: 0px">
 

                <% if (session.getAttribute("nick") == null) {%> 
                
                
                                    <script type="text/javascript">
                                        
$("#submit").click(function() {
        /*$.post("/loginAjax", $("#loginForm").serialize(), function(data) {
            if (data === "OK") {
                window.location = window.location.pathname;
            }
            else {
                alert(data);
               mostrarMensajeErrorLogin(data);
            }
        }
        );*/
        logueo();
    });
                    function mostrarMensajeErrorLogin(msg) {
                        var html = '<div id="errorLogin" class="alert alert-error">\n\
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
<strong>' + msg + '</strong>\n\
</div>';
                        $('#alertaLogin').html(html);
                    }
                    function logueo() {
                        var rem = $("#RememberMe").is(':checked') ? "1" : "0";
                        $.post("/loginAjax", {nick: $("#nick_login").val(),
                            pass: $("#pass_login").val(), remember: rem}, function(data) {
                            if (data !== "OK") {
                                //$("#errorLogin").html(data);
                                mostrarMensajeErrorLogin(data);
                            }
                            else {
                                window.location.reload();

                            }
                        });
                    }
                </script>
                <% }%>
                    <% HttpSession sesion = request.getSession();
                        if (sesion.getAttribute("nick") != null) {%> 
                    <a class="btn btn-primary" href="/usuarios/info"><i class="icon-user icon-white"></i> <%= sesion.getAttribute("nick")%></a>
                    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/usuarios/info"><i class="icon-pencil"></i>  Perfil</a></li>
                        

                        <% if (sesion.getAttribute("tipo").equals(TipoUsuario.CLIENTE)) {%>
                        <li class="divider"></li>
                        <li><a href="/InfoCarrito"><i class="icon-shopping-cart"></i> Carrito de Compras</a></li>
                        <li class="divider"></li>
                        <li><a href="/clientes/editarsuscripciones"><i class="icon-th-list"></i> Editar suscripciones</a></li>                        
                            <% }%>

                        <% if (sesion.getAttribute("tipo").equals(TipoUsuario.PROVEEDOR)) {%>
                        <li class="divider"></li>
                        <li><a href="/proveedores/RegistrarProducto"><i class="icon-plus"></i>  Registrar producto</a></li>
                        <li><a href="/proveedores/ListarReclamos"><i class="icon-thumbs-down"></i>  Reclamos</a></li>
                            <% }%>
                        <li class="divider"></li>
                        <li><a href="/logoff"><i class="icon-off"></i>  Sign Out</a></li>
                    </ul>
                    <% } else {%>
                    <!--<a class="btn btn-primary" href="/"><i class="icon-user icon-white"></i>  Visitante</a> -->
                    <form class="navbar-form form-search pull-right" style="margin-left: 5px;" action="" method="post" id="loginFormBar" >
                    <input type="text" id="nick_login" class="input-small" name="nick" style="margin: 3px" placeholder="Usuario">
                    <input type="password" id="pass_login" class="input-small" name="pass" style="margin: 3px" placeholder="Contraseņa">
                    <label class="checkbox active" style="margin: 3px; color: white">
                        <input type="checkbox" id="RememberMe" name="remember" value="1">&nbsp;Recordarme
                    </label>
                    <input type="button" style="margin: 3px" class="btn btn-success" id="submit" onclick="logueo();" value="Login">
                    <label><a href="/registrarusuario" class="btn btn-link" style="margin: 3px">Registrarse</a></label>

                </form>
                    <% }%>

                </div>

               
            </div>
            <form method="get" action="/BuscarProducto" id="custom-search-form" class="navbar-form form-search form-horizontal pull-right" style="padding-right:10px; ">
                <div class="input-append">
                    <input id="Search" name="search" type="text" placeholder="Search" class="input-medium search-query">
                    <button type="submit" class="btn"><i class="icon-search"></i></button>
                </div>
            </form>
        </div>
    </div>
<div id="alertaLogin" class="text-center"></div>
</div>
                    
