<!-- Included Bootstrap CSS Files -->
<link rel="stylesheet" href="../js/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="../js/bootstrap/css/bootstrap-responsive.min.css" />
<!--<link rel="stylesheet" href="../js/bootstrap/css/bootstrap.css" />-->
<link rel="stylesheet" href="../js/bootstrap-datetimepicker-0.0.11/css/bootstrap-datetimepicker.min.css" />

<!-- Includes FontAwesome -->
<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css" />

<!-- Css -->	
<link rel="stylesheet" href="../css/style.css" />

<script type="text/javascript" src="../js/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="../js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/holder.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.min.js"></script>
