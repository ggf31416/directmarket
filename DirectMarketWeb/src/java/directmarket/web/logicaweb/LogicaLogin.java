/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.logicaweb;

import directmarket.servidor.DataLogueo;
import directmarket.web.controladores.EstadoLogin;
import directmarket.servidor.DataUsuarioBas;
import directmarket.servidor.NoSeEncontroException_Exception;
import directmarket.servidor.PublUsu;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author guillermo
 */
public class LogicaLogin {

    public static void logoff(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            Integer idCon = (Integer) session.getAttribute("idCon");
            FabricaWS.wsUsuarios(session.getServletContext()).quitardeConectados(session.getAttribute("nick").toString(),idCon);
            session.invalidate();
        }
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                cookie.setValue("-");
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }

    }

    public static void fallar(HttpSession session) {
        session.setAttribute("nick", null);
        session.setAttribute("email", null);
        session.setAttribute("tipo", null);
        session.setAttribute("estadoLogin", EstadoLogin.LOGUEO_INCORRECTO);
        session.setAttribute("idCon", null);
    }

    public static void autenticar(String nick, String pass, HttpSession session) throws NoSeEncontroException_Exception {
        PublUsu usrCtrl = FabricaWS.wsUsuarios(session.getServletContext());
        DataLogueo datos = usrCtrl.autenticar(nick, pass);
        session.setAttribute("nick", datos.getNick());
        session.setAttribute("email", datos.getEmail());
        session.setAttribute("tipo", datos.getTipo());
        session.setAttribute("estadoLogin", EstadoLogin.LOGUEO_CORRECTO);
        session.setAttribute("idCon", datos.getIdConexion());
    }

    public static void autenticarRemember(String nick, String pass, HttpSession session, HttpServletResponse response, boolean remember) throws NoSeEncontroException_Exception {
        autenticar(nick, pass, session);
        if (remember) {
            // tremendamente inseguro. NO HACER ASI !!!
            Cookie c = new Cookie("tokensecreto", nick);
            c.setHttpOnly(true);
            c.setMaxAge(3600 * 24); // dura un dia
            c.setPath("/");
            // lo agrega a la respuesta
            response.addCookie(c);
        }
    }

    public static void checkearCookie(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Cookie[] cookies = request.getCookies();
        PublUsu pu = FabricaWS.wsUsuarios(request.getServletContext());
        if (cookies != null && cookies.length > 0) {
            for (Cookie c : cookies) {
                try {
                    if (c.getName().equals("tokensecreto")) {
                        DataUsuarioBas datos = pu.getInfoBas(c.getValue());
                        session.setAttribute("nick", datos.getNick());
                        session.setAttribute("email", datos.getEmail());
                        session.setAttribute("tipo", datos.getTipo());
                        session.setAttribute("estadoLogin", EstadoLogin.LOGUEO_CORRECTO);
                        int idCon = pu.agregaraConectados(datos.getNick());
                        session.setAttribute("idCon", idCon);
                    }
                } catch (Exception ex) {
                    // nick incorrecto, no big deal
                }
            }
        }
    }
}

