/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.logicaweb;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author guillermo
 */
public class Logos {
    
    public static String mostrarAvatar(String nombre){
                if(nombre == null || nombre.isEmpty()){
            return "/static/imagenes/sinavatar.jpg";
        }
        else{
            try {
                return "/imagenes/" + URLEncoder.encode(nombre, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        }
    
    }
    
    public static String mostrarLogo(String nombre){
        if(nombre == null || nombre.isEmpty()){
            return "/static/imagenes/sinfoto.png";
        }
        else{
            try {
                return "/imagenes/" + URLEncoder.encode(nombre, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
