/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.logicaweb;

import java.io.File;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author tprog087
 */
public class Subida {

    public static ServletFileUpload configurarSubida(HttpServlet serv) {
        DiskFileItemFactory factory = new DiskFileItemFactory();

        int maxMemoria = 1024 * 1024;
        int maxTamanio = 10 * 1024 * 1024;

        ServletContext servletContext = serv.getServletConfig().getServletContext();
        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");

        // Set factory constraints
        factory.setSizeThreshold(maxMemoria);
        factory.setRepository(repository);

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // Set overall request size constraint
        upload.setSizeMax(maxTamanio);
        return upload;
    }
}
