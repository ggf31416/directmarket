/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.servidor.DataCliente;
import directmarket.servidor.DataEstadProd;
import directmarket.servidor.DataEstadistica;
import directmarket.servidor.DataOrdenLin;
import directmarket.servidor.DataProductoBas;
import directmarket.servidor.DataProveedor;
import directmarket.servidor.PublOrden;
import directmarket.servidor.PublOrdenService;
import directmarket.servidor.PublUsu;
import directmarket.servidor.PublUsuService;
import directmarket.servidor.TipoUsuario;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import org.json.simple.JSONArray;


/**
 *
 * @author tprog086
 */
@WebServlet(name = "VerInfoUsuario",urlPatterns = {"/usuarios/info"})
public class VerInformacionUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            PublUsu port = FabricaWS.wsUsuarios(this);
            PublOrden po = FabricaWS.wsOrdenes(this);
            
            HttpSession sesion = request.getSession();
            
            String nick = (String) sesion.getAttribute("nick");
            TipoUsuario tipo = (TipoUsuario) sesion.getAttribute("tipo");
            if(tipo == TipoUsuario.CLIENTE){
                DataCliente cli = port.verInformacionCliente(nick);
                request.setAttribute("datos", cli);
                List<Integer> ordenes = cli.getOrdenes();
                List<DataOrdenLin> lineas = new ArrayList<DataOrdenLin>();
                for (Integer o : ordenes) {
                    lineas.add(po.informacionOrden(o));
                }
                request.setAttribute("datosOrdenes", lineas);
                request.getRequestDispatcher("/WEB-INF/VerInformacionUsuarios.jsp").forward(request, response);
            }
            else{
                DataProveedor prov = port.verInformacionProveedor(nick);
                request.setAttribute("datos", prov);
                List<DataProductoBas> productos = prov.getProductos();
                request.setAttribute("datosProductos", productos);
                DataEstadistica stat = prov.getEstadisticas();

                // Creo las tablas javascript con json para la grafica por mes
                JSONArray arrayPorMes = new JSONArray();
                JSONArray cabeceraMes = new JSONArray();
                cabeceraMes.add("Mes");
                for(XMLGregorianCalendar c : stat.getMeses()){
                    Date d = c.toGregorianCalendar().getTime();
                    DateFormat df = new SimpleDateFormat("MMM-YY");
                    cabeceraMes.add(df.format(d));
                }
                arrayPorMes.add(cabeceraMes);
                for(DataEstadProd dep : stat.getEstadProductos()){
                    JSONArray datosProd = new JSONArray();
                    datosProd.add(dep.getTitulo());
                    for(Double d : dep.getPorMes()){
                        datosProd.add(d);
                    }
                    arrayPorMes.add(datosProd);
                }

                
                // Creo la tabla para el grafico de torta
                JSONArray arrayTotal = new JSONArray();
                JSONArray cabecera = new JSONArray();
                cabecera.add("Producto");
                cabecera.add("Total");
                arrayTotal.add(cabecera);
                for(DataEstadProd dep : stat.getEstadProductos()){
                    JSONArray datosProd = new JSONArray();
                    datosProd.add(dep.getTitulo());
                    datosProd.add(dep.getTotalVentas());
                    arrayTotal.add(datosProd);
                }
                 
                // mando los datos
                String jsStatMes = arrayPorMes.toJSONString();
                request.setAttribute("statMes", jsStatMes);
                
                String jsStatTotal = arrayTotal.toJSONString();
                request.setAttribute("statTotal", jsStatTotal);
                
                request.getRequestDispatcher("/WEB-INF/VerInformacionUsuarios.jsp").forward(request, response);
            }
            
        } catch (Exception ex) {            
            // mandar error
        }finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
