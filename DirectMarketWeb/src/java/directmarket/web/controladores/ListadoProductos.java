/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;


import directmarket.servidor.DataProductoBas;
import directmarket.servidor.NoSeEncontroException_Exception;
import directmarket.servidor.PublProd;
import directmarket.servidor.PublProdService;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tprog088
 */
@WebServlet(name = "ListadoProductos", urlPatterns = {"/listadoproductos"})
public class ListadoProductos extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        PublProd pp = FabricaWS.wsProductos(this);
        try {
            List<DataProductoBas> lista;

            Integer categoria = 0;
            String catNombre = null;
            try {
                categoria = Integer.valueOf(request.getParameter("Categoria"));
                
            } catch (NumberFormatException ex) {
            }
            catNombre = request.getParameter("catnombre");
            if (categoria != 0) {
                lista = pp.productosPorCategoria(categoria).getItem();
                request.setAttribute("catActual", categoria);
            } else if (catNombre != null){
                lista = pp.productosPorCategoriaNombre(catNombre).getItem();
            }
            else {
                lista = pp.listarProductos().getItem(); // lista todos
            }
            request.setAttribute("origen", "listadoproductos");
            // paginado
            int pagina = 1;
            if (request.getParameter("pagina") != null) // es la primera vez
            {
                pagina = Integer.parseInt(request.getParameter("pagina").toString());
            }
            int min = (pagina - 1) * 8;
            int max = Math.min(lista.size(), min + 8);
            request.setAttribute("listaproductos", lista.subList(min, max));
            request.setAttribute("cantpaginas", (int) Math.ceil(lista.size() / 8.0));
            request.setAttribute("paginaactual", pagina);
            new InfoCarrito().obtenerDatosCarrito(request, response); // obtengo datos del carrito (lo hace si es cliente)
            request.getRequestDispatcher("/WEB-INF/ListadoProductos.jsp").forward(request, response);
        } catch (NoSeEncontroException_Exception ex) {
            response.sendError(404, ex.getMessage());

        } finally {
            out.close();
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
