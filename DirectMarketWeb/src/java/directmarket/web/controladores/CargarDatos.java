/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.servidor.DirectMarketException_Exception;
import directmarket.servidor.PublGeneral;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author guillermo
 */
@WebServlet(name = "CargarDatos", urlPatterns = {"/cargardatos"})
public class CargarDatos extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        PublGeneral pg = FabricaWS.wsGeneral(this);
        cargarDatos(response);
        request.getRequestDispatcher("/").forward(request, response);
    }
    
    public void cargarDatos(ServletResponse response) throws IOException{
                    PublGeneral pg = FabricaWS.wsGeneral(this);
            try {
                pg.cargarDatosPrueba();
            }  catch (DirectMarketException_Exception ex1) {
                    ((HttpServletResponse)response).sendError(500, ex1.toString());
                }

            
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
