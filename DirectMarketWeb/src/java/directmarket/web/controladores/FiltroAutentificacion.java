/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;


import directmarket.web.logicaweb.LogicaLogin;
import directmarket.servidor.TipoUsuario;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author guillermo
 */
@WebFilter(filterName = "FiltroAutentificacion", urlPatterns = {"/","/publico/*","/guest/*", "/clientes/*", "/proveedores/*", "/usuarios/*"})
public class FiltroAutentificacion implements Filter {

    private static final boolean debug = true;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public FiltroAutentificacion() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        /*if (debug) {
            log("FiltroAutentificacion:doFilter()");
        }*/

        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession sesion = req.getSession();
        /*
         if (req.getRequestURI().startsWith("/public")) { // todos pueden acceder
         continuar(request, response, chain);
         } else {*/
        String nick = (String) sesion.getAttribute("nick");
        TipoUsuario tipo = (TipoUsuario) sesion.getAttribute("tipo");
        // comprueba cookie
        if(req.getSession().getAttribute("nick") == null){
            LogicaLogin.checkearCookie(req);
        }
        
        if (req.getServletPath().startsWith("/guest")) { // solo pueden acceder usuarios no logueados
            if (nick == null) {
                continuar(request, response, chain);
            } else {
                req.setAttribute("error","Debe cerrar la sesión.");
                req.getRequestDispatcher("/").forward(request, response);
            }
        } else if (req.getServletPath().startsWith("/usuarios")) { // clientes o proveedores
            if (nick != null) {
                continuar(request, response, chain);
            } else {
                mandarLogin("Debe loguearse para acceder a esta información.", req, response);
            }
        } else if (req.getServletPath().startsWith("/clientes")) { // solo clientes
            if (nick != null && tipo == TipoUsuario.CLIENTE) {
                continuar(request, response, chain);
            } else {
                mandarLogin("El área es exclusiva para clientes.", req, response);
            }
        } else if (req.getServletPath().startsWith("/proveedores")) { // solo proveedores

            if (nick != null && tipo == TipoUsuario.PROVEEDOR) {
                continuar(request, response, chain);
            } else {
                mandarLogin("El área es exclusiva para proveedores.", req, response);
            }
       }
        else{
            continuar(request, response, chain);
        }
    }

    private void mandarLogin(String msg, HttpServletRequest request, ServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("error", msg);
        request.getSession().setAttribute("redir", request.getServletPath()+(request.getPathInfo() == null ? "" : request.getPathInfo() ));
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/Login.jsp");
        dispatcher.forward(request, response);

    }

    private void continuar(ServletRequest request, ServletResponse response,
            FilterChain chain) {

        Throwable problem = null;
        try {
            // sigue los siguientes filtros (si los hubiera) y despues al jsp
            chain.doFilter(request, response);
        } catch (Throwable t) {
            problem = t;
            t.printStackTrace();
        }

    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("FiltroAutentificacion:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("FiltroAutentificacion()");
        }
        StringBuffer sb = new StringBuffer("FiltroAutentificacion(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);

        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
                pw.print(stackTrace);
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
}
