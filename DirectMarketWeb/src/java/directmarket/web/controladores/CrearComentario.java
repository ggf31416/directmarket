/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;


import directmarket.servidor.PublComent;
import directmarket.servidor.PublComentService;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileUploadException;

/**
 *
 * @author tprog086
 */
@WebServlet(name = "CrearComentario", urlPatterns = {"/CrearComentario"})
public class CrearComentario extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileUploadException, ParseException {

        String TipoComentario = request.getParameter("Tipo");
        PrintWriter out = response.getWriter();
        try {
            
            PublComent port = FabricaWS.wsComentarios(this);
            //IComentarios Interface_Comentarios = FabricaControladores.getComentariosInstance();

                response.setContentType("text/html;charset=UTF-8");

                String comentario = request.getParameter("Texto");
                String NickCreador = (String) request.getSession().getAttribute("nick");//request.getParameter("NickCreador");
                //SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
                //Date Fecha = formatoDeFecha.parse(request.getParameter("Fecha"));
                int NroRef = Integer.parseInt(request.getParameter("NroRef"));
                Integer idAuto;
                if (TipoComentario.equalsIgnoreCase("C")) {
                    idAuto =port.agregarComentario(comentario, NickCreador, NroRef);
                } else {
                    int idPadre = Integer.parseInt(request.getParameter("Padre"));
                    idAuto = port.agregarRespuesta(comentario, NickCreador, NroRef, idPadre);
                }
                out.write(idAuto.toString());
            }
         finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FileUploadException ex) {
            Logger.getLogger(CrearComentario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(CrearComentario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FileUploadException ex) {
            Logger.getLogger(CrearComentario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(CrearComentario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
