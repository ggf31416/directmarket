/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.servidor.DataOrdenLin;
import directmarket.servidor.PublOrden;
import directmarket.servidor.TipoUsuario;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tprog089
 */
@WebServlet(name = "InfoCarrito", urlPatterns = {"/InfoCarrito"})
public class InfoCarrito extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {

            //ManejadorNegocios manejadorNeg = ManejadorNegocios.getInstance();
            HttpSession sesion = request.getSession();
            PublOrden ordenes = FabricaWS.wsOrdenes(this);
            String nickUsuarioLogueado = (String) sesion.getAttribute("nick");
            TipoUsuario tipoUsuarioLogueado = (TipoUsuario) sesion.getAttribute("tipo");
            if (tipoUsuarioLogueado == TipoUsuario.CLIENTE) {
                DataOrdenLin dataCarrito = ordenes.getCarrito(nickUsuarioLogueado);
                request.setAttribute("carrito", dataCarrito);

            }
            if (tipoUsuarioLogueado == TipoUsuario.CLIENTE) {
                request.getRequestDispatcher("/WEB-INF/VerInfoCarrito.jsp").forward(request, response);
            }
        } finally {
            out.close();

        }
    }

    public void obtenerDatosCarrito(HttpServletRequest request, HttpServletResponse response) {
        HttpSession sesion = request.getSession();
        PublOrden ordenes = FabricaWS.wsOrdenes(request.getServletContext());
        String nickUsuarioLogueado = (String) sesion.getAttribute("nick");
        TipoUsuario tipoUsuarioLogueado = (TipoUsuario) sesion.getAttribute("tipo");
        if (tipoUsuarioLogueado == TipoUsuario.CLIENTE) {
            DataOrdenLin dataCarrito = ordenes.getCarrito(nickUsuarioLogueado);
            request.setAttribute("carrito", dataCarrito);

        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
