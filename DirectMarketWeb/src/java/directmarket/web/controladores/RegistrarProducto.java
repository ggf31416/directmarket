/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;


import directmarket.web.logicaweb.Subida;
import directmarket.servidor.DataImagen;
import directmarket.servidor.DataImagenArray;
import directmarket.servidor.PublProd;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.java.dev.jaxb.array.StringArray;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author tprog087
 */
@WebServlet(name = "RegistrarProducto", urlPatterns = {"/proveedores/RegistrarProducto","/existeRef","/dispTitulo"})
public class RegistrarProducto extends HttpServlet {

   
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        PublProd pp = FabricaWS.wsProductos(this);
        if (request.getServletPath().contains("existeRef")){
            
            Integer ref = Integer.valueOf(request.getParameter("ref"));
            
            boolean yaExiste = pp.existeProducto(ref);
            out.write(yaExiste ? "Ex" : "OK" );
            
        }
        else if (request.getServletPath().contains("dispTitulo")){
            
            String tit = request.getParameter("titulo").toString();
            
            boolean disponible = pp.tituloDisponible(tit);
            out.write(disponible ? "OK" : "Ex" );
            
        }
        else{
            request.getRequestDispatcher("/WEB-INF/RegistrarProducto.jsp").forward(request, response);
        }
        out.close();
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            PublProd pp = FabricaWS.wsProductos(this);
            request.setCharacterEncoding("UTF-8");
            if (!request.getRequestURI().contains("ajax")) { // request comun
                response.setContentType("text/html;charset=UTF-8");


                String titulo, ref, precio, desc, espec;
                titulo = ref = desc = espec = precio = "";
                int nro_ref = 0;
                double precio_num = 0;
                 // ordena por el name
                TreeMap<String,DataImagen> imagenes = new TreeMap<String, DataImagen>();
                List<String> cats = new ArrayList<String>();
                // Create a factory for disk-based file items

                ServletFileUpload upload = Subida.configurarSubida(this);
                // Parse the request
                List<FileItem> items = upload.parseRequest(request);

                // Process the uploaded items
                Iterator<FileItem> iter = items.iterator();
                DataImagen img = null;
                while (iter.hasNext()) {
                    FileItem item = iter.next();

                    if (item.isFormField()) {
                        if (item.getFieldName().equals("titulo")) {
                            titulo = item.getString();
                        } else if (item.getFieldName().equals("desc")) {
                            desc = item.getString();
                        } else if (item.getFieldName().equals("esp")) {
                            espec =item.getString();
                        } else if (item.getFieldName().equals("precio")) {
                            precio = item.getString();
                        } else if (item.getFieldName().equals("nro_ref")) {
                            ref = item.getString();
                            
                        } else if (item.getFieldName().matches("cata[\\d]+")){
                             // categorias al mejor estilo PHP de los 90'
                            String nombreCat = item.getString();
                            cats.add(nombreCat);
                            
                        }
                        
                    } else {
                        // por el momento no usa un stream sino memoria
                        //if (item.getFieldName().equals("imagen")){ //no necesario pues hay una sola imagen
                        if (item.get().length > 4){
                            img = new DataImagen();//FabricaDataImagen.desdeArray(item.get(), item.getName());
                            img.setDatos(item.get());
                            img.setNombre(item.getName());
                            imagenes.put(item.getFieldName(), img);
                        }
                    }
                    
                }
                HttpSession sesion = request.getSession();
                String nick = (String) sesion.getAttribute("nick");
                precio_num = Float.parseFloat(precio);
                nro_ref = Integer.parseInt(ref);
                DataImagenArray listaImagenes = new DataImagenArray();
                listaImagenes.getItem().addAll(imagenes.values());
                
                StringArray sa = new StringArray();
                sa.getItem().addAll(cats);
                pp.altaProducto(titulo, nro_ref, desc, espec, precio_num, nick ,sa,listaImagenes, 0);
                response.sendRedirect("/publico/infoproducto?ref=" + ref);
                //request.getRequestDispatcher().forward(request, response);

            }



        } catch (Exception ex) {
            response.sendError(500, ex.getMessage());
        } finally {
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
