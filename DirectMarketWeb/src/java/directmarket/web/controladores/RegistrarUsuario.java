/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.web.logicaweb.LogicaLogin;
import directmarket.web.logicaweb.Subida;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import directmarket.servidor.*;
import directmarket.web.logicaweb.FabricaWS;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author guillermo
 */
@WebServlet(urlPatterns = {"/registrarusuario", "/verificarusuarioajax"})
public class RegistrarUsuario extends HttpServlet {

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/RegistrarUsuario.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        PublUsu port = FabricaWS.wsUsuarios(this);

        if (!request.getRequestURI().contains("ajax")) { // request comun
            try {
                response.setContentType("text/html;charset=UTF-8");
                String nick, nombre, apellido, pass, nacim, email, web, compania, tipo;
                nick = nombre = apellido = pass = nacim = email = web = compania = tipo = "";
                // Create a factory for disk-based file items

                ServletFileUpload upload = Subida.configurarSubida(this);
                // Parse the request
                List<FileItem> items = upload.parseRequest(request);
                directmarket.servidor.DataImagen imgx = new directmarket.servidor.DataImagen();
                // Process the uploaded items
                Iterator<FileItem> iter = items.iterator();
                DataImagen img = null;

                while (iter.hasNext()) {
                    FileItem item = iter.next();

                    if (item.isFormField()) {
                        if (item.getFieldName().equals("nick")) {
                            nick = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("email")) {
                            email = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("nombre")) {
                            nombre = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("apellido")) {
                            apellido = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("pass")) {
                            pass = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("nacimiento")) {
                            nacim = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("tipo")) {
                            tipo = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("web")) {
                            web = item.getString("UTF-8");
                        } else if (item.getFieldName().equals("compania")) {
                            compania = item.getString("UTF-8");
                        }
                    } else {
                        // por el momento no usa un stream sino memoria
                        //if (item.getFieldName().equals("imagen")){ //no necesario pues hay una sola imagen
                        if (item.get().length > 4) {
                            //img = FabricaDataImagen.desdeArray(item.get(), item.getName());
                            imgx.setDatos(item.get());
                            imgx.setNombre(item.getName());
                        }
                    }
                }
                Date fecha = new SimpleDateFormat("dd/MM/yyyy").parse(nacim);
                GregorianCalendar cfecha = new GregorianCalendar();
                cfecha.setTime(fecha);
                XMLGregorianCalendar xfecha = DatatypeFactory.newInstance().newXMLGregorianCalendar(cfecha);
                if (tipo.equals("c")) {

                    port.altaUsuario(nick, email, nombre, apellido, xfecha, imgx, pass);

                } else {
                    port.altaProveedor(nick, email, nombre, apellido, xfecha, compania, web, imgx, pass);
                }
                LogicaLogin.autenticar(nick, pass, request.getSession());
                // hacer respuesta 
                response.sendRedirect("/usuarios/info");
                //request.getRequestDispatcher("/usuarios/info").forward(request, response);
            } catch (Exception ex) {
                request.setAttribute("errores", "Error:" + ex.getMessage());
                //out.write("<script>history.go(-1);</script>");
                //request.getRequestDispatcher("/WEB-INF/RegistrarUsuario.jsp").forward(request, response);
                response.sendError(500, ex.toString()); // comprueba todo con javascript
            } finally {
                out.close();
            }
        } else { // ajax
            try {
                if (request.getParameter("nick") != null) {
                    String nick = (String) request.getParameter("nick");
                    if (port.validarNick(nick)) {
                        out.write("OK");
                    } else {
                        out.write("Usado");
                    }
                } else if (request.getParameter("email") != null) {
                    // chequear unicidad email
                    try {
                        String email = request.getParameter("email");
                        port.mailValido(email);
                        out.write("OK");
                    } catch (NegocioException_Exception e) {
                        out.write("Inv");
                    } catch (YaExisteException_Exception e) {
                        out.write("YaExiste");
                    }
                }
            } catch (Exception ex) {
                response.sendError(500, ex.getMessage());
            } finally {
                out.close();
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
