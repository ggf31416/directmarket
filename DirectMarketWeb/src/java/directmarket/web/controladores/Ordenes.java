/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.servidor.DataOrdenLin;
import directmarket.servidor.PublOrden;
import directmarket.servidor.TipoEstado;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tprog088
 */
@WebServlet(name = "Ordenes", urlPatterns = {"/OrdenConfirma","/Ordenes","/ConfirmarOrden", "/InfoOrden", "/CancelarOrden"})
public class Ordenes extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        PublOrden po = FabricaWS.wsOrdenes(this);
        try {
            if (request.getRequestURI().contains("/CancelarOrden")) {
                String nick = request.getSession().getAttribute("nick").toString();
                po.borrarCarrito(nick);
              request.getRequestDispatcher("/listadoproductos").forward(request, response);
            }
            if (request.getRequestURI().contains("/InfoOrden")) {
                int nroOrden = Integer.valueOf(request.getParameter("optionsRadios"));
                if (request.getParameter("descargar")!= null)
                    request.setAttribute("descargar","si");
                DataOrdenLin dataOrden = po.informacionOrden(nroOrden);
                request.setAttribute("dataorden", dataOrden);
                request.setAttribute("estaPreparada", dataOrden.getEstados().get(dataOrden.getEstados().size() - 1).getEstado() == TipoEstado.PREPARADA);
                
                request.getRequestDispatcher("/WEB-INF/VerInfoOrden.jsp").forward(request, response);
            }
            if (request.getRequestURI().contains("Confirmar")) {
                po.confirmarOrden(request.getSession().getAttribute("nick").toString());
                request.getRequestDispatcher("/usuarios/info").forward(request, response);
            }
             if (request.getRequestURI().contains("/OrdenConfirma")) {
                 po.ordenConfirmada(Integer.valueOf(request.getParameter("nroOrden")));
                 out.write("OK");
             }
            } catch (Exception ex) {
             response.sendError(500, ex.getMessage());
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
