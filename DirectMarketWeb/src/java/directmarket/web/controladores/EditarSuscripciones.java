/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.servidor.DataInfoCliente;
import directmarket.servidor.NoSeEncontroException_Exception;
import directmarket.servidor.PublUsu;
import directmarket.servidor.TipoNotificacion;
import java.io.IOException;
import directmarket.servidor.TipoNotificacionArray;
import directmarket.web.logicaweb.FabricaWS;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tprog089
 */
@WebServlet(name = "EditarSuscripciones", urlPatterns = {"/clientes/editarsuscripciones"})
public class EditarSuscripciones extends HttpServlet {

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String nick = (String) request.getSession().getAttribute("nick");
            DataInfoCliente info = FabricaWS.wsUsuarios(this).verInfoCliente(nick);
            List<TipoNotificacion> des = info.getDesuscriptos();

            request.setAttribute("notif_pr", des.contains(TipoNotificacion.REGISTRO_PRODUCTO) ? "" : "checked");
            request.setAttribute("notif_com", des.contains(TipoNotificacion.NUEVO_COMENTARIO) ? "" : "checked");
            request.setAttribute("notif_ord", des.contains(TipoNotificacion.CAMBIO_ESTADO_ORDEN_DE_COMPRA) ? "" : "checked");
            request.setAttribute("notif_rec", des.contains(TipoNotificacion.ATENCION_RECLAMO) ? "" : "checked");

            request.getRequestDispatcher("/WEB-INF/DesuscribirCliente.jsp").forward(request, response);
        } catch (NoSeEncontroException_Exception ex) {
            response.sendError(500, ex.toString());
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        PublUsu port = FabricaWS.wsUsuarios(this);
        try {
            // Obtengo el cliente
            HttpSession session = request.getSession();
            String nick = (String) session.getAttribute("nick");

            String s = request.getParameter("suscripciones");

            List<String> suscripciones = new ArrayList<String>();
            if (s != null) {
                suscripciones = Arrays.asList(request.getParameterValues("suscripciones"));
            }
            ArrayList<TipoNotificacion> excluidas = new ArrayList();
            if (!suscripciones.contains("registroProducto")) {
                excluidas.add(TipoNotificacion.REGISTRO_PRODUCTO);
            }
            if (!suscripciones.contains("nuevoComentario")) {
                excluidas.add(TipoNotificacion.NUEVO_COMENTARIO);
            }
            if (!suscripciones.contains("cambioEstadoOrden")) {
                excluidas.add(TipoNotificacion.CAMBIO_ESTADO_ORDEN_DE_COMPRA);
            }
            if (!suscripciones.contains("AtencionReclamo")) {
                excluidas.add(TipoNotificacion.ATENCION_RECLAMO);
            }

            TipoNotificacionArray tna = new TipoNotificacionArray();
            tna.getItem().addAll(excluidas);
            port.cambiarSuscripcionesUsuario(nick, tna);
            response.sendRedirect("/");
        }
    
    catch (Exception ex ) {
            Logger.getLogger(EditarSuscripciones.class.getName()).log(Level.SEVERE, null, ex);
    }
}
/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
@Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
