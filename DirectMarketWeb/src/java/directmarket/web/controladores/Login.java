/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import directmarket.web.logicaweb.LogicaLogin;
import directmarket.servidor.NoSeEncontroException;
import directmarket.servidor.NoSeEncontroException_Exception;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;

/**
 *
 * @author guillermo
 */
@WebServlet(name = "Login", urlPatterns = {"/login", "/loginAjax", "/logoff"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession();
        String nick = request.getParameter("nick");
        String pass = request.getParameter("pass");
        boolean remember = "1".equals(request.getParameter("remember"));
        PrintWriter out = response.getWriter();
        boolean ajax = request.getServletPath().contains("Ajax");
        try {
            if (request.getServletPath().contains("logoff")) {
                LogicaLogin.logoff(request, response);
                response.sendRedirect("/");
            } else {
                LogicaLogin.autenticarRemember(nick, pass, sesion, response, remember);
                if (ajax) {
                    out.write("OK");

                } else {
                    String redireccionar = "/";
                    if (sesion.getAttribute("redir") != null) {
                        redireccionar = sesion.getAttribute("redir").toString();
                    }
                    request.getRequestDispatcher(redireccionar).forward(request, response);
                }
            }
        } catch (NoSeEncontroException_Exception ex) {
            LogicaLogin.fallar(sesion);
            if (ajax) {
                out.write("<font color=\"ff0000\"><b>Login failed</b>: "+ex.getMessage()+"</font>");
                //out.write("Login failed: "+ex.getMessage());
            } else {
                RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
                request.setAttribute("error", ex.toString());
                dispatcher.forward(request, response);
            }

        } finally {
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
