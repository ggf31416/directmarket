/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.servidor.DataReclamo;
import directmarket.servidor.PublProd;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tprog088
 */
@WebServlet(name = "ListarReclamos", urlPatterns = {"/proveedores/ListarReclamos", "/proveedores/IngresarReclamo", "/proveedores/AtencionReclamo"})
public class Reclamos extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        PublProd port = FabricaWS.wsProductos(this);
        try {
            if (request.getRequestURI().contains("/Listar")) {
                String nick = (String) request.getSession().getAttribute("nick");
                List<DataReclamo> reclamos = port.listarReclamos(nick).getItem();
                request.setAttribute("reclamos", reclamos);
                request.getRequestDispatcher("/WEB-INF/ListarReclamos.jsp").forward(request, response);
            }
        }finally {
            out.close();
        }
    }

    
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PublProd port = FabricaWS.wsProductos(this);
        
        if (request.getRequestURI().contains("/AtencionReclamo")) {
            int id = Integer.valueOf(request.getParameter("id"));
            String respuesta = String.valueOf(request.getParameter("respuesta"));
            int nroRef = Integer.valueOf(request.getParameter("refProdRec"));

            FabricaWS.wsProductos(this).atenderReclamo(id, respuesta, nroRef);
            request.setAttribute("id", id);
            response.sendRedirect("/ListarReclamos");
        }else{
            String msj = request.getParameter("Mensaje");
            int ref = Integer.valueOf(request.getParameter("Ref"));
            port.agregarReclamo((String) request.getSession().getAttribute("nick"), msj, ref);
            request.setAttribute("ingresado", "true");
            request.getRequestDispatcher("/publico/infoproducto?ref=" + ref).forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
