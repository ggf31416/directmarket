/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;


import directmarket.servidor.DataProductoBas;
import directmarket.servidor.PublProd;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tprog090
 */
@WebServlet(name = "BuscarProducto", urlPatterns = {"/BuscarProducto", "/OrdenarBusqueda"})
public class BuscarProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            
            String valorBusqueda = (String) request.getParameter("search");
            PublProd productos = FabricaWS.wsProductos(this);
            List<DataProductoBas> lista;
            if (request.getServletPath().contains("BuscarProducto") || request.getSession().getAttribute("resultadoBusqueda") == null) {
                lista = productos.buscarProductos(valorBusqueda).getItem();
                request.getSession().setAttribute("resultadoBusqueda", lista);
            } else {
                lista = ordenar(request);
            }
            int pagina = 1;
            if (request.getParameter("pagina") != null) // es la primera vez
            {
                pagina = Integer.parseInt(request.getParameter("pagina").toString());
            }
            request.setAttribute("totalproductos", lista.size());
            int min = (pagina - 1) * 8;
            int max = Math.min(lista.size() , min + 8);
            List<DataProductoBas> sublista = lista.subList(min, max);
            request.setAttribute("listaproductos", sublista);
            request.setAttribute("cantpaginas", (int)Math.ceil(lista.size()/8.0));
            request.setAttribute("paginaactual", pagina);
            request.setAttribute("c",request.getParameter("c"));
            request.setAttribute("o",request.getParameter("o"));
            request.setAttribute("search",request.getParameter("search"));
            request.getRequestDispatcher("/WEB-INF/BuscarProducto.jsp").forward(request, response);
        } finally {
            out.close();
        }
    }

    /**
     * Parametros c : pre -> precio, com -> compras, sino alfabetico o: desc ->
     * descendente, sino ascendente
     *
     * @param request
     * @return
     */
    private List<DataProductoBas> ordenar(HttpServletRequest request) {
        //ej FiltroProducto?c=pre&o=asc
        String criterio = (String) request.getParameter("c");
        String orden = (String) request.getParameter("o");

        HttpSession session = request.getSession();
        // asume que ya esta en la sesión
        List<DataProductoBas> lista = (List<DataProductoBas>) session.getAttribute("resultadoBusqueda");
        boolean esDescendente = false;
        if (orden.equalsIgnoreCase("desc")) {
            esDescendente = true;
        }
        if (criterio.equalsIgnoreCase("pre")) {
            ordenar(lista, OrdenFiltro.PRECIO, esDescendente);
        } else if (criterio.equalsIgnoreCase("com")) {
            ordenar(lista, OrdenFiltro.COMPRAS, esDescendente);
        } else {
            ordenar(lista, OrdenFiltro.ALFABETICO, esDescendente);
        }
        return lista;

    }

    enum OrdenFiltro {

        ALFABETICO, PRECIO, COMPRAS
    }

    private void ordenar(List<DataProductoBas> lista, OrdenFiltro orden, boolean descendente) {
        Comparator porTitulo = new Comparator<DataProductoBas>() {
            @Override
            public int compare(DataProductoBas t, DataProductoBas t1) {
                return t.getTitulo().toLowerCase().compareTo(t1.getTitulo().toLowerCase());
            }
        };
        Comparator porPrecio = new Comparator<DataProductoBas>() {
            @Override
            public int compare(DataProductoBas t, DataProductoBas t1) {
                return Double.compare(t.getPrecio(), t1.getPrecio());
            }
        };
        Comparator porCompras = new Comparator<DataProductoBas>() {
            @Override
            public int compare(DataProductoBas t, DataProductoBas t1) {
                return Integer.compare(t.getCantComprada(), t1.getCantComprada());
            }
        };
        if (orden == OrdenFiltro.ALFABETICO) {
            if (descendente) {
                Collections.sort(lista, Collections.reverseOrder(porTitulo));
            } else {
                Collections.sort(lista, porTitulo);
            }
        } else if (orden == OrdenFiltro.PRECIO) {
            if (descendente) {
                Collections.sort(lista, Collections.reverseOrder(porPrecio));
            } else {
                Collections.sort(lista, porPrecio);
            }
        } else {
            if (descendente) {
                Collections.sort(lista, Collections.reverseOrder(porCompras));
            } else {
                Collections.sort(lista, porCompras);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
