/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;


import directmarket.servidor.DataProducto;
import directmarket.servidor.DataReclamo;
import directmarket.servidor.PublComent;
import directmarket.servidor.PublOrden;
import directmarket.servidor.PublProd;
import directmarket.servidor.TipoUsuario;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tprog088
 */
@WebServlet(name = "VerinfoProducto", urlPatterns = {"/publico/infoproducto"})
public class VerinfoProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        // hay que mejorar para que no sea necesario llamar 3 controladores
        PublProd pp = FabricaWS.wsProductos(this);
        PublOrden po = FabricaWS.wsOrdenes(this);
        PublComent pc = FabricaWS.wsComentarios(this);
        try {
            // llamada a /publico/infoproducto?ref=3
            String ingre = (String)request.getAttribute("ingresado");
            int ref = Integer.valueOf((String)request.getParameter("ref"));
            DataProducto dp = pp.obtenerDatosProducto(ref);
            request.setAttribute("Ref", dp.getNroReferencia());
            request.setAttribute("Titulo", dp.getTitulo());
            request.setAttribute("Descripcion", dp.getDescripcion());
            String specML = dp.getEspecificacion().replaceAll("\\r?\\n", "<br />");
            request.setAttribute("Especificacion", specML);
            request.setAttribute("Precio", dp.getPrecio());
            request.setAttribute("Proveedor", dp.getNickProveedor());
            request.setAttribute("Imagenes", dp.getNombreImagenes());
            request.setAttribute("Categorias", dp.getListaCategorias());
            request.setAttribute("Comentarios", dp.getCantidadComentarios());
            request.setAttribute("ingresado", ingre);
            request.setAttribute("Puntajes", dp.getPuntajes());
            
            String nickCliente = (String) request.getSession().getAttribute("nick");
            if(request.getSession().getAttribute("tipo") == TipoUsuario.CLIENTE){
                request.setAttribute("AgregoCarrito", po.carritoContieneProducto(nickCliente, ref));
                request.setAttribute("compro", pp.comproProducto(nickCliente, ref));
                request.setAttribute("puedeComentar", pc.puedeComentar(nickCliente, ref));
                request.setAttribute("puntuo", pp.existePuntuacion(nickCliente, ref));
                request.setAttribute("puntuacion", pp.obtenerValorPuntaje(nickCliente, ref));
                request.setAttribute("existeReclamo", pp.existeReclamoDeCliente(nickCliente, ref));
                List<DataReclamo> reclamos = pp.listarReclamosProducto(nickCliente, ref).getItem();
                request.setAttribute("reclamos", reclamos);
            }
            request.getRequestDispatcher("/WEB-INF/VerInfoProducto.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VerinfoProducto.class.getName()).log(Level.SEVERE, null, ex);
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
