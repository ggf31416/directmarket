/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import directmarket.servidor.DataConversacion;
import directmarket.servidor.DataMensaje;
import directmarket.servidor.RespuestaChat;
import directmarket.web.logicaweb.FabricaWS;
import directmarket.web.logicaweb.Logos;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.java.dev.jaxb.array.StringArray;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author guillermo
 */
@WebServlet(name = "Chat", urlPatterns = {"Chat", "ObtenerMensajes"})
public class Chat extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getServletPath().contains("Chat")) {
            }
        } finally {
            out.close();
        }
    }

    private JSONObject obtenerMensajes(HttpServletRequest req, HttpServletResponse resp) {
        HashMap<String, Integer> nuevos = new HashMap<String, Integer>();

        Integer idCon = (Integer) req.getSession().getAttribute("idConexion");
        Integer ultimoMens = (Integer) req.getSession().getAttribute("ultimoMensaje");
        String mi = (String) req.getSession().getAttribute("nick");
        StringArray sa = new StringArray();
        RespuestaChat r = FabricaWS.wsUsuarios().obtenerMensajes(mi, idCon, ultimoMens, sa);
        if (idCon == 0) {
            ultimoMens = r.getUltimoMensaje();
            req.getSession().setAttribute("ultimoMensaje", ultimoMens);
        }
        JSONObject resultado = new JSONObject();
        JSONObject conversaciones = new JSONObject();
        for (DataConversacion c : r.getConversaciones()) {
            JSONObject conversacion = new JSONObject();
            StringBuilder html = new StringBuilder();
            for(DataMensaje m : c.getMensajes()){
            if (m.getId() > ultimoMens && !m.getRemitente().equals(mi)) {
                String remit = m.getRemitente();
                Integer existentes = nuevos.containsKey(remit) ? nuevos.get(remit) : 0;
                nuevos.put(m.getRemitente(), existentes + 1);
            }
            html.append(escribirMensaje(m, req));
            conversacion.put("html", html);
            conversacion.put("nuevos", nuevos.get(c.getOtroNick()));
            conversacion.put("nick", c.getOtroNick());
            conversaciones.put(c.getOtroNick(), conversacion);
            }
        }
        
        return resultado;
    }

    private String escribirMensaje(DataMensaje m, HttpServletRequest req) {
        String mi = (String) req.getSession().getAttribute("nick");
        String html = " <span id=\"comment_{id-Mensaje}\">\n"
                + "        <div class=\"bubble_row\">\n"
                + "            <div class=\"you_icon\">  \n"
                + "               <img  class=\"img-polaroid\" src=\"{ruta-imagen}\" style=\"width: 50px; height: 50px\" /></a>\n"
                + "            </div>\n"
                + "            <div class=\"bubble you\">\n"
                + "                <span class='text-error'>{fecha}</span>：{usr} <br />\n"
                + "                {texto}"
                + "            </div>\n"
                + "        </div>\n"
                + "    </span>";
        html = html.replace("{id-Mensaje}", Integer.toString(m.getId()));
        if (m.getRemitente().equals(mi)) {
            html = html.replace("you", "me");
            html = html.replace("{usr}", "Yo");
        } else {
            html = html.replace("{usr}", m.getRemitente());
        }
        String logo = Logos.mostrarAvatar(m.getRemitente());
        html = html.replace("{ruta-imagen}", logo);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM HH:mm");
        String fecha = sdf.format(m.getFecha().toGregorianCalendar().getTime());
        html = html.replace("{fecha}", fecha);
        html = html.replace("{texto}", m.getTexto());
        return html;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
