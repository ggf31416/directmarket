/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Font;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.pdf.*;
import directmarket.servidor.DataLinea;
import directmarket.servidor.DataOrdenLin;
import directmarket.servidor.PublOrden;
import directmarket.servidor.PublOrdenService;
import directmarket.web.logicaweb.FabricaWS;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author tprog086
 */
@WebServlet(name = "DescargarPDF", urlPatterns = {"/DescargarPDF"})
public class DescargarPDF extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, BadElementException, DocumentException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            /* TODO output your page here. You may use following sample code. */
            
            
            PublOrden po = FabricaWS.wsOrdenes(this);
            
             int nro =  Integer.valueOf(request.getParameter("Nroorden"));
             DataOrdenLin dol = po.informacionOrden(nro);
             List<DataLinea> lineas = dol.getLineas();
            
             Document documento = new Document();
             ByteArrayOutputStream pdf = new ByteArrayOutputStream();
             PdfWriter.getInstance(documento, pdf);
                               
             Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
             Font h4 = new Font(Font.FontFamily.HELVETICA,14,Font.ITALIC);
             Font h1 = new Font(Font.FontFamily.HELVETICA,18,Font.BOLD);

                                documento.open();
                                documento.addTitle("Información de orden de compra Nro. "+nro);
                                Paragraph preface = new Paragraph();
                                preface.add(new Paragraph("Información de orden de compra Nro. " + nro, titleFont));
                                documento.add(preface);
                                
                                for (DataLinea dl : lineas) {                                                                                                            
                                    documento.add(new Paragraph(" "));
                                    documento.add(new Paragraph("Producto:",h4));
                                    documento.add(new Paragraph(dl.getTitulo()));
                                    documento.add(new Paragraph(" "));
                                    documento.add(new Paragraph("Cantidad:",h4));
                                    documento.add(new Paragraph(Integer.toString(dl.getCantidad())));
                                    documento.add(new Paragraph(" "));
                                    documento.add(new Paragraph("Precio:",h4));
                                    documento.add(new Paragraph(Double.toString(dl.getPrecio())+" USD"));
                                    documento.add(new Paragraph(" "));
                                    documento.add(new Paragraph("Subtotal:",h4));
                                    documento.add(new Paragraph(Double.toString(dl.getSubtotal())+" USD"));
                                    documento.add(new Paragraph("__________"));
                                    documento.add(new Paragraph(""));
                                }
                                String url = request.getServerName()+ ":"+request.getLocalPort()+ "/InfoOrden?optionsRadios="+dol.getNroOrden();
                                documento.add(new Paragraph(" "));
                                documento.add(new Paragraph("Monto total:",h1));
                                documento.add(new Paragraph(Double.toString(dol.getTotal())+" USD"));
                                BarcodeQRCode qrcode = new BarcodeQRCode(url, 1, 1, null);
                                
                                Image img = qrcode.getImage();
                                img.scaleToFit(150, 150);
                                documento.add(new Paragraph(" "));
                                documento.add(new Paragraph("Por más informacion visita el siguiente enlace:"));
                                documento.add(img);
                                documento.close();
                                 // setting some response headers
                                response.setHeader("Expires", "0");
                                response.setHeader("Cache-Control",
                                "must-revalidate, post-check=0, pre-check=0");
                                response.setHeader("Pragma", "public");
                                // setting the content type
                                response.setContentType("application/pdf");
                                // the contentlength
                                response.setContentLength(pdf.size());
                                // write ByteArrayOutputStream to the ServletOutputStream
                                OutputStream os = response.getOutputStream();
                                pdf.writeTo(os);
                                os.flush();
                                os.close();
                                pdf.close();
            
        } finally {            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (BadElementException ex) {
            Logger.getLogger(DescargarPDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(DescargarPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (BadElementException ex) {
            Logger.getLogger(DescargarPDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(DescargarPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
