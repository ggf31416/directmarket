/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.web.controladores;


import directmarket.servidor.DataImagen;
import directmarket.web.logicaweb.FabricaWS;
import java.io.IOException;
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tprog087
 */
@WebServlet(name = "CargarImagenes", urlPatterns = {"/imagenes/*"})
public class CargarImagenes extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        //PrintWriter out = response.getWriter();
        // obtiene el nombre del archivo
        String nombre = URLDecoder.decode(request.getPathInfo().substring(1),"UTF-8");
         // obtiene el stream binario de salida
        ServletOutputStream salida =  response.getOutputStream();
        try {
            
            DataImagen img  = FabricaWS.wsGeneral(this).getImagen(nombre);
            if (img != null){
                
                String content = contentTypeByExt(img.getExtension());
                if (content != null) response.setContentType(content);
                
                byte[] datos = img.getDatos();
                response.setHeader("Content-Length", Integer.toString(datos.length));
                int bloque = 1024;
                for(int i = 0; i < datos.length; i+=bloque)
                    // sin el min se caia en el ultimo bloque
                    salida.write(datos,i,Math.min(bloque,datos.length - i)); // escribe de a bloques
                
            } // falta manejar cuando no encuentra la imagen
        } finally {            
            salida.close();
        }
    }
    
    private String contentTypeByExt(String ext){
        if (ext.toLowerCase().contains("jpg")) return "image/jpeg";
        if (ext.toLowerCase().contains("jpeg")) return "image/jpeg";
        if (ext.toLowerCase().contains("png")) return "image/png";
        if (ext.toLowerCase().contains("bmp")) return "image/bmp";
        else return null; // se recomienda no establecer el content type si el tipo es desconocido
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
