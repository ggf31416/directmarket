/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.entidades;

import java.util.GregorianCalendar;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tprog089
 */
@MappedSuperclass
public abstract class ReclamoSuper {

    private String texto;
    @Temporal(TemporalType.TIMESTAMP)
    private GregorianCalendar fecha;
    private String cliente;
    private int nroRefProd;
    private String NombreProd;


    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public GregorianCalendar getFecha() {
        return fecha;
    }

    public void setFecha(GregorianCalendar fecha) {
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getNroRefProd() {
        return nroRefProd;
    }

    public void setNroRefProd(int nroRefProd) {
        this.nroRefProd = nroRefProd;
    }

    public String getNombreProd() {
        return NombreProd;
    }

    public void setNombreProd(String NombreProd) {
        this.NombreProd = NombreProd;
    }



    public ReclamoSuper() {
    }

    public ReclamoSuper(String texto, GregorianCalendar fecha, String cliente, int nroRefProd, String NombreProd) {
        this.texto = texto;
        this.fecha = fecha;
        this.cliente = cliente;
        this.nroRefProd = nroRefProd;
        this.NombreProd = NombreProd;
    }

    
}
