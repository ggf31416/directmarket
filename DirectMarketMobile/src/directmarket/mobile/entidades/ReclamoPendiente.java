/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.entidades;

import java.io.Serializable;
import java.util.GregorianCalendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author tprog089
 */
@Table(name = "RPendientes") @Entity(name = "ReclamoPendiente")
public class ReclamoPendiente extends ReclamoSuper implements Serializable {
    @Id() @GeneratedValue(strategy = GenerationType.AUTO)
    private int idAuto;

    public int getIdAuto() {
        return idAuto;
    }

    public void setIdAuto(int idAuto) {
        this.idAuto = idAuto;
    }

    public ReclamoPendiente() {
    }

    public ReclamoPendiente(String texto, GregorianCalendar fecha, String cliente, int nroRefProd, String NombreProd) {
        super(texto, fecha, cliente, nroRefProd, NombreProd);
    }
    
    
}
