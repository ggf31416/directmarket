/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.entidades;

import directmarket.servidor.DataLinea;
import directmarket.servidor.DataOrdenLin;
import directmarket.servidor.TipoEstado;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tprog087
 */
@Entity @Table(name = "Ordenes")
public class Orden implements Serializable {
    @Id
    private int nroOrden;
    private String email;
    private String nick;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecha;
    private double total;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true) 
    @JoinColumn(name = "NroOrden",referencedColumnName = "nroOrden")
    private List<Linea> lineas = new ArrayList<>();
    private boolean confirmada;

    public Orden() {
    }

    public Orden(DataOrdenLin data) {
        this.nroOrden = data.getNroOrden();
        this.email = data.getEmail();
        this.fecha = data.getFecha().toGregorianCalendar();
        this.nick = data.getNick();
        this.total = data.getTotal();
        for(DataLinea l : data.getLineas()){
            Linea  lin = new Linea(l);
            this.lineas.add(lin);
        }
        this.confirmada = (data.getEstadoActual() == TipoEstado.CONFIRMADA);
    }

    public int getNroOrden() {
        return nroOrden;
    }

    public String getEmail() {
        return email;
    }

    public String getNick() {
        return nick;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public double getTotal() {
        return total;
    }

    public List<Linea> getLineas() {
        return lineas;
    }

    public boolean isConfirmada() {
        return confirmada;
    }

    public void setConfirmada(boolean confirmada) {
        this.confirmada = confirmada;
    }
    
    
    
}
