/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.entidades;

import directmarket.servidor.DataLinea;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author tprog087
 */
@Entity @Table (name = "Lineas")
public class Linea implements Serializable {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private int id;
    @Column(name = "referencia")
    private int ref;
    private String titulo;
    private double precio;
    private double subtotal;
    private int cantidad;

    public Linea() {
    }

    public Linea(DataLinea data) {
       this.ref = data.getRef();
       this.titulo = data.getTitulo();
       this.precio = data.getPrecio();
       this.subtotal = data.getSubtotal();
       this.cantidad = data.getCantidad();
    }

    public int getId() {
        return id;
    }

    public int getRef() {
        return ref;
    }

    public String getTitulo() {
        return titulo;
    }

    public double getPrecio() {
        return precio;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public int getCantidad() {
        return cantidad;
    }
    
    
}
