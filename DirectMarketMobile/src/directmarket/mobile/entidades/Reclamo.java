/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.entidades;

import directmarket.servidor.DataReclamo;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author tprog087
 */
@Entity
@Table(name = "Reclamos")
public class Reclamo extends ReclamoSuper implements Serializable {

    @Id
    private int id;
    private String respuesta;
    private String proveedor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    
    
    public Reclamo(DataReclamo r, String proveedor) {
        super(r.getTexto(), r.getFecha().toGregorianCalendar(), r.getCliente(), r.getNroRefProd(), r.getNombreProd());
        this.id = r.getId();
        this.proveedor = proveedor;
        this.respuesta = r.getRespuesta();

    }

    public Reclamo() {
    }
}
