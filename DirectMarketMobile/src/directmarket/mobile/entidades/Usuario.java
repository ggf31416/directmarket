/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.entidades;

import directmarket.servidor.TipoUsuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Guillermo
 */
@Entity @Table(name = "Usuarios")
public class Usuario implements Serializable {
    @Id
    private String nick;
    @Enumerated(EnumType.ORDINAL)
    private TipoUsuario tipo; 
    @OneToMany(fetch = FetchType.LAZY,orphanRemoval = true,cascade = CascadeType.ALL)
    private List<Orden> ordenes;
    @OneToMany(fetch = FetchType.LAZY,orphanRemoval = true,cascade = CascadeType.ALL)
    private List<Reclamo> reclamos;
    @OneToMany(fetch = FetchType.LAZY,orphanRemoval = true,cascade = CascadeType.ALL)
    private List<Reclamo> reclamosNuevos;
    
    
    public Usuario() {
        ordenes = new ArrayList<>();
        reclamos = new ArrayList<>();
        reclamosNuevos = new ArrayList<>();
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }

    public List<Orden> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<Orden> ordenes) {
        this.ordenes = ordenes;
    }

    public List<Reclamo> getReclamos() {
        return reclamos;
    }

    public void setReclamos(List<Reclamo> reclamos) {
        this.reclamos = reclamos;
    }

    public List<Reclamo> getReclamosNuevos() {
        return reclamosNuevos;
    }

    public void setReclamosNuevos(List<Reclamo> reclamosNuevos) {
        this.reclamosNuevos = reclamosNuevos;
    }

    
}
