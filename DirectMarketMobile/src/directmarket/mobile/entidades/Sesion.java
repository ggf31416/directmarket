/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.entidades;

import directmarket.servidor.TipoUsuario;
import java.io.Serializable;
import java.util.Observable;


/**
 *
 * @author tprog088
 */
//@Entity @Table(schema = "Sesion")
public class Sesion extends Observable implements Serializable{
    //@Id
    private int id = 1;
    private String nick;
    //@Enumerated(EnumType.ORDINAL)
    private TipoUsuario tipo;
    //@Transient
    private boolean conectado;

    //@Transient


    public  void setNick(String nick) {
        this.nick = nick;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }

    public boolean isConectado() {
        return conectado;
    }

    public void setConectado(boolean conectado) {
        this.conectado = conectado;
        this.setChanged();
        this.notifyObservers(this.conectado);
    }



    public String getNick() {
        return nick;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }


    

    
}
