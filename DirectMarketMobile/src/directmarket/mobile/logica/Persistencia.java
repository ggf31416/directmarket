/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.logica;


import directmarket.mobile.entidades.Orden;
import directmarket.mobile.entidades.Reclamo;
import directmarket.mobile.entidades.ReclamoPendiente;
import directmarket.mobile.entidades.ReclamoSuper;
import directmarket.mobile.entidades.Sesion;
import directmarket.servidor.DataReclamo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author tprog088
 */
public class Persistencia {
    EntityManagerFactory emf;
    EntityManager em;
    
    public void abrir(){
         emf = Persistence.createEntityManagerFactory("DirectMarketMobilePU");
         em = emf.createEntityManager();
    }

    public Persistencia() {
        abrir();
    }
    
    
    
    public void cerrar(){
        em.close();
        emf.close();
    }
    
    public void iniciarTrans(){
        if(!em.getTransaction().isActive()){
         em.getTransaction().begin();
        }
    }
    
    public void cerrarTrans(){
        em.getTransaction().commit();
    }

    public void guardarReclamo(ReclamoSuper r) {
        em.merge(r);
    }
    
    public void eliminarReclamo(ReclamoSuper r){
        em.remove(r);
    }
    
    
    public void eliminarReclamos(String prov){
        iniciarTrans();
        Query q = em.createQuery("From Reclamo r Where r.proveedor = :p");
        q.setParameter("p", prov);
        List<Reclamo> eliminar  = q.getResultList();

        for(Reclamo r : eliminar){
            em.remove(r);
        }
        
        cerrarTrans();
    }
    

    public void guardarOrden(Orden o) {
        em.persist(o);
    }
    
    public void eliminarOrdenes(String cliente){
        iniciarTrans();
        for(Orden o : obtenerOrdenes(cliente)){
            em.remove(o);
        }
        cerrarTrans();
    }

    public List<Reclamo> obtenerReclamosProv(String prov) {
        Query q = em.createQuery("From Reclamo r Where r.proveedor = :p");
        q.setParameter("p", prov);
        List<Reclamo> res = q.getResultList();
        return res;
    }
    
    public List<ReclamoPendiente> obtenerReclamosEnviar(){
        Query q = em.createQuery("From ReclamoPendiente r");
        List<ReclamoPendiente> res = q.getResultList();
        return res;
    }
    
    public List<Orden> obtenerOrdenes(String cliente){
        Query q = em.createQuery("From Orden o Where o.nick = :n");
        q.setParameter("n", cliente);
        List<Orden> res = q.getResultList();
        return res;
    }
    
    public Orden infoOrden(int id){
        return em.find(Orden.class, id);
    }
    
    public Sesion cargarSesion(){
        return em.find(Sesion.class, 1); // id hardcoreado
    }
    
    public void guardarSesion(Sesion s){
        em.getTransaction().begin();
        em.merge(s);
        em.getTransaction().commit();
    }
    

}
