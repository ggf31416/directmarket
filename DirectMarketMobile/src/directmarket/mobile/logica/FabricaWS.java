/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.logica;

import directmarket.servidor.PublComent;
import directmarket.servidor.PublComentService;
import directmarket.servidor.PublGeneral;
import directmarket.servidor.PublGeneralService;
import directmarket.servidor.PublOrden;
import directmarket.servidor.PublOrdenService;
import directmarket.servidor.PublProd;
import directmarket.servidor.PublProdService;
import directmarket.servidor.PublUsu;
import directmarket.servidor.PublUsuService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.ws.BindingProvider;

/**
 *
 * @author tprog087
 */
public class FabricaWS {

    private static PublProd prod;
    private static PublUsu usu;
    private static PublComent coment;
    private static PublGeneral general;
    private static PublOrden orden;
    private static String urlBase;
    private static long tiempoUltimoConfig = 0;

    private static void cambiarURLs(Object port, String param) {
        //long t1 = System.nanoTime();
         actualizarURL();
        BindingProvider bindingProvider = (BindingProvider) port;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, urlBase + param);
        //System.out.println("cambiar url tomo " + (System.nanoTime() - t1) / 1000 + " us");

    }
    
    private static void crearConfig() {
        String dir = System.getProperty("user.home") + "/.Direct Market/" + "movil.properties";
        File f = new File(dir);
        if(!f.exists()){
            try {
                f.createNewFile(); // crea nuevo archivo
                FileOutputStream os = new FileOutputStream(f);
                Properties prop = new Properties();
                prop.setProperty("protocolo", "http");
                prop.setProperty("host","localhost");
                prop.setProperty("port", "8080");
                prop.store(os, "Opciones de la aplicacion movil");
            } catch (IOException ex) {
                ex.printStackTrace(); // no deberia ocurrir
            }
        }
    }

    private static void actualizarURL() {
        //long t1 = System.nanoTime();
        // una pequeña optimizacion si el filesystem anda lento, solo cambia la config si es mas  vieja de 1seg
        if (Math.abs(new Date().getTime() - tiempoUltimoConfig) > 1000) { 
        System.out.println("Diff milis: " + Math.abs(new Date().getTime() - tiempoUltimoConfig));
            tiempoUltimoConfig = new Date().getTime();
            Properties defaultProps = new Properties();
            FileInputStream in;
            String dir = System.getProperty("user.home") + "/.Direct Market/" + "movil.properties";
            try {
                in = new FileInputStream(dir);
                defaultProps.load(in);
                in.close();
                String host = defaultProps.getProperty("host");
                String port = defaultProps.getProperty("port");
                String protocolo = defaultProps.getProperty("protocolo");
                String urlVieja = urlBase;
                urlBase = protocolo + "://" + host + ":" + port + "/";
                if (urlVieja == null || !urlVieja.equals(urlBase)){
                    System.out.println("Nueva URL Base WS = " + urlBase);
                }

            } catch (IOException ex) {
                crearConfig();
                urlBase = "http://localhost:8080/";
                System.err.println("Advertencia: No se encontro config movil en " + dir + ",creando archivo y usando http://localhost:8080/");
                System.err.println("Causa: " + ex.getMessage());
            }
        }
        //System.out.println("actualizarURL tomo " + (System.nanoTime() - t1) / 1000 + " us");
    }

    private static URL getURL(String param) throws MalformedURLException {
        return new URL(urlBase + param + "?wsdl");
    }

    public static PublProd wsProductos() {
        try {
            //long milis = new Date().getTime();
            if (prod == null) {
                long t1 = System.nanoTime();
                actualizarURL();
                URL url = getURL("publprod");

                PublProdService s = new PublProdService(url);
                prod = s.getPublProdPort();
                System.out.println("obtener wsProductos tomo " + (System.nanoTime() - t1) / 1000 + " us");
            }
            else{
                cambiarURLs(prod, "publprod");
            }
            //System.out.println("Obtener productos tomo ms " + (new Date().getTime() - milis));
            return prod;
        } catch (MalformedURLException ex) {
            throw new RuntimeException("URL Productos Invalida", ex);
        }
    }

    public static PublUsu wsUsuarios() {
        try {

            if (usu == null) {
                actualizarURL();
                URL url = getURL("publusu");
                usu = new PublUsuService(url).getPublUsuPort();
                
            }
            else{
                cambiarURLs(usu, "publusu");
            }
            return usu;
        } catch (MalformedURLException ex) {
            throw new RuntimeException("URL Productos Invalida", ex);
        }
    }

    public static PublComent wsComentarios() {
        try {
            if (coment == null) {
                actualizarURL();
                URL url = getURL("publcoment");
                coment = new PublComentService(url).getPublComentPort();
            }
            else{
                cambiarURLs(coment, "publcoment");
            }
            return coment;
        } catch (MalformedURLException ex) {
            throw new RuntimeException("URL Productos Invalida", ex);
        }
    }

    public static PublOrden wsOrdenes() {
        try {
            if (orden == null) {
                actualizarURL();
                URL url = getURL("publorden");
                orden = new PublOrdenService(url).getPublOrdenPort();
            }
            else{
                cambiarURLs(orden, "publorden");
            }
            return orden;
        } catch (MalformedURLException ex) {
            throw new RuntimeException("URL Productos Invalida", ex);
        }
    }

    public static PublGeneral wsGeneral() {
        try {
            if (general == null) {
                actualizarURL();
                URL url = getURL("publgeneral");
                general = new PublGeneralService(url).getPublGeneralPort();
            }
            else{
                cambiarURLs(general, "publgeneral");
            }
            return general;
        } catch (MalformedURLException ex) {
            throw new RuntimeException("URL Productos Invalida", ex);
        }
    }
}
