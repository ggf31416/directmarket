/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.logica.tareas;

import directmarket.mobile.logica.Manejador;
import java.util.TimerTask;

/**
 *
 * @author tprog087
 */
public class CheqConec extends TimerTask {
    @Override
    public void run() {
        if(!Manejador.getInstancia().isConectado()){
            Manejador.getInstancia().chequearConectividad();
        }
    }

}
