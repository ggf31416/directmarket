/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.logica.tareas;

import directmarket.mobile.logica.Manejador;
import java.util.TimerTask;

/**
 *
 * @author tprog087
 */
//http://www.mkyong.com/java/how-to-run-a-task-periodically-in-java/
public class ActualizarDatos extends TimerTask{

    @Override
    public void run() {
        Manejador.getInstancia().actualizar();
    }
    
}
