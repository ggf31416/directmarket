/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.logica.tareas;

import directmarket.mobile.logica.Manejador;
import java.util.TimerTask;

/**
 *
 * @author Guillermo
 */
public class ActualizarMensajes extends TimerTask{

    @Override
    public void run() {
        Manejador.getInstancia().obtenerMensajes();
    }
    
}
