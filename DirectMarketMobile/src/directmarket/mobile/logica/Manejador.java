/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.logica;

import directmarket.mobile.logica.tareas.ActualizarDatos;
import directmarket.mobile.logica.tareas.CheqConec;
import directmarket.mobile.entidades.Linea;
import directmarket.mobile.entidades.Orden;
import directmarket.mobile.entidades.Reclamo;
import directmarket.mobile.entidades.ReclamoPendiente;
import directmarket.mobile.entidades.Sesion;
import directmarket.mobile.logica.tareas.ActualizarMensajes;
import directmarket.servidor.DataConversacion;
import directmarket.servidor.DataLogueo;
import directmarket.servidor.DataMensaje;
import directmarket.servidor.DataOrdenLin;
import directmarket.servidor.DataReclamo;
import directmarket.servidor.DataReclamoArray;
import directmarket.servidor.NoSeEncontroException_Exception;
import directmarket.servidor.PublProd;
import directmarket.servidor.PublUsu;
import directmarket.servidor.RespuestaChat;
import directmarket.servidor.TipoUsuario;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Set;
import java.util.Timer;
import javax.swing.JOptionPane;
import javax.xml.ws.WebServiceException;
import net.java.dev.jaxb.array.StringArray;

/**
 *
 * @author tprog087
 */
public class Manejador extends Observable {

    //private Map<Integer, Orden> ordenes = new HashMap<>();
    private HashMap<Integer, Linea> comprados = new HashMap<>();
    private HashMap<String, List<DataMensaje>> msgTodos = new HashMap<>();
    private HashMap<String, List<DataMensaje>> msgNuevos = new HashMap<>();
    private HashMap<String, Boolean> preguntarConexion = new HashMap<>();
    //private List<Reclamo> reclamos = new ArrayList<>();
    //private List<Reclamo> reclamosnuevos = new ArrayList<>();
    private int segRefresco = 60;
    private int segConectividad = 5;
    private int segChat = 5;
    private static Manejador instancia = null;
    private static Sesion sesion;
    private int idConexion;
    private int ultimoMensaje;
    private Timer timerChat;
    Persistencia pers;

    public static Manejador getInstancia() {
        if (instancia == null) {
            instancia = new Manejador();
        }
        return instancia;
    }

    public void desconectar() {
        String nick = getSesion().getNick();
        FabricaWS.wsUsuarios().quitardeConectados(nick, idConexion);
        Manejador.getInstancia().getSesion().setNick(null);
        Manejador.getInstancia().getSesion().setTipo(null);
        msgTodos = new HashMap<>();
        msgNuevos = new HashMap<>();
        ultimoMensaje = 0;
        idConexion = 0;
        timerChat.cancel();
    }

    public DataLogueo loguear(String nick, String pass) throws NoSeEncontroException_Exception {
        PublUsu ctrlusu = FabricaWS.wsUsuarios();
        DataLogueo user = ctrlusu.autenticar(nick, pass);
        getSesion().setNick(user.getNick());
        getSesion().setTipo(user.getTipo());
        getSesion().setConectado(true);
        idConexion = ((int) user.getIdConexion());
        timerChat = new Timer();
        ActualizarMensajes msg = new ActualizarMensajes();
        timerChat.scheduleAtFixedRate(msg, 0, 1000 * segChat);
        return user;
    }

    public void obtenerMensajes() {
        try {
            String esteUsuario = this.getSesion().getNick();
            StringArray sa = new StringArray();
            sa.getItem().addAll(this.preguntarConexion.keySet());
            RespuestaChat resp = FabricaWS.wsUsuarios().obtenerMensajes(esteUsuario, idConexion, ultimoMensaje, sa);
            
            List<DataConversacion> nuevos = resp.getConversaciones();
            // ya notifique los mensajes asi que los puedo descartar
            msgNuevos = new HashMap<>();
            // separa los msg por usuario

            for (DataConversacion dc : nuevos) {
                String otroUsuario = dc.getOtroNick();
                // msg nuevos
                if (!msgNuevos.containsKey(otroUsuario)) {
                    msgNuevos.put(otroUsuario, new ArrayList<DataMensaje>());
                }

                SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");
                for (DataMensaje m : dc.getMensajes()) {
                    System.out.println(sdf.format(new Date()) + "," + m.getRemitente() + "->" + m.getDestinatario() + ": " + m.getTexto());
                    msgNuevos.get(otroUsuario).add(m);
                    // todos los msg
                    if (!msgTodos.containsKey(otroUsuario)) {
                        msgTodos.put(otroUsuario, new ArrayList<DataMensaje>());
                    }
                    msgTodos.get(otroUsuario).add(m);
                    
                }
                
            }
            ultimoMensaje = resp.getUltimoMensaje();
            for (String n : this.preguntarConexion.keySet()) {
                preguntarConexion.put(n, resp.getConectados().contains(n));
            }

            if (msgNuevos.size() > 0) {
                this.setChanged();
                this.notifyObservers("Mensajes");
            }
            this.setChanged();
            this.notifyObservers("Conexion");
        } catch (WebServiceException ex) {
            sesion.setConectado(false);
        }
    }

    public HashMap<String, List<DataMensaje>> getMsgTodos() {
        return msgTodos;
    }

    public HashMap<String, List<DataMensaje>> getMsgNuevos() {
        return msgNuevos;
    }

    public boolean estaConectado(String nick) {
        if (preguntarConexion.containsKey(nick)) {
            return preguntarConexion.get(nick);
        }
        return FabricaWS.wsUsuarios().estaconectado(nick);
    }

    public void agregarPregConexion(String nick) {
        preguntarConexion.put(nick, FabricaWS.wsUsuarios().estaconectado(nick));
    }

    public void quitarPregConexion(String nick) {
        preguntarConexion.remove(nick);
    }

    private Manejador() {
        sesion = new Sesion();
        pers = new Persistencia();

        Timer t = new Timer();
        ActualizarDatos actualizador = new ActualizarDatos();
        t.scheduleAtFixedRate(actualizador, segRefresco * 1000, segRefresco * 1000);
        CheqConec chc = new CheqConec();
        t.scheduleAtFixedRate(chc, segConectividad * 1000, segConectividad * 1000);


    }

    public void chequearConectividad() {
        try {
            FabricaWS.wsGeneral().checkearConectividad();
            sesion.setConectado(true);
        } catch (WebServiceException ex) {
            sesion.setConectado(false);
        }
    }

    @SuppressWarnings("empty-statement")
    public synchronized void actualizar() {
        System.out.println("Actualizar al " + new Date().getTime());
        chequearConectividad();
        if (sesion.isConectado()) {
            try {

                if (sesion.getNick() != null) {
                    if (sesion.getTipo() == TipoUsuario.CLIENTE) {


                        List<DataOrdenLin> info = FabricaWS.wsUsuarios().listarOrdenCliente(sesion.getNick()).getItem();
                        pers.eliminarOrdenes(sesion.getNick());
                        pers.iniciarTrans();
                        for (DataOrdenLin dol : info) {
                            Orden o = new Orden(dol);
                            pers.guardarOrden(o);
                        }
                        pers.cerrarTrans();

                        comprados.clear();
                        for (Orden o : pers.obtenerOrdenes(sesion.getNick())) {
                            if (o.isConfirmada()) {
                                for (Linea l : o.getLineas()) {
                                    comprados.put(l.getId(), l);
                                }
                            }
                        }


                        enviarReclamos();

                    } else {
                        DataReclamoArray tmp = FabricaWS.wsProductos().listarReclamos(sesion.getNick());
                        pers.eliminarReclamos(sesion.getNick());
                        pers.iniciarTrans();
                        for (DataReclamo r : tmp.getItem()) {
                            pers.guardarReclamo(new Reclamo(r, sesion.getNick()));
                        }
                        pers.cerrarTrans();
                    }
                    sesion.setConectado(true);
                }
            } catch (WebServiceException ex) {
                sesion.setConectado(false);
            }
        }
    }

    public void enviarReclamos() {
        List<ReclamoPendiente> enviar = pers.obtenerReclamosEnviar();
        Iterator<ReclamoPendiente> i = enviar.iterator();
        while (i.hasNext()) {
            try {
                ReclamoPendiente r = i.next();
                pers.iniciarTrans();
                FabricaWS.wsProductos().agregarReclamo(r.getCliente(), r.getTexto(), r.getNroRefProd());
                pers.eliminarReclamo(r);
                i.remove();
                pers.cerrarTrans();
            } catch (WebServiceException ex) {
                sesion.setConectado(false);
            }
        }
    }

    public List<Orden> listarOrdenes() {
        actualizar();
        return pers.obtenerOrdenes(sesion.getNick());
    }

    public Orden informacionOrden(int numeroOrden) {
        actualizar();
        return pers.infoOrden(numeroOrden);
    }

    public List<Reclamo> verReclamos() {
        actualizar();
        return pers.obtenerReclamosProv(sesion.getNick());
    }

    public List<Linea> listarcomprados() {

        actualizar();

        return new ArrayList<Linea>(comprados.values());
    }

    public void AgregarReclamo(ReclamoPendiente r) {
        try {
            FabricaWS.wsProductos().agregarReclamo(r.getCliente(), r.getTexto(), r.getNroRefProd());
        } catch (Exception ex) {
            pers.iniciarTrans();
            pers.guardarReclamo(r);
            pers.cerrarTrans();
        }

    }

    public Sesion getSesion() {
        return sesion;
    }

    public void setConectado(boolean conectado) {
        this.getSesion().setConectado(conectado);
    }

    public boolean isConectado() {
        return this.getSesion().isConectado();
    }

    public List<DataMensaje> obtenerNuevosMensajes(String otroUsuario) {
        List<DataMensaje> res = msgNuevos.get(otroUsuario);
        if (res == null) {
            res = new ArrayList<>();
        }
        return res;
    }

    public List<DataMensaje> obtenerMensajes(String otroUsuario) {
        List<DataMensaje> res = msgTodos.get(otroUsuario);
        if (res == null) {
            res = new ArrayList<>();
        }
        return res;
    }

    public void enviarMensaje(String msg, String dest) {
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");
        System.out.println(sdf.format(new Date()) + ", Enviado a " + dest + ":" + msg);
        FabricaWS.wsUsuarios().agregarMensaje(msg, this.getSesion().getNick(), dest);
    }
}
