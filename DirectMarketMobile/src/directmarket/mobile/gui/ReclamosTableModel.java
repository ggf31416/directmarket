/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.gui;


import directmarket.mobile.entidades.Reclamo;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;


public class ReclamosTableModel extends AbstractTableModel {

    private List<Reclamo> reclamos = new ArrayList<>();

    @Override
    public int getRowCount() {
        return reclamos.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Reclamo d = reclamos.get(rowIndex);
        if (columnIndex == 0)
            return d.getCliente();
        else if (columnIndex == 1)
            return d.getNombreProd();
        else if (columnIndex == 2)
                return d.getNroRefProd();
        else {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            return sdf.format(d.getFecha().getTime());
        }
    }
    public Reclamo getReclamo(int rowIndex){
        return reclamos.get(rowIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false; //To change body of generated methods, choose Tools | Templates.
    }

    public List<Reclamo> getUsuarios() {
        return reclamos;
    }

    public void setReclamos(List<Reclamo> reclamos) {
        this.reclamos = reclamos;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "Cliente";
        }
        if (column == 1) {
            return "Producto";
        }
        if (column == 2) {
            return "nro Ref.";
        }
        if (column == 3) {
            return "Fecha";
        }
        return "";
    }
}