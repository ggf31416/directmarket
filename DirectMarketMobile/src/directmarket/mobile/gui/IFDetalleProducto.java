/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.gui;

import directmarket.mobile.entidades.Sesion;
import directmarket.mobile.logica.FabricaWS;
import directmarket.mobile.logica.Manejador;
import directmarket.servidor.DataComentario;
import directmarket.servidor.DataImagen;
import directmarket.servidor.DataProducto;
import directmarket.servidor.DataReclamo;
import directmarket.servidor.DirectMarketException_Exception;
import directmarket.servidor.TipoUsuario;
import directmarket.utilidades.ImagenesUtilGUI;
import java.awt.Component;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.naming.ldap.ManageReferralControl;
import javax.swing.DefaultListModel;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.xml.ws.WebServiceException;

/**
 *
 * @author Guillermo
 */
public class IFDetalleProducto extends javax.swing.JInternalFrame implements Observer{
    
    private List<DataReclamo> reclamos;
    private JDesktopPane pane;
    private int ref;
    private HashMap<String, DataImagen> avatares = new HashMap<>();
    private List<DataImagen> imagenes;
    private String proveedor;

    /**
     * Creates new form IFDetalleProducto
     */
    public IFDetalleProducto(JDesktopPane pane, int ref) {
        initComponents();
        this.pane = pane;
        this.ref = ref;
        Manejador.getInstancia().addObserver(this);
    }
    
    private void limpiarInfoProducto() {
        lblMostrarTitulo.setText("");
        lblMostrarNroRef.setText("");
        lblMostrarDescripcion.setText("");
        lblMostrarProveedor.setText("");
        lblMostrarPrecio.setText("");
        txtAreaEspecificacion.setText("");
        
    }

    @Override
    public void update(Observable o, Object arg) {
        if(arg.equals("Conexion")){
            botonChat(this.proveedor);
        }
    }
    
    private class ImagenCellRenderer extends DefaultTableCellRenderer {
        
        public ImagenCellRenderer() {
            this.setMinimumSize(new Dimension(48, 48));
        }
        
        @Override
        protected void setValue(Object value) {
            DataImagen di = (DataImagen) value;
            try {
                ImagenesUtilGUI.mostrarImagen(di, this, 48, true);
                this.setText("");
            } catch (Exception ex) {
                this.setText(ex.toString());
                this.setIcon(null);
            }
            
        }
    }
    
    private void mostrarInfo(DataProducto dp) {
        this.proveedor = dp.getNickProveedor();
        DefaultListModel<String> modelo = new DefaultListModel<>();
        lblMostrarTitulo.setText(dp.getTitulo());
        lblMostrarNroRef.setText(Integer.toString(dp.getNroReferencia()));
        lblMostrarPrecio.setText(Double.toString(dp.getPrecio()));
        lblMostrarDescripcion.setText(dp.getDescripcion());
        lblMostrarProveedor.setText(dp.getNickProveedor());
        txtAreaEspecificacion.setText(dp.getEspecificacion());
        for (String c : dp.getListaCategorias()) {
            modelo.addElement(c);
        }
        jListCategorias.setModel(modelo);
    }
    
    private void botonChat(String nickProv){
        Manejador m = Manejador.getInstancia();
        Manejador.getInstancia().agregarPregConexion(nickProv);
        boolean esCliente = m.getSesion().getNick() != null && m.getSesion().getTipo() == TipoUsuario.CLIENTE;
        btChat.setEnabled(esCliente && m.estaConectado(nickProv));
    }
    
    private void mostrarImagenes(DataProducto data) {
        imagenes = new ArrayList<>();
        for (String nombre : data.getNombreImagenes()) {
            DataImagen img = FabricaWS.wsGeneral().getImagen(nombre);
            imagenes.add(img);
        }
        if (imagenes.size() >= 1) {
            ImagenesUtilGUI.mostrarImagen(imagenes.get(0), lblMiniatura1);
        }
        if (imagenes.size() >= 2) {
            ImagenesUtilGUI.mostrarImagen(imagenes.get(1), lblMiniatura2);
        }
        if (imagenes.size() >= 3) {
            ImagenesUtilGUI.mostrarImagen(imagenes.get(2), lblMiniatura3);
        }
        if (imagenes.size() >= 1) {
            ImagenesUtilGUI.mostrarImagen(imagenes.get(0), lblGrande);
        }
    }
    
    private void mostrarComentarios() {
        treeComent.setCellRenderer(new ComentarioTreeRenderer());
        DefaultTreeModel modelo = (DefaultTreeModel) treeComent.getModel();
        DefaultMutableTreeNode raiz = new DefaultMutableTreeNode("Comentarios");
        modelo.setRoot(raiz);
        
        List<DataComentario> comentarios = FabricaWS.wsComentarios().listarComentarios(this.ref).getItem();
        for (DataComentario dc : comentarios) {
            mostrarComentarioRec(dc, raiz);
        }
        
        modelo.reload();
        for (int i = 0; i < treeComent.getRowCount(); i++) {
            treeComent.expandRow(i);
        }
        treeComent.setRootVisible(false);
        treeComent.setShowsRootHandles(true);

        // agrego listener
        treeComent.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                DefaultMutableTreeNode nodo = (DefaultMutableTreeNode) (e.getPath().getLastPathComponent());
                Object nodoInfo = nodo.getUserObject();
                if (nodoInfo instanceof DataComentario) {
                    DataComentario dc = (DataComentario) nodo.getUserObject();
                    lblNumero.setText("# " + dc.getId());
                    lblNick.setText(dc.getNick());
                    String fecha = sdf.format(dc.getFecha().toGregorianCalendar().getTime());
                    lblFecha.setText(fecha);
                    txtTexto.setText(dc.getTexto());
                    ImagenesUtilGUI.mostrarImagen(avatares.get(dc.getImagen()), lblAvatar);
                    barPuntaje.setVisible(dc.isPuntuo());
                    barPuntaje.setValue(dc.getPuntaje());
                    barPuntaje.setString("Puntaje: " + dc.getPuntaje());
                }
            }
        });
    }
    
    private void mostrarComentarioRec(DataComentario dc, MutableTreeNode padre) {
        if (!avatares.containsKey(dc.getImagen()) && dc.getImagen() != null) {
            DataImagen img = FabricaWS.wsGeneral().getImagen(dc.getImagen());
            avatares.put(dc.getImagen(), img);
        }
        DefaultMutableTreeNode hijo = new DefaultMutableTreeNode(dc);
        padre.insert(hijo, padre.getChildCount());
        for (DataComentario resp : dc.getRespuestas()) {
            mostrarComentarioRec(resp, hijo);
        }
    }
    
    private class ComentarioTreeRenderer extends DefaultTreeCellRenderer {
        
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
            DefaultMutableTreeNode nodo = (DefaultMutableTreeNode) value;
            if (nodo.getUserObject() instanceof DataComentario) {
                DataComentario com = (DataComentario) nodo.getUserObject();
                String txt = com.getTexto().trim();
                String fecha = sdf.format(com.getFecha().toGregorianCalendar().getTime());
                value = com.getNick() + ": " + ((txt.length() > 23) ? txt.substring(0, 20) + "..." : txt) + "  " + fecha;
            }
            return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }
    }
    
    private void mostrarReclamos() {
        String nick = Manejador.getInstancia().getSesion().getNick();
        reclamos = FabricaWS.wsProductos().listarReclamosProducto(nick, ref).getItem();
        DefaultTableModel modelo = (DefaultTableModel) tablaReclamos.getModel();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for (DataReclamo r : reclamos) {
            String fecha = sdf.format(r.getFecha().toGregorianCalendar().getTime());
            Object[] fila = new Object[]{fecha, r.getTexto(), r.isAtendido()};
            modelo.addRow(fila);
        }
        tablaReclamos.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tablaReclamos.getSelectedRow() > -1) {
                    int idx = tablaReclamos.convertRowIndexToModel(tablaReclamos.getSelectedRow());
                    mostrarReclamos(idx);
                }
            }
        });
        
    }
    
    private void mostrarReclamos(int idx) {
        DataReclamo rec = reclamos.get(idx);
        txtRecTexto.setText(rec.getTexto());
        if (rec.isAtendido()) {
            txtRecResp.setText(rec.getRespuesta());
        } else {
            txtRecResp.setText("");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabs = new javax.swing.JTabbedPane();
        tabInfo = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblNroReferencia = new javax.swing.JLabel();
        lblDescripcion = new javax.swing.JLabel();
        lblProveedor = new javax.swing.JLabel();
        lblPrecio = new javax.swing.JLabel();
        lblEspecificacion = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtAreaEspecificacion = new javax.swing.JTextArea();
        lblCategorias = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jListCategorias = new javax.swing.JList();
        lblMostrarTitulo = new javax.swing.JLabel();
        lblMostrarNroRef = new javax.swing.JLabel();
        lblMostrarDescripcion = new javax.swing.JLabel();
        lblMostrarProveedor = new javax.swing.JLabel();
        lblMostrarPrecio = new javax.swing.JLabel();
        btChat = new javax.swing.JButton();
        tabImag = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        lblMiniatura1 = new javax.swing.JLabel();
        lblMiniatura2 = new javax.swing.JLabel();
        lblMiniatura3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        lblGrande = new javax.swing.JLabel();
        tabComent = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        Detalle = new javax.swing.JPanel();
        lblNick = new javax.swing.JLabel();
        lblNumero = new javax.swing.JLabel();
        lblAvatar = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtTexto = new javax.swing.JTextArea();
        lblFecha = new javax.swing.JLabel();
        barPuntaje = new javax.swing.JProgressBar();
        jScrollPane5 = new javax.swing.JScrollPane();
        treeComent = new javax.swing.JTree();
        tabReclamo = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaReclamos = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtRecTexto = new javax.swing.JTextArea();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtRecResp = new javax.swing.JTextArea();
        btCerrar = new javax.swing.JButton();

        setClosable(true);
        setTitle("Detalle de Producto");
        setMaximumSize(new java.awt.Dimension(320, 480));
        setMinimumSize(new java.awt.Dimension(320, 480));
        setPreferredSize(new java.awt.Dimension(320, 480));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabs.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        tabs.setPreferredSize(new java.awt.Dimension(304, 410));

        tabInfo.setBorder(javax.swing.BorderFactory.createTitledBorder("Información del Producto seleccionado"));
        tabInfo.setEnabled(false);
        tabInfo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo.setText("Título : ");
        tabInfo.add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 28, -1, -1));

        lblNroReferencia.setText("Nº de Ref.:");
        tabInfo.add(lblNroReferencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 49, -1, -1));

        lblDescripcion.setText("Descripción : ");
        tabInfo.add(lblDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 70, -1, -1));

        lblProveedor.setText("Proveedor : ");
        tabInfo.add(lblProveedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 91, -1, -1));

        lblPrecio.setText("Precio :");
        tabInfo.add(lblPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 112, -1, -1));

        lblEspecificacion.setText("Especificaciones :");
        tabInfo.add(lblEspecificacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 133, -1, -1));

        txtAreaEspecificacion.setEditable(false);
        txtAreaEspecificacion.setColumns(20);
        txtAreaEspecificacion.setLineWrap(true);
        txtAreaEspecificacion.setRows(5);
        txtAreaEspecificacion.setWrapStyleWord(true);
        jScrollPane3.setViewportView(txtAreaEspecificacion);

        tabInfo.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 154, 272, 100));

        lblCategorias.setText("Categorías:");
        tabInfo.add(lblCategorias, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 260, -1, -1));

        jListCategorias.setMaximumSize(new java.awt.Dimension(284, 70));
        jListCategorias.setMinimumSize(new java.awt.Dimension(284, 70));
        jListCategorias.setPreferredSize(new java.awt.Dimension(284, 70));
        jListCategorias.setVisibleRowCount(3);
        jScrollPane4.setViewportView(jListCategorias);

        tabInfo.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 281, 272, 82));

        lblMostrarTitulo.setText("jLabel1");
        tabInfo.add(lblMostrarTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 28, 188, -1));

        lblMostrarNroRef.setText("jLabel2");
        tabInfo.add(lblMostrarNroRef, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 49, 188, -1));

        lblMostrarDescripcion.setText("jLabel3");
        tabInfo.add(lblMostrarDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 70, 188, -1));

        lblMostrarProveedor.setText("jLabel4");
        tabInfo.add(lblMostrarProveedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 91, -1, -1));

        lblMostrarPrecio.setText("jLabel5");
        tabInfo.add(lblMostrarPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 112, 105, -1));

        btChat.setText("Chat");
        btChat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btChatActionPerformed(evt);
            }
        });
        tabInfo.add(btChat, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 90, -1, -1));

        tabs.addTab("Info", tabInfo);

        tabImag.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Miniatura"));

        lblMiniatura1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblMiniatura1.setMaximumSize(new java.awt.Dimension(80, 80));
        lblMiniatura1.setMinimumSize(new java.awt.Dimension(80, 80));
        lblMiniatura1.setName(""); // NOI18N
        lblMiniatura1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblMiniatura1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblMiniatura1MouseEntered(evt);
            }
        });

        lblMiniatura2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblMiniatura2.setMaximumSize(new java.awt.Dimension(80, 80));
        lblMiniatura2.setMinimumSize(new java.awt.Dimension(80, 80));
        lblMiniatura2.setName(""); // NOI18N
        lblMiniatura2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblMiniatura2MouseEntered(evt);
            }
        });

        lblMiniatura3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblMiniatura3.setMaximumSize(new java.awt.Dimension(80, 80));
        lblMiniatura3.setMinimumSize(new java.awt.Dimension(80, 80));
        lblMiniatura3.setName(""); // NOI18N
        lblMiniatura3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblMiniatura3MouseEntered(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMiniatura1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblMiniatura2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblMiniatura3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(lblMiniatura3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMiniatura1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblMiniatura2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        tabImag.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 300, -1));

        lblGrande.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblGrande.setMaximumSize(new java.awt.Dimension(256, 256));
        lblGrande.setMinimumSize(new java.awt.Dimension(256, 256));
        lblGrande.setName(""); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lblGrande, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addComponent(lblGrande, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabImag.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 113, 305, 250));

        tabs.addTab("Imágenes", tabImag);

        jSplitPane1.setDividerLocation(120);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        Detalle.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Detalle.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblNick.setText("Nick");
        Detalle.add(lblNick, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 40, -1, -1));

        lblNumero.setText("#Nro");
        Detalle.add(lblNumero, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, -1, -1));

        lblAvatar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblAvatar.setMaximumSize(new java.awt.Dimension(64, 64));
        lblAvatar.setMinimumSize(new java.awt.Dimension(64, 64));
        lblAvatar.setName(""); // NOI18N
        Detalle.add(lblAvatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 4, 60, 60));

        txtTexto.setEditable(false);
        txtTexto.setColumns(20);
        txtTexto.setLineWrap(true);
        txtTexto.setRows(5);
        txtTexto.setWrapStyleWord(true);
        jScrollPane1.setViewportView(txtTexto);

        Detalle.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 70, 290, 170));

        lblFecha.setText("18/11/2013 15:29");
        Detalle.add(lblFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, -1, -1));

        barPuntaje.setForeground(new java.awt.Color(51, 0, 255));
        barPuntaje.setMaximum(5);
        barPuntaje.setString("[Sin puntaje]");
        barPuntaje.setStringPainted(true);
        Detalle.add(barPuntaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 40, 100, -1));

        jSplitPane1.setRightComponent(Detalle);

        jScrollPane5.setViewportView(treeComent);

        jSplitPane1.setLeftComponent(jScrollPane5);

        javax.swing.GroupLayout tabComentLayout = new javax.swing.GroupLayout(tabComent);
        tabComent.setLayout(tabComentLayout);
        tabComentLayout.setHorizontalGroup(
            tabComentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
        );
        tabComentLayout.setVerticalGroup(
            tabComentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
        );

        tabs.addTab("Comentarios", tabComent);

        tabReclamo.setPreferredSize(new java.awt.Dimension(317, 410));

        tablaReclamos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Texto", "At"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaReclamos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(tablaReclamos);
        tablaReclamos.getColumnModel().getColumn(0).setPreferredWidth(10);
        tablaReclamos.getColumnModel().getColumn(1).setPreferredWidth(30);
        tablaReclamos.getColumnModel().getColumn(2).setMinWidth(2);
        tablaReclamos.getColumnModel().getColumn(2).setPreferredWidth(2);

        jLabel4.setText("Texto:");

        jLabel5.setText("Respuesta:");

        txtRecTexto.setEditable(false);
        txtRecTexto.setColumns(20);
        txtRecTexto.setLineWrap(true);
        txtRecTexto.setRows(4);
        txtRecTexto.setWrapStyleWord(true);
        jScrollPane7.setViewportView(txtRecTexto);

        txtRecResp.setEditable(false);
        txtRecResp.setColumns(20);
        txtRecResp.setLineWrap(true);
        txtRecResp.setRows(5);
        txtRecResp.setWrapStyleWord(true);
        jScrollPane6.setViewportView(txtRecResp);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout tabReclamoLayout = new javax.swing.GroupLayout(tabReclamo);
        tabReclamo.setLayout(tabReclamoLayout);
        tabReclamoLayout.setHorizontalGroup(
            tabReclamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabReclamoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
            .addGroup(tabReclamoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(23, 23, 23))
        );
        tabReclamoLayout.setVerticalGroup(
            tabReclamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabReclamoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabs.addTab("Reclamos", tabReclamo);

        getContentPane().add(tabs, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 310, 400));

        btCerrar.setText("Cerrar");
        btCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 310, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btCerrarActionPerformed
    
    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        this.pane.remove(this);
        if(!Menu.getInstance().getChat().tieneUsuario(this.proveedor)){
            Manejador.getInstancia().quitarPregConexion(this.proveedor);
        }
        Manejador.getInstancia().deleteObserver(this);
    }//GEN-LAST:event_formInternalFrameClosing
    
    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        try {
            limpiarInfoProducto();
            DataProducto data = FabricaWS.wsProductos().obtenerDatosProducto(ref);
            mostrarInfo(data);
            mostrarImagenes(data);
            mostrarComentarios();
            Sesion ses = Manejador.getInstancia().getSesion();
            String nick = ses.getNick();
            if (nick != null && ses.getTipo() == TipoUsuario.CLIENTE) {
                mostrarReclamos();
            } else {
                tabs.remove(tabReclamo);
            }
            botonChat(data.getNickProveedor());
        } catch (DirectMarketException_Exception ex) {
            JOptionPane.showMessageDialog(this.rootPane, ex.toString());
        }catch(WebServiceException ex){
            Manejador.getInstancia().setConectado(false);
            JOptionPane.showMessageDialog(rootPane, "Sin conexión","Error", JOptionPane.ERROR_MESSAGE);
            this.dispose();
        }
    }//GEN-LAST:event_formInternalFrameOpened
    
    private void lblMiniatura1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMiniatura1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMiniatura1MouseClicked
    
    private void lblMiniatura1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMiniatura1MouseEntered
        if (imagenes.size() >= 1) {
            ImagenesUtilGUI.mostrarImagen(imagenes.get(0), lblGrande);
        }
    }//GEN-LAST:event_lblMiniatura1MouseEntered
    
    private void lblMiniatura2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMiniatura2MouseEntered
        if (imagenes.size() >= 2) {
            ImagenesUtilGUI.mostrarImagen(imagenes.get(1), lblGrande);
        }
    }//GEN-LAST:event_lblMiniatura2MouseEntered
    
    private void lblMiniatura3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMiniatura3MouseEntered
        if (imagenes.size() >= 3) {
            ImagenesUtilGUI.mostrarImagen(imagenes.get(2), lblGrande);
        }
    }//GEN-LAST:event_lblMiniatura3MouseEntered

    private void btChatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btChatActionPerformed
        IF_Chat chat = Menu.getInstance().getChat();
        chat.agregarTab(this.proveedor);
        chat.setVisible(true);
    }//GEN-LAST:event_btChatActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Detalle;
    private javax.swing.JProgressBar barPuntaje;
    private javax.swing.JButton btCerrar;
    private javax.swing.JButton btChat;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JList jListCategorias;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel lblAvatar;
    private javax.swing.JLabel lblCategorias;
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblEspecificacion;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblGrande;
    private javax.swing.JLabel lblMiniatura1;
    private javax.swing.JLabel lblMiniatura2;
    private javax.swing.JLabel lblMiniatura3;
    private javax.swing.JLabel lblMostrarDescripcion;
    private javax.swing.JLabel lblMostrarNroRef;
    private javax.swing.JLabel lblMostrarPrecio;
    private javax.swing.JLabel lblMostrarProveedor;
    private javax.swing.JLabel lblMostrarTitulo;
    private javax.swing.JLabel lblNick;
    private javax.swing.JLabel lblNroReferencia;
    private javax.swing.JLabel lblNumero;
    private javax.swing.JLabel lblPrecio;
    private javax.swing.JLabel lblProveedor;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JPanel tabComent;
    private javax.swing.JPanel tabImag;
    private javax.swing.JPanel tabInfo;
    private javax.swing.JPanel tabReclamo;
    private javax.swing.JTable tablaReclamos;
    private javax.swing.JTabbedPane tabs;
    private javax.swing.JTree treeComent;
    private javax.swing.JTextArea txtAreaEspecificacion;
    private javax.swing.JTextArea txtRecResp;
    private javax.swing.JTextArea txtRecTexto;
    private javax.swing.JTextArea txtTexto;
    // End of variables declaration//GEN-END:variables
}
