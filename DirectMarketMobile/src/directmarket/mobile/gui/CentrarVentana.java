/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.mobile.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import javax.swing.JDesktopPane;

/**
 *
 * @author Gonzalo Miraballes
 */
public class CentrarVentana {

    public static Point getPosition(JDesktopPane jDskPanePrincipal,
            Component com) {
        Dimension desktopSize = jDskPanePrincipal.getSize();
        Dimension jInternalFrameSize = com.getSize();
        return new Point((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 2);
    }
}

