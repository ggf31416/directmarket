/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.utilidades;



import directmarket.mobile.entidades.Linea;
import directmarket.servidor.DataProductoBas;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author tprog087
 */
public class TableModelListadoProductosLin extends AbstractTableModel {

    private List<Linea> listado = new ArrayList<>();

    @Override
    public int getRowCount() {
        return listado.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Linea prod = listado.get(rowIndex);
        return columnIndex == 0 ? prod.getRef() : prod.getTitulo();
        //DataUsuarioBas d = usuarios.get(rowIndex);
        //return  columnIndex == 0 ? d.getNick() : d.getEmail();
    }

    @Override
    public String getColumnName(int column) {
        return new String[]{"Nro.", "Título"}[column]; //To change body of generated methods, choose Tools | Templates.
    }

    public List<Linea> getProductos() {
        return listado;
    }

    public void setProductos(List<Linea> produc) {
        this.listado = produc;
    }
}
