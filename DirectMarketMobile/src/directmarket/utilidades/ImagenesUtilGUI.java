/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.utilidades;

import directmarket.servidor.DataImagen;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;


/**
 *
 * @author Guillermo
 */
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ImagenesUtilGUI {

    public static JFileChooser obtenerChooser() {
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagenes", "jpg", "jpeg", "gif", "png", "bmp");
        fc.setFileFilter(filter);
        fc.setAcceptAllFileFilterUsed(false);
        return fc;
    }

    /**
     * Obtiene una minuatura que mantiene el aspecto
     *
     * @param img
     * @param pixels
     * @return
     */
    public static Image obtenerMiniatura(BufferedImage img, int pixels) {
        if (pixels == 0) {
            pixels = 64;
        }
        int w = -1;
        int h = -1;
        if (img.getHeight() > img.getWidth()) {
            h = pixels;
        }
        if (img.getHeight() < img.getWidth()) {
            w = pixels;
        }
        return img.getScaledInstance(w, h, Image.SCALE_SMOOTH);
    }

    
    public static void mostrarImagen(DataImagen di, JLabel lbl) {
        int pix = lbl.getHeight();
        if (pix == 0) {
            pix = 64; // hack
        }
        mostrarImagen(di, lbl, pix, false);
    }

    private static BufferedImage obtenerImg(DataImagen di) {
        try {
            ImageIO.setUseCache(false); // desactiva el cache de disco que da problemas en la fing
            return ImageIO.read(new ByteArrayInputStream(di.getDatos()));
        } catch (IOException ex) {
            return null;
        }
    }

public static void mostrarImagen(DataImagen di, JLabel lbl, int pixels, boolean aumentarSiMenor) {
        //String soportados[] = ImageIO.getReaderFormatNames();
        if (di != null) {
            BufferedImage myPicture = obtenerImg(di);
            // si el alto o ancho son mayores a pixeles o si se especifico que se aumente la imagen si es de menor tamano
            if ((myPicture.getHeight() > pixels || myPicture.getWidth() > pixels) || aumentarSiMenor) {
                Image miniatura = obtenerMiniatura(myPicture, pixels);
                lbl.setIcon(new ImageIcon(miniatura));
            } else {
                lbl.setIcon(new ImageIcon(myPicture));
            }
        } else {
            lbl.setIcon(null);
        }
    }
}
