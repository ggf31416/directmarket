/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.utilidades;

import directmarket.servidor.DataImagen;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author tprog086
 */
public class ImagenesHoriz extends AbstractTableModel {

    private List<DataImagen> imagenes = new ArrayList<>();

    public void setImagenes(List<DataImagen> imagenes) {
        this.imagenes = imagenes;
    }

    public List<DataImagen> getImagenes() {
        return imagenes;
    }

    @Override
    public int getRowCount() {
        return imagenes.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        return "Imagen";
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        return imagenes.get(columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void agregar(DataImagen di) {
        imagenes.add(di);
        this.fireTableRowsInserted(imagenes.size() - 1, imagenes.size() - 1);
    }

    public void eliminar(int fila) {
        imagenes.remove(fila);
        this.fireTableRowsDeleted(fila, fila);
    }
}
