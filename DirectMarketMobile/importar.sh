DIR_COMPILAR=build/classes
PAQUETE_WS= #Vacio = paquete predeterminado
DIR_WS=$DIR_COMPILAR/$PAQUETE_WS
DIR_WS_GEN=build/generated-sources/jax-ws
URL_BASE=$1 #Toma la ruta donde estan los wsdl 
if [ $# -eq 0 ]
  then #Si no hay nada usa una por defecto
    URL_BASE=http://localhost:8080
fi
mkdir -p $DIR_WS #crea las carpetas
mkdir -p $DIR_WS_GEN
echo -----Importando WSDL desde $URL_BASE ---- 
wsimport -keep -d $DIR_WS $URL_BASE/publusu?wsdl
wsimport -keep -d $DIR_WS $URL_BASE/publprod?wsdl
wsimport -keep -d $DIR_WS $URL_BASE/publcoment?wsdl
wsimport -keep -d $DIR_WS $URL_BASE/publorden?wsdl
wsimport -keep -d $DIR_WS $URL_BASE/publgeneral?wsdl
wsimport -keep -d $DIR_WS $URL_BASE/publaccesos?wsdl
RETVAL=$?
[ $RETVAL -ne 0 ] && exit $RETVAL
echo 
echo -----Fin Importacion WSDL ---- 
cp -r $DIR_WS/* $DIR_WS_GEN/
