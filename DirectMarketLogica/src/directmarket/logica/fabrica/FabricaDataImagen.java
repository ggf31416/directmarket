/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.fabrica;

import directmarket.accesoDatos.dataTypes.DataImagen;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author tprog087
 */
public class FabricaDataImagen {

    public static DataImagen desdeFile(File f) throws IOException{

  
            FileInputStream fis = new FileInputStream(f);
            byte[] memoria = new byte[(int) f.length()];
            fis.read(memoria);
            fis.close();
            DataImagen di = new DataImagen(memoria, f.getName());
            return di;
            //return directmarket.guardarImagen(di);

        
    }
    
//    public static DataImagen desdeRuta(String ruta) throws IOException {
//        File f = new File(ruta);
//        return desdeFile(f);
//    }
//
    public static DataImagen desdeArray(byte[] arr, String nombre) {
        DataImagen img = new DataImagen(arr, nombre);
       return img;
    }

    public static DataImagen desdeImage(RenderedImage img, String nombre, String formato) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(img.getWidth() * img.getHeight() * 3 + 1024);
            ImageIO.write(img, formato, baos);
            baos.close();
            DataImagen di = new DataImagen(baos.toByteArray(), nombre);
            return di;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static DataImagen crearImagenEnBlanco(int ancho, int alto, String nombre, String formato) {
        BufferedImage enBlanco = new BufferedImage(ancho, alto, ColorSpace.TYPE_RGB);
        DataImagen di = FabricaDataImagen.desdeImage(enBlanco, nombre, formato);
        return di;
    }
}
