/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.fabrica;

import directmarket.logica.controladores.ControladorAccesos;
import directmarket.logica.controladores.ControladorComentarios;
import directmarket.logica.controladores.ControladorImagenes;
import directmarket.logica.controladores.ControladorOrdenes;
import directmarket.logica.controladores.ControladorProductos;
import directmarket.logica.controladores.ControladorUsuarios;
import directmarket.logica.interfaces.IAccesos;
import directmarket.logica.interfaces.IComentarios;
import directmarket.logica.interfaces.IImagenCtrl;
import directmarket.logica.interfaces.IOrdenes;
import directmarket.logica.interfaces.IProductos;
import directmarket.logica.interfaces.IUsuario;

/**
 *
 * @author Gonzalo Miraballes
 */
public class FabricaControladores {


    public FabricaControladores() {
    }
    
    public static IUsuario getUsuarioInstance() {
        return new ControladorUsuarios();
    }

    public static IOrdenes getOrdenesInstance() {
        return new ControladorOrdenes(new ControladorProductos());
    }

    public static IImagenCtrl getImagenesInstance() {
        return new ControladorImagenes();
    }
    
    public static IComentarios getComentariosInstance(){
        return new ControladorComentarios();
    }
    
    public static IProductos getProductosInstance(){
        return new ControladorProductos();
    }
    public static IAccesos getAccesosInstance(){
        return new ControladorAccesos();
    }
}
