/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.utilidades;

import directmarket.excepciones.DirectMarketException;
import directmarket.logica.controladores.ControladorComentarios;
import directmarket.logica.controladores.ControladorProductos;
import directmarket.logica.controladores.ControladorUsuarios;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.interfaces.IComentarios;
import directmarket.logica.interfaces.IOrdenes;
import directmarket.logica.interfaces.IProductos;
import directmarket.logica.interfaces.IUsuario;
import directmarket.logica.manejadores.ManejadorNegocios;
import directmarket.logica.manejadores.ManejadorUsuarios;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author tprog090
 */
public class DatosPrueba {

    public static void cargarDatosPrueba() throws DirectMarketException {
        
        ControladorProductos negocios = (ControladorProductos) FabricaControladores.getProductosInstance();
        ControladorUsuarios usuarios = (ControladorUsuarios) FabricaControladores.getUsuarioInstance();
        
        cargarDatosPruebaSinImag();

        // alta clientes

        usuarios.setImagenLocalCliente("Phil", "u_Phil.jpg");

        // alta proveedores

        usuarios.setImagenLocalCliente("Tim1", "u_Tim1.jpg");
        usuarios.setImagenLocalCliente("Eddy", "u_Eddy.jpg");
        usuarios.setImagenLocalCliente("CraigX", "u_CraigX.jpg");
        usuarios.setImagenLocalCliente("Johnny", "u_Johnny.jpg");        


        // categorias

        negocios.setImagenLocalProducto(1, Arrays.asList("product-1_0_IM1_specs_color.jpg"));
        negocios.setImagenLocalProducto(2, Arrays.asList("product-2_0_IM2_compare_color_iphone4s.jpg"));
        negocios.setImagenLocalProducto(3, Arrays.asList("product-3_0_IM3_n4-buy-1200.jpg", "product-3_1_IM4_N4_Google_Maps-1200.jpg"));
        negocios.setImagenLocalProducto(5, Arrays.asList("product-5_0_IM5_samsung-galaxy-s4.jpg"));
        negocios.setImagenLocalProducto(6, Arrays.asList("product-6_0_IM6_samsung-galaxy-ace-s5830.jpg"));
        negocios.setImagenLocalProducto(9, Arrays.asList("product-9_0_IM7_hero.png"));
        negocios.setImagenLocalProducto(10, Arrays.asList("product-10_0_IM8_specs_color.jpg"));

    }

    public static void cargarDatosPruebaSinImag() throws DirectMarketException {

        IProductos negocios = FabricaControladores.getProductosInstance();
        IUsuario usuarios = FabricaControladores.getUsuarioInstance();
        IOrdenes ordenes = FabricaControladores.getOrdenesInstance();
        IComentarios comentarios = FabricaControladores.getComentariosInstance();
        ManejadorNegocios.getInstance().reiniciar();
        ManejadorUsuarios.getInstance().reiniciar();
        
        
        
        // alta clientes
        usuarios.AltaUsuario("Dan", "dan.riccio@gmail.com", "Daniel", "Riccio", new GregorianCalendar(1963, Calendar.JULY, 5).getTime(), null, "danr");
        usuarios.AltaUsuario("Phil", "phil.schiller@gmail.com", "Philip", "Schiller", new GregorianCalendar(1961, Calendar.OCTOBER, 7).getTime(), null, "philip61");
        usuarios.AltaUsuario("BruceS", "bruce.sewell@gmail.com", "Bruce", "Sewell", new GregorianCalendar(1959, Calendar.DECEMBER, 3).getTime(), null, "bruces");
        usuarios.AltaUsuario("JeffW", "jeff.williams@gmail.com", "Jeff", "Wiliams", new GregorianCalendar(1964, Calendar.NOVEMBER, 27).getTime(), null, "jeffw");
        usuarios.AltaUsuario("Ricky", "ricky.r@gmail", "Ricky", "Ricón", new GregorianCalendar(1980, Calendar.AUGUST, 26).getTime(), null, "rickyr");


        // alta proveedores
        usuarios.AltaProveedor("Tim1", "tim.cook@apple.com", "Tim", "Cook", new GregorianCalendar(1960, Calendar.NOVEMBER, 1).getTime(), "Apple", "http://www.apple.com", null, "tim123");
        usuarios.AltaProveedor("Eddy", "eddy.cue@samsung.com", "Eduardo", "Cue", new GregorianCalendar(1965, Calendar.SEPTEMBER, 2).getTime(), "Samsung", "http://www.samsung.com", null, "edd");
        usuarios.AltaProveedor("CraigX", "craig.feder@sony.com", "Craig", "Federighi", new GregorianCalendar(1970, Calendar.MAY, 4).getTime(), "Sony", "http://us.playstation.com", null, "craig@");
        usuarios.AltaProveedor("Johnny", "johnny.ive@outlook.com", "Jonathan", "Ive", new GregorianCalendar(1967, Calendar.FEBRUARY, 12).getTime(), "Microsoft", "http://www.xbox.com", null, "john");
        usuarios.AltaProveedor("OpenPeter", "peter.open@htc.com", "Peter", "Oppenhemeir", new GregorianCalendar(1963, Calendar.AUGUST, 5).getTime(), "HTC", "http://www.htc.com", null, "peter42");


        // categorias
        negocios.altaCategoria("Celulares", "Categorías", false);
        negocios.altaCategoria("Sistemas Operativos", "Celulares", false);
        negocios.altaCategoria("iOS", "Sistemas Operativos", true);
        negocios.altaCategoria("Android", "Sistemas Operativos", true);
        negocios.altaCategoria("Windows Phone", "Sistemas Operativos", true);
        negocios.altaCategoria("Symbian", "Sistemas Operativos", true);
        negocios.altaCategoria("Blackberry OS", "Sistemas Operativos", true);
        negocios.altaCategoria("Equipos", "Celulares", false);
        negocios.altaCategoria("iPhone", "Equipos", true);
        negocios.altaCategoria("Nexus", "Equipos", true);
        negocios.altaCategoria("Samsung", "Equipos", false);
        negocios.altaCategoria("Galaxy S3", "Samsung", true);
        negocios.altaCategoria("Galaxy S4", "Samsung", true);
        negocios.altaCategoria("Galaxy Ace", "Samsung", true);
        negocios.altaCategoria("Blackberry", "Equipos", true);
        negocios.altaCategoria("Nokia", "Equipos", true);
        negocios.altaCategoria("Accesorios", "Celulares", false);
        negocios.altaCategoria("Protectores", "Accesorios", true);
        negocios.altaCategoria("Baterías", "Accesorios", true);
        negocios.altaCategoria("Apple", "Categorías", true);
        negocios.altaCategoria("Videojuegos", "Categorías", false);
        negocios.altaCategoria("Playstation", "Videojuegos", true);
        negocios.altaCategoria("Xbox", "Videojuegos", true);
        // titulo,numero,desc,espec,precio,prov,cat
        String especIPH5 = "Capacidad: 16 GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        String especIPH4 = "Capacidad: 16 GB\nPeso: 140 g\nPantalla: 3.5\"\nVersiones de Wifi: b/g/n";
        String especNex4 = "Capacidad: 8 GB\nPeso: 139 g\nPantalla: 4.7\"\nVersión de Android: 4.3";
        String especGa3 = "Dimensiones: 136.6 x 70.6 x 8.6mm\nPeso: 133 g\nPantalla: 4.8\"\nVersión de Android: 4.0.4";
        String especGa4 = "Dimensiones: 136.6 x 69.8 x 7.9mm\nPeso: 130 g\nPantalla: 4.99\"\nVersión de Android: 4.2.2";
        String especAs5 = "Dimensiones: 112.4 x 59.9 x 11.5mm\nPeso: 113 g\nPantalla: 3.5\"\nVersión de Android: 2.3";
        String especPCG = "Dimensiones: 12.5 cm x 6.7 cm x 2.0 cm\nPeso: 44 g";
        String especPMH = "Dimensiones: 12.4 cm x 7.0 cm x 1.3 cm\nPeso: 26 g";
        String especIRD = "Capacidad: 16 GB\nPeso: 652 g\nPantalla: 9.7\"\nProcesador: A6X";
        String especIM = "Capacidad: 16 GB\nPeso: 308 g\nPantalla: 7.9\"\nProcesador: A5";
        String especRIX = "Dimensiones: 7.5 cm x 4.2 cm x 1.8 cm\nPeso: 111 g";
        String especCIX = "Garantía: 3 meses\nDimensiones: 5.91 in x 4.33 in x 1.77 in\nPeso: 7.83 oz";
        String especCHP = "Dimensiones: 0 in x 0 in x 0 in\nPeso: 7.83 oz";
        String especCP3 = "Dimensiones: 16.0 cm x 9.5 cm x 5.0 cm\nPeso: 184 g";
        String especIPH5S = "Capacidad: 16 GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        String especIPH5C = "Capacidad: 16 GB\nPeso: 132 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        String especIPAA = "Capacidad: 16 GB\nPeso: 469 g\nPantalla: 9.7\"\nVersiones de Wifi: a/b/g/n con MIMO";
        String especIPAM = "Capacidad: 16 GB\nPeso: 331 g\nPantalla: 7.9\"\nVersiones de Wifi: a/b/g/n con MIMO";
        
        // titulo,numero,desc,espec,precio,prov,cat
        negocios.altaProducto("iPhone 5", 1, "El último celular de Apple", especIPH5, 199, "Tim1", Arrays.asList("iPhone", "iOS", "Apple"), 0);
        negocios.altaProducto("iPhone 4S", 2, "El siguiente celular al iPhone 4", especIPH4, 99, "Tim1", Arrays.asList("iPhone", "iOS", "Apple"), 0);
        negocios.altaProducto("Nexus4", 3, "El celular de Google", especNex4, 299, "Eddy", Arrays.asList("Android", "Nexus"), 0);
        negocios.altaProducto("Samsung Galaxy S3", 4, "La versión S3 de la línea Samsung Galaxy", especGa3, 415, "Eddy", Arrays.asList("Android", "Galaxy S3"), 0);
        negocios.altaProducto("Samsung Galaxy S4", 5, "La versión S4 de la línea Samsung Galaxy", especGa4, 839.99, "Eddy", Arrays.asList("Android", "Galaxy S4"), 0);
        negocios.altaProducto("Galaxy Ace S5830", 6, "La versión Ace de la línea Samsung Galaxy", especAs5, 237, "Eddy", Arrays.asList("Android", "Galaxy Ace"), 0);
        negocios.altaProducto("Protector de cuero para Galaxy", 7, "Asombroso protector de cuero para Samsumg Galaxy I900", especPCG, 3.0, "Eddy", Arrays.asList("Protectores"), 0);
        negocios.altaProducto("Protector de aluminio para HTC", 8, "El mejor protector de aluminio para HTC Desire HD", especPMH, 3.40, "OpenPeter", Arrays.asList("Protectores"), 0);
        negocios.altaProducto("iPad Retina Display", 9, "La última tableta de Apple con pantalla Retina", especIRD, 499, "Tim1", Arrays.asList("Apple", "iOS"), 0);
        negocios.altaProducto("iPad Mini", 10, "La primera tableta chica de Apple", especIM, 329, "Tim1", Arrays.asList("Apple", "iOS"), 0);
        negocios.altaProducto("Receptor inalámbrico para Xbox", 11, "Receptor inalámbrico de color negro para controles de Xbox 360", especRIX, 10.99, "Johnny", Arrays.asList("Xbox"), 0);
        negocios.altaProducto("Control inalámbrico para Xbox", 12, "Control inalámbrico de 2.4 GHz para Xbox 360", especCIX, 27.27, "Johnny", Arrays.asList("Xbox"), 0);
        negocios.altaProducto("Cable HDMI para PS3", 13, "Es un cable HDMI para PS3", especCHP, 7.99, "CraigX", Arrays.asList("Playstation"), 0);
        negocios.altaProducto("Control para PS3", 14, "Control inalámbrico Dualshock 3 de color azul para Playstation 3", especCP3, 30.80, "CraigX", Arrays.asList("Playstation"), 0);
        negocios.altaProducto("iPhone 5S", 15, "La evolución del iPhone 5, con Touch ID y A7", especIPH5S, 199, "Tim1", Arrays.asList("iPhone","iOS","Apple"), 0);
        negocios.altaProducto("iPhone 5C", 16, "Dale color a tu vida con esta nueva gama de iPhone", especIPH5C, 99, "Tim1", Arrays.asList("iPhone","iOS","Apple"), 0);
        negocios.altaProducto("iPad Air", 17, "Más ligero, más delgado y con mejor procesador que el anterior iPad", especIPAA, 499, "Tim1", Arrays.asList("iOS","Apple"), 0);
        negocios.altaProducto("iPad Mini Retina Display", 18, "Igual que el modelo anterior, solo que con pantalla Retina", especIPAM, 399, "Tim1", Arrays.asList("iOS","Apple"), 0);
        
        // creación y procesamiento de órdenes
        
        // O1
        ordenes.elegirClienteOrden("Dan");
        ordenes.agregarProductoLinea("Dan", 1, 1);
        ordenes.agregarProductoLinea("Dan", 9, 2);
        ordenes.agregarProductoLinea("Dan", 10, 2);
        ordenes.recibirOrden("Dan", new GregorianCalendar(2013, Calendar.AUGUST, 12).getTime());
        ordenes.ordenPreparada(1,new GregorianCalendar(2013, Calendar.AUGUST, 13).getTime());
        ordenes.ordenConfirmada(1,new GregorianCalendar(2013, Calendar.AUGUST, 20).getTime());
        
        // O2
        ordenes.elegirClienteOrden("Dan");
        ordenes.agregarProductoLinea("Dan", 3, 3);
        ordenes.recibirOrden("Dan", new GregorianCalendar(2013, Calendar.AUGUST, 12).getTime());
        
        // O3
        ordenes.elegirClienteOrden("Phil");
        ordenes.agregarProductoLinea("Phil", 13, 2);
        ordenes.agregarProductoLinea("Phil", 14, 3);
        ordenes.recibirOrden("Phil", new GregorianCalendar(2013, Calendar.AUGUST, 12).getTime());
        ordenes.ordenPreparada(3, new GregorianCalendar(2013, Calendar.AUGUST, 30).getTime());
        
        // O4
        ordenes.elegirClienteOrden("BruceS");
        ordenes.agregarProductoLinea("BruceS", 12, 4);
        ordenes.recibirOrden("BruceS", new GregorianCalendar(2013, Calendar.SEPTEMBER, 19).getTime());
        ordenes.ordenPreparada(4, new GregorianCalendar(2013, Calendar.SEPTEMBER, 20).getTime());
        ordenes.ordenConfirmada(4, new GregorianCalendar(2013, Calendar.SEPTEMBER, 25).getTime());
        
        // O5
        ordenes.elegirClienteOrden("JeffW");
        ordenes.agregarProductoLinea("JeffW", 7, 1);
        ordenes.recibirOrden("JeffW", new GregorianCalendar(2013, Calendar.SEPTEMBER, 17).getTime());
        ordenes.ordenPreparada(5, new GregorianCalendar(2013, Calendar.SEPTEMBER, 20).getTime());
        ordenes.ordenConfirmada(5, new GregorianCalendar(2013, Calendar.SEPTEMBER, 25).getTime());
        
        // O6
        ordenes.elegirClienteOrden("Phil");
        ordenes.agregarProductoLinea("Phil", 1, 1);
        ordenes.recibirOrden("Phil", new GregorianCalendar(2013, Calendar.AUGUST, 9).getTime());
        ordenes.ordenPreparada(6, new GregorianCalendar(2013, Calendar.SEPTEMBER, 12).getTime());
        ordenes.ordenConfirmada(6, new GregorianCalendar(2013, Calendar.SEPTEMBER, 19).getTime());
        
        // O7
        ordenes.elegirClienteOrden("Ricky");
        ordenes.agregarProductoLinea("Ricky", 1, 1);
        ordenes.agregarProductoLinea("Ricky", 15, 1);
        ordenes.agregarProductoLinea("Ricky", 16, 1);
        ordenes.recibirOrden("Ricky", new GregorianCalendar(2013, Calendar.AUGUST, 12).getTime());
        ordenes.ordenPreparada(7, new GregorianCalendar(2013, Calendar.AUGUST, 13).getTime());
        ordenes.ordenConfirmada(7, new GregorianCalendar(2013, Calendar.AUGUST, 20).getTime());        
        // O8
        ordenes.elegirClienteOrden("Ricky");
        ordenes.agregarProductoLinea("Ricky", 2, 1);
        ordenes.agregarProductoLinea("Ricky", 17, 1);
        ordenes.agregarProductoLinea("Ricky", 10, 1);
        ordenes.agregarProductoLinea("Ricky", 9, 1);
        
        ordenes.recibirOrden("Ricky", new GregorianCalendar(2013, Calendar.SEPTEMBER, 13).getTime());
        ordenes.ordenPreparada(8, new GregorianCalendar(2013, Calendar.SEPTEMBER, 14).getTime());
        ordenes.ordenConfirmada(8, new GregorianCalendar(2013, Calendar.SEPTEMBER, 21).getTime());
        
        // O9
        ordenes.elegirClienteOrden("Ricky");
        ordenes.agregarProductoLinea("Ricky", 18, 1);
        ordenes.agregarProductoLinea("Ricky", 16, 1);
        ordenes.agregarProductoLinea("Ricky", 2, 1);
        ordenes.agregarProductoLinea("Ricky", 15, 1);
        ordenes.recibirOrden("Ricky", new GregorianCalendar(2013, Calendar.SEPTEMBER, 14).getTime());
        ordenes.ordenPreparada(9, new GregorianCalendar(2013, Calendar.SEPTEMBER, 15).getTime());
        ordenes.ordenConfirmada(9, new GregorianCalendar(2013, Calendar.SEPTEMBER, 20).getTime());
        
        // 10
        ordenes.elegirClienteOrden("Ricky");
        ordenes.agregarProductoLinea("Ricky", 17, 1);
        ordenes.agregarProductoLinea("Ricky", 10, 1);
        ordenes.agregarProductoLinea("Ricky", 9, 1);
        ordenes.recibirOrden("Ricky", new GregorianCalendar(2013, Calendar.OCTOBER, 10).getTime());
        ordenes.ordenPreparada(10, new GregorianCalendar(2013, Calendar.OCTOBER, 11).getTime());
        ordenes.ordenConfirmada(10, new GregorianCalendar(2013, Calendar.OCTOBER, 20).getTime());
        
        // 11
        ordenes.elegirClienteOrden("Ricky");
        ordenes.agregarProductoLinea("Ricky", 1, 1);
        ordenes.agregarProductoLinea("Ricky", 9, 1);
        ordenes.agregarProductoLinea("Ricky", 10, 1);
        ordenes.agregarProductoLinea("Ricky", 16, 1);
        ordenes.recibirOrden("Ricky", new GregorianCalendar(2013, Calendar.OCTOBER, 25).getTime());
        ordenes.ordenCancelada(11, new GregorianCalendar(2013, Calendar.OCTOBER, 26).getTime());
        
        // 12
        ordenes.elegirClienteOrden("Ricky");
        ordenes.agregarProductoLinea("Ricky", 1, 1);
        ordenes.agregarProductoLinea("Ricky", 9, 1);
        ordenes.agregarProductoLinea("Ricky", 10, 1);
        ordenes.agregarProductoLinea("Ricky", 16, 1);
        ordenes.recibirOrden("Ricky", new GregorianCalendar(2013, Calendar.OCTOBER, 24).getTime());
        ordenes.ordenPreparada(12, new GregorianCalendar(2013, Calendar.OCTOBER, 25).getTime());
        ordenes.ordenConfirmada(12, new GregorianCalendar(2013, Calendar.NOVEMBER, 1).getTime()); 
        
        // negocios.ModificarProducto(7, "Protector de cuero para Galaxy", 3.5, "Asombroso protector de cuero para Samsumg Galaxy I900", especPCG, Arrays.asList("Protectores"));
        
        
        int c1 = comentarios.agregarComentario("El mejor iPhone hasta el momento. Es la mejor compra que he hecho en años. Le pasa el trapo a todos los \n"
                + "teléfonos Android.", new GregorianCalendar(2013, Calendar.SEPTEMBER, 19).getTime(), "Dan", 1);

        int c2 = comentarios.agregarRespuesta("Me parece que tu comentario es un poco desubicado. Hay muy buenos teléfonos que creo que mejoran las prestaciones de este, como el Samsung Galaxy S4.",
                new GregorianCalendar(2013, Calendar.SEPTEMBER, 19).getTime(), "Phil", 1, c1);

        int c3 = comentarios.agregarRespuesta("No creo, supe tener un Galaxy S2 y lo tenía que reiniciar todos los días. Nunca más vuelvo a Android.",
                new GregorianCalendar(2013, Calendar.SEPTEMBER, 20).getTime(), "Dan", 1, c2);

        int c4 = comentarios.agregarRespuesta("Se ha mejorado mucho desde entonces, pero me parece que estás muy cerrado con tu opinión. Saludos.",
                new GregorianCalendar(2013, Calendar.SEPTEMBER, 20).getTime(), "Phil", 1, c3);

        int c5 = comentarios.agregarComentario("¡Excelente control! Puedo disfrutar de mi GTA V sin la molestia de cables", new GregorianCalendar(2013, Calendar.SEPTEMBER, 25).getTime(), "BruceS", 12);
        
        int c6 = comentarios.agregarComentario("Retracto lo que escribí antes....se me rompió a los 3 dias. Me han estafado.", new GregorianCalendar(2013, Calendar.SEPTEMBER, 28).getTime(), "BruceS", 12);
        
        int c7 = comentarios.agregarComentario("Cumple su cometido. No he notado ninguna rayita nueva en mi Samsung. ", new GregorianCalendar(2013, Calendar.SEPTEMBER, 25).getTime(), "JeffW", 7);
        negocios.AgregarReclamo("Phil","Me demoró más de un mes en llegar el teléfono, estaría bueno que mejoraran los tiempos de envío" , 1,new GregorianCalendar(2013, Calendar.SEPTEMBER, 19).getTime());
        negocios.AgregarReclamo("BruceS", "Lo puse en los comentarios y quiero escribirlo acá. Su producto me vino defectuoso, espero un \n" +
"reembolso de dinero.", 12, new GregorianCalendar(2013, Calendar.SEPTEMBER, 28).getTime());
        negocios.AgregarReclamo("BruceS","No he tenido respuesta de parte suya. Estaría bueno que mejoraran su servicio de atención al cliente, \n" +
"porque me parece que el actual es pésimo." , 12, new GregorianCalendar(2013, Calendar.OCTOBER, 15).getTime());
        negocios.AgregarReclamo("Ricky","Me han enviado un iPhone 4 en lugar del iPhone 5 que encargué…" , 1, new GregorianCalendar(2013, Calendar.NOVEMBER, 1).getTime());
        negocios.AgregarReclamo("Ricky","Vino con algunas rayitas. Estaría bueno que controlaran que estas cosas no pasaran en el traslado del \n" +
"producto." , 9, new GregorianCalendar(2013, Calendar.NOVEMBER, 1).getTime());
        negocios.AgregarReclamo("Ricky","Este dispositivo vino fallado de fábrica, me gustaría que me lo reembolsaran." , 10,new GregorianCalendar(2013, Calendar.NOVEMBER, 1).getTime());
        negocios.AgregarReclamo("Ricky","No vinieron los auriculares incluidos en la caja." , 16, new GregorianCalendar(2013, Calendar.NOVEMBER, 1).getTime());
        negocios.puntuarProducto("Dan", 1, 5);
        negocios.puntuarProducto("Dan", 10, 5);
        negocios.puntuarProducto("Dan", 9, 5);
        negocios.puntuarProducto("Phil", 1, 3);
        negocios.puntuarProducto("Ricky", 1, 3);
        negocios.puntuarProducto("Ricky", 10, 4);
        negocios.puntuarProducto("Ricky", 9, 3);

        ManejadorUsuarios.getInstance().getPreferenciasEnvio().setActivarEnvio(true); // activo interrupciones, digo notificaciones ;-)

    
    }
    
}
