/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.utilidades;

import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.logica.fabrica.FabricaDataImagen;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

/**
 *
 * @author Guillermo
 */
public class ImagenesHelper {

    private String directorio;
    private static String directorioPrincipal = System.getProperty("user.home") + "/.Direct Market/Imagenes";

    public static String getDirectorioPrincipal() {
        return directorioPrincipal;
    }

    public static void setDirectorioPrincipal(String directorioPrincipal) {
        ImagenesHelper.directorioPrincipal = directorioPrincipal;
    }

    public static ImagenesHelper Usuarios() {
        return new ImagenesHelper(File.separator + "Usuarios");
    }

    public static ImagenesHelper Productos() {
        return new ImagenesHelper(File.separator + "Productos");
    }

    private ImagenesHelper(String _directorio) {
        this.directorio = _directorio;
    }

    private File obtenerFile(String nombre) {
        return new File(directorioPrincipal + directorio + File.separator + nombre);
    }

    public String guardarImagen(DataImagen di, boolean sobreescribir) {
        if (di != null) {
            try {
                File destino = obtenerFile(di.getNombre());
                // codigo si se quiere que la entrada no sobreescriba lo que haya con el mismo nombre
                int numero = 1;
                while (!sobreescribir && destino.exists()) {
                    String soloNombre = destino.getName();
                    String sinExt = soloNombre.substring(0, soloNombre.lastIndexOf("."));
                    String ext = obtenerExt(soloNombre);
                    // le agrega _num al nombre del archivo
                    destino = obtenerFile(sinExt + "_" + Integer.toString(numero) + ext);
                }

                destino.getParentFile().mkdirs(); // crea la carpeta Imagenes
                destino.createNewFile(); // crea el archivo
                FileOutputStream fos = new FileOutputStream(destino);
                fos.write(di.getDatos()); // escribe los datos
                fos.close(); // cierra el archivo
                return destino.getName(); // retorna el path del archivo
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return null;
    }

    private String obtenerExt(String nombre) {
        int idx = nombre.lastIndexOf(".");
        return (idx != -1) ? nombre.substring(idx) : "";
    }

    public void cambiarNombre(DataImagen di, String nuevo) {
        if (di != null) { // consideramos que di = null como sin imagen
            di.setNombre(nuevo + obtenerExt(di.getNombre()));
        }
    }

    public DataImagen cargarImagen(String nombre) throws IOException {
        File src = obtenerFile(nombre);
        return FabricaDataImagen.desdeFile(src);
    }

    public void borrarImagen(String nombre) {
        File destino = obtenerFile(nombre);
        destino.delete();
    }

    private BufferedImage generarThumb(DataImagen imagen, int pixels) {
        BufferedImage img = imagen.getImagen();
        double ratio = (double) img.getHeight() / img.getWidth();
        if (ratio > 0.6 && ratio < 1.66) {
            // el aspecto de la imagen no es muy dispar, lo escalo
            int w = pixels;
            int h = pixels;

            if (img.getHeight() > img.getWidth()) {
                h = pixels;
                w = (int) (pixels / ratio);
            } else if (img.getHeight() < img.getWidth()) {
                w = pixels;
                h = (int) (pixels * ratio);
            }
            // Graphics 2D  image es mejor pero para obtener buena calidad tengo que iterarlo
            return getScaledInstance(img, w, h, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
        } else {
            // toma un cuadrado (esquina superior izquierda)
            int min = Math.min(img.getHeight(), img.getWidth());
            BufferedImage res = img.getSubimage(0, 0, min, min);
            return getScaledInstance(res, pixels, pixels, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
        }
    }

    public String guardarThumbnail(DataImagen img, String nombre) {
        try {
            BufferedImage thumb = generarThumb(img, 140);
            File f = obtenerFile(nombre + ".jpg");
            f.createNewFile();

            ImageOutputStream ios = ImageIO.createImageOutputStream(f);
            Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpeg");
            ImageWriter writer = iter.next();
            ImageWriteParam params = writer.getDefaultWriteParam();
            // generalmente progresivo es mas pequeño
            params.setProgressiveMode(ImageWriteParam.MODE_DEFAULT);
            // establece la calidad
            params.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            params.setCompressionQuality(0.9f); // 0.00 = menor tamaño, 1.00 = mayor calidad

            // escritura
            writer.setOutput(ios);
            writer.write(null, new IIOImage(thumb, null, null), params);
            // cierra
            writer.dispose();
            return f.getName();
        } catch (IOException ex) {
            throw new RuntimeException("Error creando la miniatura", ex);
        }
    }

    // getScaledInstance no me sirve porque devuelve un toolkit image y necesito un renderedimage
    // https://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
    /**
     * Convenience method that returns a scaled instance of the provided
     * {@code BufferedImage}.
     *
     * @param img the original image to be scaled
     * @param targetWidth the desired width of the scaled instance, in pixels
     * @param targetHeight the desired height of the scaled instance, in pixels
     * @param hint one of the rendering hints that corresponds to
     * {@code RenderingHints.KEY_INTERPOLATION} (e.g. null null null     {@code RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BICUBIC})
     * @param higherQuality if true, this method will use a multi-step scaling
     * technique that provides higher quality than the usual one-step technique
     * (only useful in downscaling cases, where {@code targetWidth} or
     * {@code targetHeight} is smaller than the original dimensions, and
     * generally only when the {@code BILINEAR} hint is specified)
     * @return a scaled version of the original {@code BufferedImage}
     */
    public BufferedImage getScaledInstance(BufferedImage img,
            int targetWidth,
            int targetHeight,
            Object hint,
            boolean higherQuality) {
        // asi me elimina la transparencia
        int type = BufferedImage.TYPE_INT_RGB;// (img.getTransparency() == Transparency.OPAQUE)
        //? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = (BufferedImage) img;
        int w = targetWidth;
        int h = targetHeight;
        if (higherQuality && w > targetWidth) {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = img.getWidth();
        }
        if (higherQuality && h > targetHeight) {
            h = img.getHeight();
        }

        /*} else {
         // Use one-step technique: scale directly from original
         // size to target size with a single drawImage() call
         w = targetWidth;
         h = targetHeight;
         }*/

        do {
            if (higherQuality && w > targetWidth) {
                w /= 2;
                if (w < targetWidth) {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight) {
                h /= 2;
                if (h < targetHeight) {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setBackground(Color.WHITE);
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }
}
