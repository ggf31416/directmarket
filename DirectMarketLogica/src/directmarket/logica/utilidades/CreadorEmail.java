/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.utilidades;

import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Notificacion;
import directmarket.logica.entidades.PreferenciasEnvio;
import directmarket.logica.entidades.TipoNotificacion;
import directmarket.logica.manejadores.ManejadorUsuarios;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * libreria mail obtenida de https://java.net/projects/javamail/pages/Home
 *
 * @author tprog087
 */
public class CreadorEmail {

    public void notificarCliente(Cliente c, Notificacion n) {

        PreferenciasEnvio pref = ManejadorUsuarios.getInstance().getPreferenciasEnvio();
        if (pref.isActivarEnvio()) {
            try {
                String to = c.getEmail();

                // Sender's email ID needs to be mentioned
                String from = pref.getFromEmail();

                // Assuming you are sending email from localhost
                // vamos a tener a  devnull STMP
                String host = pref.getHostSMTP();


                Properties properties = new Properties();

                // Setup mail server
                properties.put("mail.smtp.host", host);
                // establece el puerto smtp, el 25 es complcado porque no somos root
                properties.put("mail.smtp.port", pref.getPuertoSMTP());


                // Get the default Session object.
                Session session = Session.getInstance(properties);


                // Create a default MimeMessage object.
                final MimeMessage message = new MimeMessage(session);

                // Set From: header field of the header.
                message.setFrom(new InternetAddress(from));

                // Set To: header field of the header.
                message.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(to));

                // Set Subject: header field
                String subject = "";
                // direccion de la web, es problematico porque depende de donde se haga el deploy

                if (n.getTipo() == TipoNotificacion.CambioEstadoOrdenDeCompra) {
                    subject = "Su orden de compra ha cambiado de estado en DirectMarket";
                } else if (n.getTipo() == TipoNotificacion.NuevoComentario) {
                    subject = "Se ha realizado un comentario en DirectMarket";
                } else if (n.getTipo() == TipoNotificacion.RegistroProducto) {
                    subject = "Hay un nuevo producto que puede interesarle en DirectMarket";
                } else if (n.getTipo() == TipoNotificacion.AtencionReclamo){
                    subject = "El proveedor ha respondido su reclamo en DirectMarket";
                }

                String direccionWeb = pref.getUrlWeb();
                String saludo = String.format("<h4>DirectMarket</h4><br><br>Estimado %s:<br><br>", c.getNombre());
                String masInfo = String.format("<br>Para m&aacute;s informaci&oacute;n vaya al siguiente <a href='http://%s'>enlace</a>.",
                        direccionWeb + n.getEnlace());

                String footer1 = "<br><br>Saludos.<br>El equipo de Direct Market.<br><br>---<br>Este email fue enviado a " + to + " porque est&aacute; suscripto a ellos. ";
                String footer2 = String.format("Si quiere desuscribirse de ellos puede <a href='http://%s'>editar</a> sus suscripciones.", direccionWeb + "/clientes/editarsuscripciones");

                String mensaje = saludo + n.getComunicado() + masInfo + footer1 + footer2;

                // Establece el asunto
                message.setSubject(subject, "UTF-8");
                // Estabblece el mensaje html
                message.setContent(mensaje, "text/html; charset=UTF-8");
                // Establece fecha
                message.setSentDate(new Date());
                
                final String sujeto = subject;

                // Manda el mensaje
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Transport.send(message);
                            System.out.println("Mensaje enviado con exito....");
                        } catch (MessagingException mex) {
                            System.out.println("Error mandando mensaje " + sujeto +": " + mex.getLocalizedMessage());
                        }
                    }
                }.run();
            } catch (MessagingException ex) {
                System.out.println("Error componiendo el mensaje: " + ex.getLocalizedMessage());
            }
        }
    }
}
