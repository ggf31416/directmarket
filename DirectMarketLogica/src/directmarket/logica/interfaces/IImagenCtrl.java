/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.interfaces;

import directmarket.accesoDatos.dataTypes.DataImagen;
import java.io.IOException;

/**
 *
 * @author Guillermo
 */
public interface IImagenCtrl {

    void setCarpetaImagenes(String ruta);

    String getCarpetaImagenes();
    
    public DataImagen getImagen(String nombre);
}
