/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.interfaces;

import directmarket.accesoDatos.dataTypes.DataCliente;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.accesoDatos.dataTypes.DataInfoCliente;
import directmarket.accesoDatos.dataTypes.DataInfoProveedor;
import directmarket.accesoDatos.dataTypes.DataLogueo;
import directmarket.accesoDatos.dataTypes.DataMensaje;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import directmarket.accesoDatos.dataTypes.DataProveedor;
import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import directmarket.accesoDatos.dataTypes.RespuestaChat;
import directmarket.excepciones.MultiExcepcion;
import directmarket.excepciones.NegocioException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.YaExisteException;
import directmarket.logica.entidades.TipoNotificacion;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Guillermo
 */
public interface IUsuario {

    void AltaUsuario(String nick, String correo, String nombre, String apellido,
            Date fecha, DataImagen imagen,String pass) throws MultiExcepcion;

    void AltaProveedor(String nick, String correo, String nombre, String apellido,
            Date fecha, String compania, String web, DataImagen imagen,String pass) throws MultiExcepcion;

    boolean ValidarNick(String nick);

    void mailValido(String nick)  throws YaExisteException,NegocioException;

    DataImagen getAvatar(String nick);

    List<DataUsuarioBas> listarProveedores();

    List<DataUsuarioBas> listarClientes();
    

    DataCliente VerInformacionCliente(String nick) throws NoSeEncontroException;

    DataProveedor VerInformacionProveedor(String nick) throws NoSeEncontroException;
    
    DataInfoCliente VerInfoCliente(String nick) throws NoSeEncontroException;

    DataInfoProveedor VerInfoProveedor(String nick) throws NoSeEncontroException;
    
    //Proveedor devolverProveedor(String nombProv) throws NoSeEncontroException;
    //void setImagenLocalCliente(String nick, String imagen);
    
    public DataLogueo autenticar(String nick,String pass) throws NoSeEncontroException;
    
    public DataUsuarioBas getInfoBas(String nick) throws NoSeEncontroException;
    
    public List<DataOrdenLin> listarOrdenesCliente(String nick);
    
    public void cambiarSuscripcionesUsuario(String nickCliente, Set<TipoNotificacion> excluidos) throws NoSeEncontroException;

    public int agregaraConectados(String nick);
    
    public void quitardeConectados(String nick,int idConexion);
    
    public boolean estaconectado(String nick);
    
    public RespuestaChat obtenerMensajes(String nick, int idConexion,int despuesDe,String[] pregConexion);

    public void agregarMensaje(String texto, String remit, String dest);
}
