/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.interfaces;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.accesoDatos.dataTypes.DataProducto;
import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.accesoDatos.dataTypes.DataReclamo;
import directmarket.excepciones.DirectMarketException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.YaExisteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author guillermo
 */
public interface IProductos {
    // registrar Producto

    void altaProducto(String tit, int nroRef, String desc, String especif, double precio,
            String nickProv, List<String> categs, List<DataImagen> imgs, Integer logo) throws DirectMarketException;

    void altaProducto(String tit, int nroRef, String desc, String especif, double precio,
            String nickProv, List<String> categs, Integer logo) throws DirectMarketException;

    //void datosPrueba();
    public List<DataProductoBas> ProductosPorProveedor(String nick);

    void ModificarProducto(int NroRef, String titulo, double precio, String descripcion, String especificacion, List<String> categs) throws DirectMarketException;

    void cambiarImagenesProducto(int NroRef, List<DataImagen> imagenes);

    List<DataProductoBas> productosPorCategoria(String nombreCateg) throws NoSeEncontroException;

    List<DataProductoBas> productosPorCategoria(int idCateg) throws NoSeEncontroException;

    List<DataCategoria> getCategoriasPrimerNivel();

    List<DataCategoria> listarCategorias();

    public List<DataCategoria> listarCategoriasConProducto();

    public DataCategoria getDatosCategoria(String nombreCateg);

    // se usa en altaCategoria
    int altaCategoria(String nombre, String nombrePadre, boolean tieneProductos) throws YaExisteException;

    List<DataProductoBas> ListarProductos();

    DataProducto obtenerDatosProducto(int nroRef) throws DirectMarketException;

    List<DataProductoBas> buscarProductos(String cadena);

    public boolean existeProducto(int ref);

    public boolean tituloDisponible(String titulo);

    public void puntuarProducto(String nick, int nroRef, int valor);

    public boolean existePuntuacion(String nick, int nroRef);

    public int obtenerValorPuntaje(String nick, int nroRef);

    public void AgregarReclamo(String nick, String texto, int nroRef);

    public void AgregarReclamo(String nick, String texto, int nroRef, Date fecha);

    public ArrayList<DataReclamo> ListarReclamos(String nick);

    public void atenderReclamo(int id, String respuesta, int nroRef);

    public boolean comproProducto(String nick, int nroRef);
    
    public ArrayList<DataReclamo> ListarReclamosProducto(String cliente, int ref);
    
    public boolean existeReclamoDeCliente(String cliente, int ref);
}
