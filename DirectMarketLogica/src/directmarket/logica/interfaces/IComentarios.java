/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.interfaces;

import directmarket.accesoDatos.dataTypes.DataComentario;
import java.util.Date;
import java.util.List;

/**
 *
 * @author guillermo
 */
public interface IComentarios {
      
    
    public int agregarComentario(  String texto, Date fecha,String nick,int ref) ;
    
    public int agregarComentario(  String texto,String nick,int ref) ;
    
    public int agregarRespuesta( String texto, Date fecha,String nick,int ref,int idPadre);
    
    public int agregarRespuesta( String texto, String nick,int ref,int idPadre);
    
    public List<DataComentario> listarComentarios(int nroRef);
    
    
    public boolean puedeComentar(String nick,int refProd);
}
