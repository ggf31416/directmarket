/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.interfaces;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.accesoDatos.dataTypes.DataOrden;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.excepciones.CarritoException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.DirectMarketException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author guillermo
 */
public interface IOrdenes {

    List<DataOrden> listarOrdenes();
    
    List<DataOrden> listarOrdenesRecibidas();

    //List<DataProductoBas> ListarProductos();

    //DataProducto obtenerDatosProducto(int nroRef) throws Exception;

    List<DataProductoBas> productosPorCategoria(String nombreCateg) throws NoSeEncontroException;

    List<DataProductoBas> productosPorCategoria(int idCateg) throws NoSeEncontroException;

    //List<DataCategoria> getCategoriasPrimerNivel();

    //List<DataCategoria> listarCategorias();

    public List<DataCategoria> listarCategoriasConProducto();

    public DataOrdenLin getCarrito(String nick);

    public double getCarritoTotal(String nick);

    public void eliminarLinea(String nick, Integer ref) throws CarritoException;

    public void eliminarOrden(int nro) throws NoSeEncontroException;

    public DataOrdenLin InformacionOrden(int nro);

    // se usa en crearOrden
    /**
     * Agrega un producto a la orden de compra actual del usuario
     *
     * @param nick Nick del usuario cliente
     * @param ref Referencia del producto
     * @param cant Cantidad del producto
     * @return Si se agrego una nueva linea
     */
    boolean agregarProductoLinea(String nick, Integer ref, Integer cant);

    // se usa en crearOrden
    void elegirClienteOrden(String nick);

    DataOrdenLin recibirOrden(String nick) throws CarritoException;

    DataOrdenLin recibirOrden(String nick, Date fecha) throws CarritoException;
    
    public boolean carritoContieneProducto(String nick, int ref);
    
    public void borrarCarrito(String nick);
    
    public void ordenCancelada(int nro, Date fecha) throws NoSeEncontroException, DirectMarketException;
    
    public void ordenPreparada(int nro, Date fecha) throws NoSeEncontroException, DirectMarketException;
    
    public void ordenConfirmada(int nro, Date fecha) throws NoSeEncontroException, DirectMarketException;
    
    public List<DataOrden> listarOrdenes(String nick);
}
