/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.interfaces;


import directmarket.logica.entidades.Acceso;
import java.util.GregorianCalendar;
import java.util.LinkedList;

/**
 *
 * @author tprog088
 */
public interface IAccesos {
    public void guardarAcceso(String ip, String url ,GregorianCalendar fecha, String userAgent);
    
    public LinkedList<Acceso> listarAccesos();

}
