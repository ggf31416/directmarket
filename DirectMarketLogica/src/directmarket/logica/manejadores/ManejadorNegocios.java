/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.manejadores;

import directmarket.logica.entidades.Acceso;
import directmarket.logica.entidades.Categoria;
import directmarket.logica.entidades.OrdenDeCompra;
import directmarket.logica.entidades.Producto;
import directmarket.logica.utilidades.ImagenesHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author tprog090
 */
public class ManejadorNegocios {

    private Map<Integer, OrdenDeCompra> ordenes;
    private Map<Integer, Producto> productos;
    private Map<String, OrdenDeCompra> carritos;
    private Map<Integer, Categoria> categorias;
    private int numeroComentario = 1;
    private int numeroCategoria = 1;
    private int numeroReclamo = 1;
    private static ManejadorNegocios instancia;
    private LinkedList<Acceso> accesos;
    public final void reiniciar() {
        ordenes = new HashMap<>();
        productos = new HashMap<>();
        carritos = new HashMap<>();
        categorias = new HashMap<>();
        accesos = new LinkedList<>();
        numeroComentario = 1;
        numeroCategoria = 1;
        numeroReclamo = 1;
        //numerosOrden = 1;
    }

    private ManejadorNegocios() {
        reiniciar();
    }

    public LinkedList<Acceso> getAccesos() {
        return accesos;
    }
    
    public static ManejadorNegocios getInstance() {
        if (instancia == null) {
            instancia = new ManejadorNegocios();
        }
        return instancia;
    }

    public OrdenDeCompra getOrdenDeCompra(int key) {
        return ordenes.get(key);
    }

    public void putOrdenDeCompra(int key, OrdenDeCompra o) {
        ordenes.put(key, o);
    }

    public Collection<OrdenDeCompra> valuesOrdenDeCompra() {
        return ordenes.values();
    }

    public boolean containsKeyOrdenDeCompra(int key) {
        return ordenes.containsKey(key);
    }

    public void removeOrdenDeCompra(int key) {
        ordenes.remove(key);
    }

    public int getCantidadOrdenes() {
        return ordenes.size();
    }

    public OrdenDeCompra getCarrito(String key) {
        return carritos.get(key);
    }

    public void putCarrito(String key, OrdenDeCompra o) {
        carritos.put(key, o);
    }

    public void removeCarrito(String key) {
        carritos.remove(key);
    }

    public Collection<OrdenDeCompra> valuesCarritos() {
        return carritos.values();
    }

    public boolean containsKeyCarritos(String key) {
        return carritos.containsKey(key);
    }

    public Producto getProducto(int key) {
        return productos.get(key);
    }

    public void putProducto(int key, Producto o) {
        productos.put(key, o);
    }

    public Collection<Producto> valuesProducto() {
        return productos.values();
    }

    public boolean containsKeyProducto(int key) {
        return productos.containsKey(key);
    }

    public Categoria getCategoria(int key) {
        return categorias.get(key);
    }

    public Categoria getCategoria(String key) {
        for (Categoria c : categorias.values()) {
            if (c.getNombre().equals(key)) {
                return c;
            }
        }
        return null;
    }

    public void putCategoria(Integer key, Categoria o) {
        categorias.put(key, o);
    }

    public Collection<Categoria> valuesCategorias() {
        return categorias.values();
    }

    public boolean containsKeyCategorias(String key) {
        return (getCategoria(key) != null);
    }

    public boolean containsKeyCategorias(int key) {
        return categorias.containsKey(key);
    }

    public int autoIdComentario() {
        return numeroComentario++;
    }

    public int autoIdCategoria() {
        return numeroCategoria++;
    }
    
    public int autoIdReclamo() {
        return numeroReclamo++;
    }

    public List<Producto> ProductosProveedor(String nick) {
        List<Producto> resultado = new ArrayList<>();
        for (Producto p : productos.values()) {
            if ((p.getProvedor().getNick()).equals(nick)) {
                resultado.add(p);
            }
        }
        return resultado;
    }
}
