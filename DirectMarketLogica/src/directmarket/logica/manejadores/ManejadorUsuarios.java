/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.manejadores;

import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.PreferenciasEnvio;
import directmarket.logica.entidades.Proveedor;
import directmarket.logica.entidades.Usuario;
import java.io.FileInputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author tprog090
 */
public class ManejadorUsuarios {
    
    private static ManejadorUsuarios instancia;
    private Map<String, Usuario> usuarios;
    private Map<String, Usuario> usuariosPorEmail;
    private int autoIdMensaje = 1;

    private PreferenciasEnvio preferenciasEnvio;
    
    private ManejadorUsuarios() {
        reiniciar();
    }
    
    public static ManejadorUsuarios getInstance() {
        if (instancia == null) {
            instancia = new ManejadorUsuarios();
        }
        return instancia;
    }
    
    public void reiniciar() {
        usuarios = new HashMap<>();
        usuariosPorEmail = new HashMap<>();
        preferenciasEnvio = new PreferenciasEnvio();
        autoIdMensaje = 1;
    }
    
    
public void cargarPreferenciasEnvio(){
        preferenciasEnvio = new PreferenciasEnvio();
        Properties defaultProps = new Properties();
            FileInputStream in;
            String dir = System.getProperty("user.home") + "/.Direct Market/" + "movil.properties";
            try {
                in = new FileInputStream(dir);
                defaultProps.load(in);
                in.close();
                preferenciasEnvio.setActivarEnvio(Boolean.parseBoolean(defaultProps.getProperty("mail_activarEnvio")));
                preferenciasEnvio.setPuertoSMTP(Integer.parseInt(defaultProps.getProperty("mail_puertoSMTP")));
                preferenciasEnvio.setHostSMTP(defaultProps.getProperty("mail_hostSMTP"));
                preferenciasEnvio.setFromEmail(defaultProps.getProperty("mail_fromEmail"));
                preferenciasEnvio.setUrlWeb(defaultProps.getProperty("mail_urlWeb"));
            }
            catch(Exception ex){
                preferenciasEnvio = new PreferenciasEnvio();
            }
    }

    
    public Usuario getUsuario(String key) {
         Usuario u = usuarios.get(key);
         if (u == null){
             u = usuariosPorEmail.get(key);
         }
         return u;
    }
    
    // tprog087 metodos que retornan el tipo especifico asi usuarios otros que el controlador de usuarios
    // no tienen que preocuparse que sea un proveedor y no un cliente y viceversa (no debería estar en
    // sus responsabilidades)
    
    /**
     * Retorna el cliente indexado por String key, si existe uno, de lo contrario retorna null
     * @param key
     * @return 
     */
    public Cliente getCliente(String key) {
        Usuario u = getUsuario(key);
        if (u instanceof Cliente) return (Cliente)u;
        else return null;
    }
    
     /**
     * Retorna el proveedor indexado por String key, si existe uno, de lo contrario retorna null
     * @param key
     * @return 
     */
    public Proveedor getProveedor(String key) {
        Usuario u = getUsuario(key);
        if (u instanceof Proveedor) return (Proveedor)u;
        else return null;
    }
    
    public void putUsuario(String key, Usuario u) {
        usuarios.put(key, u);
        usuariosPorEmail.put(u.getEmail(), u);
    }
    
    public Collection<Usuario> valuesUsuario(){
        return usuarios.values();
    }
    
    public boolean containsKeyUsuario(String nick) {
        return usuarios.containsKey(nick) ;
    }

    public PreferenciasEnvio getPreferenciasEnvio() {
        return preferenciasEnvio;
    }

    public void setPreferenciasEnvio(PreferenciasEnvio preferenciasEnvio) {
        this.preferenciasEnvio = preferenciasEnvio;
    }
    
    public int getAutoIdMensaje( ){
        return autoIdMensaje++;
    }
    
    
}
