/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.accesoDatos.dataTypes.DataProducto;
import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.accesoDatos.dataTypes.DataPuntaje;
import directmarket.accesoDatos.dataTypes.DataReclamo;
import directmarket.excepciones.DirectMarketException;
import directmarket.excepciones.NegocioException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.YaExisteException;
import directmarket.logica.entidades.Categoria;
import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Notificacion;
import directmarket.logica.entidades.Producto;
import directmarket.logica.entidades.Proveedor;
import directmarket.logica.entidades.Puntaje;
import directmarket.logica.entidades.Reclamo;
import directmarket.logica.interfaces.IProductos;
import directmarket.logica.manejadores.ManejadorNegocios;
import directmarket.logica.manejadores.ManejadorUsuarios;
import directmarket.logica.utilidades.ImagenesHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author guillermo
 */
public class ControladorProductos implements IProductos {

    private ImagenesHelper imghelper = ImagenesHelper.Productos();

    @Override
    public void altaProducto(String tit, int nroRef, String desc, String especif,
            double precio, String nickProv, List<String> categs, List<DataImagen> imgs, Integer logo)
            throws DirectMarketException {
        if (ManejadorNegocios.getInstance().containsKeyProducto(nroRef)) {
            throw new YaExisteException("Producto ya existente.");
        }
        if (!tituloDisponible(tit)) {
            throw new YaExisteException("Titulo de Producto ya existente.");
        }
        if (precio < 0) {
            throw new NegocioException("Precio", "Precio debe ser positivo.");
        }
        Proveedor prov = ManejadorUsuarios.getInstance().getProveedor(nickProv);
        if (prov == null) {
            throw new NoSeEncontroException("proveedor", "No se encontro proveedor con ese nombre");
        }



        List<Categoria> lista_categs = new ArrayList<>();
        for (int i = 0; i < categs.size(); i++) {
            if (!ManejadorNegocios.getInstance().containsKeyCategorias(categs.get(i))) {
                throw new NoSeEncontroException("Categoria no encontrada.");
            }
            Categoria c = ManejadorNegocios.getInstance().getCategoria(categs.get(i));
            lista_categs.add(c);
        }
        verificarCategoriasTP(lista_categs);
        Producto p = new Producto(tit, nroRef, desc, especif, precio, prov,
                lista_categs);
        prov.agregarProducto(p);
        arreglarCategorias(p, lista_categs);
        ManejadorNegocios.getInstance().putProducto(nroRef, p);
        manejarImagenes(p, imgs);
        manejarThumb(p, imgs, logo);

        Notificacion aviso = new Notificacion(prov, p);
        prov.getPtrSubj().notifyObservers(aviso);

    }

    private void manejarImagenes(Producto p, List<DataImagen> imgs) {
        for (int i = 0; i < imgs.size(); i++) {
            String nombre = imgs.get(i).getNombre();
            // si ya no empieza en product-[digitos]_(algo sin /)
            String sinExt = nombre.substring(0, nombre.lastIndexOf("."));
            //if (!nombre.matches("^product\\-[\\d]+_[^\\\\]+")) {
            imghelper.cambiarNombre(imgs.get(i), "product-" + p.getNroReferencia() + "_" + i); // + "_" + sinExt);
            //}
            String img = imghelper.guardarImagen(imgs.get(i), true);
            p.getImagenes().add(img);
        }
    }

    private void manejarThumb(Producto p, List<DataImagen> imgs, Integer logo) {

        if (imgs.isEmpty() || logo == null) {
            p.setLogo(null);
        } else {
            if (logo >= imgs.size()) {
                logo = 0;
            }
            DataImagen di = imgs.get(logo);
            if (di.getNombre().contains(".png")) {
                // no creo un thumbnail porque me da problemas con la transparencia
                p.setLogo(di.getNombre());
            } else { // creo el thumbail
                String nombre = "thumb_p" + p.getNroReferencia();
                String th = imghelper.guardarThumbnail(di, nombre);
                p.setLogo(th);
            }
        }
    }

    @Override
    public List<DataCategoria> listarCategorias() {
        ArrayList<DataCategoria> cats = new ArrayList<>();
        for (Categoria c : ManejadorNegocios.getInstance().valuesCategorias()) {
            cats.add(c.getDataRec());
        }
        return cats;
    }

    /**
     * Devuelve las categorias que tienen algun producto
     *
     * @return
     */
    @Override
    public List<DataCategoria> listarCategoriasConProducto() {
        ArrayList<DataCategoria> cats = new ArrayList<>();
        for (Categoria c : ManejadorNegocios.getInstance().valuesCategorias()) {
            if (c.getTieneProductos() && !c.getProductos().isEmpty()) {
                cats.add(c.getData());
            }
        }
        return cats;
    }

    @Override
    public List<DataCategoria> getCategoriasPrimerNivel() {
        List<DataCategoria> ldc = new ArrayList<>();
        for (Categoria c : ManejadorNegocios.getInstance().valuesCategorias()) {
            if (c.getNombrePadre().equals(Categoria.PRINCIPAL)) {
                ldc.add(c.getDataRec());
            }
        }
        return ldc;
    }

    /*private DataCategoria aDataCategoria(Categoria c) {
     DataCategoria dc = new DataCategoria(c.getNombre(), c.getNombrePadre(), c.getTieneProductos());
     for (Categoria cat : c.getSubcategorias()) {
     dc.getSubcategorias().add(this.aDataCategoria(cat));
     }
     for (Producto p : c.getProductos()) {
     dc.getProductos().add(p.getNroReferencia());
     }
     return dc;
     }*/
    @Override
    public DataCategoria getDatosCategoria(String nombreCateg) {
        Categoria c = ManejadorNegocios.getInstance().getCategoria(nombreCateg);
        return c.getDataRec();
    }

    private DataProductoBas aDataProductoBas(Producto p) {

        return p.getDataProductoBas();
    }

    private DataPuntaje aDataPuntaje(Puntaje pje) {
        return new DataPuntaje(pje.getValor(), pje.getProducto().getNroReferencia(), pje.getCliente().getNick());
    }

    private DataProducto aDataProducto(Producto p) throws DirectMarketException {
        DataProducto dp;
        try {
            dp = new DataProducto(p.getTitulo(), p.getNroReferencia(), p.getDescripcion(),
                    p.getEspecificacion(), p.getPrecio(), p.getProvedor().getNick(),
                    p.getCantidadComprada(), p.getCantidadComentarios(), p.getLogo());
            for (Categoria cat : p.getCategorias()) {
                dp.getListaCategorias().add(cat.getNombre());
            }
            for (String img : p.getImagenes()) {
                dp.getListaImagenes().add(imghelper.cargarImagen(img));
            }
            for (Puntaje pje : p.getPuntajes()) {
                dp.getPuntajes().add(this.aDataPuntaje(pje));
            }
        } catch (IOException ex) {
            throw new NoSeEncontroException("imagen", "No se encontro imagen para " + p.getTitulo(), ex);
        }
        //dp.setListacomentarios(p.listarComentarios());
        return dp;
    }

    public List<DataProductoBas> productosPorCategoria(Categoria c) throws NoSeEncontroException {
        ArrayList<DataProductoBas> res = new ArrayList<>();
        for (Producto p : c.getProductos()) {
            res.add(aDataProductoBas(p));
        }
        Collections.sort(res);
        return res;
    }

    // falta convertir a datatype
    @Override
    public List<DataProductoBas> productosPorCategoria(String nombreCateg) throws NoSeEncontroException {
        Categoria c = ManejadorNegocios.getInstance().getCategoria(nombreCateg);
        if (c == null) {
            throw new NoSeEncontroException("categoria", "No se encontró categoría con id " + nombreCateg);
        }
        return productosPorCategoria(c);
    }

    @Override
    public List<DataProductoBas> productosPorCategoria(int idCateg) throws NoSeEncontroException {
        Categoria c = ManejadorNegocios.getInstance().getCategoria(idCateg);
        if (c == null) {
            throw new NoSeEncontroException("categoria", "No se encontró categoría con id " + idCateg);
        }
        return productosPorCategoria(c);
    }

    @Override
    public int altaCategoria(String nombre, String nombrePadre, boolean tieneProductos) throws YaExisteException {
        if (ManejadorNegocios.getInstance().containsKeyCategorias(nombre)) {
            throw new YaExisteException("Categoría ya existente");
        }
        Categoria padre = null;
        if (!nombrePadre.equals(Categoria.PRINCIPAL)) {
            padre = ManejadorNegocios.getInstance().getCategoria(nombrePadre);
            if (padre.getTieneProductos()) {
                throw new YaExisteException("La categoria padre no admite hijos.");
            }
        }
        int id = ManejadorNegocios.getInstance().autoIdCategoria();
        Categoria c = new Categoria(id, nombre, padre, tieneProductos);
        ManejadorNegocios.getInstance().putCategoria(id, c);
        if (!nombrePadre.equals(Categoria.PRINCIPAL)) {
            ManejadorNegocios.getInstance().getCategoria(nombrePadre).agregarSubcategoria(c);
        }
        return c.getId();
    }

    private void arreglarCategorias(Producto p, List<Categoria> lista_categs) {
        for (int k = 0; k < lista_categs.size(); k++) {
            lista_categs.get(k).agregarProducto(p);
        }

    }

    private List<Categoria> obtenerListaCat(List<String> categs) throws NoSeEncontroException {
        List<Categoria> lista_categs = new ArrayList<>();
        for (int i = 0; i < categs.size(); i++) {
            if (!ManejadorNegocios.getInstance().containsKeyCategorias(categs.get(i))) {
                throw new NoSeEncontroException("Categoria no encontrada.");
            }
            Categoria c = ManejadorNegocios.getInstance().getCategoria(categs.get(i));
            lista_categs.add(c);
        }
        return lista_categs;
    }

    private void verificarCategoriasTP(List<Categoria> lista_categs) throws NegocioException {
        if (lista_categs.isEmpty()) {
            throw new NegocioException("categoria", "El producto debe pertenecer al menos a una categoria");
        }
        for (Categoria cat : lista_categs) {
            if (!cat.getTieneProductos()) {
                throw new NegocioException("categoria", "La categoría " + cat.getNombre() + " no puede contener productos");
            }
        }
    }

    @Override
    public List<DataProductoBas> ListarProductos() {
        ArrayList<DataProductoBas> res = new ArrayList<>();
        for (Producto p : ManejadorNegocios.getInstance().valuesProducto()) {
            res.add(aDataProductoBas(p));
        }
        Collections.sort(res);
        return res;
    }

    @Override
    public DataProducto obtenerDatosProducto(int nroRef) throws DirectMarketException {
        Producto prod = ManejadorNegocios.getInstance().getProducto(nroRef);
        DataProducto dp = aDataProducto(prod);
        return dp;
    }

    private boolean verificarTitulo(String titulo, Producto original) {
        titulo = titulo.trim();
        for (Producto p : ManejadorNegocios.getInstance().valuesProducto()) {
            // comprueba si el titulo ya existe que no sea el que se está modificando
            if (p.getTitulo().equals(titulo) && !p.equals(original)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void ModificarProducto(int NroRef, String titulo, double precio, String descripcion, String especificacion, List<String> categorias) throws DirectMarketException {
        Producto prodModificar = ManejadorNegocios.getInstance().getProducto(NroRef);
        if (!verificarTitulo(titulo, prodModificar)) {
            throw new YaExisteException("titulo", "El titulo ingresado ya esta registrado en el sistema y es utilizado por otro producto");
        }
        if (precio < 0) {
            throw new NegocioException("precio", "el precio debe ser positivo");
        }
        prodModificar.setTitulo(titulo);
        prodModificar.setPrecio(precio);
        prodModificar.setDescripcion(descripcion);
        prodModificar.setEspecificacion(especificacion);
        List<Categoria> categs = obtenerListaCat(categorias);
        verificarCategoriasTP(categs);
        for (Categoria c : categs) {
            c.eliminarProducto(prodModificar);
        }
        prodModificar.getCategorias().clear();
        prodModificar.setCategorias(categs);
        arreglarCategorias(prodModificar, categs);

        //Faltan modificar las imagenes

    }

    public void setImagenLocalProducto(Integer refProducto, List<String> nombreImagenes) {
        try {
            Producto prod = ManejadorNegocios.getInstance().getProducto(refProducto);
            prod.setImagenes(new ArrayList<>(nombreImagenes));
            DataImagen di = imghelper.cargarImagen(nombreImagenes.get(0));
            manejarThumb(prod, Arrays.asList(di), 0);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void altaProducto(String tit, int nroRef, String desc, String especif, double precio, String nickProv, List<String> categs, Integer logo) throws DirectMarketException {
        altaProducto(tit, nroRef, desc, especif, precio, nickProv, categs, new ArrayList<DataImagen>(), logo);
    }

    @Override
    public void cambiarImagenesProducto(int NroRef, List<DataImagen> imagenes) {
        Producto p = ManejadorNegocios.getInstance().getProducto(NroRef);
        /* temporalmente no borra las imagenes, simplemente las pasa por arriba si ya existen
         * asi no nos rompe los datos de prueba
         for (String elim : p.getImagenes()) {
         imghelper.borrarImagen(elim);
         }*/
        p.borrarImagenes();
        manejarImagenes(p, imagenes);

    }

    @Override
    public List<DataProductoBas> ProductosPorProveedor(String nick) {
        List<Producto> prod_proveedor = ManejadorNegocios.getInstance().ProductosProveedor(nick);
        List<DataProductoBas> res = new ArrayList<>();
        for (Producto u : prod_proveedor) {
            res.add(aDataProductoBas(u));
        }
        return res;
    }

    private boolean containsCase(String text, String query, boolean sensitive) {
        if (sensitive) {
            return text.contains(query);
        } else {
            return text.toLowerCase().contains(query.toLowerCase());
        }
    }

    @Override
    public List<DataProductoBas> buscarProductos(String cadena) {
        if (cadena == null) {
            cadena = "";
        }
        boolean caseSensitive = cadena.trim().length() <= 3;
        Set<DataProductoBas> res = new TreeSet<>();
        cadena = cadena.trim();

        for (Producto p : ManejadorNegocios.getInstance().valuesProducto()) {
            if (containsCase(p.getTitulo(), cadena, false)) { // siempre insensitivo
                res.add(aDataProductoBas(p));
            } else if (containsCase(p.getProvedor().getNick(), cadena, caseSensitive)
                    || containsCase(p.getProvedor().getNombre(), cadena, caseSensitive)
                    || containsCase(p.getProvedor().getApellido(), cadena, caseSensitive)) {
                res.add(aDataProductoBas(p));
            }
            Collection<String> cats = todasCategorias(p);
            for (String c : cats) {
                if (containsCase(c, cadena, caseSensitive)) {
                    res.add(aDataProductoBas(p));
                }
            }
        }
        List<DataProductoBas> ret = new ArrayList<>(res);
        return ret;
    }

    private Collection<String> todasCategorias(Producto p) {
        Collection<String> res = new TreeSet<>();
        for (Categoria c : p.getCategorias()) {
            res.add(c.getNombre());
            String padre = c.getNombrePadre();
            while (!padre.equals(Categoria.PRINCIPAL)) {
                res.add(padre);
                padre = ManejadorNegocios.getInstance().getCategoria(padre).getNombrePadre();
            }
        }
        return res;
    }

    @Override
    public boolean existeProducto(int ref) {
        return ManejadorNegocios.getInstance().containsKeyProducto(ref);
    }

    @Override
    public boolean tituloDisponible(String titulo) {
        return verificarTitulo(titulo, null);
    }

    @Override
    public void puntuarProducto(String nick, int nroRef, int valor) {
        if (valor <= 5 && !existePuntuacion(nick, nroRef)) {
            Producto p = ManejadorNegocios.getInstance().getProducto(nroRef);
            Cliente c = ManejadorUsuarios.getInstance().getCliente(nick);
            Puntaje punt = new Puntaje(valor, c, p);
            p.agregarPuntaje(punt);
        }
    }

    @Override
    public int obtenerValorPuntaje(String nick, int nroRef) {
        int valor = 0;
        Producto prod = ManejadorNegocios.getInstance().getProducto(nroRef);
        ArrayList<Puntaje> list_pjs = prod.getPuntajes();
        for (Puntaje p : list_pjs) {
            if (p.getCliente().getNick().equals(nick)) {
                valor = p.getValor();
                break;
            }
        }
        return valor;
    }

    @Override
    public boolean existePuntuacion(String nick, int nroRef) {
        boolean existe = false;
        Producto prod = ManejadorNegocios.getInstance().getProducto(nroRef);
        ArrayList<Puntaje> list_pjs = prod.getPuntajes();
        for (Puntaje p : list_pjs) {
            if (p.getCliente().getNick().equals(nick)) {
                existe = true;
                break;
            }
        }
        return existe;
    }

    @Override
    public void AgregarReclamo(String nick, String texto, int nroRef, Date fecha) {
        Producto p = ManejadorNegocios.getInstance().getProducto(nroRef);
        Cliente c = ManejadorUsuarios.getInstance().getCliente(nick);
        Reclamo r = new Reclamo(ManejadorNegocios.getInstance().autoIdReclamo(), texto, c, p, fecha);
        p.agregarReclamo(r);
    }

    @Override
    public void AgregarReclamo(String nick, String texto, int nroRef) {
        if (texto != null && texto != "") {
            AgregarReclamo(nick, texto, nroRef, new Date());
        } else {
            throw new RuntimeException("No puede hacer reclamo vacio");
        }
    }

    @Override
    public ArrayList<DataReclamo> ListarReclamos(String nick) {
        ArrayList<DataReclamo> reclamos = ManejadorUsuarios.getInstance().getProveedor(nick).getReclamos();
        Collections.sort(reclamos);
        return reclamos;
    }

    @Override
    public ArrayList<DataReclamo> ListarReclamosProducto(String cliente, int ref) {
        Producto p = ManejadorNegocios.getInstance().getProducto(ref);
        ArrayList<DataReclamo> res = new ArrayList<>();
        for (DataReclamo r : p.getProvedor().getReclamos()) {
            if (r.getCliente().equals(cliente)) {
                res.add(r);
            }
        }
        return res;
    }

    @Override
    public boolean existeReclamoDeCliente(String cliente, int ref) {
        Producto p = ManejadorNegocios.getInstance().getProducto(ref);
        boolean res = false;
        for (DataReclamo r : p.getProvedor().getReclamos()) {
            if (r.getCliente().equals(cliente)) {
                res = true;
                break;
            }
        }
        return res;
    }

    @Override
    public void atenderReclamo(int id, String respuesta, int nroRef) {
        Producto p = ManejadorNegocios.getInstance().getProducto(nroRef);
        p.atenderReclamo(id, respuesta);
    }

    @Override
    public boolean comproProducto(String nick, int nroRef) {
        Cliente cliente = ManejadorUsuarios.getInstance().getCliente(nick);
        return cliente.comproProducto(nroRef);
    }
}
