/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.logica.interfaces.IImagenCtrl;
import directmarket.logica.utilidades.ImagenesHelper;
import java.io.IOException;

/**
 *
 * @author Guillermo
 */
public class ControladorImagenes implements IImagenCtrl {

    @Override
    public void setCarpetaImagenes(String ruta) {
        ImagenesHelper.setDirectorioPrincipal(ruta);
    }

    @Override
    public String getCarpetaImagenes() {
        return ImagenesHelper.getDirectorioPrincipal();

    }

    @Override
    public DataImagen getImagen(String nombre) {
        try {
            if (nombre.startsWith("product") || nombre.startsWith("thumb_p")) {
                return ImagenesHelper.Productos().cargarImagen(nombre);
            } else {
                return ImagenesHelper.Usuarios().cargarImagen(nombre);
            }
        } catch (IOException ex) {
            return null;
        }
    }
}
