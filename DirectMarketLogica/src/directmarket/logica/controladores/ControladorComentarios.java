/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.accesoDatos.dataTypes.DataComentario;
import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Producto;
import directmarket.logica.interfaces.IComentarios;
import directmarket.logica.manejadores.ManejadorNegocios;
import directmarket.logica.manejadores.ManejadorUsuarios;
import java.util.Date;
import java.util.List;

/**
 *
 * @author guillermo
 */
public class ControladorComentarios implements IComentarios{
    
    private int crearComentario(String texto, Date fecha, String nick, int ref, Integer idPadre) {
        Cliente c = ManejadorUsuarios.getInstance().getCliente(nick);
        Producto p = ManejadorNegocios.getInstance().getProducto(ref);
        if (c == null) {
            throw new RuntimeException("No existe cliente con nick = " + nick);
        }
        if (p == null) {
            throw new RuntimeException("No existe producto con ref = " + ref);
        }
        if (!puedeComentar(nick, ref)) {
            throw new RuntimeException("Usuario no tiene derechos para comentar este producto");
        }
        if (texto == null) {
            throw new RuntimeException("No se puede agregar comentario vacio");
        }
        int id = ManejadorNegocios.getInstance().autoIdComentario(); // id automatico unico en sistema
        if (idPadre == null) {
            p.agregarComentario(id, texto, fecha, c);
        } else {
            if (!p.contieneComId(idPadre)) {
                throw new RuntimeException("No existe comentario cal cual se quiere responder");
            }
            p.agregarRespuesta(id, texto, fecha, c, idPadre);
        }
        return id;
    }

    @Override
    public int agregarComentario(String texto, Date fecha, String nick, int ref) {
        return crearComentario(texto, fecha, nick, ref, null);
    }

    @Override
    public int agregarRespuesta(String texto, Date fecha, String nick, int ref, int idPadre) {
        return crearComentario(texto, fecha, nick, ref, idPadre);
    }

    @Override
    public boolean puedeComentar(String nick, int refProd) {
        Cliente c = ManejadorUsuarios.getInstance().getCliente(nick);
        return c.comproProducto(refProd);
    }

  

    @Override
    public int agregarComentario(String texto, String nick, int ref) {
        return agregarComentario(texto, new Date(), nick, ref);
    }

    @Override
    public int agregarRespuesta(String texto, String nick, int ref, int idPadre) {
        return agregarRespuesta(texto, new Date(), nick, ref, idPadre);
    }

    @Override
    public List<DataComentario> listarComentarios(int nroRef) {
        Producto p = ManejadorNegocios.getInstance().getProducto(nroRef);
        return p.listarComentarios();
    }
    
}
