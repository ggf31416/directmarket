/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.accesoDatos.dataTypes.DataOrden;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.excepciones.CarritoException;
import directmarket.excepciones.DirectMarketException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Estado;
import directmarket.logica.entidades.Linea;
import directmarket.logica.entidades.OrdenDeCompra;
import directmarket.logica.entidades.Producto;
import directmarket.logica.entidades.TipoEstado;
import directmarket.logica.entidades.Notificacion;
import directmarket.logica.interfaces.IOrdenes;
import directmarket.logica.manejadores.ManejadorNegocios;
import directmarket.logica.manejadores.ManejadorUsuarios;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author guillermo
 */
public class ControladorOrdenes implements IOrdenes {

    private ControladorProductos ctrlProd;

    public ControladorOrdenes(ControladorProductos ctrlProd) {
        this.ctrlProd = ctrlProd;
    }

    @Override
    public void elegirClienteOrden(String nick) {
        if (!ManejadorNegocios.getInstance().containsKeyCarritos(nick)) {
            Cliente c = ManejadorUsuarios.getInstance().getCliente(nick);
            if (c == null) {
                throw new RuntimeException("No se encontró cliente con ese nick");
            }
            OrdenDeCompra ord = new OrdenDeCompra(c);
            ManejadorNegocios.getInstance().putCarrito(nick, ord);
        }
    }

    @Override
    public List<DataOrden> listarOrdenes() {
        ArrayList<DataOrden> res = new ArrayList<>();
        for (OrdenDeCompra o : ManejadorNegocios.getInstance().valuesOrdenDeCompra()) {
            res.add(o.getData());
        }
        return res;
    }

    @Override
    public List<DataOrden> listarOrdenesRecibidas() {
        ArrayList<DataOrden> res = new ArrayList<>();
        for (OrdenDeCompra o : ManejadorNegocios.getInstance().valuesOrdenDeCompra()) {
            if (o.obtenerEstadoActual().getTipoEstado() == TipoEstado.RECIBIDA) {
                res.add(o.getData());
            }
        }
        return res;
    }

    @Override
    public DataOrdenLin InformacionOrden(int nro) {
        if (ManejadorNegocios.getInstance().containsKeyOrdenDeCompra(nro)) {
            return ManejadorNegocios.getInstance().getOrdenDeCompra(nro).getDataLin();
        }
        return null;
    }

    /*@Override
     public List<DataOrdenLin> listarOrdenesLin() {
     ArrayList<DataOrdenLin> res = new ArrayList<>();
     for (OrdenDeCompra o : ManejadorNegocios.getInstance().valuesOrdenDeCompra()) {
     res.add(o.getDataLin());
     }
     return res;
     }*/
    @Override
    public void eliminarLinea(String nick, Integer ref) throws CarritoException {
        if (!ManejadorNegocios.getInstance().containsKeyCarritos(nick)) {
            throw new CarritoException("El carrito ya está confirmado");
        }
        OrdenDeCompra orden = ManejadorNegocios.getInstance().getCarrito(nick);
        orden.eliminarProducto(ref);
    }

    /*
     @Override
     public DataOrdenLin getInfoOrden(int numero) {
     return ManejadorNegocios.getInstance().getOrdenDeCompra(numero).getDataLin();
     }
     */
    @Override
    public void eliminarOrden(int nro) throws NoSeEncontroException {
        if (ManejadorNegocios.getInstance().containsKeyOrdenDeCompra(nro)) {
            OrdenDeCompra o = ManejadorNegocios.getInstance().getOrdenDeCompra(nro);
            o.cancelar();
            ManejadorNegocios.getInstance().removeOrdenDeCompra(nro);
        } else {
            throw new NoSeEncontroException("No existe orden #" + nro);
        }
    }

    @Override
    public void ordenCancelada(int nro, Date fecha) throws NoSeEncontroException, DirectMarketException {
        if (ManejadorNegocios.getInstance().containsKeyOrdenDeCompra(nro)) {
            OrdenDeCompra o = ManejadorNegocios.getInstance().getOrdenDeCompra(nro);
            // me parece innecesario ademas romperia todo si el servidor tuviera mal la hora durante un momento
            /*Date fechaAnterior = o.obtenerEstadoActual().getFecha();
            if(fechaAnterior.compareTo(fecha) > 0) {
                throw new DirectMarketException("Fecha incorrecta.");
            }*/
            if(o.obtenerEstadoActual().getTipoEstado()!=TipoEstado.RECIBIDA) {
                throw new DirectMarketException("La orden #" + nro +
                        " ya no puede cancelarse.");
            }
            o.setEstados(new Estado(TipoEstado.CANCELADA, fecha));
            Notificacion aviso = new Notificacion(o);
            o.getPtrSubj().notifyObservers(aviso);
        } else {
            throw new NoSeEncontroException("No existe orden #" + nro);
        }
    }

    @Override
    public void ordenPreparada(int nro, Date fecha) throws NoSeEncontroException, DirectMarketException {
        if (ManejadorNegocios.getInstance().containsKeyOrdenDeCompra(nro)) {
            OrdenDeCompra o = ManejadorNegocios.getInstance().getOrdenDeCompra(nro);
            /*Date fechaAnterior = o.obtenerEstadoActual().getFecha();
            if(fechaAnterior.compareTo(fecha) > 0) {
                throw new DirectMarketException("Fecha incorrecta.");
            }*/
            if(o.obtenerEstadoActual().getTipoEstado()!=TipoEstado.RECIBIDA) {
                throw new DirectMarketException("La orden #" + nro + 
                        " no está en estado RECIBIDA.");
            }
            
            o.setEstados(new Estado(TipoEstado.PREPARADA, fecha));
            Notificacion aviso = new Notificacion(o);
            o.getPtrSubj().notifyObservers(aviso);
        } else {
            throw new NoSeEncontroException("No existe orden #" + nro);
        }
    }

    @Override
    public void ordenConfirmada(int nro, Date fecha) throws NoSeEncontroException, DirectMarketException {
        if (ManejadorNegocios.getInstance().containsKeyOrdenDeCompra(nro)) {
            OrdenDeCompra o = ManejadorNegocios.getInstance().getOrdenDeCompra(nro);
            /*Date fechaAnterior = o.obtenerEstadoActual().getFecha();
            if(fechaAnterior.compareTo(fecha) > 0) {
                throw new DirectMarketException("Fecha incorrecta.");
            }*/
            if(o.obtenerEstadoActual().getTipoEstado()!=TipoEstado.PREPARADA) {
                throw new DirectMarketException("Es imposible que " + o.getCliente() +
                        " confirme la orden #" + nro + " porque no está en estado PREPARADA.");
            }
            o.setEstados(new Estado(TipoEstado.CONFIRMADA, fecha));
            this.agregarObserversDeProdYProv(o);
            Notificacion aviso = new Notificacion(o);
            o.getPtrSubj().notifyObservers(aviso);
        } else {
            throw new NoSeEncontroException("No existe orden #" + nro);
        }
    }

    private void agregarObserversDeProdYProv(OrdenDeCompra ord) {
        /* Este método es llamado cuando una orden pasa a estado CONFIRMADA.
         * Lo que hace es, para cada una de las líneas:
         *  -Agregar el cliente a la colección de observadores del producto.
         *  -Obtener el proveedor de cada producto, y agregar el cliente a la
         *  colección de observadores del proveedor.
         */
        Cliente obs = ord.getCliente();
        for(Linea l : ord.getLineas().values()){
            // No es una lista, es un hashmap
            Producto p = l.getProducto();
            p.getPtrSubj().addObserver(obs);
            p.getProvedor().getPtrSubj().addObserver(obs);
        }

    }

    @Override
    public DataOrdenLin recibirOrden(String nick) throws CarritoException {

        return recibirOrden(nick, new Date());
    }

    @Override
    public DataOrdenLin recibirOrden(String nick, Date fecha) throws CarritoException {
        if (ManejadorNegocios.getInstance().containsKeyCarritos(nick)) {
            OrdenDeCompra ord = ManejadorNegocios.getInstance().getCarrito(nick);
            if (ord.cantLineas() > 0) {
                ord.setFecha(fecha);
                int nroOrdenNuevo = ManejadorNegocios.getInstance().getCantidadOrdenes() + 1;
                ord.setNroOrden(nroOrdenNuevo);
                ManejadorNegocios.getInstance().putOrdenDeCompra(ord.getNroOrden(), ord);
                ord.recibir(fecha);
                ord.getPtrSubj().addObserver(ord.getCliente());
                ManejadorNegocios.getInstance().removeCarrito(nick);
                return ord.getDataLin();
            } else {
                return new DataOrdenLin();
            }

        } else {
            throw new CarritoException("El carrito ya está confirmado");
        }
    }

    @Override
    public DataOrdenLin getCarrito(String nick) {
        if (!ManejadorNegocios.getInstance().containsKeyCarritos(nick)) {
            return new DataOrdenLin();
        }
        return ManejadorNegocios.getInstance().getCarrito(nick).getDataLin();
    }

    @Override
    public double getCarritoTotal(String nick) {
        if (!ManejadorNegocios.getInstance().containsKeyCarritos(nick)) {
            return 0;
        }
        return ManejadorNegocios.getInstance().getCarrito(nick).getTotal();
    }

    /**
     * Agrega un producto a la orden de compra actual del usuario
     *
     * @param nick Nick del usuario cliente
     * @param ref Referencia del producto
     * @param cant Cantidad del producto
     * @return Total de la factura
     */
    @Override
    public boolean agregarProductoLinea(String nick, Integer ref, Integer cant) {
        if (!ManejadorNegocios.getInstance().containsKeyCarritos(nick)) {
            elegirClienteOrden(nick);
        }
        if (cant > 0) {
            Producto prod = ManejadorNegocios.getInstance().getProducto(ref);
            OrdenDeCompra ord = ManejadorNegocios.getInstance().getCarrito(nick);
            return ord.agregarProducto(cant, prod);
        }
        return false;
    }

    @Override
    public List<DataProductoBas> productosPorCategoria(String nombreCateg) throws NoSeEncontroException {
        return ctrlProd.productosPorCategoria(nombreCateg);
    }

    @Override
    public List<DataProductoBas> productosPorCategoria(int idCateg) throws NoSeEncontroException {
        return ctrlProd.productosPorCategoria(idCateg);
    }

    @Override
    public List<DataCategoria> listarCategoriasConProducto() {
        return ctrlProd.listarCategoriasConProducto();
    }

    @Override
    public boolean carritoContieneProducto(String nick, int ref) {
        OrdenDeCompra ord = ManejadorNegocios.getInstance().getCarrito(nick);
        if (ord == null) {
            return false;
        }
        return ord.contieneProducto(ref);
    }

    @Override
    public void borrarCarrito(String nick) {
        ManejadorNegocios.getInstance().removeCarrito(nick);
    }
    
        
    @Override
    public List<DataOrden> listarOrdenes(String nick){
        ArrayList<DataOrden> res = new ArrayList<>();
        for (OrdenDeCompra o : ManejadorUsuarios.getInstance().getCliente(nick).getOrdenes()) {
            res.add(o.getData());
        }
        return res;

    }
}
