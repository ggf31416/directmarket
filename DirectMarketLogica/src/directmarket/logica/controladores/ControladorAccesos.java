/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.logica.entidades.Acceso;
import directmarket.logica.interfaces.IAccesos;
import directmarket.logica.manejadores.ManejadorNegocios;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author tprog088
 */
public class ControladorAccesos implements IAccesos {

    @Override
    public void guardarAcceso(String ip, String url, GregorianCalendar fecha, String userAgent) {
        try {
            LinkedList<Acceso> accesos = ManejadorNegocios.getInstance().getAccesos();
            Iterator<Acceso> it = accesos.descendingIterator();

            while (it.hasNext() && it.next().antiguedad(fecha) > 30) {
                it.remove(); // borra los que tienen mas de 30 dias
            }
            if (accesos.size() == 10000) {
                accesos.removeLast(); // borra el primero
            }
            String so = soDesdeUA(userAgent);
            String br = browserDesdeUA(userAgent);
            Acceso acc = new Acceso(ip, url, br, so, fecha);
            accesos.addFirst(acc); // agrego al final
        } catch (Exception ex) {
            // existe un error muy esporadico pero no puedo debugearlo,  lo unico que se pierde es un acceso
        }


    }

    @Override
    public LinkedList<Acceso> listarAccesos() {
        LinkedList<Acceso> accesos = ManejadorNegocios.getInstance().getAccesos();
        Iterator<Acceso> it = accesos.descendingIterator();

        while (it.hasNext() && it.next().antiguedad(new GregorianCalendar()) > 30) {
            it.remove(); // borra los que tienen mas de 30 dias
        }


        return (LinkedList<Acceso>) accesos.clone();
    }

    // http://user-agent-string.info/list-of-ua
    // https://developer.mozilla.org/en-US/docs/Browser_detection_using_the_user_agent
    private String soDesdeUA(String ua) {
        String lua = ua.toLowerCase();
        if (lua.contains("windows")) {
            return "Windows";
        } else if (lua.contains("android")) {
            return "Android";
        } else if (lua.contains("linux")) {
            return "Linux";
        } else if (lua.contains("macintosh")) {
            return "Mac";
        } else if (lua.contains("ipad") || lua.contains("iphone")) {
            return "iOS";
        } else if (lua.contains("blackberry")) {
            return "BlackBerry OS";
        } else {
            return "Desconocido";
        }
    }

    private String browserDesdeUA(String ua) {
        if (ua.contains("Seamonkey/")) {
            return "SeaMonkey";
        } else if (ua.contains("Firefox/")) {
            return "Firefox";
        } else if (ua.contains("Chromium/")) {
            return "Chromium";
        } else if (ua.contains("Chrome/")) {
            return "Chrome";
        } else if (ua.contains("MSIE")) {
            return "IE";
        } else if (ua.contains("Opera/")) {
            return "Opera";
        } else if (ua.contains("konqueror/")) {
            return "Konqueror";
        } // Safari es muy dificil de detectar con precisión
          else if (ua.contains("Safari/") && !ua.contains("Chrome/")){ 
              if(ua.contains("Linux")) return "Konqueror";
              else return "Safari";
        } else {
            return "Desconocido";
        }
    }
}
