/*
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.accesoDatos.dataTypes.DataCliente;
import directmarket.accesoDatos.dataTypes.DataEstadistica;
import directmarket.logica.entidades.Estadistica;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.accesoDatos.dataTypes.DataInfoCliente;
import directmarket.accesoDatos.dataTypes.DataInfoProveedor;
import directmarket.accesoDatos.dataTypes.DataLogueo;
import directmarket.accesoDatos.dataTypes.DataMensaje;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import directmarket.accesoDatos.dataTypes.DataProveedor;
import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import directmarket.accesoDatos.dataTypes.RespuestaChat;
import directmarket.excepciones.DirectMarketException;
import directmarket.excepciones.MultiExcepcion;
import directmarket.excepciones.Multierror;
import directmarket.excepciones.NegocioException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.YaExisteException;
import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Mensaje;
import directmarket.logica.entidades.OrdenDeCompra;
import directmarket.logica.entidades.Proveedor;
import directmarket.logica.entidades.TipoNotificacion;
import directmarket.logica.entidades.Usuario;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.interfaces.IUsuario;
import directmarket.logica.manejadores.ManejadorNegocios;
import directmarket.logica.manejadores.ManejadorUsuarios;
import directmarket.logica.utilidades.ImagenesHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import org.jasypt.util.password.BasicPasswordEncryptor;

/**
 *
 * @author fabian
 */
public class ControladorUsuarios implements IUsuario {

    public ControladorUsuarios() {
    }

    @Override
    public boolean ValidarNick(String nick) {
        return !(nick.isEmpty() || ManejadorUsuarios.getInstance().containsKeyUsuario(nick));
    }

    private DataCliente dcDesdeCliente(Cliente c) {
        DataImagen di = getAvatar(c.getNick());
        DataCliente dc = c.getDataCliente();
        dc.setImagen(di);
        return dc;
    }

    private DataProveedor dpDesdeProv(Proveedor p) {
        DataProveedor dp = p.getDataProv();
        dp.setEstadisticas(crearEstadisticas(p.getNick(), new GregorianCalendar()));
        return dp;
    }

    @Override
    public DataProveedor VerInformacionProveedor(String nick) throws NoSeEncontroException {
        Proveedor p = ManejadorUsuarios.getInstance().getProveedor(nick);
        if (p == null) {
            throw new NoSeEncontroException("El nick introducido no corresponde a un proveedor.");
        }
        DataProveedor dp = dpDesdeProv(p);
        DataImagen di = getAvatar(p.getNick());
        dp.setImagen(di);
        return dp;
    }

    @Override
    public DataInfoProveedor VerInfoProveedor(String nick) throws NoSeEncontroException {
        Proveedor p = ManejadorUsuarios.getInstance().getProveedor(nick);
        if (p == null) {
            throw new NoSeEncontroException("El nick introducido no corresponde a un proveedor.");
        }
        DataProveedor dp = dpDesdeProv(p);
        return dp;
    }

    public boolean fechaValida(Date d) {
        // consideremos que tiene al menos 1 año. (la aduana me pidio que no deje recién nacidos
        // comprar en el exterior... ;)
        TimeUnit tu = TimeUnit.DAYS;
        long dias = tu.convert(new Date().getTime() - d.getTime(), TimeUnit.MILLISECONDS);
        return dias >= 365;
    }

    private Multierror validarDatos(Usuario u) {
        Multierror mu = new Multierror();
        if (!ValidarNick(u.getNick())) {
            mu.agregarError(new YaExisteException("nick", "El nick introducido ya está en uso."));
        }
        if (!nombreValido(u.getNombre())) {
            mu.agregarError(new NegocioException("nombre", "El nombre es inválido."));
        }
        if (!nombreValido(u.getApellido())) {
            mu.agregarError(new NegocioException("apellido", "El apellido es inválido."));
        }
        if (!fechaValida(u.getNacimiento())) {
            mu.agregarError(new NegocioException("nacimiento", "La fecha de nacimiento no es válida."));
        }
        if (existeEmail(u.getEmail())) {
            mu.agregarError(new YaExisteException("correo", "El correo ya está registrado en el Sistema."));
        }

        try {
            esMail(u.getEmail());
        } catch (DirectMarketException ex) {
            mu.agregarError(ex);
        }
        return mu;
    }

    @Override
    public void mailValido(String email) throws YaExisteException, NegocioException {
        if (existeEmail(email)) {
            throw new YaExisteException("correo", "El correo ya está registrado en el Sistema.");
        }
        esMail(email);

    }

    private void manejarImagen(Usuario u, DataImagen imagen) {
        ImagenesHelper ih = ImagenesHelper.Usuarios();
        ih.cambiarNombre(imagen, "u_" + u.getNick());
        String img = (imagen != null && imagen.getDatos() != null) ? ih.guardarImagen(imagen, true) : null;
        u.setImagen(img);
    }

    public void esMail(String correo) throws NegocioException {
        //RFC822 
        Pattern ptr = Pattern.compile("(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*:(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)(?:,\\s*(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*))*)?;\\s*)");
        if (correo.isEmpty()) {
            throw new NegocioException("correo", "Falta ingresar el correo.");
        }
        if (correo.contains(" ") || !ptr.matcher(correo).matches()) {
            throw new NegocioException("correo", "El correo no es válido.");
        }
    }

    public boolean nombreValido(String txt) {
        txt = txt.trim();
        if (txt.isEmpty()) {
            return false;
        }
        for (int i = 0; i < txt.length(); i++) {
            char c = txt.charAt(i);
            if (Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void AltaUsuario(String nick, String correo, String nombre,
            String apellido, Date fecha, DataImagen imagen, String pass) throws MultiExcepcion {
        Usuario u = new Cliente(nick, correo, nombre, apellido, fecha, null);
        Multierror mu = validarDatos(u);
        String encryptedPassword = encriptarContraseña(pass);
        if (mu.hayError()) {
            throw new MultiExcepcion(mu);
        }
        if (imagen != null && imagen.getNombre() != null) {
            manejarImagen(u, imagen);
        }
        storeEncryptedPassword(nick,encryptedPassword);
        ManejadorUsuarios.getInstance().putUsuario(nick, u);
    }

    private boolean webValida(String web) {

        //String regex = "(https?)://[a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        //String regex = "[a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        String regex = "^((https?://)|([^:]+)\\.)(.+)\\.(.+)";
        return !web.isEmpty() && web.trim().matches(regex);
    }

    private boolean companiaValida(String compania) {
        return !compania.isEmpty();
    }

    @Override
    public void AltaProveedor(String nick, String correo, String nombre, String apellido, Date fecha, String compania, String web, DataImagen imagen, String pass) throws MultiExcepcion {

        Usuario u = new Proveedor(nick, correo, nombre, apellido, fecha, "", compania, web);
        String encryptedPassword = encriptarContraseña(pass);
        Multierror mu = validarDatos(u);
        if (!companiaValida(compania)) {
            //throw  new RuntimeException(compania);
            mu.agregarError(new NegocioException("compania", "La compañia no puede estar vacía."));
        }
        if (!webValida(web)) {
            //throw  new RuntimeException(compania);
            mu.agregarError(new NegocioException("web", "La web no es válida."));
        }
        if (mu.hayError()) {
            throw new MultiExcepcion(mu);
        }
        storeEncryptedPassword(nick,encryptedPassword);
        manejarImagen(u, imagen);
        ManejadorUsuarios.getInstance().putUsuario(nick, u);
    }

    @Override
    public DataImagen getAvatar(String nick) {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        if (u.getImagen() != null && !u.getImagen().trim().isEmpty()) {
            try {
                return ImagenesHelper.Usuarios().cargarImagen(u.getImagen());
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    @Override
    public List<DataUsuarioBas> listarProveedores() {
        ArrayList<DataUsuarioBas> prov = new ArrayList<>();
        for (Usuario u : ManejadorUsuarios.getInstance().valuesUsuario()) {
            if (u instanceof Proveedor) {
                prov.add(u.getDataBas());
            }
        }
        return prov;

    }

    @Override
    public List<DataUsuarioBas> listarClientes() {
        ArrayList<DataUsuarioBas> clientes = new ArrayList<>();
        for (Usuario u : ManejadorUsuarios.getInstance().valuesUsuario()) {
            if (u instanceof Cliente) {
                clientes.add(u.getDataBas());
            }
        }
        return clientes;

    }

    @Override
    public DataCliente VerInformacionCliente(String nick) throws NoSeEncontroException {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        if (u instanceof Cliente) {
            DataCliente dc = dcDesdeCliente((Cliente) u);
            return dc;
        } else {
            throw new NoSeEncontroException("El nick introducido no corresponde a un Cliente.");
        }
    }

    @Override
    public DataInfoCliente VerInfoCliente(String nick) throws NoSeEncontroException {
        return (DataInfoCliente) VerInformacionCliente(nick);
    }

    public Usuario getUsuario(String nick) {
        return ManejadorUsuarios.getInstance().getUsuario(nick);
    }

    public void setImagenLocalCliente(String nick, String imagen) {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        u.setImagen(imagen);
    }

    private boolean existeEmail(String mail) {
        for (Usuario u : ManejadorUsuarios.getInstance().valuesUsuario()) {
            if (u.getEmail().equalsIgnoreCase(mail)) {
                return true;
            }
        }
        return false;
    }

    public String encriptarContraseña(String userPassword) {
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        String encryptedPassword = passwordEncryptor.encryptPassword(userPassword);
        return encryptedPassword;
    }
    
    @Override
    public DataLogueo autenticar(String nick, String inputPassword) throws NoSeEncontroException {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        String encryptedPassword = getEncryptedPassword(nick); // obtengo contraseña encriptada de passwords.properties
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        if (u != null && passwordEncryptor.checkPassword(inputPassword, encryptedPassword)) {
            int idCon = agregaraConectados(nick);
            return new DataLogueo(idCon, u.getDataBas());
        } else {
            throw new NoSeEncontroException("Usuario y/o contraseña inválidos.");
        }
    }    

    public void storeEncryptedPassword(String nick, String encryptedPassword) {
        String dir = System.getProperty("user.home") + "/.Direct Market/" + "passwords.txt";
        Properties prop = new Properties();
        try {
            FileInputStream is = new FileInputStream(dir);
            prop.load(is);
            FileOutputStream os = new FileOutputStream(dir);
            prop.setProperty(nick, encryptedPassword);
            prop.store(os, "Passwords encriptados");
        } catch (FileNotFoundException ex) { // si no encuentra passwords.properties, lo crea
            try {               
                File f = new File(dir);
                f.getParentFile().mkdirs(); // crea directorio si no existe
                f.createNewFile(); // crea un nuevo archivo
                FileOutputStream os = new FileOutputStream(f);
                prop.setProperty(nick, encryptedPassword);
                prop.store(os, "Passwords encriptados");
            } catch (IOException ex1) {
                ex.printStackTrace();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getEncryptedPassword(String nick) {
        String res = null;
        try {
            String dir = System.getProperty("user.home") + "/.Direct Market/" + "passwords.txt";
            FileInputStream is = new FileInputStream(dir);
            Properties prop = new Properties();
            prop.load(is);
            res = prop.getProperty(nick);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return res;
    }

    @Override
    public DataUsuarioBas getInfoBas(String nick) throws NoSeEncontroException {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        if (u != null) {
            return u.getDataBas();
        } else {
            throw new NoSeEncontroException("No existe usuario con nick " + nick + ".");
        }
    }

    public DataEstadistica crearEstadisticas(String nickProv, Calendar fechaActual) {
        Proveedor prov = ManejadorUsuarios.getInstance().getProveedor(nickProv);

        Calendar inicio = new GregorianCalendar(fechaActual.get(Calendar.YEAR) - 1,
                (fechaActual.get(Calendar.MONTH) + 1), 1, 0, 0, 0);
        inicio.setLenient(true); // por defecto pero no esta de mas, para que cambie de DICIEMBRE + 1 a ENERO

        Estadistica stat = new Estadistica(inicio, fechaActual);
        for (OrdenDeCompra o : ManejadorNegocios.getInstance().valuesOrdenDeCompra()) {
            // optiene un inicio en el proximo mes al actual del año pasado, asi tiene el mes actual y los 11 anteriores

            if (o.getFecha().after(inicio.getTime())) { // si tiene menos de 1 año
                o.agregarEstadisticas(stat, prov.getProductos());
            }
        }
        return stat.getData();
    }

    @Override
    public void cambiarSuscripcionesUsuario(String nickCliente, Set<TipoNotificacion> excluidos) throws NoSeEncontroException {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nickCliente);
        if (u instanceof Cliente) {
            Cliente c = (Cliente) u;
            c.cambiarSuscripcion(excluidos);
        } else {
            throw new NoSeEncontroException("El nick introducido no corresponde a un Cliente.");
        }
    }

    @Override
    public List<DataOrdenLin> listarOrdenesCliente(String nick) {
        Cliente c = ManejadorUsuarios.getInstance().getCliente(nick);
        List<DataOrdenLin> ordenes = new ArrayList<>();
        if (c != null) {
            for (Integer nro : c.getnumdeOrdenes()) {
                OrdenDeCompra o = ManejadorNegocios.getInstance().getOrdenDeCompra(nro);
                ordenes.add(o.getDataLin());
            }
        }
        return ordenes;
    }


    /**
     * Marca como conectado, si no se tiene la contraseña para autenticar
     * @return id conexion
     */
    public int agregaraConectados(String nick) {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        int id = u.conectar(new Date());
        return id;
    }

    
    public void quitardeConectados(String nick, int idConexion) {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        u.desconectar(idConexion);
    }

    @Override
    public boolean estaconectado(String nick) {
        Usuario u = ManejadorUsuarios.getInstance().getUsuario(nick);
        return u.estaConectado(new Date());
    }
    
    @Override
    public RespuestaChat obtenerMensajes(String nick, int idConexion,int despuesDe,String[] pregConexion){
        ManejadorUsuarios mu =  ManejadorUsuarios.getInstance();
        Usuario u = mu.getUsuario(nick);
        u.registrarAcceso(idConexion, new Date());
       RespuestaChat r = u.obtenerMensajes(despuesDe);
        List<String> conectados = new ArrayList<>();
        for(String n : pregConexion){
            Usuario u2 = mu.getUsuario(n);
            if(u2 != null && u2.estaConectado(new Date())){
                conectados.add(n);
            }
        }
        r.setConectados(conectados);
        return r;
    }
    
    public void agregarMensaje(String texto,String remit,String dest){
        ManejadorUsuarios mu = ManejadorUsuarios.getInstance();
        Usuario r = mu.getUsuario(remit);
        Usuario d = mu.getUsuario(dest);
        if (r != null && d != null){
            Mensaje m = new Mensaje(mu.getAutoIdMensaje(),texto, r, d, new GregorianCalendar());
            r.agregarMensaje(m);
            d.agregarMensaje(m);
        }
        else{
            throw new RuntimeException("No existe remit o dest");
        }
    }


}
