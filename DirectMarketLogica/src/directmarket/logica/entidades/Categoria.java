/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

/**
 *
 * @author fabian
 */
import directmarket.accesoDatos.dataTypes.DataCategoria;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Categoria {

    private int id;
    private String nombre;
    private int idPadre;
    private String nombrePadre;
    private boolean tieneProductos;
    private List<Producto> productos;
    private List<Categoria> subcategorias;
    public static final String PRINCIPAL = "Categorías";

    public Categoria() {
        this.productos = new ArrayList<>();
        this.subcategorias = new ArrayList<>();
    }

    public Categoria(int id, String nombre, Categoria padre, boolean tieneProductos) {
        this.id = id;
        this.productos = new ArrayList<>();
        this.subcategorias = new ArrayList<>();
        this.nombre = nombre;
        this.tieneProductos = tieneProductos;
        if (padre != null) {
            this.nombrePadre = padre.getNombre();
            this.idPadre = padre.getId();
        }
        else{
            this.nombrePadre = PRINCIPAL;
            this.idPadre = 0;
        }

    }

    public boolean getTieneProductos() {
        return tieneProductos;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public String getNombre() {
        return nombre;
    }

    public void agregarProducto(Producto prod) {
        productos.add(prod);
    }

    public void agregarSubcategoria(Categoria cat) {
        subcategorias.add(cat);
    }

    public String getNombrePadre() {
        return nombrePadre;
    }

    public List<Categoria> getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategorias(List<Categoria> subcategorias) {
        this.subcategorias = subcategorias;
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Categoria other = (Categoria) obj;
        return Objects.equals(this.nombre, other.nombre);
    }

    public void eliminarProducto(Producto p) {
        productos.remove(p);
    }

    public int getId() {
        return id;
    }

    public int getIdPadre() {
        return idPadre;
    }
    
    public DataCategoria getData(){
        DataCategoria dc = new DataCategoria(id, nombre, nombrePadre, idPadre, tieneProductos);
        return dc;
    }
    
    public DataCategoria getDataRec(){
        DataCategoria dc = getData();
        for(Categoria c : subcategorias){
            dc.getSubcategorias().add(c.getDataRec());
        }
        return dc;
    }
}
