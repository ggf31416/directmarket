/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataReclamo;
import java.util.Date;

/**
 *
 * @author Juan
 */
public class Reclamo {
    private int id;
    private String texto;
    private Cliente cliente;
    private Producto producto;
    private Date fecha;
    private String respuesta;
    private boolean atendido = false;
    private Subject subj;

    public Reclamo(int id, String texto, Cliente cliente, Producto producto, Date fecha) {
        this.id = id;
        this.texto = texto;
        this.cliente = cliente;
        this.producto = producto;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
        Notificacion aviso = new Notificacion(this);
        this.getSubj().notifyObservers(aviso);
    }

    public boolean isAtendido() {
        return atendido;
    }

    public void setAtendido(boolean atendido) {
        this.atendido = atendido;
    }

    public Subject getSubj() {
        return subj;
    }

    public void setSubj(Subject subj) {
        this.subj = subj;
    }
    
    public DataReclamo getDataReclamo(){
        return new DataReclamo(getId(), getTexto(), getFecha(), getCliente().getNick(), getProducto().getNroReferencia(),getProducto().getTitulo(), getRespuesta(), isAtendido());
    }
}
