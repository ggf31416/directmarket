/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataComentario;
import directmarket.accesoDatos.dataTypes.DataProductoBas;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author fabian
 */
public class Producto {

    private String titulo;
    private int nroReferencia;
    private String descripcion;
    private String especificacion;
    private double precio;
    private Proveedor proveedor;
    private ArrayList<Reclamo> reclamos = new ArrayList<>();
    private ArrayList<Puntaje> puntajes = new ArrayList<>();
    private List<Categoria> categorias = new ArrayList<>();
    private List<String> imagenes = new ArrayList<>();
    private Map<Integer, Comentario> comentarios = new TreeMap<>();
    private int cantidadComprada = 0;
    private String logo;
    private Subject ptrSubj;

    public Producto() {
    }

    public Producto(String titulo, int nroReferencia, String descripcion, String especificacion, double precio,
            Proveedor proveedor, List<Categoria> categorias) {
        this.titulo = titulo;
        this.nroReferencia = nroReferencia;
        this.descripcion = descripcion;
        this.especificacion = especificacion;
        this.precio = precio;
        this.proveedor = proveedor;
        this.categorias = categorias;
        this.imagenes = new ArrayList<>();
        this.logo = null;
        this.ptrSubj = new Subject();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getNroReferencia() {
        return nroReferencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEspecificacion() {
        return especificacion;
    }

    public void setEspecificacion(String especificacion) {
        this.especificacion = especificacion;
    }

    public double getPrecio() {
        return precio;
    }

    public Subject getPtrSubj() {
        return ptrSubj;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Proveedor getProvedor() {
        return proveedor;
    }

    public void setProvedor(Proveedor provedor) {
        this.proveedor = provedor;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public Map<Integer, Comentario> getComentarios() {
        return comentarios;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = new ArrayList<>(categorias);
    }

    public List<String> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<String> imagenes) {
        this.imagenes = imagenes;
    }

    @Override
    public String toString() {
        return getTitulo();
    }

    public void borrarImagenes() {
        this.imagenes.clear();
    }

    /**
     * Devuelve la lista de comentarios de primer nivel con los demas anidados
     *
     * @return
     */
    public List<DataComentario> listarComentarios() {
        ArrayList<DataComentario> res = new ArrayList<>();
        HashMap<Cliente, Puntaje> puntHash = new HashMap<>();
        for (Puntaje p : puntajes) {
            puntHash.put(p.getCliente(), p);
        }
        // un puntaje por usuario max
        for (Comentario c : comentarios.values()) {
            // por cada comentario pido al hashmap el puntaje del cliente
            if (puntHash.containsKey(c.getCliente())) {
                int puntaje = puntHash.get(c.getCliente()).getValor();
                c.setPuntuo(true);
                c.setPuntaje(puntaje);
            }

        }
        for (Comentario c : comentarios.values()) {
            if (c.isPrimerNivel()) { // get data es recursivo
                res.add(c.getData());
            }
        }

        return res;
    }

    public void agregarComentario(int id, String texto, Date fecha, Cliente cliente) {
        Comentario c = new Comentario(id, texto, fecha, cliente, this);

        Notificacion aviso = new Notificacion(this);
        this.getPtrSubj().notifyObservers(aviso);

        comentarios.put(id, c);
    }

    public void agregarRespuesta(int id, String texto, Date fecha, Cliente cliente, int idPadre) {
        Comentario c = new Comentario(id, texto, fecha, cliente, this);

        Notificacion aviso = new Notificacion(this);
        this.getPtrSubj().notifyObservers(aviso);

        Comentario padre = comentarios.get(idPadre);
        padre.agregarRespuesta(c);
        comentarios.put(id, c);
    }

    public int getCantidadComentarios() {
        return comentarios.size();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.nroReferencia;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        return this.nroReferencia == other.nroReferencia;
    }

    public int getCantidadComprada() {
        return cantidadComprada;
    }

    public void aumentarCantidadComprada(int cant) {
        this.cantidadComprada += cant;
    }

    public void reducirCantidadComprada(int cant) {
        if (cantidadComprada < cant) {
            throw new RuntimeException("La reduccion es mayor a la cantidad existente");
        }
        this.cantidadComprada -= cant;
    }

    public boolean contieneComId(int id) {
        for (Comentario c : comentarios.values()) {
            if (c.getId() == id) {
                return true;
            }
        }
        return false;
    }

    public DataProductoBas getDataProductoBas() {
        String logo = getLogo();
        DataProductoBas dp = new DataProductoBas(getTitulo(), getNroReferencia(), getDescripcion(), getEspecificacion(), getPrecio(), getProvedor().getNick(), getCantidadComprada(), logo);
        return dp;
    }

    public ArrayList<Reclamo> getReclamos() {
        return reclamos;
    }

    public void agregarReclamo(Reclamo reclamo) {
        this.reclamos.add(reclamo);
    }

    public void atenderReclamo(int id, String respuesta) {
        for (Reclamo rec : reclamos) {
            if (rec.getId() == id) {
                rec.setRespuesta(respuesta);
                rec.setAtendido(true);
                return;
            }
        }
    }

    public ArrayList<Puntaje> getPuntajes() {
        return puntajes;
    }

    public void agregarPuntaje(Puntaje puntaje) {
        this.puntajes.add(puntaje);
    }
}
