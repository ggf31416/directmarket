/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataEstadistica;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author guillermo
 */

public class Estadistica {

    private Map<String, EstadProd> estadisticas= new HashMap<>();
    private List<Integer> keyMeses = new ArrayList<>();
    private List<Calendar> meses = new ArrayList<>();
    
    public void agregar(String titulo,double monto, Date fecha){
        Calendar c = new GregorianCalendar();
        c.setTime(fecha);
        if (!estadisticas.containsKey(titulo)){
            estadisticas.put(titulo, new EstadProd(titulo,keyMeses));
        }
        estadisticas.get(titulo).agregar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), monto);
    }

    public Map<String, EstadProd> getEstadisticas() {
        return estadisticas;
    }
    
    private void elegirIntervalo(Calendar inicio,Calendar fin){
        Calendar var = new GregorianCalendar(inicio.get(Calendar.YEAR), inicio.get(Calendar.MONTH), 1, 0, 0, 0);
        for(;!var.after(fin);var.add(Calendar.MONTH, 1)){
            int mes = var.get(Calendar.MONTH);
            int anio = var.get(Calendar.YEAR);
            //String key = anio + "-" + (mes + 1);
            int key = 12 * anio + mes;
            meses.add(new GregorianCalendar(anio, mes, 1));
            keyMeses.add(key);
        }
    }

    public Estadistica(Calendar inicio,Calendar fin) {
        elegirIntervalo(inicio, fin);
    }
    
    public DataEstadistica getData(){
        DataEstadistica dstat = new DataEstadistica();
        dstat.setMeses(meses);
        for(EstadProd ep : estadisticas.values()){
            dstat.addDEP(ep.getData());
        }
        return dstat;
    }
    
    
}
