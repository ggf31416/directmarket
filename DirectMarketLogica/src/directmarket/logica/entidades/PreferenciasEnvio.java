/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

/**
 *
 * @author tprog087
 */
public class PreferenciasEnvio {
    private boolean activarEnvio = false;
    private String urlWeb = "localhost:10018";
    private int puertoSMTP = 20018;
    private String hostSMTP = "localhost";
    private String fromEmail = "no-reply@directmarket.com";

    public boolean isActivarEnvio() {
        return activarEnvio;
    }

    public void setActivarEnvio(boolean activarEnvio) {
        this.activarEnvio = activarEnvio;
    }

    public String getUrlWeb() {
        return urlWeb;
    }

    public void setUrlWeb(String urlWeb) {
        this.urlWeb = urlWeb;
    }

    public int getPuertoSMTP() {
        return puertoSMTP;
    }

    public void setPuertoSMTP(int puertoSMTP) {
        this.puertoSMTP = puertoSMTP;
    }

    public String getHostSMTP() {
        return hostSMTP;
    }

    public void setHostSMTP(String hostSMTP) {
        this.hostSMTP = hostSMTP;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
    
    
}
