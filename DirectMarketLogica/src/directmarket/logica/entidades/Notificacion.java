/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

/**
 *
 * @author Pablo
 */
public class Notificacion {
    /*
     * Cada objeto notificación es creado luego que el sujeto correspondiente
     * (producto, comentario u orden de compra) ha cambiado (hasChanged = true).
     */

    private TipoNotificacion tipo;
    private String comunicado;
    private String enlace;

    public Notificacion(Proveedor prov, Producto pr) {
        this.tipo = TipoNotificacion.RegistroProducto;
        this.comunicado = "El proveedor " + prov.getNombre() + " " + prov.getApellido()
                + " al que usted le compr&oacute, ha registrado un nuevo producto:" + pr.getTitulo()+".";
        this.enlace = "/publico/infoproducto?ref=" + pr.getNroReferencia();
    }
    
    public Notificacion(Reclamo r){
       this.tipo = TipoNotificacion.AtencionReclamo;
       Proveedor prov = r.getProducto().getProvedor();
       this.comunicado = "El proveedor " + prov.getNombre() + " " + prov.getApellido() + " ha respondido su reclamo.";
       this.enlace = "/publico/infoproducto?ref=" + r.getProducto().getNroReferencia();
    }

    public Notificacion(Producto prod) {
        this.tipo = TipoNotificacion.NuevoComentario;
        this.comunicado = "Un cliente ha publicado un comentario sobre el art&iacuteculo "
                + prod.getTitulo() + " que usted compr&oacute.";
        this.enlace = "/publico/infoproducto?ref=" + prod.getNroReferencia();
    }

    public Notificacion(OrdenDeCompra ord) {
        this.tipo = TipoNotificacion.CambioEstadoOrdenDeCompra;
        this.comunicado = "La orden de compra " + ord.getNroOrden()
                + " ha cambiado de estado a " + ord.obtenerEstadoActual().getTipoEstado().toString() + ".";
        this.enlace = "/InfoOrden?optionsRadios=" + ord.getNroOrden();
    }

    public TipoNotificacion getTipo() {
        return tipo;
    }

    public String getComunicado() {
        return comunicado;
    }

    public String getEnlace() {
        return enlace;
    }
}
