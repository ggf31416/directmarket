/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataEstadProd;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 *
 * @author Guillermo
 */

public class EstadProd {

    private double totalVentas = 0.0;
    private Map<Integer, Double> porMes = new TreeMap<>();
    private String titulo;

    public EstadProd(String titulo,Iterable<Integer> meses) {
        this.titulo = titulo;
        for(Integer mes : meses){
            porMes.put(mes, 0.0);
        }
    }

    public void agregar(int anio,int mes, double monto) {
        int key = anio * 12 + mes;
        if (!porMes.containsKey(key)) {
            //porMes.put(key, 0.0);
            throw new RuntimeException("Mes no incluido");
        }
        porMes.put(key, porMes.get(key) + monto);
        totalVentas += monto;
    }

    public double getTotalVentas() {
        return totalVentas;
    }

    public void setTotalVentas(double totalVentas) {
        this.totalVentas = totalVentas;
    }


    public String getTitulo() {
        return titulo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.titulo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EstadProd other = (EstadProd) obj;
        if (!Objects.equals(this.titulo, other.titulo)) {
            return false;
        }
        return true;
    }

    public DataEstadProd getData(){
        DataEstadProd dep = new DataEstadProd();
        dep.setTitulo(titulo);
        dep.setPorMes(new ArrayList<>(porMes.values()));
        dep.setTotalVentas(totalVentas);
        return dep;
    }
    
    
}
