/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;
import java.util.Observable;

/**
 *
 * @author Pablo
 */
public class Subject extends Observable {

    public Subject() {
    }

    @Override
    public void notifyObservers(Object arg) {
        setChanged();
        super.notifyObservers(arg); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void notifyObservers() {
        setChanged();
        super.notifyObservers(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
