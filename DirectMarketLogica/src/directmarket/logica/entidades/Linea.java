/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataLinea;

/**
 *
 * @author Fabian
 */
public class Linea {

    private Producto producto;
    private int cantidad;
    private double precioUnitario;

    public void setCantidad(int _cantidad) {
        this.cantidad = _cantidad;
    }

    public int getCantidad() {
        return this.cantidad;
    }

    public void setPrecioUnitario(double _PrecioUnitario) {
        this.precioUnitario = _PrecioUnitario;
    }

    public double getPrecioUnitario() {
        return this.precioUnitario;
    }

    public double getSubtotal() {
        return Math.round(100.0 *(this.cantidad * this.precioUnitario))/100.0;
    }

    public Linea() {
    }

    public Linea(int cant, Producto prod) {
        this.cantidad = cant;
        this.precioUnitario = prod.getPrecio();
        this.producto = prod;

    }

    public void agregarCantidad(int cant) {
        this.cantidad += cant;
    }

    public DataLinea getData() {
        DataLinea dli = new DataLinea(producto.getNroReferencia(), producto.getTitulo(), precioUnitario, getSubtotal(), cantidad, producto.getLogo());
        return dli;
    }

    @Override
    public String toString() {
        return producto.getTitulo() + " x " + cantidad + " = " + getSubtotal();
    }
    
    public void modificarCantidadComprada(){
        this.producto.aumentarCantidadComprada(cantidad);
    }
    
    public void cancelar(){
        this.producto.reducirCantidadComprada(cantidad);
    }

    public Producto getProducto() {
        return producto;
    }
    
    
}