/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataComentario;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Guillermo
 */
public class Comentario implements Comparable<Comentario>{
    private int id;
    private Cliente cliente;
    private Producto producto;
    private String texto;
    private Date fecha;
    private Comentario padre;
    private boolean puntuo = false;
    private int puntaje;
    private List<Comentario> respuestas = new ArrayList<>();

    public Comentario(int id,  String texto, Date fecha,Cliente cliente, Producto producto) {
        this.id = id;
        this.cliente = cliente;
        this.producto = producto;
        this.texto = texto;
        this.fecha = fecha;
        this.padre = null;
    }

    public int getId() {
        return id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Producto getProducto() {
        return producto;
    }

    public String getTexto() {
        return texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public Comentario getPadre() {
        return padre;
    }

    public void setPadre(Comentario padre) {
        this.padre = padre;
    }
    
    public void agregarRespuesta(Comentario c){
        this.respuestas.add(c);
        c.setPadre(this);
    }
    
    public DataComentario getData(){
        DataComentario dc = new DataComentario(id, cliente.getNick(),cliente.getNombre(),cliente.getApellido(),cliente.getImagen(), producto.getNroReferencia(), texto, fecha);
        if (this.padre != null) dc.setIdPadre(this.padre.id);
        for(Comentario r : respuestas){
            dc.agregarRespuesta(r.getData());
        }
        dc.setPuntaje(this.puntaje);
        dc.setPuntuo(this.puntuo);
        return dc;
    }

    @Override
    public int compareTo(Comentario o) {
        return Integer.compare(this.id, o.id);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Comentario other = (Comentario) obj;
        return (this.id == other.id);
    }
    
    public boolean isPrimerNivel(){
        return (this.padre == null);
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public boolean isPuntuo() {
        return puntuo;
    }

    public void setPuntuo(boolean puntuo) {
        this.puntuo = puntuo;
    }
    
    
    
    
    
}
