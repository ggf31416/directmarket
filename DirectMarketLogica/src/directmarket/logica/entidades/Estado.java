/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataEstado;
import java.util.Date;

/**
 *
 * @author Victor
 */

public class Estado {
    private TipoEstado estado;
    private Date fecha;

    public Estado(TipoEstado estado, Date fecha) {
        this.estado = estado;
        this.fecha = fecha;
    }

    public TipoEstado getTipoEstado() {
        return estado;
    }

    public void setEstado(TipoEstado estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    public DataEstado getData() {
        DataEstado de = new DataEstado(estado, fecha);
        return de;
    }
    
}
