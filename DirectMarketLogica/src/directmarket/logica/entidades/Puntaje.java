/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

/**
 *
 * @author Juan
 */
public class Puntaje {
    private int valor;
    private Cliente cliente;
    private Producto producto;

    public Puntaje(int valor, Cliente cliente, Producto producto) {
        this.valor = valor;
        this.cliente = cliente;
        this.producto = producto;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
}
