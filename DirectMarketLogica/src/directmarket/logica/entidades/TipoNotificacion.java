/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

/**
 *
 * @author tprog087
 */
public enum TipoNotificacion {
    RegistroProducto, 
    NuevoComentario, 
    CambioEstadoOrdenDeCompra,
    AtencionReclamo
};