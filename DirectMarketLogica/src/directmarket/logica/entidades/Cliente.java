/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataCliente;
import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import directmarket.accesoDatos.dataTypes.TipoUsuario;
import directmarket.logica.utilidades.CreadorEmail;
import java.util.Date;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Observable;
import java.util.Set;
import java.util.TreeSet;
import java.util.Observer;

/**
 *
 * @author Guillermo
 */
public class Cliente extends Usuario implements Observer {
    // deberiamos hacer booleanos o un set de excluidos?
    private EnumSet<TipoNotificacion> desuscriptos = EnumSet.noneOf(TipoNotificacion.class);
    /*private boolean notifOrden = true;
    private boolean notifProducto = true;
    private boolean notifComent = true;*/

    
    
    private Set<OrdenDeCompra> ordenes = new TreeSet<>();
    
    public Cliente() {
        
    }

    public Cliente(String Nick, String Email, String Nombre, String Apellido, Date Fecha, String Imagen) {
        super(Nick, Email, Nombre, Apellido, Fecha, Imagen);
    }

    public ArrayList<Integer> getnumdeOrdenes() {
        ArrayList<Integer> nordenes = new ArrayList<>();
        for (OrdenDeCompra ord : ordenes) {
            nordenes.add(ord.getNroOrden());
        }
        return nordenes;
    }

    public Set<OrdenDeCompra> getOrdenes() {
        return ordenes;
    }
    

    public void agregarOrden(OrdenDeCompra ord) {
        this.ordenes.add(ord);
    }

    public void removerOrden(OrdenDeCompra orden) {
        this.ordenes.remove(orden);
    }
    
    public boolean comproProducto(int nroRef){
        for(OrdenDeCompra ord : ordenes){
            if(ord.contieneProducto(nroRef) && (ord.obtenerEstadoActual().getTipoEstado() == TipoEstado.CONFIRMADA)) {
                return true;
            } 
        }
        return false;
    }


    @Override
    public DataUsuarioBas getDataBas() {
        return new DataUsuarioBas(getNick(), getEmail(), TipoUsuario.Cliente);
    }
    
    public DataCliente getDataCliente(){
        DataCliente dc = new DataCliente(getNick(), getEmail(), getNombre(), getApellido(),
                getNacimiento(), getnumdeOrdenes(),desuscriptos);
        dc.setNombreImagen(this.getImagen());
        
        return dc;
    }

    @Override
    public void update(Observable o, Object o1) {
        Notificacion notif = (Notificacion)o1;
        CreadorEmail crEm = new CreadorEmail();
        if(!desuscriptos.contains(notif.getTipo())) {
            crEm.notificarCliente(this, notif);
        }
    }
    
    public void cambiarSuscripcion(Set<TipoNotificacion> excluidos){
        if (excluidos.isEmpty()) this.desuscriptos = EnumSet.noneOf(TipoNotificacion.class);
        else this.desuscriptos = EnumSet.copyOf(excluidos);
    }
    
    
    
}
