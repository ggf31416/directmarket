package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataMensaje;
import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import directmarket.accesoDatos.dataTypes.RespuestaChat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Objects;

public abstract class Usuario {

    private String nick;
    private String email;
    private String nombre;
    private String apellido;
    private Date nacimiento;
    private String imagen;
    private ArrayList<Mensaje> mensajes = new ArrayList<>();
    // atributos para determinar conexion/desconexion chat
    private HashMap<Integer,Date> ultimosAccesos = new HashMap<>();
    private int idConexion = 0;
    private int ultimoMensaje = 0;
    
    
   
    
    public void agregarMensaje(Mensaje m){
        this.mensajes.add(m);
        
    }
    
    public RespuestaChat obtenerMensajes(int despuesDe){
        ArrayList<DataMensaje> res = new ArrayList<>();
        if(!mensajes.isEmpty()){
            ultimoMensaje = mensajes.get(mensajes.size() -1).getId();
        }
        for(int i = mensajes.size() - 1; i >= 0 && mensajes.get(i).getId() > despuesDe; i--){
            res.add( mensajes.get(i).getDate());
        }
        Collections.reverse(res);
        RespuestaChat r = new RespuestaChat();
        r.setUltimoMensaje(ultimoMensaje);
        for(DataMensaje m : res){
            String otroNick = m.getRemitente().equals(this.nick) ? m.getDestinatario() : m.getRemitente();
            r.agregarMensaje(otroNick, m);
        }
        return r;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public void setNick(String _nick) {
        this.nick = _nick;
    }

    public String getNick() {
        return this.nick;
    }

    public String getEmail() {
        return this.email;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setApellido(String _apellido) {
        this.apellido = _apellido;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setImagen(String _imagen) {
        this.imagen = _imagen;
    }

    public String getImagen() {
        return this.imagen;
    }

    public Usuario() {
    }

    public Usuario(String Nick, String Email, String Nombre, String Apellido, Date Fecha, String Imagen) {
        this.nick = Nick.trim();
        this.email = Email.trim();
        this.nombre = Nombre.trim();
        this.apellido = Apellido.trim();
        this.nacimiento = Fecha;
        this.imagen = Imagen;
    }

    @Override
    public String toString() {
        return this.getNick();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.nick);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        return Objects.equals(this.nick, other.nick);
    }
    
    public abstract DataUsuarioBas getDataBas();
    
    public int getAutoIdConexion(){
        return idConexion++;
    }
    
    public void registrarAcceso(int idConexion,Date actual){
        ultimosAccesos.put(idConexion, actual);
    }
    
    public int conectar(Date actual){
        int auto = getAutoIdConexion();
        registrarAcceso(auto, actual);
        return auto;
    }
    
    public void desconectar(int idConexion){
        ultimosAccesos.remove(idConexion);
    }
    
    public boolean estaConectado(Date actual){
        Iterator<Entry<Integer,Date>> it = ultimosAccesos.entrySet().iterator();
        long timeOut = 10000;
        boolean conectado = false;
        while(it.hasNext()){ // elimina conexiones antiguas
            Entry<Integer,Date> acc = it.next();
            long diff = actual.getTime() - acc.getValue().getTime();
            if(diff > timeOut){
                it.remove();
            }
            else{
                conectado = true;
            }
        }
        return conectado;
    }


}