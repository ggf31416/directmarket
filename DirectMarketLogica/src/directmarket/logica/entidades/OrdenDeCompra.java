/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

/**
 *
 * @author Fabian
 */
import directmarket.accesoDatos.dataTypes.DataOrden;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OrdenDeCompra implements Comparable<OrdenDeCompra> {

    private int nroOrden;
    private Date fecha;
    private double total = 0;
    private Cliente cliente;
    private ArrayList<Estado> estados = new ArrayList<>();
    Map<Integer, Linea> lineas = new HashMap<>();
    private Subject ptrSubj;

    public void setNroOrden(int _NroOrden) {
        this.nroOrden = _NroOrden;
    }

    public int getNroOrden() {
        return this.nroOrden;
    }

    public void setFecha(Date _fecha) {
        this.fecha = _fecha;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public double getTotal() {
        return this.total;
    }

    public OrdenDeCompra(Cliente cli) {
        this.cliente = cli;
        this.ptrSubj = new Subject();
    }

    public boolean agregarProducto(int cant, Producto prod) {
        Integer ref = prod.getNroReferencia();
        if (lineas.containsKey(ref)) {
            lineas.get(ref).agregarCantidad(cant);
            actualizarTotal();
            return false;
        } else {
            Linea nueva = new Linea(cant, prod);
            lineas.put(ref, nueva);
            actualizarTotal();
            return true;
        }

    }
    /*
     public DataLinea cambiarCantProducto(int cant, Producto prod) {
     Integer ref = prod.getNroReferencia();
     if (lineas.containsKey(ref)) {
     Linea lin = lineas.get(ref);
     lin.setCantidad(cant);
     actualizarTotal();
     return lin.getData();
     }
     return null;
     }*/

    public void eliminarProducto(Integer ref) {
        lineas.remove(ref);
        actualizarTotal();
    }

    private void actualizarTotal() {
        double t = 0;
        for (Linea l : lineas.values()) {
            t += l.getSubtotal();
        }
        total = Math.round(100 * t) / 100.0; //redondea a 2 lugares
    }

    public DataOrden getData() {
        DataOrden d = new DataOrden(nroOrden, getFecha(), total);
        d.setNick(cliente.getNick());
        d.setEmail(cliente.getEmail());
        d.setEstados(this.obtenerEstadoActual().getData());
        if(!estados.isEmpty()){
            d.setEstadoActual(estados.get(estados.size() - 1).getTipoEstado());
        }
        return d;
    }

    public DataOrdenLin getDataLin() {
        DataOrdenLin d = new DataOrdenLin(nroOrden, getFecha(), total);
        d.setNick(cliente.getNick());
        d.setEmail(cliente.getEmail());
        for (Linea l : lineas.values()) {
            d.agregarLinea(l.getData());
        }
        for(int i=0; i<estados.size(); i++) {
            d.setEstados(estados.get(i).getData());
        }
        if(!estados.isEmpty()){
            d.setEstadoActual(estados.get(estados.size() - 1).getTipoEstado());
        }
        
        return d;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Subject getPtrSubj() {
        return ptrSubj;
    }

    @Override
    public String toString() {
        return Integer.toString(nroOrden) + " " + cliente.getNick() + " " + total;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.nroOrden;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final OrdenDeCompra other = (OrdenDeCompra) obj;
        return (this.nroOrden == other.nroOrden);
    }

    private void asociarAlCliente() {
        this.cliente.agregarOrden(this);
    }

    private void desasociar() {
        this.cliente.removerOrden(this);
    }

    @Override
    public int compareTo(OrdenDeCompra o) {
        return this.nroOrden - o.nroOrden;
    }

    public int cantLineas() {
        return lineas.size();
    }

    public void recibir(Date fecha) {
        asociarAlCliente();
        for (Linea l : lineas.values()) {
            l.modificarCantidadComprada();
        }
        this.estados.add(new Estado(TipoEstado.RECIBIDA, fecha));
    }

    public void cancelar() {
        if (this.nroOrden != 0) {
            desasociar();
            for (Linea l : lineas.values()) {
                l.cancelar();
            }
        }
        if (this.getNroOrden() == 0) {
            // es carrito
            this.lineas.clear();
            this.total = 0;
        }
    }

    public boolean contieneProducto(int nroRef) {
        return this.lineas.containsKey(nroRef);
    }

    public ArrayList<Estado> getEstados() {
        return estados;
    }

    public Estado obtenerEstadoActual() {
        return estados.get(estados.size() - 1);
    }

    public void setEstados(Estado estados) {
        this.estados.add(estados);
    }

    public Map<Integer, Linea> getLineas() {
        return lineas;
    }

    /**
     * Agrega a la estadistica las ventas de productos en esta orden que esten
     * contenidos en 'productos'
     *
     * @param stat DataEstadistica a la que agregar
     * @param productos Productos a incluir en la estadistica
     */
    public void agregarEstadisticas(Estadistica stat, Iterable<Producto> productos) {
        if (obtenerEstadoActual().getTipoEstado() == TipoEstado.CONFIRMADA) {
            for (Producto pr : productos) {
                Linea lin = lineas.get(pr.getNroReferencia());
                if (lin != null) {
                    stat.agregar(pr.getTitulo(), lin.getSubtotal(), this.fecha);
                }
            }
        }
    }
}
