/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.accesoDatos.dataTypes.DataProveedor;
import directmarket.accesoDatos.dataTypes.DataReclamo;
import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import directmarket.accesoDatos.dataTypes.TipoUsuario;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Guillermo
 */
public class Proveedor extends Usuario {

    private String compania;
    private String web;
    private ArrayList<Producto> productos;
    
    private Subject ptrSubj;

    public Proveedor() {
    }

    public Proveedor(String Nick, String Email, String Nombre, String Apellido, Date Fecha, String Imagen, String compania, String web) {
        super(Nick, Email, Nombre, Apellido, Fecha, Imagen);
        this.compania = compania.trim();
        this.web = web.trim();
        this.productos = new ArrayList<Producto>();
        this.ptrSubj = new Subject();
    }

    public String getCompania() {
        return compania;
    }

    public String getWeb() {
        return web;
    }

    public Subject getPtrSubj() {
        return ptrSubj;
    }
    
    
    @Override
    public DataUsuarioBas getDataBas() {
        return new DataUsuarioBas(getNick(), getEmail(), TipoUsuario.Proveedor);
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }
    
    
    public DataProveedor getDataProv(){
        DataProveedor dp = new DataProveedor(getNick(), getEmail(), getNombre(), getApellido(),
                getNacimiento(), getCompania(), getWeb(), getDataProductos());
        dp.setNombreImagen(this.getImagen());
        return dp;
    }
    public ArrayList<DataProductoBas> getDataProductos(){
        ArrayList<DataProductoBas> lis = new ArrayList<DataProductoBas>();
        for(Producto p: productos)
            lis.add(p.getDataProductoBas());
        return lis;
    }
    public void agregarProducto(Producto p){
        productos.add(p);
    }
    public ArrayList<DataReclamo> getReclamos(){
     ArrayList<DataReclamo> res = new ArrayList<>();
     for(Producto p: productos)
         for(Reclamo r : p.getReclamos())
             res.add(r.getDataReclamo());
     return res;
    }
}
