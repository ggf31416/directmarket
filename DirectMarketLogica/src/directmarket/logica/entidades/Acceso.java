/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author tprog088
 */
public class Acceso {
    String ip;
    String url;
    String browser;
    String so;
    GregorianCalendar fecha;

    public Acceso(String ip, String url, String browser, String so, GregorianCalendar fecha) {
        this.ip = ip;
        this.url = url;
        this.browser = browser;
        this.so = so;
        this.fecha = (GregorianCalendar)fecha.clone();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public GregorianCalendar getFecha() {
        return fecha;
    }

    public void setFecha(GregorianCalendar fecha) {
        this.fecha = fecha;
    }
    
    public int antiguedad(GregorianCalendar actual){
        long dmilis = (actual.getTimeInMillis() - this.fecha.getTimeInMillis());
        return (int)(dmilis / (1000 * 3600 * 24));
    }

    @Override
    public String toString() {
        return "Acceso{" + "ip=" + ip + ", url=" + url + ", browser=" + browser + ", so=" + so + ", fecha=" + fecha.getTime().toString() + '}';
    }
    

 
    
}
