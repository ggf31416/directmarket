/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.accesoDatos.dataTypes.DataMensaje;
import java.util.GregorianCalendar;

/**
 *
 * @author Guillermo
 */
public class Mensaje {
    private int id;
    private String texto;
    private Usuario remitente;
    private Usuario destinatario;
    private GregorianCalendar fecha;

    public Mensaje(int id, String texto, Usuario remitente, Usuario destinatario, GregorianCalendar fecha) {
        this.id = id;
        this.texto = texto;
        this.remitente = remitente;
        this.destinatario = destinatario;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public String getTexto() {
        return texto;
    }

    public Usuario getRemitente() {
        return remitente;
    }

    public Usuario getDestinatario() {
        return destinatario;
    }

    public GregorianCalendar getFecha() {
        return fecha;
    }
    
    
    public DataMensaje getDate(){
        DataMensaje m = new DataMensaje(id, texto, this.remitente.getNick(), this.destinatario.getNick(), fecha);
        return m;
    }
}
