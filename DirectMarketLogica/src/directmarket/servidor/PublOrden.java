/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.servidor;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.accesoDatos.dataTypes.DataOrden;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.excepciones.CarritoException;
import directmarket.excepciones.DirectMarketException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.manejadores.ManejadorUsuarios;
import java.util.Date;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;

/**
 *
 * @author guillermo
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class PublOrden {
    
    private Endpoint endpoint = null;
    
    public PublOrden() {
    }

    @WebMethod(exclude = true)
    public void publicar(String puerto) {
        endpoint = Endpoint.publish("http://"+ puerto +"publorden", this);
    }
    
    @WebMethod(exclude = true)
    public Endpoint getEndpoint() {
        return endpoint;
    }

    @WebMethod
    public DataProductoBas[] productosPorCategoriaNombre(String nombreCateg) throws NoSeEncontroException{
        DataProductoBas[] res = new DataProductoBas[0]; // usar toArray() directamente va a dar no se puede castear
        return FabricaControladores.getOrdenesInstance().productosPorCategoria(nombreCateg).toArray(res);
    }

    @WebMethod
    public DataProductoBas[] productosPorCategoria(int idCateg) throws NoSeEncontroException{
         DataProductoBas[] res = new DataProductoBas[0];
        return FabricaControladores.getOrdenesInstance().productosPorCategoria(idCateg).toArray(res);
    }

    @WebMethod
    public DataCategoria[] listarCategoriasConProducto(){
        DataCategoria[] res = new DataCategoria[0];
        return FabricaControladores.getOrdenesInstance().listarCategoriasConProducto().toArray(res);
    }

    @WebMethod
    public DataOrdenLin getCarrito(String nick){
        return FabricaControladores.getOrdenesInstance().getCarrito(nick);
    }
    @WebMethod
    public double getCarritoTotal(String nick){
        return FabricaControladores.getOrdenesInstance().getCarritoTotal(nick);
    }

    @WebMethod
    public void eliminarLinea(String nick, Integer ref) throws CarritoException{
         FabricaControladores.getOrdenesInstance().eliminarLinea(nick, ref);
    }

    @WebMethod
    public void eliminarOrden(int nro) throws NoSeEncontroException{
        FabricaControladores.getOrdenesInstance().eliminarOrden(nro);
    }
    
    public void borrarCarrito(@WebParam(name = "nick")String nick){
        FabricaControladores.getOrdenesInstance().borrarCarrito(nick);
    }

    @WebMethod
    public DataOrdenLin InformacionOrden(@WebParam(name = "nro")int nro){
        return FabricaControladores.getOrdenesInstance().InformacionOrden(nro);
    }
 
    // se usa en crearOrden
    /**
     * Agrega un producto a la orden de compra actual del usuario
     *
     * @param nick Nick del usuario cliente
     * @param ref Referencia del producto
     * @param cant Cantidad del producto
     * @return Si se agrego una nueva linea
     */
    @WebMethod
    public boolean agregarProductoLinea(@WebParam(name = "nick")String nick, @WebParam(name = "ref")Integer ref, @WebParam(name = "cant")Integer cant){
        return FabricaControladores.getOrdenesInstance().agregarProductoLinea(nick, ref, cant);
    }

    /**
     * pasa la orden (del carrito)  a estado recibida
     */
    @WebMethod
    public DataOrdenLin confirmarOrden(@WebParam(name = "nick")String nick) throws CarritoException{
        return FabricaControladores.getOrdenesInstance().recibirOrden(nick);
    }

    
    @WebMethod
    public boolean carritoContieneProducto(@WebParam(name = "nick")String nick, @WebParam(name = "ref")int ref){
        return FabricaControladores.getOrdenesInstance().carritoContieneProducto(nick, ref);
    }
    

    /**
     * Pasa la orden a estado confirmada
     * @param nro
     * @throws DirectMarketException
     */
    @WebMethod
     public void ordenConfirmada(@WebParam(name = "nro")int nro) throws DirectMarketException{
       FabricaControladores.getOrdenesInstance().ordenConfirmada(nro, new Date());
    }
    @WebMethod
     public DataOrden[] listarOrdenes(@WebParam(name = "nick")String nick){
        DataOrden[] res = new DataOrden[0];
        return FabricaControladores.getOrdenesInstance().listarOrdenes(nick).toArray(res);
    }
}
