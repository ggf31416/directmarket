/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.servidor;

import directmarket.accesoDatos.dataTypes.DataCliente;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.accesoDatos.dataTypes.DataInfoCliente;
import directmarket.accesoDatos.dataTypes.DataInfoProveedor;
import directmarket.accesoDatos.dataTypes.DataLogueo;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import directmarket.accesoDatos.dataTypes.DataProveedor;
import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import directmarket.accesoDatos.dataTypes.RespuestaChat;
import directmarket.excepciones.MultiExcepcion;
import directmarket.excepciones.NegocioException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.YaExisteException;
import directmarket.logica.entidades.TipoNotificacion;
import directmarket.logica.fabrica.FabricaControladores;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;

/**
 *
 * @author tprog088
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class PublUsu {

    private Endpoint endpoint = null;
    
    public PublUsu() {
    }

    @WebMethod(exclude = true)
    public void publicar(String puerto) {
        endpoint = Endpoint.publish( "http://"+ puerto +"publusu", this);
    }

    @WebMethod(exclude = true)
    public Endpoint getEndpoint() {
        return endpoint;
    }

    @WebMethod
   public void AltaUsuario(@WebParam(name = "nick") String nick, @WebParam(name = "correo") String correo, 
            @WebParam(name="nombre") String nombre, @WebParam(name = "apellido") String apellido,
            @WebParam(name = "fecha") Date fecha, @WebParam(name = "imagen") DataImagen imagen, @WebParam(name = "pass") String pass) throws MultiExcepcion {
        FabricaControladores.getUsuarioInstance().AltaUsuario(nick, correo, nombre, apellido, fecha, imagen, pass);
        
    }

    @WebMethod
    public void AltaProveedor(@WebParam(name = "nick") String nick, @WebParam(name = "correo") String correo, @WebParam(name = "nombre") String nombre, @WebParam(name = "apellido") String apellido,
            @WebParam(name = "fecha") Date fecha, @WebParam(name = "compania") String compania, @WebParam(name = "web") String web, @WebParam(name = "imagen") DataImagen imagen, @WebParam(name = "pass") String pass) throws MultiExcepcion {
        FabricaControladores.getUsuarioInstance().AltaProveedor(nick, correo, nombre, apellido, fecha, compania, web, imagen, pass);
        
    }

    @WebMethod
    public boolean ValidarNick(@WebParam(name = "nick") String nick) {
        return FabricaControladores.getUsuarioInstance().ValidarNick(nick);
    }

    @WebMethod
    public void mailValido(@WebParam(name = "nick") String nick) throws YaExisteException, NegocioException {
        FabricaControladores.getUsuarioInstance().mailValido(nick);
    }

    @WebMethod
    public DataImagen getAvatar(@WebParam(name = "nick") String nick) {
        return FabricaControladores.getUsuarioInstance().getAvatar(nick);
    }

    @WebMethod
    public DataUsuarioBas[] listarProveedores() {
        return FabricaControladores.getUsuarioInstance().listarProveedores().toArray(new DataUsuarioBas[0]) ;
    }

    @WebMethod
    public DataUsuarioBas[] listarClientes() {
        return  FabricaControladores.getUsuarioInstance().listarClientes().toArray(new DataUsuarioBas[0]);
    }
    
    @WebMethod
    public DataCliente VerInformacionCliente(@WebParam(name = "nick") String nick) throws NoSeEncontroException {
        return FabricaControladores.getUsuarioInstance().VerInformacionCliente(nick);
    }

    @WebMethod
    public DataProveedor VerInformacionProveedor(@WebParam(name = "nick") String nick) throws NoSeEncontroException {
        return FabricaControladores.getUsuarioInstance().VerInformacionProveedor(nick);
    }

    @WebMethod
    public DataInfoCliente VerInfoCliente(@WebParam(name = "nick") String nick) throws NoSeEncontroException {
        return FabricaControladores.getUsuarioInstance().VerInfoCliente(nick);
    }

    @WebMethod
    public DataInfoProveedor VerInfoProveedor(@WebParam(name = "nick") String nick) throws NoSeEncontroException {
        return FabricaControladores.getUsuarioInstance().VerInfoProveedor(nick);
    }

    //Proveedor devolverProveedor(String nombProv) throws NoSeEncontroException;
    //void setImagenLocalCliente(String nick, String imagen);
    @WebMethod
    public DataLogueo autenticar(@WebParam(name = "nick") String nick, @WebParam(name = "pass") String pass) throws NoSeEncontroException {
        return FabricaControladores.getUsuarioInstance().autenticar(nick, pass);
    }

    @WebMethod
    public DataUsuarioBas getInfoBas(@WebParam(name = "nick") String nick) throws NoSeEncontroException {
        return FabricaControladores.getUsuarioInstance().getInfoBas(nick);
    }
    
    public DataOrdenLin[] listarOrdenCliente(@WebParam(name = "nick") String nick){
        return FabricaControladores.getUsuarioInstance().listarOrdenesCliente(nick).toArray(new DataOrdenLin[0]);
    }
    
    @WebMethod
    public void cambiarSuscripcionesUsuario(String nickCliente, TipoNotificacion[] excluidos) throws NoSeEncontroException {        
        Set<TipoNotificacion> exc = new HashSet<>(Arrays.asList(excluidos));
      
        FabricaControladores.getUsuarioInstance().cambiarSuscripcionesUsuario(nickCliente, exc);
    }    
        public int agregaraConectados(@WebParam(name = "nick") String nick){
            return FabricaControladores.getUsuarioInstance().agregaraConectados(nick);
        }
    
    @WebMethod
    public void quitardeConectados(@WebParam(name = "nick") String nick,@WebParam(name = "idConexion") int idConexion){
         FabricaControladores.getUsuarioInstance().quitardeConectados(nick, idConexion);
    }
    
    @WebMethod
    public boolean estaconectado(@WebParam(name = "nick") String nick){
        return FabricaControladores.getUsuarioInstance().estaconectado(nick);
    }
    
    @WebMethod
    public RespuestaChat obtenerMensajes(@WebParam(name = "nick") String nick, @WebParam(name = "idConexion") int idConexion,
            @WebParam(name = "despuesDe") int despuesDe,@WebParam(name = "pregConexion") String[] pregConexion){
         return FabricaControladores.getUsuarioInstance().obtenerMensajes(nick, idConexion, despuesDe,pregConexion);
    }
    
    @WebMethod
    public void agregarMensaje(@WebParam(name = "texto")String texto,@WebParam(name = "remit")String remit,@WebParam(name = "dest")String dest){
        FabricaControladores.getUsuarioInstance().agregarMensaje(texto,remit,dest);
    }
}
