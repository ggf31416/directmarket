/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.servidor;

import directmarket.accesoDatos.dataTypes.DataComentario;
import directmarket.logica.fabrica.FabricaControladores;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;

/**
 *
 * @author tprog087
 */
@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class PublComent {
         
    
    private Endpoint endpoint = null;
    
    public PublComent() {
    }

    @WebMethod(exclude = true)
    public void publicar(String puerto) {
        endpoint = Endpoint.publish("http://"+ puerto +"publcoment", this);
    }

    @WebMethod(exclude = true)
    public Endpoint getEndpoint() {
        return endpoint;
    }
    
    @WebMethod
    public int agregarComentario(  String texto,String nick,int ref) {
        return FabricaControladores.getComentariosInstance().agregarComentario(texto, nick, ref);
    }
    
    
    @WebMethod
    public int agregarRespuesta( String texto, String nick,int ref,int idPadre){
        return FabricaControladores.getComentariosInstance().agregarRespuesta(texto, nick, ref, idPadre);
    }
    
    @WebMethod
    public DataComentario[] listarComentarios(int nroRef){
        DataComentario[] res = new DataComentario[0];
        return FabricaControladores.getComentariosInstance().listarComentarios(nroRef).toArray(res);
    }
    
    @WebMethod
    public boolean puedeComentar(String nick,int refProd){
        return FabricaControladores.getComentariosInstance().puedeComentar(nick, refProd);
    }
}
