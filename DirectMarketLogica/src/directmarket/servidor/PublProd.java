/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.servidor;

import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.accesoDatos.dataTypes.DataProducto;
import directmarket.accesoDatos.dataTypes.DataReclamo;
import directmarket.excepciones.DirectMarketException;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.YaExisteException;
import directmarket.logica.fabrica.FabricaControladores;
import java.util.Arrays;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;

/**
 *
 * @author Pablo
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class PublProd {

    private Endpoint endpoint = null;

    public PublProd() {
    }

    @WebMethod(exclude = true)
    public void publicar(String puerto) {
        endpoint = Endpoint.publish("http://" + puerto + "publprod", this);
    }

    @WebMethod(exclude = true)
    public Endpoint getEndpoint() {
        return endpoint;
    }

    @WebMethod
    public void altaProducto(@WebParam(name = "tit")String tit, @WebParam(name = "nroRef")int nroRef, @WebParam(name = "desc")String desc, @WebParam(name = "especif")String especif, @WebParam(name = "precio")double precio, @WebParam(name = "nickProv")String nickProv, @WebParam(name = "categs")String[] categs, @WebParam(name = "imgs")DataImagen[] imgs, @WebParam(name = "logo")Integer logo)
            throws DirectMarketException {
        FabricaControladores.getProductosInstance().altaProducto(tit, nroRef, desc, especif, precio, nickProv, Arrays.asList(categs), Arrays.asList(imgs), logo);
    }

    @WebMethod
    public DataCategoria[] listarCategorias() {
        return FabricaControladores.getProductosInstance().listarCategorias().toArray(new DataCategoria[0]);
    }

    @WebMethod
    public DataCategoria[] listarCategoriasConProducto() {
        return FabricaControladores.getProductosInstance().listarCategoriasConProducto().toArray(new DataCategoria[0]);
    }

    @WebMethod
    public DataCategoria[] getCategoriasPrimerNivel() {
        return FabricaControladores.getProductosInstance().getCategoriasPrimerNivel().toArray(new DataCategoria[0]);
    }

    @WebMethod
    public DataCategoria getDatosCategoria(@WebParam(name = "nombreCateg")String nombreCateg) {
        return FabricaControladores.getProductosInstance().getDatosCategoria(nombreCateg);
    }

    @WebMethod
    public DataProductoBas[] productosPorCategoriaNombre(@WebParam(name = "nombreCateg")String nombreCateg) throws NoSeEncontroException {
        return FabricaControladores.getProductosInstance().productosPorCategoria(nombreCateg).toArray(new DataProductoBas[0]);
    }

    @WebMethod
    public DataProductoBas[] productosPorCategoria(@WebParam(name = "idCateg")int idCateg) throws NoSeEncontroException {
        return FabricaControladores.getProductosInstance().productosPorCategoria(idCateg).toArray(new DataProductoBas[0]);
    }

    @WebMethod
    public int altaCategoria(@WebParam(name = "nombre")String nombre, @WebParam(name = "nombrePadre")String nombrePadre, @WebParam(name = "tieneProductos")boolean tieneProductos) throws YaExisteException {
        return FabricaControladores.getProductosInstance().altaCategoria(nombre, nombrePadre, tieneProductos);
    }

    @WebMethod
    public DataProductoBas[] ListarProductos() {
        return FabricaControladores.getProductosInstance().ListarProductos().toArray(new DataProductoBas[0]);
    }

    @WebMethod
    public DataProducto obtenerDatosProducto(@WebParam(name = "nroRef")int nroRef) throws DirectMarketException {
        return FabricaControladores.getProductosInstance().obtenerDatosProducto(nroRef);
    }

    @WebMethod
    public void cambiarImagenesProducto(@WebParam(name = "NroRef")int NroRef, @WebParam(name = "imagenes")DataImagen[] imagenes) {
        FabricaControladores.getProductosInstance().cambiarImagenesProducto(NroRef, Arrays.asList(imagenes));
    }

    @WebMethod
    public DataProductoBas[] ProductosPorProveedor(@WebParam(name = "nick")String nick) {
        return FabricaControladores.getProductosInstance().ProductosPorProveedor(nick).toArray(new DataProductoBas[0]);
    }

    @WebMethod
    public DataProductoBas[] buscarProductos(@WebParam(name = "cadena")String cadena) {
        return FabricaControladores.getProductosInstance().buscarProductos(cadena).toArray(new DataProductoBas[0]);
    }

    @WebMethod
    public boolean existeProducto(@WebParam(name = "ref")int ref) {
        return FabricaControladores.getProductosInstance().existeProducto(ref);
    }

    @WebMethod
    public boolean tituloDisponible(@WebParam(name = "titulo")String titulo) {
        return FabricaControladores.getProductosInstance().tituloDisponible(titulo);
    }

    @WebMethod
    public void puntuarProducto(@WebParam(name = "nick")String nick, @WebParam(name = "nroRef")int nroRef, @WebParam(name = "valor")int valor) {
        FabricaControladores.getProductosInstance().puntuarProducto(nick, nroRef, valor);
    }

    @WebMethod
    public boolean existePuntuacion(@WebParam(name = "nick")String nick, @WebParam(name = "nroRef")int nroRef) {
        return FabricaControladores.getProductosInstance().existePuntuacion(nick, nroRef);
    }

    @WebMethod
    public int obtenerValorPuntaje(@WebParam(name = "nick")String nick, @WebParam(name = "nroRef")int nroRef) {
        return FabricaControladores.getProductosInstance().obtenerValorPuntaje(nick, nroRef);
    }

    @WebMethod
    public void AgregarReclamo(@WebParam(name = "nick")String nick, @WebParam(name = "texto") String texto, @WebParam(name = "nroRef")int nroRef) {
        FabricaControladores.getProductosInstance().AgregarReclamo(nick, texto, nroRef);
    }

    @WebMethod
    public DataReclamo[] ListarReclamos(@WebParam(name = "nick")String nick) {
        return FabricaControladores.getProductosInstance().ListarReclamos(nick).toArray(new DataReclamo[0]);
    }

    @WebMethod
    public void atenderReclamo(@WebParam(name = "id")int id, @WebParam(name = "respuesta")String respuesta, @WebParam(name = "nroRef")int nroRef) {
        FabricaControladores.getProductosInstance().atenderReclamo(id, respuesta, nroRef);
    }

    @WebMethod
    public boolean comproProducto(@WebParam(name = "cliente") String cliente, @WebParam(name = "ref") int ref) {
        return FabricaControladores.getProductosInstance().comproProducto(cliente, ref);
    }

    @WebMethod
    public DataReclamo[] ListarReclamosProducto(@WebParam(name = "cliente") String cliente, @WebParam(name = "ref") int ref) {
        return FabricaControladores.getProductosInstance().ListarReclamosProducto(cliente, ref).toArray(new DataReclamo[0]);
    }

    @WebMethod
    public boolean existeReclamoDeCliente(@WebParam(name = "cliente") String cliente, @WebParam(name = "ref") int ref) {
        return FabricaControladores.getProductosInstance().existeReclamoDeCliente(cliente, ref);
    }
}
