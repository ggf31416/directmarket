/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.servidor;

import directmarket.logica.fabrica.FabricaControladores;
import java.util.GregorianCalendar;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;

/**
 *
 * @author tprog088
 */
@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class PublAccesos {
     private Endpoint endpoint = null;
    
    public PublAccesos() {
    }

    @WebMethod(exclude = true)
    public void publicar(String puerto) {
        endpoint = Endpoint.publish("http://"+ puerto +"publaccesos", this);
    }

    @WebMethod(exclude = true)
    public Endpoint getEndpoint() {
        return endpoint;
    }
    @WebMethod
    public void guardarAcceso(String ip, String url ,GregorianCalendar fecha, String userAgent){
        FabricaControladores.getAccesosInstance().guardarAcceso(ip, url, fecha, userAgent);
    }
  
}


