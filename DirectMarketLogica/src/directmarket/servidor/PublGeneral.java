/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.servidor;

import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.excepciones.DirectMarketException;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.utilidades.DatosPrueba;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;

/**
 *
 * @author guillermo
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class PublGeneral {

    private Endpoint endpoint = null;

    @WebMethod(exclude = true)
    public void publicar(String puerto) {
        endpoint = Endpoint.publish("http://"+ puerto +"publgeneral", this);
    }

    @WebMethod(exclude = true)
    public Endpoint getEndpoint() {
        return endpoint;
    }

    public PublGeneral() {
    }

    @WebMethod
    public void cargarDatosPrueba() throws DirectMarketException {
        DatosPrueba.cargarDatosPrueba();
    }

    @WebMethod
    public void cargarDatosPruebaSinImag() throws DirectMarketException {
        DatosPrueba.cargarDatosPruebaSinImag();
    }

    @WebMethod
    public DataImagen getImagen(String nombre) {
        return FabricaControladores.getImagenesInstance().getImagen(nombre);
    }
    
    @WebMethod // no devuelve nada
    public void checkearConectividad(){
        
    }
}
