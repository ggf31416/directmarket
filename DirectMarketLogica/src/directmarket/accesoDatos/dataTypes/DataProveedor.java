/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Proveedor con imagen
 * @author fabian
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataProveedor extends DataInfoProveedor{

    DataImagen imagen;




    public DataImagen getImagen() {
        return imagen;
    }

    public void setImagen(DataImagen imagen) {
        this.imagen = imagen;
    }

    public DataProveedor(String nick, String email, String nombre, String apellido, Date fecha, String campania, String web, ArrayList<DataProductoBas> productos) {
        super(nick, email, nombre, apellido, fecha, campania, web, productos);
    }

   



   
}
