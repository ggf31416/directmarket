package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Gonzalo Miraballes
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataCategoria implements Serializable{
    private int id;
    private String nombre;
    private String nombrePadre;
    private Integer idPadre;
    private boolean tieneProductos;
    private List<DataCategoria> subcategorias;
    
    public static final String PRINCIPAL = "Categorías";

    public DataCategoria() {
    }

    public DataCategoria(int id, String nombre, String nombrePadre, Integer idPadre, boolean tieneProductos) {
        this.id = id;
        this.nombre = nombre;
        this.nombrePadre = nombrePadre;
        this.idPadre = idPadre;
        this.tieneProductos = tieneProductos;
        subcategorias = new ArrayList<>();
    }

    public DataCategoria(int id, String nombre, boolean tieneProductos) {
        this.id = id;
        this.nombre = nombre;
        this.tieneProductos = tieneProductos;
         subcategorias = new ArrayList<>();
         nombrePadre = PRINCIPAL;
         idPadre = 0;
    }





    public List<DataCategoria> getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategorias(List<DataCategoria> subcategorias) {
        this.subcategorias = subcategorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombrePadre() {
        return nombrePadre;
    }

    public void setNombrePadre(String nombrePadre) {
        this.nombrePadre = nombrePadre;
    }

    public boolean getTieneProductos() {
        return tieneProductos;
    }

    public void setTieneProductos(boolean tieneProductos) {
        this.tieneProductos = tieneProductos;
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DataCategoria other = (DataCategoria) obj;
        return (this.id == other.id) || Objects.equals(this.nombre, other.nombre) ;

    }

    public int getId() {
        return id;
    }

    public Integer getIdPadre() {
        return idPadre;
    }
    
    
}