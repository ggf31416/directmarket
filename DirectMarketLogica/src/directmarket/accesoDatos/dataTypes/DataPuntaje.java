/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Gonzalo Miraballes
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataPuntaje implements Serializable {
    int valor;
    int producto;
    String cliente;

    public DataPuntaje() {
    }

    public DataPuntaje(int valor, int producto, String cliente) {
        this.valor = valor;
        this.producto = producto;
        this.cliente = cliente;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getProducto() {
        return producto;
    }

    public void setProducto(int producto) {
        this.producto = producto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    
}
