/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import directmarket.logica.entidades.TipoEstado;
import java.util.Date;

/**
 *
 * @author Gonzalo Miraballes
 */

public class DataEstado {
    private TipoEstado estado;
    private Date fecha;

    public DataEstado(TipoEstado estado, Date fecha) {
        this.estado = estado;
        this.fecha = fecha;
    }

    public TipoEstado getEstado() {
        return estado;
    }

    public void setEstado(TipoEstado estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
}
