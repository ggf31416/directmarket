/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataLogueo extends DataUsuarioBas{
    private int idConexion;

    public int getIdConexion() {
        return idConexion;
    }

    public void setIdConexion(int idConexion) {
        this.idConexion = idConexion;
    }

    public DataLogueo() {
    }

    public DataLogueo(int idConexion,DataUsuarioBas orig) {
        super(orig.getNick(), orig.getEmail(), orig.getTipo());
        this.idConexion = idConexion;
    }

    @Override
    public String toString() {
        return super.toString() +  ";idConexion=" + idConexion;
    }
    
    
}
