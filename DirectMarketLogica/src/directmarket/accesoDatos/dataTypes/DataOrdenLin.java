/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog087
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataOrdenLin extends DataOrden {

    public List<DataLinea> lineas;

    public List<DataLinea> getLineas() {
        return lineas;
    }

    public void setLineas(List<DataLinea> lineas) {
        this.lineas = lineas;
    }

    public void agregarLinea(DataLinea l) {
        lineas.add(l);
    }

    public DataOrdenLin(int nroOrden, Date fecha, double total) {
        super(nroOrden, fecha, total);
        this.lineas = new ArrayList<>();
    }

    public DataOrdenLin() {
        super(0, new Date(), 0);
        this.lineas = new ArrayList<>();
    }
    
    public int getCantidadProductos(){
        return lineas.size();
    }
    
}
