/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Comentario;
import directmarket.logica.entidades.Producto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataComentario implements Serializable{

    private int id;
    private String nick;
    private String nombre;
    private String apellido;
    private String imagen;
    private int nroRef;
    private String texto;
    private Date fecha;
    private Integer idPadre;
    private List<DataComentario> respuestas = new ArrayList<>();
    private int puntaje;
    private boolean puntuo;

    public DataComentario() {
    }

    public DataComentario(int id, String nick, String nombre,String apellido,String imagen, int nroRef, String texto, Date fecha) {
        this.id = id;
        this.nick = nick;
        this.nroRef = nroRef;
        this.texto = texto;
        this.fecha = fecha;
        this.nombre = nombre;
        this.apellido = apellido;
        this.imagen = imagen;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }
    
    public void agregarRespuesta(DataComentario dc){
        respuestas.add(dc);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNroRef() {
        return nroRef;
    }

    public void setNroRef(int nroRef) {
        this.nroRef = nroRef;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<DataComentario> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<DataComentario> respuestas) {
        this.respuestas = respuestas;
    }

    public String getNick() {
        return nick;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataComentario other = (DataComentario) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getImagen() {
        return imagen;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public boolean isPuntuo() {
        return puntuo;
    }

    public void setPuntuo(boolean puntuo) {
        this.puntuo = puntuo;
    }
    
}
