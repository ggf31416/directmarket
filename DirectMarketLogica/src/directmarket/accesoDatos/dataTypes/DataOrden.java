/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import directmarket.logica.entidades.TipoEstado;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog087
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataOrden implements Serializable{

    private int nroOrden;
    private String email;
    private String nick;
    private Date fecha;
    private double total;
    private ArrayList<DataEstado> estados = new ArrayList<>();
    private TipoEstado estadoActual = TipoEstado.RECIBIDA;

    public DataOrden() {
    }

    public DataOrden(int nroOrden, Date fecha, double total) {
        this.nroOrden = nroOrden;
        this.fecha = fecha;
        this.total = total;
    }

    public int getNroOrden() {
        return nroOrden;
    }

    public void setNroOrden(int nroOrden) {
        this.nroOrden = nroOrden;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public ArrayList<DataEstado> getEstados() {
        return estados;
    }
    
    public DataEstado obtenerEstado() {
        return estados.get(estados.size()-1);
    }

    public void setEstados(DataEstado estado) {
        this.estados.add(estado);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.nroOrden;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DataOrden other = (DataOrden) obj;
        return this.nroOrden == other.nroOrden;
    }

    public Object[] getModel() {
        return new Object[]{this.nroOrden};
    }

    public TipoEstado getEstadoActual() {
        return estadoActual;
    }

    public void setEstadoActual(TipoEstado estadoActual) {
        this.estadoActual = estadoActual;
    }


    
    
}
