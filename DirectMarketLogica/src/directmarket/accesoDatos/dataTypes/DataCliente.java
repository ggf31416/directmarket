/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import directmarket.logica.entidades.TipoNotificacion;
import java.util.Date;
import java.util.ArrayList;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog088
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataCliente extends DataInfoCliente{


    DataImagen imagen;

    public DataCliente( String nick, String email,String nombre, String apellido, Date fecha,ArrayList<Integer> ordenes,Set<TipoNotificacion> desuscriptos) {
        super(nick, email, nombre, apellido, fecha,ordenes,desuscriptos);
    }




    public DataImagen getImagen() {
        return imagen;
    }

    public void setImagen(DataImagen imagen) {
        this.imagen = imagen;
    }




}
