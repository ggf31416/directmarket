/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import directmarket.logica.entidades.TipoNotificacion;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataInfoCliente extends DataUsuario {

    ArrayList<Integer> ordenes;
    Set<TipoNotificacion> desuscriptos ;


    public ArrayList<Integer> getnumOrdenes() {
        return ordenes;
    }

    public DataInfoCliente(String nick, String email, String nombre, String apellido, Date fecha ,ArrayList<Integer> ordenes,Set<TipoNotificacion> desuscriptos) {
        super( nick, email,nombre, apellido, fecha ,TipoUsuario.Cliente);
        this.ordenes = ordenes;
        this.desuscriptos = desuscriptos;
    }

    public Set<TipoNotificacion> getDesuscriptos() {
        return desuscriptos;
    }
    
    
}
