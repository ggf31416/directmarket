/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataUsuarioBas implements Serializable{

    private String nick;
    private String email;
    private TipoUsuario tipo;

    public TipoUsuario getTipo() {
        return tipo;
    }

    public DataUsuarioBas() {
    }

    
    
    public DataUsuarioBas(String nick, String email,TipoUsuario tipo) {
        this.nick = nick;
        this.email = email;
        this.tipo =tipo;
    }

    public String getNick() {
        return nick;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return nick;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.nick);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DataUsuarioBas other = (DataUsuarioBas) obj;
        return Objects.equals(this.nick, other.nick) && (other.tipo == this.tipo);
    }
}
