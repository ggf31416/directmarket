/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog087
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataProductoBas implements Comparable<DataProductoBas>, Serializable{

    private String titulo;
    private int nroReferencia;
    private String descripcion;
    private String especificacion;
    private double precio;
    private String nickProveedor;
    private int cantComprada;
    private String logo;

    public DataProductoBas() {
    }

    public DataProductoBas(String titulo, int nroReferencia, String descripcion,
            String especificacion, double precio, String nickProveedor, int cantComprada, String logo) {
        this.nroReferencia = nroReferencia;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.especificacion = especificacion;
        this.nickProveedor = nickProveedor;
        this.cantComprada = cantComprada;
        this.logo = logo;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getNroReferencia() {
        return nroReferencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getLogo() {
        return logo;
    }
    
    public String getEspecificacion() {
        return especificacion;
    }

    public double getPrecio() {
        return precio;
    }

    public String getNickProveedor() {
        return nickProveedor;
    }

    public int getCantComprada() {
        return cantComprada;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
    
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setNroReferencia(int nroReferencia) {
        this.nroReferencia = nroReferencia;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setEspecificacion(String especificacion) {
        this.especificacion = especificacion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setNickProveedor(String nickProveedor) {
        this.nickProveedor = nickProveedor;
    }

    public void setCantComprada(int stock) {
        this.cantComprada = stock;
    }

    @Override
    public String toString() {
        return titulo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.nroReferencia;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DataProductoBas other = (DataProductoBas) obj;
        return (this.nroReferencia == other.nroReferencia);
    }

    @Override
    public int compareTo(DataProductoBas t) {
        return this.titulo.toLowerCase().compareTo(t.titulo.toLowerCase());
    }
}
