/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Pablo
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
public class DataProducto extends DataProductoBas {

    private List<String> listaCategorias = new ArrayList<>();
    private List<DataImagen> listaImagenes = new ArrayList<>();
    private List<DataPuntaje> puntajes = new ArrayList<>();

    private int cantidadComentarios;
    
    public DataProducto() {
    }

    public DataProducto(String titulo, int nroReferencia, String descripcion,
            String especificacion, double precio, String nickProveedor, int cantCompr,int cantComent,String logo) {
        super(titulo, nroReferencia, descripcion, especificacion, precio, nickProveedor, cantCompr, logo);
        this.cantidadComentarios = cantComent;
    }

    @XmlTransient
    public List<DataImagen> getListaImagenes() {
        return listaImagenes;
    }

    public void setListaImagenes(List<DataImagen> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }

    
    public List<String> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<String> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }
    
    
    public List<String> getNombreImagenes(){
        List<String> lis = new ArrayList<>();
        for(DataImagen d: listaImagenes)
            lis.add(d.getNombre());
        return lis;
    }
    
    public void setNombreImagenes(List<String>  val){
        // vacio
    }

    public int getCantidadComentarios() {
        return cantidadComentarios;
    }

    public void setCantidadComentarios(int cantidadComentarios) {
        this.cantidadComentarios = cantidadComentarios;
    }
    
    public List<DataPuntaje> getPuntajes() {
        return puntajes;
    }

    public void setPuntajes(List<DataPuntaje> puntajes) {
        this.puntajes = puntajes;
    }    
}