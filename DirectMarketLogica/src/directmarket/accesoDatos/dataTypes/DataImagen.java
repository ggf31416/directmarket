/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;
import javax.imageio.ImageIO;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataImagen implements Serializable{

    private byte[] datos;

    private String nombre;
    // para bean
    private String extension;
    
    
    @XmlTransient
    private BufferedImage imagen;

    // usar la fabrica
    public DataImagen(byte[] datos, String nombre) {
        this.datos = datos;
        this.nombre = nombre;
        extension = obtenerExt();
    }

    public DataImagen() {
    }

    public void setDatos(byte[] datos) {
        this.datos = datos;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
        extension = obtenerExt(); 
    }

    /*public void setImagen(BufferedImage imagen) {
     this.imagen = imagen;
     }*/
    public byte[] getDatos() {
        return datos;
    }

    public String getNombre() {
        return nombre;
    }

    //@XmlTransient
    public BufferedImage getImagen() {
        try {
            if (imagen == null) {
                ImageIO.setUseCache(false); // desactiva el cache de disco que da problemas en la fing
                imagen = ImageIO.read(new ByteArrayInputStream(datos));
            }
            return imagen;
        } catch (IOException ex) {
            return null;
        }
    }
    
   
    public String obtenerExt() {
        int idx = nombre.lastIndexOf(".");
        return (idx != -1) ? nombre.substring(idx) : "";
    }
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DataImagen other = (DataImagen) obj;
        return Objects.equals(this.nombre, other.nombre);
    }
}
