/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RespuestaChat implements Serializable{
    private List<DataConversacion> conversaciones = new ArrayList<>();
    private List<String> conectados;
    private int ultimoMensaje;



    public RespuestaChat() {
        conversaciones = new ArrayList<>();
        conectados = new ArrayList<>();
    }

    public List<DataConversacion> getConversaciones() {
        return conversaciones;
    }

    public void setConversaciones(List<DataConversacion> conversaciones) {
        this.conversaciones = conversaciones;
    }
    



    public List<String> getConectados() {
        return conectados;
    }

    public void setConectados(List<String> conectados) {
        this.conectados = conectados;
    }

    public int getUltimoMensaje() {
        return ultimoMensaje;
    }

    public void setUltimoMensaje(int ultimoMensaje) {
        this.ultimoMensaje = ultimoMensaje;
    }
    
    public void agregarMensaje(String nick, DataMensaje m){
        boolean nuevo = true;
        for(DataConversacion c : conversaciones){
            if(c.getOtroNick().equals(nick)){
                c.agregarMensaje(m);
                nuevo = false;
            }
        }
        if(nuevo){
            DataConversacion c = new DataConversacion();
            c.setOtroNick(nick);
            c.agregarMensaje(m);
            this.conversaciones.add(c);
        }
    }
    
    
}
