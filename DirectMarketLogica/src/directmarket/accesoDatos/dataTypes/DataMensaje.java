/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import java.util.GregorianCalendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataMensaje implements Serializable{
        private int id;
    private String texto;
    private String remitente;
    private String destinatario;
    private GregorianCalendar fecha;

    public DataMensaje() {
    }

    public DataMensaje(int id, String texto, String remitente, String destinatario, GregorianCalendar fecha) {
        this.id = id;
        this.texto = texto;
        this.remitente = remitente;
        this.destinatario = destinatario;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public GregorianCalendar getFecha() {
        return fecha;
    }

    public void setFecha(GregorianCalendar fecha) {
        this.fecha = fecha;
    }
    
    
}
