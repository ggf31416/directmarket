/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import directmarket.logica.entidades.Mensaje;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataConversacion implements Serializable{
    private String otroNick;
    private List<DataMensaje> mensajes = new ArrayList<>();

    public String getOtroNick() {
        return otroNick;
    }

    public void setOtroNick(String otroNick) {
        this.otroNick = otroNick;
    }

    public List<DataMensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<DataMensaje> mensajes) {
        this.mensajes = mensajes;
    }
    
    public void agregarMensaje(DataMensaje m){
        mensajes.add(m);
    }

}
