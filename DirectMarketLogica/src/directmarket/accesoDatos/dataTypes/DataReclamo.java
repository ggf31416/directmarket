/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog088
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataReclamo implements Serializable, Comparable<DataReclamo>{
    int id;
    String texto;
    Date fecha;
    String cliente;
    int nroRefProd;
    String NombreProd;
    String respuesta;
    boolean atendido;

    public DataReclamo(int id, String texto, Date fecha, String cliente, int nroRefProd, String NombreProd, String respuesta, boolean atendido) {
        this.id = id;
        this.texto = texto;
        this.fecha = fecha;
        this.cliente = cliente;
        this.nroRefProd = nroRefProd;
        this.NombreProd = NombreProd;
        this.respuesta = respuesta;
        this.atendido = atendido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getNroRefProd() {
        return nroRefProd;
    }

    public void setNroRefProd(int nroRefProd) {
        this.nroRefProd = nroRefProd;
    }

    public String getNombreProd() {
        return NombreProd;
    }

    public void setNombreProd(String NombreProd) {
        this.NombreProd = NombreProd;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public boolean isAtendido() {
        return atendido;
    }

    public void setAtendido(boolean atendido) {
        this.atendido = atendido;
    }

    @Override
    public int compareTo(DataReclamo t) {
        return this.fecha.compareTo(t.fecha);
    } 
    
}
