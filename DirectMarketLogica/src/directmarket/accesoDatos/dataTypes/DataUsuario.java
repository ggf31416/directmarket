/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataUsuario extends DataUsuarioBas{
    
    String nombre;
    String apellido;
    Date fecha;
    String nombreImagen;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public DataUsuario(String nick, String email,String nombre, String apellido, Date fecha,  TipoUsuario tipo) {
        super(nick, email, tipo);
        this.nombre = nombre;
        this.apellido = apellido;
        this.fecha = fecha;
    }

   

}
