/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import directmarket.logica.entidades.Estadistica;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author guillermo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataInfoProveedor extends DataUsuario {

    
    String campania;
    String web;
    ArrayList<DataProductoBas> productos;
    DataEstadistica estadisticas;

    public ArrayList<DataProductoBas> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<DataProductoBas> productos) {
        this.productos = productos;
    }


    public String getCampania() {
        return campania;
    }

    public void setCampania(String campania) {
        this.campania = campania;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public DataInfoProveedor(String nick, String email, String nombre, String apellido, Date fecha, String campania, String web,ArrayList<DataProductoBas> productos) {
        super(nick, email, nombre, apellido, fecha, TipoUsuario.Proveedor);
        this.campania = campania;
        this.web = web;
        this.productos = productos;
    }

    public DataEstadistica getEstadisticas() {
        return estadisticas;
    }

    public void setEstadisticas(DataEstadistica estadisticas) {
        this.estadisticas = estadisticas;
    }



    
    
}
