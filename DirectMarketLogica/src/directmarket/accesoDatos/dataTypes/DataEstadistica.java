/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog087
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataEstadistica {
    private List<Calendar> meses = new ArrayList<>();
    private List<DataEstadProd> estadProductos = new ArrayList<>();

    public List<Calendar> getMeses() {
        return meses;
    }

    public void setMeses(List<Calendar> meses) {
        this.meses = meses;
    }


    public List<DataEstadProd> getEstadProductos() {
        return estadProductos;
    }

    public void setEstadProductos(List<DataEstadProd> estadProductos) {
        this.estadProductos = estadProductos;
    }
    
    public void addDEP(DataEstadProd dep){
        this.estadProductos.add(dep);
    }
    
}
