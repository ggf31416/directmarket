/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog087
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataLinea implements Serializable{

    int ref;
    String titulo;
    double precio;
    double subtotal;
    int cantidad;
    String logo;

    public DataLinea() {
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getRef() {
        return ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public DataLinea(int ref, String titulo, double precio, double subtotal, int cantidad, String logo) {
        this.ref = ref;
        this.titulo = titulo;
        this.precio = precio;
        this.subtotal = subtotal;
        this.cantidad = cantidad;
        this.logo = logo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.ref;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.ref == ((DataLinea) obj).ref;
    }

    public Object[] getModel() {
        return new Object[]{this.titulo, this.cantidad, this.precio, this.subtotal};
    }
}
