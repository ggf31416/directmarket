/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.accesoDatos.dataTypes;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author tprog087
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataEstadProd implements Serializable{
    private String titulo;
    private List<Double> porMes;
    private Double totalVentas;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Double> getPorMes() {
        return porMes;
    }

    public void setPorMes(List<Double> porMes) {
        this.porMes = porMes;
    }

    public Double getTotalVentas() {
        return totalVentas;
    }

    public void setTotalVentas(Double totalVentas) {
        this.totalVentas = totalVentas;
    }
    
    
    
    
}
