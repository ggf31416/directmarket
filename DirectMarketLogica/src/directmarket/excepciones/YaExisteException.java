/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.excepciones;

/**
 *
 * @author admin
 */
public class YaExisteException extends DirectMarketException {

    public YaExisteException(String message) {
        super(message);
    }

    public YaExisteException(String message, Throwable cause) {
        super(message, cause);
    }

    public YaExisteException(String campo, String message) {
        super(campo, message);
    }

    public YaExisteException(String campo, String message, Throwable cause) {
        super(campo, message, cause);
    }
}
