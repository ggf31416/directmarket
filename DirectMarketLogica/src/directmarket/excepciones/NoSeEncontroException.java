/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.excepciones;

/**
 *
 * @author admin
 */
public class NoSeEncontroException extends DirectMarketException {

    public NoSeEncontroException(String message) {
        super(message);
    }

    public NoSeEncontroException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSeEncontroException(String campo, String message) {
        super(campo, message);
    }

    public NoSeEncontroException(String campo, String message, Throwable cause) {
        super(campo, message, cause);
    }
}
