/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.excepciones;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Guillermo
 */
public class Multierror {

    private Map<String, String> mensajes = new HashMap<>();

    public void agregarError(String campo, String mensaje) {
        mensajes.put(campo.toLowerCase(), mensaje);
    }

    public void agregarError(DirectMarketException ex) {
        mensajes.put(ex.getCampo().toLowerCase(), ex.getMessage());
    }

    public boolean hayError() {
        return mensajes.size() > 0;
    }

    public Map<String, String> getMensajes() {
        return mensajes;
    }
}
