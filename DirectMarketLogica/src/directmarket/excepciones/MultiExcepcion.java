/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.excepciones;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Guillermo
 */
public class MultiExcepcion extends DirectMarketException {

    private Multierror error;
    //private Map<String, String> mensajes = new HashMap<>();

    public MultiExcepcion(Multierror error) {
        this.error = error;
    }

    public void agregarError(String campo, String mensaje) {
        error.agregarError(campo, mensaje);
    }

    public void agregarError(DirectMarketException ex) {
        error.agregarError(ex);
    }

    public boolean hayError() {
        return error.hayError();
    }

    public Map<String, String> getMensajes() {
        return error.getMensajes();
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        for (String e : getMensajes().keySet()) {
            sb.append(e).append(" -> ").append(getMensajes().get(e)).append("\n");
        }
        return sb.toString();
    }
}
