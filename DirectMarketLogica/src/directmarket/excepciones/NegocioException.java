/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.excepciones;

/**
 *
 * @author admin
 */
public class NegocioException extends DirectMarketException {

    public NegocioException(String message) {
        super(message);
    }

    public NegocioException(String message, Throwable cause) {
        super(message, cause);
    }

    public NegocioException(String campo, String message) {
        super(campo, message);
    }

    public NegocioException(String campo, String message, Throwable cause) {
        super(campo, message, cause);
    }
}
