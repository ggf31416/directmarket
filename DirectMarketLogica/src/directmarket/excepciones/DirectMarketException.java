/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.excepciones;

/**
 *
 * @author tprog087
 */
public class DirectMarketException extends Exception {

    private String campo = "";

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public DirectMarketException() {
    }

    public DirectMarketException(String message) {
        super(message);
    }

    public DirectMarketException(String campo, String message) {
        super(message);
        this.campo = campo;
    }

    public DirectMarketException(String message, Throwable cause) {
        super(message, cause);

    }

    public DirectMarketException(String campo, String message, Throwable cause) {
        super(message, cause);
        this.campo = campo;
    }
}
