/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.excepciones;

import javax.xml.ws.WebFault;

/**
 *
 * @author Guillermo
 */
public class CarritoException extends NegocioException {

    public CarritoException(String message) {
        super(message);
    }

    public CarritoException(String campo, String message) {
        super(campo, message);
    }
    
    
}
