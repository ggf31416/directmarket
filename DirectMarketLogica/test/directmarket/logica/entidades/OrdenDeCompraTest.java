/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.entidades;

import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Producto;
import directmarket.logica.entidades.OrdenDeCompra;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guillermo
 */
public class OrdenDeCompraTest {

    public OrdenDeCompraTest() {
    }

    @Test
    public void noCambiaTotal() {
        Producto p = new Producto();
        p.setPrecio(200);
        Cliente c = new Cliente();
        c.setNick("nick");
        OrdenDeCompra compra = new OrdenDeCompra(c);
        compra.agregarProducto(10, p);
        p.setPrecio(300);
        assertEquals("Cambio el total al cambiar el precio de un producto", 2000, compra.getTotal(), 0.000001);
    }
}