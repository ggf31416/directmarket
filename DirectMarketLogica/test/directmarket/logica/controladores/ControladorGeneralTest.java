/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.logica.controladores.ControladorImagenes;
import directmarket.logica.fabrica.FabricaControladores;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Gonzalo Miraballes
 */
public class ControladorGeneralTest {

    ControladorImagenes ctrl;

    public ControladorGeneralTest() {
    }

    @Before
    public void setUp() {
        ctrl = (ControladorImagenes) FabricaControladores.getImagenesInstance();
    }

    @After
    public void tearDown() {
    }

   

    @Test
    public void testValidarSetPath() {
        String orig = ctrl.getCarpetaImagenes(); // no fue pensado para ser testeado
        ctrl.setCarpetaImagenes("c:\\");
        assertEquals("El path no fue guardado", "c:\\", ctrl.getCarpetaImagenes());
        ctrl.setCarpetaImagenes(orig); //sino afecta el otro
    }
    
    
}