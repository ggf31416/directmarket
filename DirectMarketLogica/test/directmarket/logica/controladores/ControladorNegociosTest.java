/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.accesoDatos.dataTypes.DataProductoBas;
import directmarket.excepciones.DirectMarketException;
import directmarket.accesoDatos.dataTypes.DataOrden;
import directmarket.accesoDatos.dataTypes.DataOrdenLin;
import directmarket.accesoDatos.dataTypes.DataCliente;
import directmarket.accesoDatos.dataTypes.DataLinea;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.excepciones.MultiExcepcion;
import directmarket.excepciones.CarritoException;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.excepciones.YaExisteException;
import directmarket.accesoDatos.dataTypes.DataProducto;
import directmarket.excepciones.NegocioException;
import directmarket.accesoDatos.dataTypes.DataCategoria;
import directmarket.accesoDatos.dataTypes.DataEstadProd;
import directmarket.accesoDatos.dataTypes.DataEstadistica;
import directmarket.logica.entidades.TipoEstado;
import directmarket.logica.fabrica.FabricaDataImagen;
import directmarket.logica.fabrica.FabricaControladores;
import directmarket.logica.interfaces.IComentarios;
import directmarket.logica.interfaces.IOrdenes;
import directmarket.logica.manejadores.ManejadorNegocios;
import directmarket.logica.manejadores.ManejadorUsuarios;
import directmarket.logica.utilidades.DatosPrueba;
import java.text.SimpleDateFormat;
import java.util.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Guillermo
 */
public class ControladorNegociosTest {

    static ControladorProductos productos;
    static IOrdenes ctrlOrdenes;
    static ControladorUsuarios usuarios;
    static IComentarios ctrlComent;

    @BeforeClass
    public static void setUpBeforeClass() {
        usuarios = (ControladorUsuarios) FabricaControladores.getUsuarioInstance();
        productos = (ControladorProductos) FabricaControladores.getProductosInstance();
        ctrlOrdenes = FabricaControladores.getOrdenesInstance();
        ctrlComent = FabricaControladores.getComentariosInstance();
    }

    public ControladorNegociosTest() {
    }

    private void cargarDatosPrueba() {
        try {
            DatosPrueba.cargarDatosPruebaSinImag();
        } catch (DirectMarketException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Before
    public void setUp() {
        ManejadorUsuarios.getInstance().reiniciar();
        ManejadorNegocios.getInstance().reiniciar();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testElegirClienteOrden() throws MultiExcepcion {
        cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("Dan");
        assertTrue("No se recibió carrito", ctrlOrdenes.getCarrito("Dan") != null);
    }

    @Test
    public void testElegirClienteOrden2() {
        cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("Dan");
        ctrlOrdenes.elegirClienteOrden("Dan");
        assertTrue("No se recibió carrito (elegido 2 veces)", ctrlOrdenes.getCarrito("Dan") != null);
    }

    @Test
    public void testElegirClienteOrden3() {
        cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("Dan");
        ctrlOrdenes.elegirClienteOrden("Phil");
        assertEquals("No se recibió carrito de Dan", "Dan", ctrlOrdenes.getCarrito("Dan").getNick());
        assertEquals("No se recibió carrito de Phil", "Phil", ctrlOrdenes.getCarrito("Phil").getNick());
    }

    @Test
    public void testProductosPorCategoria() throws NoSeEncontroException {
        cargarDatosPrueba();
        List<DataProductoBas> prodApple = productos.productosPorCategoria("Apple");
        assertEquals("La categoria Apple esta asociada a 8 productos!!", 8, prodApple.size());
    }
    
        @Test
    public void testProductosPorCategoria2() throws NoSeEncontroException {
        cargarDatosPrueba();
        int idApple = ManejadorNegocios.getInstance().getCategoria("Apple").getId();
        List<DataProductoBas> prodApple = productos.productosPorCategoria(idApple);
        assertEquals("La categoria Apple esta asociada a 8 productos!!", 8, prodApple.size());
    }

    @Test
    public void testAltaCategoria() throws Exception {
        productos.altaCategoria("Celulares", "Categorías", false);
        assertFalse("Fallo el alta", productos.listarCategorias().isEmpty());
    }

    
        @Test(expected = YaExisteException.class)
    public void testAltaCategoriaDup() throws Exception {
        productos.altaCategoria("Celulares", "Categorías", false);
        productos.altaCategoria("Celulares", "Categorías", false);
        System.out.println("acepta categoria duplicada");
    }


    @Test
    public void testListarCategorias() throws YaExisteException {
        productos.altaCategoria("Celulares", "Categorías", false);
        productos.altaCategoria("Sistemas Operativos", "Celulares", false);
        DataCategoria dcCel = new DataCategoria(0,"Celulares", false);
        DataCategoria dcSO = new DataCategoria(0,"Sistemas Operativos", "Celulares", 0,false);
        assertTrue("Celulares no listado", productos.listarCategorias().contains(dcCel));
        assertTrue("SO no listado", productos.listarCategorias().contains(dcSO));
    }

    @Test(expected = YaExisteException.class)
    public void testAltaCategoriaPadreProductos() throws Exception {
        productos.altaCategoria("Celulares", "Categorías", true);
        productos.altaCategoria("Sistemas Operativos", "Celulares", false);
        System.out.println("No debe permitir agregar subcategorias a categorias con tieneProducto = true");
    }

    @Test
    public void testListarCategoriasConProducto() {
        cargarDatosPrueba();
        List<DataCategoria> cats = productos.listarCategoriasConProducto();
        // 17 pueden tener producto pero solo 11 tienen alguno
        assertEquals("No hay la cantidad esperada de categorias finales", 11, cats.size());
    }

    @Test
    public void testGetCategoriasPrimerNivel() {
        cargarDatosPrueba();
        List<DataCategoria> cats = productos.getCategoriasPrimerNivel();
        assertEquals("No hay la cantidad esperada de categorias de primer nivel", 3, cats.size());
    }

    @Test
    public void testListarOrdenes() {
        cargarDatosPrueba();
        List<DataOrden> ordenes = ctrlOrdenes.listarOrdenes();
        assertEquals("La cantidad de ordenes no es correcta", 12, ordenes.size());
    }

    @Test
    public void testInformacionOrden() {
        cargarDatosPrueba();
        DataOrdenLin datos = ctrlOrdenes.InformacionOrden(3);
        assertEquals("nro", 3, datos.getNroOrden());
        assertEquals("nick", "Phil", datos.getNick());
        assertEquals("fecha", new GregorianCalendar(2013, Calendar.AUGUST, 12).getTime(), datos.getFecha());
        assertEquals("email", "phil.schiller@gmail.com", datos.getEmail());
        assertEquals("total", 108.38, datos.getTotal(), 0.00000001);
    }

    @Test
    public void testListarProductos() throws Exception {
        cargarDatosPrueba();
        List<DataProductoBas> lista = productos.ListarProductos();
        assertEquals("cantidad productos", 18, lista.size());
        // comprueba por nro referencia
        DataProductoBas nex4 = lista.get(3);
        assertTrue("No contiene nex4", lista.contains(nex4));
    }

    @Test
    public void testModificarProducto() throws Exception {
        cargarDatosPrueba();
        productos.ModificarProducto(2, "TituloNuevo", 2000,  "nueva descripcion", "nueva especificacion", Arrays.asList("Nexus", "Android"));
        DataProducto dp = productos.obtenerDatosProducto(2);
        assertEquals("precio", 2000, dp.getPrecio(), 0.00000001);
        assertEquals("titulo", "TituloNuevo", dp.getTitulo());
        assertEquals("descripcion", "nueva descripcion", dp.getDescripcion());
        assertEquals("especificacion", "nueva especificacion", dp.getEspecificacion());
    }
    
    @Test
    public void testModificarProductoNoCambioTitulo() throws Exception {
        cargarDatosPrueba();
        productos.ModificarProducto(2, "iPhone 4S", 2000,  "nueva descripcion", "nueva especificacion", Arrays.asList("Nexus", "Android"));


        DataProducto dp = productos.obtenerDatosProducto(2);
        assertEquals("precio", 2000, dp.getPrecio(), 0.00000001);
        assertEquals("titulo", "iPhone 4S", dp.getTitulo());
        assertEquals("descripcion", "nueva descripcion", dp.getDescripcion());
        assertEquals("especificacion", "nueva especificacion", dp.getEspecificacion());
    }

    @Test(expected = Exception.class)
    public void testModificarProductoTituloyaExiste() throws Exception {
        cargarDatosPrueba();
        productos.ModificarProducto(1, "Nexus4", 200,  "nueva descripcion", "nueva especificacion", Arrays.asList("Nexus", "Android"));
    }

    @Test(expected = Exception.class)
    public void testModificarProductoprecionegativo() throws Exception {
        cargarDatosPrueba();
        productos.ModificarProducto(1, "TituloNuevo", -1,  "nueva descripcion", "nueva especificacion", Arrays.asList("Nexus", "Android"));
    }

    @Test(expected = Exception.class)
    public void testModificarProductoCategorianoexiste() throws Exception {
        cargarDatosPrueba();
        productos.ModificarProducto(1, "TituloNuevo", 100, "nueva descripcion", "nueva especificacion", Arrays.asList("Categoria2", "Android"));
    }

    @Test // faltaba poner @Test
    public void testEliminarOrden() throws Exception {
        cargarDatosPrueba();
        DataCliente dc = usuarios.VerInformacionCliente("Phil");
        assertTrue("No está en usuario", dc.getnumOrdenes().contains(3));
        ctrlOrdenes.eliminarOrden(3);
        DataOrdenLin data = ctrlOrdenes.InformacionOrden(3);
        assertNull("No se elimino orden", data);
        dc = usuarios.VerInformacionCliente("Phil");
        assertFalse("No elimino de usuario", dc.getnumOrdenes().contains(3));
    }
    
    @Test(expected = DirectMarketException.class)
    public void testEliminarOrdenNoExistente() throws Exception {
        cargarDatosPrueba();
        DataCliente dc = usuarios.VerInformacionCliente("Phil");
        assertTrue("No está en usuario", dc.getnumOrdenes().contains(3));
        ctrlOrdenes.eliminarOrden(999);
        System.out.println("Eliminar una orden que no existe no causo una excepcion");
    }

  /*  @Test(expected = DirectMarketException.class)
    public void testConfirmarOrdenVacia() throws Exception {
        cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        ctrlOrdenes.recibirOrden("BruceS");
        System.out.println("No comprueba si hay alguna linea");
    }
*/
    @Test
    public void testConfirmarOrdenValida() throws Exception {
        cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        ctrlOrdenes.agregarProductoLinea("BruceS", 1, 10);
        DataOrdenLin dol = ctrlOrdenes.recibirOrden("BruceS");
        assertEquals("No esta bien el total", 1990, dol.getTotal(), 0.0000001);
    }

    @Test
    public void testConfirmarOrden_String_Date() throws Exception {
        cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        ctrlOrdenes.agregarProductoLinea("BruceS", 1, 10);
        ctrlOrdenes.recibirOrden("BruceS", new GregorianCalendar(2012, 1, 1).getTime());
        
    }

    @Test
    public void testGetCarritoVacio() {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        DataOrdenLin dol = ctrlOrdenes.getCarrito("UsuarioInexistente");
        assertNotNull(dol);
    }

    @Test
    public void testGetCarrito() throws CarritoException {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        ctrlOrdenes.agregarProductoLinea("BruceS", 12, 4);
        DataOrdenLin dol = ctrlOrdenes.getCarrito("BruceS");
        assertTrue(dol.getLineas().size() == 1);
        assertEquals("Titulo de producto inesperado",
                "Control inalámbrico para Xbox",
                dol.getLineas().get(0).getTitulo());
        assertEquals("Cantidad de producto inesperada",
                4, dol.getLineas().get(0).getCantidad());
        assertEquals("Precio de producto inesperado",
                "27.27", String.valueOf(dol.getLineas().get(0).getPrecio()));
        assertEquals("Subtotal de linea inesperado",
                "109.08", String.valueOf(dol.getLineas().get(0).getSubtotal()));
    }

    @Test
    public void testGetCarritoTotalVacio() {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        assertTrue(ctrlOrdenes.getCarritoTotal("UsuarioInexistente") == 0);
    }

    @Test
    public void testGetCarritoTotal() throws CarritoException {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        ctrlOrdenes.agregarProductoLinea("BruceS", 12, 4);
        ctrlOrdenes.agregarProductoLinea("BruceS", 1, 2);
        assertTrue(ctrlOrdenes.getCarritoTotal("BruceS") == 507.08);
    }

    @Test
    public void testAgregarProductoLineaYaEstaProducto() throws Exception {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("Dan");
        ctrlOrdenes.agregarProductoLinea("Dan", 1, 5);
        ctrlOrdenes.agregarProductoLinea("Dan", 1, 5);
        DataOrdenLin dol = ctrlOrdenes.getCarrito("Dan");
        DataLinea dl = dol.getLineas().get(0);
        int cant = dl.getCantidad();
        int ref = dl.getRef();
        assertEquals("No coincide ref", 1, ref);
        assertEquals("No coincide cant", 10, cant);
    }

    @Test
    public void testAgregarProductoLinea() throws CarritoException {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("BruceS");
        assertTrue(ctrlOrdenes.agregarProductoLinea("BruceS", 12, 4));
    }

    

    @Test
    public void testEliminarLinea() throws Exception {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("Dan");
        ctrlOrdenes.agregarProductoLinea("Dan", 1, 5);
        ctrlOrdenes.agregarProductoLinea("Dan", 2, 3);
        ctrlOrdenes.agregarProductoLinea("Dan", 3, 4);
        ctrlOrdenes.eliminarLinea("Dan", 1);
        ctrlOrdenes.eliminarLinea("Dan", 3);
        DataOrdenLin dol = ctrlOrdenes.getCarrito("Dan");
        DataLinea dl = dol.getLineas().get(0);
        assertEquals("No coincide ref", 2, dl.getRef());
        assertEquals("No coincide cant", 3, dl.getCantidad());
    }
    
    @Test(expected = NegocioException.class)
    public void testEliminarLineaConf() throws Exception {
        this.cargarDatosPrueba();
        ctrlOrdenes.elegirClienteOrden("Dan");
        ctrlOrdenes.agregarProductoLinea("Dan", 1, 5);
        ctrlOrdenes.recibirOrden("Dan");
        ctrlOrdenes.eliminarLinea("Dan", 1);
        System.out.println("Eliminar despues de confirmar");
    }


    @Test
    public void testAltaProducto_info() throws Exception {
        usuarios.AltaProveedor("Tim1", "tim.cook@apple.com", "Tim", "Cook", new GregorianCalendar(1960, 11, 1).getTime(), "Apple", "http://www.apple.com", null,"pass");
        productos.altaCategoria("Celulares", "Categorías", false);
        productos.altaCategoria("Equipos", "Celulares", false);
        productos.altaCategoria("iPhone", "Equipos", true);
        productos.altaCategoria("Sistemas Operativos", "Celulares", false);
        productos.altaCategoria("iOS", "Sistemas Operativos", true);
        productos.altaCategoria("Apple", "Categorías", true);
        String especIPH5 = "Capacidad: 16GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        productos.altaProducto("iPhone 5", 1, "El último celular de Apple", especIPH5, 199, "Tim1", Arrays.asList("iPhone", "iOS", "Apple"),0);
        DataProducto dp = productos.obtenerDatosProducto(1);
        assertEquals("Nro", 1, dp.getNroReferencia());
        assertEquals("Titulo", "iPhone 5", dp.getTitulo());
        assertEquals("Desc", "El último celular de Apple", dp.getDescripcion());
        assertEquals("Espec", especIPH5, dp.getEspecificacion());
        assertEquals("Precio", 199, dp.getPrecio(), 0.00000001);
        assertEquals("Nick", "Tim1", dp.getNickProveedor());
        List<String> cat = dp.getListaCategorias();
        assertTrue("En iPhone?", cat.contains("iPhone"));
        assertTrue("En iOS?", cat.contains("iOS"));
    }
    
        @Test
    public void testAltaProducto_notif() throws Exception {
            cargarDatosPrueba();
            ManejadorUsuarios.getInstance().getPreferenciasEnvio().setActivarEnvio(true);
        String especIPH5 = "Capacidad: 16GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        productos.altaProducto("iPhone 7", 100, "El último celular de Apple", especIPH5, 199, "Tim1", Arrays.asList("iPhone", "iOS", "Apple"),0);
    }
    
     @Test(expected = DirectMarketException.class)
    public void testAltaProductoCatVacia() throws Exception {
        usuarios.AltaProveedor("Tim1", "tim.cook@apple.com", "Tim", "Cook", new GregorianCalendar(1960, 11, 1).getTime(), "Apple", "http://www.apple.com", null,"pass");
        productos.altaCategoria("Apple", "Categorías", true);
        String especIPH5 = "Capacidad: 16GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        productos.altaProducto("iPhone 5", 1, "El último celular de Apple", especIPH5, 199, "Tim1", new ArrayList<String>(),0);
         System.out.println("Dejo ingresar producto sin categoria");
    }

    @Test(expected = NegocioException.class)
    public void testAltaProductoSuperCat() throws Exception {
        cargarDatosPrueba();
        String especIPH5 = "Capacidad: 16GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        productos.altaProducto("iPhone 6", 9999, "El último celular de Apple", especIPH5, 199, "Tim1", Arrays.asList("Celulares"),0);
        System.out.println("No debe permitir agregar un producto a una categoria con tieneProductos = false");
    }

    @Test(expected = NoSeEncontroException.class)
    public void testAltaProductoCatNoExistente() throws Exception {
        usuarios.AltaProveedor("Tim1", "tim.cook@apple.com", "Tim", "Cook", new GregorianCalendar(1960, 11, 1).getTime(), "Apple", "http://www.apple.com", null,"pass");
        productos.altaCategoria("Celulares", "Categorías", false);
        String especIPH5 = "Capacidad: 16GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        productos.altaProducto("iPhone 5", 1, "El último celular de Apple", especIPH5, 199, "Tim1", Arrays.asList("Gatos"),0);
        System.out.println("Debe existir la categoria antes de agregar un producto");
    }

    @Test(expected = DirectMarketException.class)
    public void testAltaProductoRepetido() throws DirectMarketException {
        cargarDatosPrueba();
        productos.altaProducto("A", 1, "Hola", "Espec", -1,"Tim1", Arrays.asList("Apple"),0);
    }
    
    @Test(expected = DirectMarketException.class)
    public void testAltaProductoPrecioNegativo() throws DirectMarketException {
        cargarDatosPrueba();
        productos.altaProducto("A", 888, "Hola", "Espec", -1,"Tim1", Arrays.asList("Apple"),0);
    }
        @Test(expected = Exception.class)
    public void testAltaProductoProvEsCliente() throws DirectMarketException {
        cargarDatosPrueba();
        productos.altaProducto("A", 888, "Hola", "Espec", 100,"Dan", Arrays.asList("Apple"),0);
    }
        
               @Test(expected = Exception.class)
    public void testAltaProductoProvNoExist() throws DirectMarketException {
        cargarDatosPrueba();
        productos.altaProducto("A", 888, "Hola", "Espec", 100,"Tim223", Arrays.asList("Apple"),0);
        System.out.println("No lanza excepcion en proveedor inexistente");
    }
    
    
    
    @Test
    public void testAltaProducto_infoConImg() throws Exception {
        DataImagen di_1 =  FabricaDataImagen.crearImagenEnBlanco(100,100, "blank100.jpg", "jpg");
        DataImagen di_2 =  FabricaDataImagen.crearImagenEnBlanco(100,300, "blank101.jpg", "jpg");
        usuarios.AltaProveedor("Tim1", "tim.cook@apple.com", "Tim", "Cook", new GregorianCalendar(1960, 11, 1).getTime(), "Apple", "http://www.apple.com", null,"pass");
        productos.altaCategoria("Celulares", "Categorías", false);
        productos.altaCategoria("Equipos", "Celulares", false);
        productos.altaCategoria("iPhone", "Equipos", true);
        productos.altaCategoria("Sistemas Operativos", "Celulares", false);
        productos.altaCategoria("iOS", "Sistemas Operativos", true);
        productos.altaCategoria("Apple", "Categorías", true);
        String especIPH5 = "Capacidad: 16GB\nPeso: 112 g\nPantalla: 4\"\nVersiones de Wifi: a/b/g/n";
        productos.altaProducto("iPhone 5", 1, "El último celular de Apple", especIPH5, 199, "Tim1", Arrays.asList("iPhone", "iOS", "Apple"),Arrays.asList(di_1,di_2),0);
        DataProducto dp = productos.obtenerDatosProducto(1);
        assertTrue("primer imagen",dp.getListaImagenes().contains(di_1));
        assertTrue("segunda imagen",dp.getListaImagenes().contains(di_2));
        assertNotNull(new ControladorImagenes().getImagen("product-1_0.jpg"));
    }
    @Test(expected = Exception.class)
    public void testComentarioVacio() throws Exception {
        cargarDatosPrueba();
        ctrlComent.agregarComentario(null, new Date(), "Dan" , 1);
    }
    @Test(expected = Exception.class)
    public void testNoPuedeComentar() {
        cargarDatosPrueba();
        ctrlComent.agregarComentario("Comentario1", new Date(), "Dan", 13);
    }
    
    @Test(expected = Exception.class)
    public void testNoPuedeComentar2() {
        cargarDatosPrueba();
        ctrlComent.agregarComentario("Comentario1", new Date(), "DanX", 1);
    }
    
    @Test(expected = Exception.class)
    public void testNoPuedeComentar3() {
        cargarDatosPrueba();
        ctrlComent.agregarComentario("Comentario1", new Date(), "Dan", 99);
    }
    
        
    @Test(expected = Exception.class)
    public void testNoPuedeComentar4() {
        cargarDatosPrueba();
        ctrlComent.agregarComentario("Comentario1", new Date(), "Dan",16 );
    }
    
    @Test
    public void testCantComentarios() throws Exception {
        cargarDatosPrueba();
        int cantAnterior = productos.obtenerDatosProducto(1).getCantidadComentarios();
        int nro = ctrlComent.agregarComentario("Comentario1", "Dan", 1);
        
        ctrlComent.agregarComentario("Comentario2",  "Dan", 1);
        ctrlComent.agregarComentario("Comentario3", "Dan", 1);
        ctrlComent.agregarComentario("Comentario4", "Dan", 1);
        ctrlComent.agregarRespuesta("Comentario5", "Dan", 1, nro);   
        int cantActual = productos.obtenerDatosProducto(1).getCantidadComentarios();
        assertEquals("Debe haberse agegado 5 comentarios",5,cantActual - cantAnterior);
   }
   @Test(expected = Exception.class)
   public void testRespuestaAProductosinComentarios(){
        cargarDatosPrueba();
        ctrlComent.agregarRespuesta("Comentario5", "BruceS", 1, 1);   
   }
   @Test
   public void testCantidadProductosProveedor(){
       cargarDatosPrueba();
       assertTrue("EL proveedor ingreso 8 productos", productos.ProductosPorProveedor("Tim1").size() == 8);
   }
   
    @Test
    public void testListarComentarios() {
        cargarDatosPrueba();
        ManejadorUsuarios.getInstance().getPreferenciasEnvio().setActivarEnvio(true);
        int nro =  ctrlComent.agregarComentario("Comentario1", "Dan", 9);
        int nro2 = ctrlComent.agregarComentario("Comentario2",  "Dan", 9);
        int nro3 = ctrlComent.agregarComentario("Comentario3", "Dan", 9);
        ctrlComent.agregarComentario("Comentario4", "Dan", 9);
        ctrlComent.agregarRespuesta("Comentario5", "Dan", 9, nro);
        assertEquals("No coincide el texto","Comentario1",ctrlComent.listarComentarios(9).get(0).getTexto());
        assertEquals("No coincide el texto","Comentario2",ctrlComent.listarComentarios(9).get(1).getTexto());
   }
   @Test
    public void testBuscarProductos(){
       cargarDatosPrueba();
        List<DataProductoBas> productos = FabricaControladores.getProductosInstance().buscarProductos("Tim1");
        assertEquals(FabricaControladores.getProductosInstance().ProductosPorProveedor("Tim1").size(),productos.size());
   }
   
      @Test
    public void testBuscarProductos2(){
       cargarDatosPrueba();
        List<DataProductoBas> productos = FabricaControladores.getProductosInstance().buscarProductos("iOS");
        assertEquals(FabricaControladores.getProductosInstance().ProductosPorProveedor("Tim1").size(),productos.size());
   }
      
            @Test
    public void testBuscarProductos3(){
       cargarDatosPrueba();
        List<DataProductoBas> productos = FabricaControladores.getProductosInstance().buscarProductos("Retina");
        assertEquals(2,productos.size());
   }
      
   @Test
   public void PuntajeMenorq5(){
       cargarDatosPrueba();
       productos.puntuarProducto("BruceS", 12, 6);
       assertTrue("Puntaje menor que 5", !productos.existePuntuacion("BruceS", 12));
   }

   @Test(expected = Exception.class)
   public void ReclamoVacio(){
       cargarDatosPrueba();
       productos.AgregarReclamo("BruceS","", 12);
   }
   @Test
   public void VerPuntaje(){
       cargarDatosPrueba();
       assertTrue("Dan puntuo el iphone 5 con 5 estrellas", productos.obtenerValorPuntaje("Dan", 1) == 5);
   }
   
   @Test
   public void EstTiene12meses(){
       cargarDatosPrueba();
       SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
       DataEstadistica Estadisticas = usuarios.crearEstadisticas("Tim1", new GregorianCalendar(2013, Calendar.DECEMBER, 1));
       List<Calendar> meses = Estadisticas.getMeses();
       String todosMeses = "";
       for(Calendar c : meses){
           todosMeses += " " + sdf.format(c.getTime());
       }
       System.out.println("Meses:" + todosMeses);
       String inicio = sdf.format(meses.get(0).getTime());
       String fin = sdf.format(meses.get(meses.size() - 1).getTime());
       assertEquals(String.format("Coleccion de meses debe tener 12 elementos, tiene %s - %s",inicio,fin) , 12,Estadisticas.getMeses().size());
       assertEquals(String.format("Inicio = %s, Fin = %s",inicio,fin) , Calendar.JANUARY,meses.get(0).get(Calendar.MONTH));
       assertEquals(String.format("Inicio = %s, Fin = %s",inicio,fin) , Calendar.DECEMBER,meses.get(meses.size()-1).get(Calendar.MONTH));
       for(DataEstadProd dep : Estadisticas.getEstadProductos()){
           assertEquals("ganancias != 12 elementos en " + dep.getTitulo(), 12,dep.getPorMes().size());
       }
       
   }
   
   @Test
   public void probarVerOrdenesRecibidas(){
       cargarDatosPrueba();
       List<DataOrden> ordenes = ctrlOrdenes.listarOrdenesRecibidas();
       assertEquals(ordenes.size(), 1);
       DataOrden ord = ordenes.get(0);
       assertEquals(ord.obtenerEstado().getEstado(), TipoEstado.RECIBIDA);
       assertEquals(ord.getNroOrden(), 2);
   }
   
      @Test
   public void probarCambioEstado() throws NoSeEncontroException, DirectMarketException{
       cargarDatosPrueba();
       ManejadorUsuarios.getInstance().getPreferenciasEnvio().setActivarEnvio(true);
       List<DataOrden> ordenes = ctrlOrdenes.listarOrdenesRecibidas();
       assertEquals(ordenes.get(0).getNroOrden(),2);
       ctrlOrdenes.ordenPreparada(2, new Date());
       ordenes = ctrlOrdenes.listarOrdenesRecibidas();
       assertEquals(ordenes.size(), 0);
   }
      
   @Test(expected = Exception.class)
   public void probarCambioEstado2() throws NoSeEncontroException, DirectMarketException{
       cargarDatosPrueba();
       ctrlOrdenes.ordenConfirmada(11, new Date());
   }
      @Test(expected = Exception.class)
   public void probarCambioEstado3() throws NoSeEncontroException, DirectMarketException{
       cargarDatosPrueba();
       ctrlOrdenes.ordenPreparada(1, new Date());
   }
      
   
   @Test
   public void Estadisticas(){
       boolean llego = false;
       cargarDatosPrueba();
       DataEstadistica Estadisticas = usuarios.crearEstadisticas("Tim1", new GregorianCalendar(2013, Calendar.DECEMBER, 1));
       for(DataEstadProd dep : Estadisticas.getEstadProductos()){
           if(dep.getTitulo().equals("iPhone 5")){
               assertEquals("Ganancia de venta de iPhone 5:", 4 * 199,dep.getTotalVentas(),0.00000001 );
               int idxAgosto = Calendar.AUGUST - Calendar.DECEMBER + 11;
               int idxOct = Calendar.OCTOBER - Calendar.DECEMBER + 11;
               double ventasAgo = dep.getPorMes().get(idxAgosto);
               double ventasOct = dep.getPorMes().get(idxOct);
               Double[] ventas = dep.getPorMes().toArray(new Double[0]);
               System.out.println("Ventas:" + Arrays.toString(ventas));
               assertEquals("Se vendieron 3 IPhone 5 en agosto:", 3 * 199,ventasAgo,0.00000001 );
               
               assertEquals("Se vendieron 1 IPhone 5 en octubre (O12, la O11 se cancelo):", 1 * 199,ventasOct,0.00000001 );
               llego = true;
           }    
       }
       assertTrue("No encuentra el iPhone 5",llego);
       
   }
}