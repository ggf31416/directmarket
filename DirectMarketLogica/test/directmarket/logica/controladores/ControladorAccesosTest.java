/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.logica.entidades.Acceso;
import directmarket.logica.manejadores.ManejadorNegocios;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tprog087
 */
public class ControladorAccesosTest {

    public ControladorAccesosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ManejadorNegocios.getInstance().reiniciar();
    }

    @After
    public void tearDown() {
    }

    public Acceso pruebaUA(String ua) {
        String ip = "192.168.0.1";
        String url = "/test";
        GregorianCalendar fecha = new GregorianCalendar();
        ControladorAccesos instance = new ControladorAccesos();
        instance.guardarAcceso(ip, url, fecha, ua);
        Acceso acc = instance.listarAccesos().get(0);
        return acc;
    }

    /**
     * Test of guardarAcceso method, of class ControladorAccesos.
     */
    @Test
    public void testGuardarAcceso1() {
        String ip = "192.168.0.1";
        String url = "/test";
        GregorianCalendar fecha = new GregorianCalendar();
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0";
        ControladorAccesos instance = new ControladorAccesos();
        instance.guardarAcceso(ip, url, fecha, userAgent);
        Acceso acc = instance.listarAccesos().get(0);
        assertEquals("IP Distinta", ip, acc.getIp());
        assertEquals("URL Distinta", url, acc.getUrl());
        assertEquals("fecha distinta", fecha, acc.getFecha());
        assertEquals("userAgent mal Firefox", "Firefox", acc.getBrowser());
        assertEquals("SO mal Linux", "Linux", acc.getSo());
        GregorianCalendar ahora = (GregorianCalendar) fecha.clone();
        ahora.add(Calendar.DAY_OF_MONTH, 3);
        int ant = acc.antiguedad(ahora);
        assertEquals("Antiguedad esta mal", 3, ant);

    }

    @Test
    public void probarChromeWindows() {
        String ua = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36";
        Acceso acc = pruebaUA(ua);
        assertEquals("Chrome Mal", "Chrome", acc.getBrowser());
        assertEquals("Windows Mal", "Windows", acc.getSo());
    }

    @Test
    public void probarSafariIOS() {
        String ua = "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25";
        Acceso acc = pruebaUA(ua);
        assertEquals("Safari Mal", "Safari", acc.getBrowser());
        assertEquals("iOS Mal", "iOS", acc.getSo());
    }

    @Test
    public void probarIE() {
        String ua = "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0";
        Acceso acc = pruebaUA(ua);
        assertEquals("IE Mal", "IE", acc.getBrowser());
        assertEquals("Windows Mal", "Windows", acc.getSo());
    }
    
   @Test
    public void probarKonqueror() {
        String ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.21 (KHTML, like Gecko) konqueror/4.10.5 Safari/537.21";
        Acceso acc = pruebaUA(ua);
        assertEquals("IE Mal", "Konqueror", acc.getBrowser());
        assertEquals("Linux Mal", "Linux", acc.getSo());
    }
    
    

    @Test
    public void probarChromeAndroid() {
        String ua = "Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>";
        Acceso acc = pruebaUA(ua);
        assertEquals("Chrome Mal", "Chrome", acc.getBrowser());
        assertEquals("Android Mal", "Android", acc.getSo());
    }

    public void probarOperaMac() {
        String ua = "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52";
        Acceso acc = pruebaUA(ua);
        assertEquals("Opera Mal", "Opera", acc.getBrowser());
        assertEquals("Mac Mal", "Mac", acc.getSo());
    }

    /**
     * Test of listarAccesos method, of class ControladorAccesos.
     */
    @Test
    public void testCantidadAccesos() {
        String ip = "192.168.0.1";
        String url = "/test";
        GregorianCalendar fecha = new GregorianCalendar();
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0";
        ControladorAccesos instance = new ControladorAccesos();
        fecha.add(Calendar.DATE, -1);
        for(int i = 0; i < 20000; i++){
            instance.guardarAcceso(ip, url, fecha, userAgent);
            fecha.add(Calendar.SECOND, 1);
        }
        List<Acceso> lista = instance.listarAccesos();
        assertEquals("Debe tener 10000 accesos", 10000, lista.size());
    }
    
        /**
     * Test of listarAccesos method, of class ControladorAccesos.
     */
    @Test
    public void testBorrarAccesosViejos() {
        String ip = "192.168.0.1";
        String url = "/test";
        GregorianCalendar fecha = new GregorianCalendar();
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0";
        ControladorAccesos instance = new ControladorAccesos();
        fecha.add(Calendar.DATE, -50);
        fecha.add(Calendar.HOUR, -1); // para evitar condicion de carrera
        for(int i = 0; i < 50; i++){
            instance.guardarAcceso(ip, url, fecha, userAgent);
            fecha.add(Calendar.DATE, 1);
        }
        List<Acceso> lista = instance.listarAccesos();
        List<Integer> antig = new ArrayList<>();
        boolean dias30 = false;
        GregorianCalendar actual = new GregorianCalendar();
        for(Acceso a : lista){
            assertTrue("Antiguedad > 30 en " + a.toString(), a.antiguedad(actual) <= 30);
            antig.add( a.antiguedad(actual));
        }
        // debería tener 31, 30 o 29?????????
        assertTrue("Muy pocos elementos",lista.size() >= 29);
        assertTrue("No tiene el de hace 30 dias",antig.contains(30));
    }
}