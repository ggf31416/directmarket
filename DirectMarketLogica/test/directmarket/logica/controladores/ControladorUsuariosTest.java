/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package directmarket.logica.controladores;

import directmarket.accesoDatos.dataTypes.DataCliente;
import directmarket.accesoDatos.dataTypes.DataImagen;
import directmarket.accesoDatos.dataTypes.DataProveedor;
import directmarket.accesoDatos.dataTypes.DataUsuarioBas;
import directmarket.accesoDatos.dataTypes.TipoUsuario;
import directmarket.excepciones.DirectMarketException;
import directmarket.logica.fabrica.FabricaDataImagen;
import directmarket.excepciones.MultiExcepcion;
import directmarket.excepciones.NoSeEncontroException;
import directmarket.logica.entidades.Cliente;
import directmarket.logica.entidades.Proveedor;
import directmarket.logica.entidades.TipoNotificacion;
import directmarket.logica.manejadores.ManejadorUsuarios;
import directmarket.logica.utilidades.DatosPrueba;
import java.util.Date;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Guillermo
 */
public class ControladorUsuariosTest {

    ControladorUsuarios ctrl;

    public ControladorUsuariosTest() {
    }

    @Before
    public void setUp() {
        ManejadorUsuarios.getInstance().reiniciar();
        ctrl = new ControladorUsuarios();
        Proveedor p = new Proveedor("prov", "prov@gmail.com", "nombre", "apellido", new Date(), null, "MiCompañia", "www.example.com");
        Cliente c = new Cliente("cli", "cli@gmail.com", "nombre", "apellido", new Date(), null);
        ManejadorUsuarios.getInstance().putUsuario("prov", p);
        ManejadorUsuarios.getInstance().putUsuario("cli", c);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testValidarNickValido() {
        ctrl = new ControladorUsuarios();
        ManejadorUsuarios.getInstance().putUsuario("nick", new Cliente());
        assertTrue(ctrl.ValidarNick("nick2"));
    }

    @Test
    public void testValidarNickRepetido() {
        ctrl = new ControladorUsuarios();
        ManejadorUsuarios.getInstance().putUsuario("nick", new Cliente());
        assertFalse(ctrl.ValidarNick("nick"));
    }

    @Test
    public void testValidarNickCase() {
        ManejadorUsuarios.getInstance().putUsuario("nick", new Cliente());
        assertTrue(ctrl.ValidarNick("NICK"));
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarNickrepetido() throws MultiExcepcion {
        ctrl.AltaUsuario("nick", "prov@gmail.com", "nombre", "apellido", new Date(), null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarNickVacio() throws MultiExcepcion {
        ctrl.AltaUsuario("", "prov@gmail.com", "nombre", "apellido", new Date(), null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarNombreVacio() throws MultiExcepcion {
        ctrl.AltaUsuario("validarDatos3", "prov@gmail.com", "", "apellido", new Date(), null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarNombreInvalido() throws MultiExcepcion {
        ctrl.AltaUsuario("validarDatos4", "prov@gmail.com", "123", "apellido", new Date(), null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarApellidoVacio() throws MultiExcepcion {
        ctrl.AltaUsuario("validarDatos5", "prov@gmail.com", "nombre", "", new Date(), null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarApellidoInvalido() throws MultiExcepcion {
        ctrl.AltaUsuario("validarDatos6", "prov@gmail.com", "nombre", "123", new Date(), null,"pass");
    }
    
    @Test(expected = DirectMarketException.class)
    public void testValidarEmailVacio() throws DirectMarketException {
        ctrl.mailValido("");
        ctrl.AltaUsuario("validarDatos7", "", "nombre", "apellido", new Date(), null,"pass");
    }
    
    @Test(expected = DirectMarketException.class)
    public void testValidarEmailConEspacios() throws DirectMarketException {
        ctrl.mailValido("Email@ Invalido");
        ctrl.AltaUsuario("validarDatos12", "Email@ Invalido", "nombre", "apellido", new Date(), null,"pass");
    }
    
    @Test(expected = MultiExcepcion.class)
    public void testValidarEmailInvalido() throws MultiExcepcion {
        ctrl.AltaUsuario("validarDatos8", "EmailInvalido", "nombre", "apellido", new Date(), null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarNombreCompaniaVacio() throws MultiExcepcion {
        ctrl.AltaProveedor("validarDatos9", "prov@gmail.com", "nombre", "apellido", new Date(), "", "https://www.comp.com", null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarWebCompaniaVacio() throws MultiExcepcion {
        ctrl.AltaProveedor("validarDatos10", "prov@gmail.com", "nombre", "apellido", new Date(), "comp2", "", null,"pass");
    }

    @Test(expected = MultiExcepcion.class)
    public void testValidarWebCompaniaInvalido() throws MultiExcepcion {
        ctrl.AltaProveedor("validarDatos11", "prov@gmail.com", "nombre", "apellido", new Date(), "comp2", "WebInvalida", null,"pass");
    }

    @Test
    public void testAltaUsuarioSinImagen() throws Exception {
        ctrl = new ControladorUsuarios();
        ctrl.AltaUsuario("cliente1", "cl@gmail.com", "Nombre", "Apellido", new GregorianCalendar(1990, 4, 27).getTime(), null,"pass");
        assertTrue(ManejadorUsuarios.getInstance().containsKeyUsuario("cliente1"));
    }

    @Test
    public void testGetSinAvatar() throws Exception {
        assertNull(ctrl.getAvatar("prov"));
    }

    /**
     * Test of VerInformacionProveedor method, of class ControladorUsuarios.
     */
    @Test
    public void testVerInformacionProveedor() throws Exception {
        DataProveedor dp = ctrl.VerInformacionProveedor("prov");
        assertEquals(dp.getWeb(), "www.example.com");
    }

    @Test
    public void testVerInformacionCliente() throws Exception {
        ctrl = new ControladorUsuarios();
        ctrl.AltaUsuario("cliente1", "cl@gmail.com", "Nombre", "Apellido", new GregorianCalendar(1990, 4, 27).getTime(), null,"pass");
        DataCliente dc = ctrl.VerInformacionCliente("cliente1");
        assertEquals(dc.getApellido(), "Apellido");
        assertEquals(dc.getNick(), "cliente1");
        assertEquals(dc.getEmail(), "cl@gmail.com");
        assertEquals(dc.getNombre(), "Nombre");
        assertEquals(dc.getFecha(), new GregorianCalendar(1990, 4, 27).getTime());
    }

    @Test(expected = NoSeEncontroException.class)
    public void testVerInformacionProveedorInvalido() throws Exception {
        ctrl.VerInformacionProveedor("cli");
    }

    @Test(expected = NoSeEncontroException.class)
    public void testVerInformacionClienteInvalido() throws Exception {
        ctrl.VerInformacionCliente("prov");
    }


    @Test
    public void testListarClientesFiltrando() throws Exception {
       ManejadorUsuarios.getInstance().putUsuario("proveedor1", new Proveedor()); // para ver si filtra solo clientes
        List<DataUsuarioBas> lst = ctrl.listarClientes();
        assertEquals(1,lst.size());
    }

    /**
     * Test of AltaProveedor method, of class ControladorUsuarios.
     */
    @Test
    public void testAltaProveedorValido() throws Exception {
        String nickProv = "prov2";
        String web = "www.comp2.com.uy";
        String comp = "comp2";
        ctrl.AltaProveedor(nickProv, "prov2@gmail.com", "n", "a", new GregorianCalendar(1990, 4, 27).getTime(), comp, web, null,"pass");
        DataProveedor dp = ctrl.VerInformacionProveedor(nickProv);
        assertEquals(dp.getWeb(), web);
        assertEquals(dp.getCampania(), comp);
    }

    /**
     * Test of listarProveedores method, of class ControladorUsuarios.
     */
    @Test
    public void testListarProveedores() {
        ManejadorUsuarios.getInstance().putUsuario("cliente1", new Cliente()); // para ver si filtra solo proveedores
        List<DataUsuarioBas> lst = ctrl.listarProveedores();
        assertTrue(lst.size() == 1);
    }

    private DataImagen crearImagenPrueba() {
        return FabricaDataImagen.crearImagenEnBlanco(100,100, "blank100.png", "png");
    }
    
    @Test
    public void testListarClientes() throws MultiExcepcion{
        ManejadorUsuarios.getInstance().reiniciar();
        ctrl.AltaUsuario("Dan", "dan.ricco@gmail.com", "Daniel","Riccio", new GregorianCalendar(1963,7,5).getTime(), null,"pass");
        ctrl.AltaUsuario("Phil", "phil.schiller@gmail.com", "Philip","Schiller", new GregorianCalendar(1961,10,7).getTime(), null,"pass");
        ctrl.AltaUsuario("BruceS", "bruce.sewell@gmail.com", "Bruce","Sewell", new GregorianCalendar(1959,12,3).getTime(), null,"pass");
        ctrl.AltaUsuario("JeffW", "jeff.williams@gmail.com", "Jeff","Wiliams", new GregorianCalendar(1964,11,27).getTime(), null,"pass");
        List<DataUsuarioBas> clientes = ctrl.listarClientes();
        assertEquals("cantidad clientes", 4,clientes.size());
        DataUsuarioBas c = new DataUsuarioBas("Phil", "phil.schiller@gmail.com", TipoUsuario.Cliente);
        assertTrue("contiene Phil", clientes.contains(c));
    }
    
    @Test(expected = NoSeEncontroException.class)
    public void testAutenticarFallido1() throws DirectMarketException{
        ctrl.AltaUsuario("Dan", "dan.ricco@gmail.com", "Daniel","Riccio", new GregorianCalendar(1963,7,5).getTime(), null,"pass123ñandu");
        ctrl.autenticar("Dan", "hola");
        System.out.println("Acepta cualquiera");
    }
    
     @Test(expected = NoSeEncontroException.class)
    public void testAutenticarFallido2() throws DirectMarketException{
        ctrl.AltaUsuario("Dan", "dan.ricco@gmail.com", "Daniel","Riccio", new GregorianCalendar(1963,7,5).getTime(), null,"pass123ñandu");
        ctrl.autenticar("Dan", "Pass123ñandu");
        System.out.println("Deberia ser case-sensitive");
    }
    
   
    public void testAutenticarCorrecto() throws DirectMarketException{
        ctrl.AltaUsuario("Dan", "dan.ricco@gmail.com", "Daniel","Riccio", new GregorianCalendar(1963,7,5).getTime(), null,"pass123ñandu");
        assertEquals("No funciona",TipoUsuario.Cliente,ctrl.autenticar("Dan", "pass123ñandu").getTipo());
    }
    
    @Test
    public void testClienteConImagen() throws Exception {
        ctrl = new ControladorUsuarios();
        DataImagen blank = crearImagenPrueba();
        ctrl.AltaUsuario("cliente1", "cl@gmail.com", "Nombre", "Apellido", new GregorianCalendar(1990, 4, 27).getTime(), blank,"pass");
        DataCliente dc = ctrl.VerInformacionCliente("cliente1");
        assertEquals(dc.getApellido(), "Apellido");
        assertEquals(dc.getNick(), "cliente1");
        assertEquals(dc.getEmail(), "cl@gmail.com");
        assertEquals(dc.getNombre(), "Nombre");
        assertEquals(dc.getFecha(), new GregorianCalendar(1990, 4, 27).getTime());
        assertNotNull("No se cargo la imagen",dc.getImagen());
    }
    
    @Test
    public void cambiarSuscrip() throws NoSeEncontroException, DirectMarketException{
        DatosPrueba.cargarDatosPruebaSinImag();
        Set<TipoNotificacion> exc = EnumSet.of(TipoNotificacion.NuevoComentario, TipoNotificacion.RegistroProducto);
        ctrl.cambiarSuscripcionesUsuario("Dan", exc);
        Set<TipoNotificacion> s = ctrl.VerInfoCliente("Dan").getDesuscriptos();
        assertEquals(2, s.size());
        assertTrue(s.contains(TipoNotificacion.NuevoComentario));
    }
    
    @Test
    public void x_cargarPrefEnvios(){
        ManejadorUsuarios.getInstance().cargarPreferenciasEnvio();
    }
    

}